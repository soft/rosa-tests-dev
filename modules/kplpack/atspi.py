#!/usr/bin/python3.5
# -*- coding: utf-8 -*-
# ┌───────────────────────────────────────────────────────────────────────────┐
# │                                                                           │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@:::::::::@@@@@@@@@@@::::::@@@@@@@@@@@::::@@@@@@@@@@@@@:@@@@@@@@@   │
# │   @@@:------------::@@@@:----------::@@@@:--------::@@@@@@@:---:@@@@@@@   │
# │   @@@:---@@@@@@@@:--:@@---:@@@@@@@---:@@---:@@@@@:@@@@@@@@:-----@@@@@@@   │
# │   @@@:---@@@@@@@@:--:@:--:@@@@@@@@:---@@---:@@@@@@@@@@@@@@--:@:--@@@@@@   │
# │   @@@:---@@@@@@@:---@@:--:@@@@@@@@:---@@@:------:::@@@@@@---@@@--:@@@@@   │
# │   @@@:-----------::@@@:--:@@@@@@@@:---@@@@@@:::::---@@@@:--:@@@:--:@@@@   │
# │   @@@:---@@@@@:--:@@@@:--:@@@@@@@@:--:@@@@@@@@@@@:--:@@:-----------:@@@   │
# │   @@@:---@@@@@@:---:@@@:--:::@@@::--:@@::-:::@@::---@@:--:@@@@@@@---:@@   │
# │   @@@:::-@@@@@@@@::::@@@@:::-----::@@@@@@:::----::@@@::::@@@@@@@@:::-@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │                                                                           │
# │                  Additional tools for ROSA Tests                          │
# │                                                                           │
# │   Copyright © 2021 LLC "NTC IT ROSA"                                      │
# │   License: GPLv3                                                          │
# │   Authors:                                                                │
# │       Michael Mosolov     <m.mosolov@rosalinux.ru>       2021             │
# │                                                                           │
# │   This program is free software; you can redistribute it and/or modify    │
# │   it under the terms of the GNU General Public License as published by    │
# │   the Free Software Foundation; either version 3, or (at your option)     │
# │   any later version.                                                      │
# │                                                                           │
# │   This program is distributed in the hope that it will be useful,         │
# │   but WITHOUT ANY WARRANTY; without even the implied warranty of          │
# │   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            │
# │   GNU General Public License for more details                             │
# │                                                                           │
# │   You should have received a copy of the GNU General Public License       │
# │   License along with this program; if not, write to the                   │
# │   Free Software Foundation, Inc.,                                         │
# │   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.           │
# │                                                                           │
# ╘═══════════════════════════════════════════════════════════════════════════╛

__version__ = '0.1.1'

import argparse
import itertools
import keyboard
import multiprocessing
import os
import pathlib
import psutil
import pyatspi
import subprocess
import sys
import threading
import time
import tkinter

from collections import deque
from Xlib import display
from typing import *

try:
  from utils import CountDown
  from utils import colored as I

except ImportError:
  from modules.kplpack.utils import CountDown
  from modules.kplpack.utils import colored as I

class AT:
  """
    Assistive Technology Service Provider Interface

    Requires:
      python3-pyatspi
      lib64atspi-gir2.0
  """

  dbus_config = pathlib.Path('/usr/share/defaults/at-spi2/accessibility.conf')

  class ApplicationNotFound(RuntimeError):
    """If the application was not found in the ATSPI tree before timeout.
    """

  def __init__(self, app_name:str, app_init_time=5.0, timeout: int = 1, delay=1.0):
    """Wraps the application with atspi interface.

      Parameters:
        app_name      (str)   - name of the application, which will be the root node for finding interface elements;
        app_init_time (float) - the waiting time(seconds) during which the application is initialized.
                                May be useful if the application is started manually using multiprocessing.Process;
        timeout       (int)   - time during which the application will be searched in the atspi tree;
        delay         (float) - delay after each action, in seconds

      Exceptions:
        AT.ApplicationNotFound - the application was not found in the ATSPI tree before timeout

    """
    self.delay = delay
    self.desktop = None
    self.app = None
    self.name = app_name
    self.description = ''

    time.sleep(app_init_time)

    self.wait_init()
    self.refresh(timeout)

    if not self.app:
      raise AT.ApplicationNotFound('Application "{}" not found!'.format(app_name))

    #atexit.register(self.__terminate)

  def wait_init(self, timeout=10, noroot=False):
    """Blocks thread execution until needed processes are found.

      Parameters:
        timeout (int)  - time during which the check of existing processes will be carried out;
        noroot  (bool) - ignore processes owned by root

      Exceptions:
        ProcessLookupError - before timeout, not all processes were detected

    """
    timeout = timeout if timeout > 0 else 1
    procs = {
      'at-spi-bus-launcher': False,
      'dbus-daemon': False,
      'at-spi2-registryd': False
    }

    for _ in CountDown(timeout or 1):
      for proc in psutil.process_iter():
        if noroot and proc.username() == 'root':
          continue
        name = proc.name()
        if name in procs:
          procs[name] = True
      if all(procs.values()):
        break

    if not all(procs.values()):
      for k,v in procs.items():
        if not v:
          print('AT::wait_init(): process {} not found!'.format(k))
      raise ProcessLookupError

  def refresh(self, timeout: int = 1):
    """Tries to update the element tree by getting a new root element.

      Parameters:
        timeout  (int) - time during which the application will be searched in the atspi tree;

      Returns:
        (bool) - True if application found
    """
    if self.desktop:
      self.desktop.clear_cache()
      self.desktop.setCacheMask(pyatspi.cache.NONE)

    for _ in CountDown(timeout or 1):
      self.desktop = pyatspi.Registry.getDesktop(0)
      if not self.app:
        for application in self.desktop:
          if self.name.lower() in application.name.lower():
            self.app = application
            self.description = self.app.get_description()
      else:
        break

    return bool(self.app)

  @staticmethod
  def find_binaries():
    """Looking for paths to binaries: at-spi-bus-launcher and at-spi2-registryd.

      Returns:
        (list)
    """
    binaries = []
    proc = subprocess.run(
      ['locate', 'at-spi-bus-launcher'],
      universal_newlines=True,
      stdout=subprocess.PIPE
    )
    binaries.append(proc.stdout.strip())
    proc = subprocess.run(
      ['locate', 'at-spi2-registryd'],
      universal_newlines=True,
      stdout=subprocess.PIPE
    )
    binaries.append(proc.stdout.strip())
    return binaries

  @staticmethod
  def find_accessible(node:object, role:str, name='', desc='', in_breadth=False):
    """Search for an accessible object in the specified node. The search can be\
      narrowed by specifying name of object and/or description.

      Parameters:
        node (Accessible) - the node;
        role        (str) - compared to accessible.getRoleName();
        name        (str) - compared to accessible.get_name();
        desc        (str) - compared to accessible.get_description();
        in_breadth (bool) - traverse accessible tree in breadth

      Returns:
        (Accessible) - first accessible object found;
        (None)       - if the accessible object was not found
    """

    for acc in AT.walk(node, not in_breadth, with_self=True):
      try:
        if role == acc.getRoleName():
          if name in acc.get_name():
            if desc in acc.get_description():
              return acc
      except AttributeError:
        continue

    return None

  @staticmethod
  def prevent_root_processes():
    """Processes (dbus-daemon --config-file accessibility.conf,
                  at-spi-bus-launcher,
                  at-spi2-registryd --use-gnome-session) running as root prevent
    ATSPI from working properly, this function kills such processes.
    Root privileges REQUIRED!
    """

    for proc in psutil.process_iter():
      try:
        owned_by = proc.username()
        name = proc.name().lower()
        cmd = proc.cmdline()

        if 'dbus-daemon' == name and owned_by == 'root':
          if 'accessibility.conf' in cmd[1]:
            proc.terminate()

        if name == 'at-spi-bus-launcher' and owned_by == 'root':
          proc.terminate()

        if name == 'at-spi2-registryd' and owned_by == 'root':
          proc.terminate()

      except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
        continue

  @staticmethod
  def walk(obj, in_depth=True, with_self=False, max_children_node=500):
    """The function is similar pyatspi.utils.findAllDescendants. Traverses all
    nodes of the ATSPI tree.

      Parameters:
        obj         (Accessible) - the node;
        in_depth          (bool) - traverse accessible tree in depth;
        with_self         (bool) - take into account the current node when
                                   traversing the tree;
        max_children_node (bool) - maximum number of children of node.
                                   This is useful when you need to exclude
                                   traversing through nodes containing
                                   spreadsheets, for example

        (Generator) - each node of the atspi tree sequentially
    """
    def nodes(o):
      try:
        child_count = o.get_child_count()

      except AttributeError:
        return []

      except Exception as e:
        if 'does not exist' in str(e):
          return []
        else:
          raise e

      if child_count <= max_children_node:
        return [o.get_child_at_index(i) for i in range(child_count)]
      else:
        return []

    stack = deque([obj] + nodes(obj)) if with_self else deque(nodes(obj))
    while stack:
      cur = stack.popleft()
      yield cur
      if in_depth:
        stack.extendleft(nodes(cur))
      else:
        stack.extend(nodes(cur))

  @staticmethod
  def force_atspi(mode='gnome' or 'qt', dbus_conf='/usr/share/rosa-tests/test_data/dbus_share.conf'):
    """Forces environment variables to enable atspi technology.

      Parameters:
        mode      (str) - set OOO_FORCE_DESKTOP;
        dbus_conf (str) - if not empty, start dbus-daemon with the specified config

      Returns:
        (None)
    """
    AT.procs = []

    if dbus_conf:
      dbus_args = ['dbus-daemon', '--nofork', '--config-file', dbus_conf]
    else:
      dbus_args = ['dbus-daemon', '--session']

    # /bin/dbus-daemon --nofork, --config-file /usr/share/rosa-tests/test_data/dbus_share.conf
    # /usr/libexec/at-spi-bus-launcher
    # /usr/libexec/at-spi2-registryd

    os.environ['NO_AT_BRIDGE'] = '0'
    os.environ['GTK_MODULES'] = 'gail:atk-bridge'
    os.environ['XDG_CURRENT_DESKTOP'] = 'KDE'
    os.environ['OOO_FORCE_DESKTOP'] = mode
    os.environ['GNOME_ACCESSIBILITY'] = '1'
    os.environ['QT_ACCESSIBILITY'] = '1'
    os.environ['QT_LINUX_ACCESSIBILITY_ALWAYS_ON'] = '1'
    os.environ['SAL_USE_VCLPLUGIN'] = 'gtk3_kde5'

  @staticmethod
  def get_actions(obj) -> List[Tuple[str, int]]:
    """Gets a dictionary of all actions available for an interface element,\
       where the key is the name of the action and the value is the ordinal.

      Parameters:
        obj (Accessible) - the node;

      Returns:
        (List[Tuple[str,int]]) - Tuple[action_name, action_number]
    """
    actions = []
    try:
      actions_count = obj.get_n_actions()
    except:
      return actions

    for i in range(actions_count):
      actions.append((obj.get_action_name(i), i))
    return actions

  @staticmethod
  def print_class(obj):
    """Lists all members of the object and also lists available actions.

      Parameters:
        obj  (Accessible) - the node;

      Returns:
        (None)
    """
    try:
      role = obj.get_role_name()
    except:
      role = 'None or Application'

    print('Reference[{}]:'.format(role))
    for l in dir(obj):
      if not l.startswith('_'):
        print('  {}'.format(l))
    try:
      actions_count = obj.get_n_actions()
    except:
      actions_count = 0

    if actions_count:
      print('Actions:')

      for name,i in AT.get_actions(obj):
        desc = obj.get_action_description(i)
        print('  {} : {} : {}'.format(i, name, desc if desc else "no description"))

  @staticmethod
  def get_coords(obj) -> Tuple[int, int]:
    """Returns X and Y coordinates of position(top left corner) of the \
      accessible relative to screen.

      Parameters:
        obj  (Accessible) - the node;

      Returns:
        Tuple[int, int] - X, Y
    """
    xy = obj.get_position(pyatspi.Accessibility.XY_SCREEN)
    return xy.x, xy.y

  @staticmethod
  def get_size(obj) -> Tuple[int, int]:
    """Returns maximum X and Y coordinates of accessible. In other words, it \
      returns the size of the component.

      Parameters:
        obj  (Accessible) - the node;

      Returns:
        Tuple[int, int] - X, Y
    """
    xy = obj.get_size()
    return xy.x, xy.y

  @staticmethod
  def get_coords_max(obj) -> Tuple[int, int]:
    """Returns the maximum X and Y coordinates from top left corner relative \
      to the screen.

      Parameters:
        obj  (Accessible) - the node;

      Returns:
        Tuple[int, int] - X + WIDTH, Y + HEIGHT
    """
    coords = AT.get_coords(obj)
    size   = AT.get_size(obj)
    return coords[0] + size[0], coords[1] + size[1]

  @staticmethod
  def get_box(obj):
    """Returns coordinates of rendering rectangle of accessible object.

      Parameters:
        obj  (Accessible) - the node;

      Returns:
        Tuple[int, int, int, int] - X, Y, X + WIDTH, Y + HEIGHT
    """
    return AT.get_coords(obj) + AT.get_coords_max(obj)

  @staticmethod
  def has_focus(obj) -> bool:
    """Returns True if the accessible object contains STATE_FOCUSED state, i.e. has focus.

      Parameters:
        obj  (Accessible) - the node;

      Returns:
        (bool)
    """
    return obj.get_state_set().contains(pyatspi.STATE_FOCUSED)

  @staticmethod
  def inside(obj_l, obj_r):
    """Returns True if obj_l is within the bounds of obj_r inclusive

      Parameters:
        obj  (Accessible) - the node;

      Returns:
        (bool)
    """
    r_min_x, r_min_y = AT.get_coords(obj_r)
    r_max_x, r_max_y = AT.get_coords_max(obj_r)
    l_x, l_y = AT.get_coords_max(obj_l)
    if r_min_x <= l_x <= r_max_x:
      if r_min_y <= l_y <= r_max_y:
        return True
    return False

  @staticmethod
  def kb_escape():
    """Emulates pressing escape on the keyboard.
    """
    esc = 9
    pyatspi.Registry.generateKeyboardEvent(esc, None, pyatspi.KEY_PRESSRELEASE)

  @staticmethod
  def kb_close():
    """Emulates pressing alt + f4 on the keyboard.
    """
    alt, f4 = 64, 70
    pyatspi.Registry.generateKeyboardEvent(alt, None, pyatspi.KEY_PRESS)
    pyatspi.Registry.generateKeyboardEvent(f4, None, pyatspi.KEY_PRESS)
    pyatspi.Registry.generateKeyboardEvent(alt, None, pyatspi.KEY_RELEASE)
    pyatspi.Registry.generateKeyboardEvent(f4, None, pyatspi.KEY_RELEASE)

  @staticmethod
  def mouse_close(obj, shift_x=0, shift_y=0):
    """Moves the mouse cursor to the upper right corner of the acccessible
    component and clicks with the left mouse button. Useful when application
    (component) does not have an exit button.

      Parameters:
        obj     (Accessible) - the node;
        shift_x (int)        - x coordinate correction;
        shift_y (int)        - y coordinate correction

      Returns:
        (None)
    """
    x, y = AT.get_coords(obj)
    w, h = AT.get_size(obj)

    top_right_corner_x = x + w - 5 + shift_x
    top_right_corner_y = y + 5 + shift_y

    pyatspi.Registry.generateMouseEvent(
      top_right_corner_x,
      top_right_corner_y,
      pyatspi.Accessibility.MOUSE_ABS
    )
    pyatspi.Registry.generateMouseEvent(
      top_right_corner_x,
      top_right_corner_y,
      pyatspi.Accessibility.MOUSE_B1C
    )

  @staticmethod
  def mouse_click(obj):
    """Moves the mouse cursor to the center of the acccessible component and
    clicks with the left mouse button.

      Parameters:
        obj     (Accessible) - the node;

      Returns:
        (None)
    """
    x, y = AT.get_coords(obj)
    w, h = AT.get_size(obj)

    center_point_x = x + w / 2
    center_point_y = y + h / 2

    pyatspi.Registry.generateMouseEvent(
      center_point_x,
      center_point_y,
      pyatspi.Accessibility.MOUSE_ABS
    )
    pyatspi.Registry.generateMouseEvent(
      center_point_x,
      center_point_y,
      pyatspi.Accessibility.MOUSE_B1C
    )

  def el(self, role:str, name='', node=None, desc='', wait=0, in_breadth=False):
    """Searches for an object in the ATSPI tree, after the object is found, \
       available actions will be attached to it as methods.

      Attached methods:
        print         - applies the static AT.print_class() method to the current item;
        %action_name% - method name corresponding to the action name for spi, for example "click";

        example:
          app = AT('Thunderbird')
          menu_file = app.el('menu', 'File')
          menu_file.print()
          menu_file.click()

      Parameters:
        role        (str) - the type of the interface element, for example "push button", "menu", "paragraph";
        name        (str) - non-localized element name, refines search;
        node (Accessible) - the node from which to start the search;
        desc        (str) - description contained in the element, refines search;
        wait        (int) - in seconds, how long to expect the appearance of an element in the tree spi
        in_breadth (bool) - traverse accessible tree in breadth

      Returns:
        (Accessible) - found element with added actions as methods.
        (None)       - if the element was not found
    """
    start_node = node or self.app
    obj_with_actions = None

    # function with predefined parameters
    class Call():
      def __init__(self, i:int, delay:float):
        self.i = i
        self.delay = delay

      def func(self):
        obj_with_actions.do_action(self.i)
        if self.delay > 0.0:
          time.sleep(self.delay)

    for _ in CountDown(wait or 1):
      obj_with_actions = self.find_accessible(start_node, role, name, desc, in_breadth)
      if obj_with_actions:
        break

    if obj_with_actions:
      for nm,i in AT.get_actions(obj_with_actions):
        setattr(obj_with_actions, nm, Call(i, self.delay).func)

      setattr(obj_with_actions, 'print', lambda: self.print_class(obj_with_actions))

    return obj_with_actions

  @staticmethod
  def test(cmd_args:List[str], mode='gtk' or 'qt', timeout=0.0):
    """Runs the command specified in cmd_args with ATSPI enabled and exits
       the program after the process is finished. May be useful when using
       some external atspi tree viewer like sniff(dogtail).

      Parameters:
        cmd_args (list of str) - subprocess.run arguments;
        mode     (literal)     - set OOO_FORCE_DESKTOP;
        timeout  (float)       - time (in seconds) before the forced termination
                                 of the process, if not specified, the process
                                 works as long as it needs.
      Returns:
        (None)

      Example:
        > AT.test(['okular', 'scan1.pdf'], 'qt')
    """

    AT.force_atspi(mode)

    proc = multiprocessing.Process(
      target=subprocess.run,
      args=(cmd_args,),
      daemon=True
    )
    proc.start()
    proc.join(timeout if timeout else None)
    exit(0)

  def __terminate(self):
    if hasattr(AT, 'procs'):
      for p in reversed(AT.procs):
        p.terminate()

class VNode:
  """
  Visible Node. Для построения дерева видимых объектов. Дерево строится исходя
  из прямоугольников отрисовки. Т.е. потомки полностью входят в область отрисовки
  их родителей.
  Thread-save.
  """
  count_objects = 0
  __id_gen = None
  __mutex = None

  def __new__(cls, *args, **kwargs):
    if cls.__id_gen is None:
      cls.__id_gen = itertools.count()
    if cls.__mutex is None:
      cls.__mutex = threading.RLock()
    cls.count_objects += 1
    return super(VNode, cls).__new__(cls)

  def __init__(self, accessible):
    self.id = next(self.__id_gen)
    self.root = self
    self.parent = None
    self.accessible = accessible

    self.x, self.y, self.x1, self.y1 = AT.get_box(self.accessible)
    self.nodes = []

  def walk(self, in_depth=False, from_root=False):
    stack = deque([self.root]) if from_root else deque(self.nodes)
    while stack:
      cur = stack.popleft()
      yield cur
      if in_depth:
        stack.extendleft(cur.nodes)
      else:
        stack.extend(cur.nodes)

  def contains(self, node:"VNode"):
    if self.x <= node.x <= self.x1:
      if self.y <= node.y <= self.y1:
        return True
    return False

  def contains_xy(self, x:int, y:int):
    if self.x <= x <= self.x1:
      if self.y <= y <= self.y1:
        return True
    return False

  def __eq__(self, node:"VNode"):
    return self.id == node.id

  def same(self, node:"VNode"):
    if self.x == node.x and self.y == node.y:
      if self.x1 == node.x1 and self.y1 == node.y1:
        return True

  def add(self, node:"VNode"):
    """Возвращает False, если элемент не был добавлен в дерево.
    """
    with self.__mutex:
      if (node.x, node.y, node.x1, node.y1) == (0, 0, 0, 0):
        return False

      last_contains = None
      if self.contains(node) and not self == node \
                              and not self.same(node):
        last_contains = self
      for n in self.walk():
        if not (n.same(node) or n == node):
          if n.contains(node):
            last_contains = n

      if last_contains:
        node.root = self.root
        node.parent = last_contains
        last_contains.nodes.append(node)
        return True
      else:
        return False

  def refresh(self):
    with self.__mutex:
      update_queue = deque()
      for node in self.walk(from_root=True):
        newbox = AT.get_box(node.accessible)
        if newbox != (node.x, node.y, node.x1, node.y1):
          node.x, node.y, node.x1, node.y1 = newbox
          update_queue.append(node)

      while update_queue:
        weak_node = update_queue.popleft()
        if weak_node.parent:
          try:
            weak_node.parent.nodes.remove(weak_node)
          except ValueError:
            continue
          weak_node.parent.nodes.extend(weak_node.nodes)
          weak_node.nodes.clear()
          self.root.add(weak_node)
        else:
          pass

  def at_point(self, x:int, y:int):
    with self.__mutex:
      if self.contains_xy(x, y):
        yield self
      for acc in self.walk():
        if acc.contains_xy(x, y):
          yield acc

  def __del__(self):
    VNode.count_objects -= 1

class SPInspector(tkinter.Tk, multiprocessing.Process):
  """
    Позволяет получать информацию об элементе интерфейса в структуре ATSPI при
    наведении на него курсора мыши.
    Объект строит собственное дерево элементов в качестве кэша т.к. встроенное
    в ATSPI дерево accessible-объектов не отражает зависимость зон отрисовки
    потомка от родителя.

    Зачем нужен этот класс:

    1)  Встроенная функция библиотеки pyatspi.findAllDescendants(node, pred) с
    предикатом работает слишком медленно для задачи поиска самого "верхнего"
    (для пользователя) из отрисованных элементов т.к. зависимость родителя и
    потомка во встроенном дереве ATSPI не отражает связь их зон отрисовки, они
    могут быть совершенно разными.

    2)  Функция API Accessible::Component::getAccessibleAtPoint не работает
    корректно, т.к. информация о слоях которую хранят accessible-объекты не
    всегда соответствует действительности.
    Т.е. Accessibility::Component::getLayer() может возвращать LAYER_BACKGROUND
    для всех accessible-объектов.

    3)  Функция Accessibility::Component::getExtents(short coord_type) возвращает
    объект BoundingBox, который содержит координаты прямоугольника отрисовки
    accessible-объекта. Но она игнорирует параметр coord_type, хотя другие
    функции использующие этот параметр обрабатывают его корректно. Это приводит
    к тому, что некоторые accessible-объекты могут "содержать" в себе другие,
    хотя на самом деле это не так.

    Проблемы 1) и 2) решаются построением собственного дерева(кэша)
    accessible-объектов.
    Проблема 3) решается сортировкой accessible-объектов имеющих общую зону
    отрисовки по наличию фокуса.

    Example:
      AT.force_atspi()

      proc = multiprocessing.Process(
        target=subprocess.run,
        args=(['okular'],),
        daemon=True
      )
      proc.start()

      app = AT('okular', timeout=20)
      menu_file = app.el('menu item', 'Файл')
      menu_exit = app.el('menu item', 'Выход', node=menu_file)
      popup = app.el('popup menu', node=menu_file)

      spi = SPInspector(app.app)
      proc.join()

  """
  __mutex = None

  def __new__(cls, *args, **kwargs):
    """
      Конструктор поддерживающий __mutex в единственном экземпляре
    """
    if cls.__mutex is None:
      cls.__mutex = multiprocessing.Lock()
    return super(SPInspector, cls).__new__(cls)

  def __init__(self, accessible):
    """Конструктор принимающий accessible-объект в качестве корневого.

      Parameters:
        accessible (Accessible) - корневой объект, обычно объект с ролью
                                  application;
      Returns:
        (None)
    """
    multiprocessing.Process.__init__(self)
    tkinter.Tk.__init__(self)

    self.eExit = multiprocessing.Event()
    self.ePause = multiprocessing.Event()
    self.ePrintObjectsUnderCursor = multiprocessing.Event()

    self.stdin = sys.stdin
    self.acc = accessible
    self.display = display.Display()
    self.screen = self.display.screen().root
    self.focused_accessible = None

    self.__make_form()
    self.__make_cache()

    keyboard.add_hotkey('Ctrl+Shift+x', self.the_end)
    keyboard.add_hotkey('F5', self.__print_tree)
    keyboard.add_hotkey('F6', self.__print_objects_under_cursor)
    keyboard.add_hotkey('space', self.__toggle_pause)

    pyatspi.Registry.registerEventListener(self.__node_changed, 'object:state-changed:showing')

    self.focus_force()
    self.start()

  def __print_objects_under_cursor(self):
    """Инициация процедуры вывода в терминал функции __print_layer.
    """
    self.ePrintObjectsUnderCursor.set()

  def __print_tree(self):
    """Вывод в терминал содержимого древовидного кэша VNode.
    """
    for node in list(self.__bounds_tree_cache):
      try:
        print('{:4}: [{}] {}'.format(node.id, node.accessible.getRoleName(), node.accessible.name))
        for child in node.walk():
          print('{:4}: --[{}] {}'.format(child.id, child.accessible.getRoleName(), child.accessible.name))
      except:
        continue

  def __toggle_pause(self):
    if self.ePause.is_set():
      print('Unpaused!')
      self.ePause.clear()
    else:
      print('Paused!')
      self.ePause.set()

  def __node_changed(self, event):
    """Обработчик события "object:state-changed:showing".

      Parameters:
        event (Accessibility::Event struct) - структура содержащая информацию о
                                              событии, включая объект события;
      Returns:
        (None)
    """
    accessible = event.source
    main_layer = list(self.__bounds_tree_cache)
    for node in main_layer:
      try:
        node.add(VNode(accessible))
      except: # accessible not available
        continue

    try:
      for n in main_layer:
        n.refresh()

    except Exception as e:
      if 'was not provided' in str(e):
        self.eExit.set()

  def __make_cache(self):
    """Создание древовидного кэша элементов ATSPI.
    """
    if self.acc:
      self.acc = AT.find_accessible(
        self.acc,
        self.acc.getRoleName(),
        self.acc.get_name(),
        self.acc.get_description()
      )
      #init tree
      self.acc.clear_cache()
      self.__bounds_tree_cache = deque()
      self.__bounds_tree_cache.append(VNode(self.acc))


      for n in AT.walk(self.acc, in_depth=False):
        new = VNode(n)
        back = self.__bounds_tree_cache.pop()
        if back.add(new):
          self.__bounds_tree_cache.append(back)
        else:
          self.__bounds_tree_cache.extend([back, new])

      #init predicate
      # def _(accessible):
      #   i = len(self.__bounds_tree_cache) -1
      #   new = VNode(accessible)
      #   if not self.__bounds_tree_cache[i].add(new):
      #     self.__bounds_tree_cache.append(new)

      # # fill tree
      # pyatspi.findAllDescendants(self.acc, _)

  def __make_form(self):
    """Инициализация оверлея.
    """
    self.overrideredirect(True)
    self.wait_visibility(self)
    # autosize
    #self.geometry("300x110")
    self.config(background="black")
    self.lift()
    self.wm_attributes('-topmost', True)
    self.wm_attributes('-alpha', 0.9)

    self.label_xy = tkinter.Label(
      master=self,
      text='X,Y:',
      font=('Castellar', 8),
      fg='white',
      bg='black',
    )
    self.label_xy.grid(row=0, column=0, sticky='w')

    self.label_objects = tkinter.Label(
      master=self,
      text='Objects: 0',
      font=('Castellar', 8),
      fg='white',
      bg='black',
    )
    self.label_objects.grid(row=0, column=1, sticky='e')

    self.label_name = tkinter.Label(
      master=self,
      text='Name:',
      font=('Castellar', 8),
      fg='white',
      bg='black',
    )
    self.label_name.grid(row=1, column=0, sticky='w')

    self.label_coords = tkinter.Label(
      master=self,
      text='[]',
      font=('Castellar', 8),
      fg='white',
      bg='black',
    )
    self.label_coords.grid(row=1, column=1, sticky='w')

    self.label_role = tkinter.Label(
      master=self,
      text='Role:',
      font=('Castellar', 8),
      fg='white',
      bg='black'
    )
    self.label_role.grid(row=2, column=0, sticky='w')

    self.label_actions = tkinter.Label(
      master=self,
      text='Actions:',
      font=('Castellar', 8),
      fg='white',
      bg='black'
    )
    self.label_actions.grid(row=3, column=0, sticky='w')

    self.message_hotkeys = tkinter.Message(
      master=self,
      text='Controls:\n'
           ' [Shift+Ctrl+X] exit\n'
           ' [Space] пауза\n'
           ' [F5] вывести содержимое кэша\n'
           ' [F6] вывести элементы под курсором',
      font=('Castellar', 8),
      fg='white',
      bg='black',
      padx=1,
      width=240
    )
    self.message_hotkeys.grid(row=4, column=0, sticky=tkinter.W)
    self.grid_columnconfigure(0, weight=1)
    #self.grid_rowconfigure(5, weight=1)

  def __at_point_cached(self, x:int, y:int, ignore_noname=True):
    """Получение из кэша списка элементов содержащих точку с координатами x,y в зоне
    своей прорисовки.

      Parameters:
        x             (int)  - координата x;
        y             (int)  - координата y;
        ignore_noname (bool) - не получать элементы, для которых не задано имени;

      Returns:
        (generator) - генератор списка элементов содержащих точку(x,y) в зоне
                      своей прорисовки
    """
    for node in list(self.__bounds_tree_cache):
      for child in node.at_point(x, y):
        name = child.accessible.get_name()
        if ignore_noname and name == '':
          continue
        #print(  '{}|{}'.format(b.accessible.getRoleName(), b.accessible.get_name()))
        yield child

  def the_end(self):
    """Инициация завершения программы.
    """
    self.eExit.set()

  def __print_layer(self, layer, x, y):
    """Вывод в терминал первичного слоя и его потомков относительно координат (x,y).

      Parameters:
        layer         (list) - список элементов полученный от функции
                               self.__at_point_cached(x, y)
        x             (int)  - координата x;
        y             (int)  - координата y;

      Returns:
        (None)
    """
    print('------at({},{})'.format(x,y))
    for a in layer:
      print('{:4}: [{}] {} {}'.format(
        a.id,
        a.accessible.getRoleName(),
        a.accessible.name,
        'focused' if AT.has_focus(a.accessible) else ''
      ))
      for b in a.walk():
        print('{:4}: --[{}] {} {}'.format(
          b.id,
          b.accessible.getRoleName(),
          b.accessible.name,
          'focused' if AT.has_focus(b.accessible) else ''
        ))

  def idle_update_overlay(self):
    """Основной обработчик SPInspector. Обновляет оверлей, получает информацию
    о элементах.
    """
    data = self.screen.query_pointer()._data
    x,y = data['root_x'], data['root_y']

    self.label_xy.config(text='X,Y: {}x{}'.format(x, y))
    self.label_objects.config(text='Objects: {}'.format(VNode.count_objects))

    first_layer = sorted(
      self.__at_point_cached(x, y),
      key=lambda x: AT.has_focus(x.accessible),
      reverse=True
    )
    if first_layer:
      if self.ePrintObjectsUnderCursor.is_set():
        self.__print_layer(first_layer, x, y)
        self.ePrintObjectsUnderCursor.clear()
      accessible_under_cur = first_layer[0].accessible

      if accessible_under_cur:
        self.label_name.config(text='Name: {}'.format(
          accessible_under_cur.get_name() or '-',
        ).strip())
        self.label_coords.config(text='[{},{},{},{}]'.format(
          *AT.get_box(accessible_under_cur)
        ))
        self.label_role.config(text='Role: {}'.format(
          accessible_under_cur.get_role_name()
        ).strip())
        self.label_actions.config(text='Actions: {}'.format(
          ','.join([str(a) for a,_ in AT.get_actions(accessible_under_cur)])
        ).strip())

    # update form position
    self.geometry('+{}+{}'.format(x + 5, y + 15))
    self.lift()
    self.focus_force()

  def terminate(self):
    """Принудительное завершение работы SPInspector. Не рекомендуется.
    Используйте горячие клавиши.
    """
    self.eExit.set()
    self.join()
    self.display.flush()

  def run(self):
    """Основной цикл обновления интерфейса tkinter.
    """
    while not self.eExit.is_set() and not time.sleep(0.01):
      if not self.ePause.is_set():
        with self.__mutex:
          try:
            self.idle_update_overlay()
          except Exception as e:
            if 'application no longer exists' in str(e):
              print('Тестируемое приложение было закрыто.')
              break
          self.update()
    self.destroy()

def print_help():
  print( '\n  {} [{}] - утилита для тестирования ATSPI.'.format(I("SPInspector", 39), __version__ ) )

  message = \
  """
  Использование:
    atspi.py [параметры] application [параметры вызываемого приложения...]

  Пример:
    atspi.py soffice --writer

  Параметры:
    application        Запуск указанного приложения в режиме ATSPI.

    --mode [gnome, qt, attach]
                       "gnome" ATSPI в режиме gnome, по-умолчанию;
                       "qt" ATSPI в режиме qt;
                       "attach" подключиться к запущенному приложению.

    -v                 Показать версию пакета.
    --version
    -h --help          Показать это сообщение и завершить работу.

  """.format()

  print( message )
  print( 'kernelplv@gmail.com // m.mosolov@rosalinux.ru // www.rosalinux.ru 2021©' )

if __name__ == '__main__':
  parser = argparse.ArgumentParser( add_help=False, usage=argparse.SUPPRESS )

  parser.add_argument('-h', '--help', action='store_true')
  parser.add_argument('--mode', choices=['gnome', 'qt', 'attach'], default='gnome')
  parser.add_argument('-v', '--version', dest='need_version', action='store_true', default=False)
  parser.add_argument('application', nargs='*')

  cmdline, additional_args = parser.parse_known_args( sys.argv[1:] )

  if cmdline.help:
    print_help()
    exit(0)

  if cmdline.need_version:
    print( __version__ )
    exit(0)

  if cmdline.application:
    app_name = cmdline.application[0]
    AT.force_atspi(cmdline.mode)

    attached = cmdline.mode == 'attach'
    if not attached:
      proc = multiprocessing.Process(
        target=subprocess.run,
        args=([app_name] + additional_args,),
        daemon=True
      )
      proc.start()

    app = AT(app_name, timeout=20)
    print('Создание кэша элементов ATSPI может занять некоторое время, '
          'пожалуйста, дождитесь инициализации оверлея...')
    try:
      spi = SPInspector(app.app)
      if not attached: proc.join()
    except AT.ApplicationNotFound as error:
      print(error)
      if not attached: proc.terminate()

  else:
    print('Укажите аргумент application. Например atspi.py okular.')
