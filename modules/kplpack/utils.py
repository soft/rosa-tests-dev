#!/usr/bin/python3.5
# -*- coding: utf-8 -*-
# ┌───────────────────────────────────────────────────────────────────────────┐
# │                                                                           │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@:::::::::@@@@@@@@@@@::::::@@@@@@@@@@@::::@@@@@@@@@@@@@:@@@@@@@@@   │
# │   @@@:------------::@@@@:----------::@@@@:--------::@@@@@@@:---:@@@@@@@   │
# │   @@@:---@@@@@@@@:--:@@---:@@@@@@@---:@@---:@@@@@:@@@@@@@@:-----@@@@@@@   │
# │   @@@:---@@@@@@@@:--:@:--:@@@@@@@@:---@@---:@@@@@@@@@@@@@@--:@:--@@@@@@   │
# │   @@@:---@@@@@@@:---@@:--:@@@@@@@@:---@@@:------:::@@@@@@---@@@--:@@@@@   │
# │   @@@:-----------::@@@:--:@@@@@@@@:---@@@@@@:::::---@@@@:--:@@@:--:@@@@   │
# │   @@@:---@@@@@:--:@@@@:--:@@@@@@@@:--:@@@@@@@@@@@:--:@@:-----------:@@@   │
# │   @@@:---@@@@@@:---:@@@:--:::@@@::--:@@::-:::@@::---@@:--:@@@@@@@---:@@   │
# │   @@@:::-@@@@@@@@::::@@@@:::-----::@@@@@@:::----::@@@::::@@@@@@@@:::-@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
# │                                                                           │
# │                  Additional tools for ROSA Tests                          │
# │                                                                           │
# │   Copyright © 2020 LLC "NTC IT ROSA"                                      │
# │   License: GPLv3                                                          │
# │   Authors:                                                                │
# │       Michael Mosolov     <m.mosolov@rosalinux.ru>       2020             │
# │                                                                           │
# │   This program is free software; you can redistribute it and/or modify    │
# │   it under the terms of the GNU General Public License as published by    │
# │   the Free Software Foundation; either version 3, or (at your option)     │
# │   any later version.                                                      │
# │                                                                           │
# │   This program is distributed in the hope that it will be useful,         │
# │   but WITHOUT ANY WARRANTY; without even the implied warranty of          │
# │   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            │
# │   GNU General Public License for more details                             │
# │                                                                           │
# │   You should have received a copy of the GNU General Public License       │
# │   License along with this program; if not, write to the                   │
# │   Free Software Foundation, Inc.,                                         │
# │   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.           │
# │                                                                           │
# ╘═══════════════════════════════════════════════════════════════════════════╛

import cProfile
import crypt
import dbus
import filecmp
import grp
import hashlib
import io
import json
import keyboard
import logging
import os
import pstats
import psutil
import pty
import pwd
import pyatspi
import pyatspi.Accessibility
import pyatspi.action
import pyatspi.utils
import re
import rpm
import select
import selinux
import shutil
import socket
import stat
import sys
import tempfile
import termios
import time
import traceback
import tty
import datetime
import asyncio

from base64 import b64decode, b64encode
from collections import deque, namedtuple
from contextlib import contextmanager
from Crypto import Random
from Crypto.Cipher import AES
from dateutil.relativedelta import relativedelta
from enum import Enum
from mmap import MAP_SHARED, mmap
from multiprocessing import Lock as MLock
from multiprocessing import Manager, Process, JoinableQueue
from multiprocessing.synchronize import Lock as classLock
from pathlib import Path
from subprocess import PIPE, STDOUT, Popen, call, check_output
from threading import Event
from threading import Lock as TLock, RLock
from threading import Thread
from time import sleep
from typing import Callable, Dict, List, Tuple, Union
from asyncio.events import get_event_loop, new_event_loop

OK  = 0
BAD = 1

def colored( string:str, foreground:int=-1, background:int=-1,
              underline=False, bold=False, dim=False, italic=False, blink=False ) -> str:
  """ The function returns a string with added ansi colorcodes.

      Parameters:
      string     (str)  - source string to be colored
      foreground (int)  - 256-bit color code
      background (int)  - 256-bit color code

      Attributes:
      hasattr(colored, 'DISABLED') - if true, the function will not colorize the text.
                                     This can be useful when the text is being saved to
                                     a file where the escape sequences would be redundant.

                                     Example:
                                       >> colored('COLORED', 9)
                                       [38;5;9mCOLORED[0m

                                       >> colored.DISABLED = 1
                                       >> colored('COLORED', 9)
                                       COLORED

      Returns:
      (str) : always returns a string type
  """

  RESET = '\x1b[0m'
  res   = ''

  if foreground < 0 and background < 0:
    return str(string)

  if hasattr(colored, 'DISABLED'):
    return str(string)

  content = str(string).splitlines()

  for part in content:

    if foreground > 0:
      buf = '\x1b[38;5;{fg}m{content}{reset}'.format( fg=foreground, content=part, reset=RESET  )
    else:
      buf = part + RESET

    if background > 0:
      buf = '\x1b[48;5;{bg}m'.format( bg=background ) + buf

    if underline:
      buf = '\x1b[4m' + buf

    if bold:
      buf = '\x1b[1m' + buf

    if dim:
      buf = '\x1b[2m' + buf

    if italic:
      buf = '\x1b[3m' + buf

    if blink:
      buf = '\x1b[5m' + buf

    if part != content[-1]:
      res += buf + '\n'
    else:
      res += buf

  return res

def colorMap( stream=sys.stdout ):

  I = colored
  W = shutil.get_terminal_size().columns
  H = shutil.get_terminal_size().lines

  standard  = ''
  intensity = ''
  color216  = ''
  greyscale = ''

  for c in range(8):
    standard += I( '{:^6}'.format(c), 15, c )

  for c in range(8,16):
    intensity += I( '{:^6}'.format(c), 0, c )

  a,b = 16, 34
  for _ in range( 1, 13 ):
    for c in range( a, b ):
      color216 += I( '{:^6}'.format(c), 16, c )
    color216 += '\n'
    a += 18
    b += 18

  for c in range( 232, 244 ):
    greyscale += I( '{:^6}'.format(c), 15, c )
  greyscale += '\n'

  for c in range( 244, 256 ):
    greyscale += I( '{:^6}'.format(c), 16, c )

  print( I( '{:^{width}} {:^{width}}', 16, 244 ).format( 'STANDART', 'HIGH-INTENSITY', width=int(96/2) ) )
  print( '{:<} {:<{width}}'.format( standard, intensity, width=int(W/2) -1 ) )
  print()

  print( I( '{:^{width}}', 16, 244 ).format( '216 COLORS', width=108 ) )
  print( color216 )

  print( I( '{:^{width}}', 16, 244 ).format( 'GREYSCALE', width=72 ) )
  print( greyscale )

class ConRun:
  """
    WARNING:
      !!! not recommended for use with important users(uid, gid) !!!

      using "with-statement" is not safe. If this fails, use the following construction:
      cr = ConRun(args..)
      if cr.start():
        somecode
      cr.stop()

    If 'output' is specified, then stdout will be written to it(list of strings),
    otherwise stdout will be printed to the main process
  """

  class OKException(Exception):
    pass

  def __init__( self,
                uid:int,
                gid:int,
                raw_context="user_u:user_r:user_t:s0-s3:c0.c1023",
                memory_bytes=8192,
                output:list = [],
                wait=True,
                stdout=None,
                stdin=None,
                stderr=None,
                direct=False ):

    self.__con = raw_context
    self.__uid = uid
    self.__gid = gid
    self.__pid = -1
    self.__daemon_pid = mmap(-1, 4, flags=MAP_SHARED)
    self.__mem = mmap(-1, memory_bytes, flags=MAP_SHARED)
    self.__out = output

    if direct:
      self.__stdout = sys.stdout
      self.__stdin  = sys.stdin
      self.__stderr = sys.stdout
    else:
      self.__stdout = stdout
      self.__stdin  = stdin
      self.__stderr = stderr

    self.__rundir = '/run/user/{}'.format( self.__uid )
    self.__btrace = sys.gettrace()

    self.__STP = True # do NOT touch this!
    self.__WAIT = wait

    self.__user = User( uid=self.__uid )

    if User().uid == self.__uid or os.getuid() == self.__uid:
      self.__PROT = True
    else:
      self.__PROT = False

  def __set_daemon_pid(self, pid:int):
    self.__daemon_pid.seek(0)
    self.__daemon_pid.write(pid.to_bytes(4, 'big', signed=True))

  def __get_daemon_pid(self):
    self.__daemon_pid.seek(0)
    return int.from_bytes(self.__daemon_pid.read(), 'big', signed=True)

  def start(self):

    self.__setdirs()
    self.__pid = os.fork()

    if self.__pid == 0:
      self.__detach()
      self.__setenv()
      return True
    else:
      return False

  def stop(self):

    if self.__pid == 0:
      os._exit(0) # need to think about it
    else:
      os.waitpid(self.__pid, 0)

      self.__setdirs(unset=True)

      if not self.__PROT:
        x( 'pkill -u {}'.format( self.__uid ) )

      return True

  def __setenv(self):

    os.environ['HOME'] = self.__user.home

    os.environ['DISPLAY'] = ':0'
    os.environ['XAUTHORITY'] = '{}/.Xauthority'.format( self.__user.home )
    # если устанавливать адрес сессии dbus возникают проблемы с
    # lo_kde5filepicker - он перестает быть видимым для atspi
    # однако без установки этой переменной пропадает главное меню в приложениях
    os.environ['DBUS_SESSION_BUS_ADDRESS'] = self.__user.dbus_session_id # must be empty if not current user

    os.environ['PWD']       = os.environ['HOME']
    os.environ['USER']      = self.__user.name
    os.environ['USERNAME']  = self.__user.name
    os.environ['LOGNAME']   = self.__user.name
    os.environ['MAIL']      = 'MAIL=/var/spool/mail/' + self.__user.name
    os.environ['ENV']       = '/{}/.bashrc'.format( self.__user.name )
    os.environ['SCREENDIR'] = self.__user.home + '/tmp'

    os.environ['XDG_RUNTIME_DIR']     = '/run/user/{}'.format( self.__uid )
    os.environ['XDG_CURRENT_DESKTOP'] = 'KDE'
    os.environ['SAL_USE_VCLPLUGIN']   = 'gtk3_kde5'
    os.environ['XDG_DATA_DIRS']       = '{}/.local/share/flatpak/exports/share:/var/lib/flatpak/exports/share:/usr/local/share:/usr/share'\
                                        .format( self.__user.home )
    os.environ['XDG_CONFIG_DIRS'] = '/etc/xdg:/etc/xdg/kde4'

    if 'XDG_SESSION_ID' in os.environ.keys():
      del os.environ['XDG_SESSION_ID']

  def __setdirs(self, unset=False):

    #create /var/user/run/xxx dir
      ex = os.path.exists( self.__rundir )
      if not self.__STP and unset and ex:

        try:
          #unmount gvfs from /run/user/xxx
          dir_gvfs = '/run/user/{}/gvfs'.format( self.__uid ) #ROSA NICKEL path
          if os.path.exists( dir_gvfs ):
            if x( 'unmount {}'.format( dir_gvfs ) ):
              print( 'ConRun: unmount {} failed! Unmount and delete manually.'.format( dir_gvfs ) )
          shutil.rmtree( self.__rundir )
        except IsADirectoryError as e:
          pass

      if not ex and not unset:
        os.mkdir( self.__rundir )
        os.chown( self.__rundir, self.__uid, self.__gid )
        chmodR( self.__rundir, '0700' )
        self.__STP = False

    #get x-server cookie [ no need to unset ]
      if not self.__PROT:
        cu_xauth_file = '{}/.Xauthority'.format( User().home )
        nu_xauth_file = '{}/.Xauthority'.format( self.__user.home )

        if os.path.exists( nu_xauth_file ):
          if not filecmp.cmp( cu_xauth_file, nu_xauth_file, shallow=False ):
            shutil.copyfile( cu_xauth_file, nu_xauth_file )
        else:
          shutil.copyfile( cu_xauth_file, nu_xauth_file )

        os.chown(nu_xauth_file, self.__uid, self.__gid)

  def __detach(self):
    """ use in forked process """

    with open( '/proc/self/loginuid', 'w') as f:
      f.write( str( self.__uid ) )

    # necessary order!
    os.setgroups( [self.__gid] )
    os.initgroups( self.__user.name, self.__gid )
    os.setgid( self.__gid )
    os.setuid( self.__uid )
    os.setegid( self.__gid )
    os.seteuid( self.__uid )

    # change work dir & file permissions mask
    os.chdir( self.__user.home )
    os.umask(0)
    os.setsid()

    # clear I/O buffers
    sys.stdout.flush()
    sys.stderr.flush()

    # collect all output
    if self.__stdin is None:
      with open(os.devnull, 'rb', 0) as devnull:
        os.dup2( devnull.fileno(), sys.stdin.fileno() )

    if self.__stdout is None:
      with open(os.devnull, 'ab', 0) as devnull:
        os.dup2( devnull.fileno(), sys.stdout.fileno() )
      sys.stdout = StreamToMem(self.__mem)

    if self.__stderr is None:
      with open(os.devnull, 'ab', 0) as devnull:
        os.dup2( devnull.fileno(), sys.stderr.fileno() )

    # # write pid
    # pidpath = '/run/user/{}/ConRun_{}_{}.pid'.format( self.__uid, self.__uid, self.__gid)

    # with open( pidpath, 'w') as pidfile:
    #   print( os.getpid(), file=pidfile )

    # atexit.register( lambda: os.remove(pidpath) )

    # signal handler for termination
    # def sigterm_handler(signo, frame):

    #   print('ConRun: fork get SIGTERM.')
    #   #os.remove( pidpath )
    #   raise SystemExit(1)

    # signal.signal( signal.SIGTERM, sigterm_handler )

  def __services(self):
    """ Running its own services, such as dbus.
        After __detach only!
    """

    # if 'DBUS_SESSION_BUS_ADDRESS' in os.environ.keys():
    #   if os.environ['DBUS_SESSION_BUS_ADDRESS'] == '':

    #     dbus_server = Popen( ['dbus-launch', '--sh-syntax'], universal_newlines=True, stdout=PIPE, stderr=STDOUT )
    #     stdout, _   = dbus_server.communicate()

    #     if dbus_server.returncode == 0:
    #       for line in stdout.splitlines():
    #         if 'DBUS_SESSION_BUS_ADDRESS' in line:
    #           os.environ['DBUS_SESSION_BUS_ADDRESS'] = line.split( '\'' )[1]
    #     else:
    #       print( '__services(): dbus launch failed, error {} - {}'.format( dbus_server.returncode, stdout )   )
    pass

  def __enter__(self):

    self.__setdirs()
    self.__pid = os.fork()

    if self.__pid == 0:
      if os.fork() == 0:
        selinux.setcon_raw( self.__con )
        self.__detach()
        self.__setenv()
        self.__services()
        self.__set_daemon_pid(os.getpid())
      else:
        os._exit(0)

    else:
      # wait daemon init
      while self.__get_daemon_pid() == 0:
        sleep(1)
      #self.__mem_stream.write(str( self.__get_daemon_pid() ))

      # wait daemon work
      if self.__WAIT:
        while self.__get_daemon_pid() != -1:
          sleep(1)
      else:
        pass

      # hack or raise self.OKException('normal situation')
      sys.settrace( lambda *args, **keys: None )
      frame = sys._getframe(1)
      frame.f_trace = self.__trace

    return self.__out

  def __exit__(self, clss, value, trace):

    def kill_all_procs():
      if not self.__PROT and self.__WAIT:
        x( 'pkill -u {}'.format( self.__uid ) )

    # in fork
    if self.__pid == 0:
      if clss:
        print( 'Exception: {} - {}; {}'.format( clss.__name__, value, traceback.format_exc() ) )

      # if isinstance(sys.stdout, StreamToMem):
      #   sys.stdout.close()

      self.__set_daemon_pid(-1)
      os._exit(0)

    # in main
    else:
      if clss is None:
        return

      if clss.__name__ == 'OKException':
        sys.settrace( self.__btrace )
        self.__setdirs(unset=True)

        if len(self.__out) > 0 and self.__stdout:
          for line in StreamToMem(self.__mem).read():
            print( line )
        else:
          self.__out[:] = StreamToMem(self.__mem).read()

        kill_all_procs()

        return True

      else: #another Exceptions
        print( clss.__name__, value, trace )
        kill_all_procs()

        return False

  def __trace(self, frame, event, arg):
    raise self.OKException('normal situation')

class StreamToLog:
  """ Fake file-like stream object that redirects writes to a logger instance.
      example: sys.stderr = StreamToLogger(logger, logging.DEBUG)
  """
  def __init__(self, logger, log_level=logging.INFO):

    self.logger    = logger
    self.log_level = log_level
    self.linebuf   = ''

    if logger.handlers:
      if hasattr( logger.handlers[0], 'baseFilename' ):
        with open( logger.handlers[0].baseFilename, 'r') as f:
          self.fileno  = f.fileno()

  def write(self, buf):

    for line in buf.rstrip().splitlines():

      if 'Gtk-WARNING' in line:
        continue
      else:
        self.logger.log(self.log_level, line.rstrip())

  def flush(self):
    pass

class StreamToMem:
  """ Allows asynchronously in a single thread to serve requests for
  writing/reading mmap.
  """

  def __init__(self, mem_file: mmap):

    self.__mem = mem_file
    self.__mem.seek(0, os.SEEK_SET)
    self.__write_cur = 0

    self.__loop    = new_event_loop()
    self.__queue   = asyncio.Queue(loop=self.__loop)
    self.__thread  = Thread(target=self.__handle, daemon=True)

    self.__thread.start()

  async def __write_bytes(self, data:str):
    data = data.encode()
    self.__mem.seek(self.__write_cur)

    if (self.__mem.tell() + sys.getsizeof( data )) >= len(self.__mem) -1:
      raise Exception('Not enough memory to write bytes')
    else:
      self.__mem.write( data + b'\n' )
      self.__mem.flush()
      self.__write_cur = self.__mem.tell()

  async def __read_bytes(self, wait=True) -> list:
    await self.__queue.join()

    strings = list()
    self.__mem.seek(0, os.SEEK_SET)

    for line in iter( self.__mem.readline, b'' ):
      if b'\n' in line:
        strings.append( line.decode('utf-8').strip('\n') )

    return strings

  def __handle(self):
    asyncio.set_event_loop(self.__loop)
    _ = self.__loop.create_task(self.__handle_queue())
    self.__loop.run_forever()
    self.__loop.close()

  async def __write(self, buf:str):
    await self.__queue.put(buf)

  async def __handle_queue(self):
    while True:
      buf = await self.__queue.get()
      for line in buf.splitlines():
        if 'Gtk-WARNING' in line:
          continue
        else:
          await self.__write_bytes(line)
      self.__queue.task_done()

  def write(self, buf):
    task = asyncio.run_coroutine_threadsafe(self.__write(buf), self.__loop)
    while not task.done(): sleep(0.1)

  def read(self):
    task = asyncio.run_coroutine_threadsafe(self.__read_bytes(), self.__loop)
    return task.result()

  def flush(self):
    pass

  def close(self):
    self.__loop.stop()
    self.__thread.join(5)

class StreamToNull(object):

  def write(self, *nothing):
    pass

  def flush(self):
    pass

class Damper:

  def __init__(self, stream = sys.stderr):
    """ Suppresses any output in %stream% until the end of with-block
    """

    if hasattr(stream, 'fileno') and hasattr(stream, 'flush'):
      self.stream        = stream
      self.stream.flush()
      self.stream_file   = self.stream.fileno()
      self.stream_backup = os.dup( self.stream_file )

  def __enter__(self):

    try:
      self.devnull = os.open( os.devnull, os.O_WRONLY )
      os.dup2( self.devnull, self.stream_file)
    finally:
      os.close( self.devnull )

  def __exit__(self, type, value, traceback):

    os.dup2(  self.stream_backup, self.stream_file )
    os.close( self.stream_backup )

    if value:
      raise Exception(value)

class Return(Enum):

  stdout  = 1
  codeout = 2
  errcode = 3
  process = 4
  ignore  = 5

class Command:

  def __init__(self):

    self.line          = ''
    self.info          = ''
    self.error         = ''
    self.pause_before  = 0
    self.ignore_output = False
    self.ignore_errors = False
    self.quiet         = False
    self.separated     = False
    self.timeout       = 0

class Terminal:
  """ This class allows you to organize the sequential execution
      of commands as if they were typed in the terminal.
  """

  class TerminalException(Exception):
    pass

  def __init__(self, stream = sys.stdout, flush=True):

    self.__commands = []

    self.__subjects = []
    self.__lock     = MLock()
    self.__manager  = Manager()

    self.__stream   = stream
    self.__flush    = flush
    self.__err_msg  = ''

    self.log        = self.__manager.list( [[0, 'Log start']] )
    self.fail       = [ 0, 'No errors' ]

  def __str__(self):

    mxlinesize = 1

    for c in self.__commands:
      mxlinesize = max(mxlinesize, len(c.line) )

    out =  'Commands'.center( mxlinesize+7, '─' )
    out += 'Separated'.center(12, '─')
    out += '\n'

    def makel(cmd):

      l = ''
      l += (cmd.line + ' ').ljust(mxlinesize+6,'.')

      l += '|' + ( '+'   if cmd.separated else '-').center(11)  # thread col
      l += '\n'
      return l

    for c in self.__commands:
      out += makel(c)

    return out

  def __enter__(self):
    return self

  def __exit__(self, type, value, traceback):

    self.run()

    if not self.ok():
      print(self.fail[1], file=self.__stream)

  def __soft_exit(self):

    for t in self.__subjects:
      t.join()

  def __hard_exit(self):

    for t in self.__subjects:
      if t.join(timeout=1.5) == None:
        t.terminate()

  def add(self, command='', separated=False, ignore_output=False, ignore_errors=False, quiet=False, pause_before=0, timeout=0):
    """ Adds a command to the pool of commands that will be run sequentially.

        Parameters:
        command       (str)    - the command to be executed;
        separated     (bool)   - execute command in a separate process;
        ignore_output (bool)   - all output will be directed to /dev/null;
        ignore_errors (bool)   - in any case, the command will return 0;
        quiet         (bool)   - command output will not be processed;
        pause_before  (float)  - pause before executing this command (seconds);
        timeout       (float)  - time allotted for command execution. if the time runs out, the command will return an error;

        Returns:
        (self)
    """
    if command:
      cmd               = Command()
      cmd.line          = command
      cmd.separated     = separated
      cmd.ignore_output = ignore_output
      cmd.ignore_errors = ignore_errors
      cmd.quiet         = quiet
      cmd.pause_before  = pause_before
      cmd.timeout       = timeout

      self.__commands.append( cmd )
      return self

  def ok(self, right_code=0):
    """ Returns true if everything is OK (== right_code), otherwise fills the last error field
    """

    for l in self.log:

      if l[0] != right_code:

        if not l[1] and not self.__err_msg:
          self.fail = [ l[0], 'Error message not provided.' ]

        elif not self.__err_msg:
          self.fail = [ l[0], l[1] ]

        else:
          self.fail = [ l[0], self.__err_msg.format( code=l[0], msg=l[1] ) ]

        return False

    return True

  def info(self, msg):
    """
      Add description to last command.
      it will be sent to __stream before executing the command
    """

    if msg and self.__commands:
      self.__commands[-1].info = msg
    return self

  def error(self, msg='Error[{code}]: {msg}'):
    """
      Adds a general error message.
      If not specified, raw output is used.
      Example: msg='Message:{msg}, Code{code}'
    """

    if msg:
      self.__err_msg = msg

  def run(self):

    try:
      for cmd in self.__commands:

        if cmd.separated:
          self.__run_one_process( cmd )
        else:
          self.__run_one( cmd )

      self.__soft_exit()

    except:
      self.__hard_exit()

  def __run_one(self, cmd):

    line = cmd.line

    if line:

      if cmd.info:
        print( cmd.info, end='', file=self.__stream )
        if self.__flush:
          self.__stream.flush()

      if cmd.ignore_errors:
        rt = Return.ignore
      elif cmd.quiet:
        rt = Return.errcode
      else:
        rt = Return.process

      result = x( command=line,
                  return_type=rt,
                  ignore_output=cmd.ignore_output,
                  log_and_lock={'log':self.log, 'Lock':self.__lock},
                  pause=cmd.pause_before,
                  timeout=cmd.timeout )

      if not cmd.ignore_errors:
        if not cmd.quiet and result.returncode != OK:
          raise self.TerminalException( 'Terminal error {}: {}'.format( result.returncode, result.stdout.read() ) )
        if cmd.quiet and result != OK:
          raise self.TerminalException( 'Terminal error {}'.format( result.returncode ) )

  def __run_one_process(self, cmd):

    line = cmd.line

    if line:

      if cmd.ignore_errors:
        rt = Return.ignore
      elif cmd.quiet:
        rt = Return.errcode
      else:
        rt = Return.process

      self.__subjects.append(
        Process( target=x, daemon=False,
                 args=( line,
                        rt,
                        cmd.ignore_output,
                        {'log':self.log, 'Lock':self.__lock},
                        cmd.pause_before,
                        cmd.timeout
                      ) ) )

      if cmd.info:
        print( cmd.info, end='', file=self.__stream )
        if self.__flush:
          self.__stream.flush()

      self.__subjects[-1].start()
      time.sleep( 1 )

  def print_log(self):

    print( '----------log-----------', file=self.__stream )

    for out in self.log:
      print( '{:4} | {}'.format( out[0], out[1] ), file=self.__stream )

class Ticker(object):
  def __init__(self, timeout=10, high_resolution=False, precision=0):
    """Timer.

      Parameters:
      timeout         (int, float) - sets timeout in seconds or fractions of seconds
      high_resolution (bool)       - use performance counter with highest available resolution
      precision       (int)        - round results to a given precision in decimal digits

    """
    self.__time_func = time.perf_counter if high_resolution else time.time
    self.__precision = precision

    self.timeout = timeout
    self.points = dict()
    self.first_point = 0.0
    self.last_point = 0.0

  def __sub__(self, other):
    if self.__precision > 0:
      return round(self.last_point - other.last_point, self.__precision)
    else:
      return self.last_point - other.last_point

  def start(self):
    if self.__precision > 0:
      self.first_point = round(self.__time_func(), self.__precision)
    else:
      self.first_point = self.__time_func()
    self.points = { 'start' : self.first_point }

    return self

  def tick(self) -> bool:
    delta = self.__time_func() - self.first_point

    if delta < self.timeout:
      return True
    else:
      return False

  def tock(self) -> float:
    if self.__precision > 0:
      return round(self.__time_func() - self.first_point, self.__precision)
    else:
      return self.__time_func() - self.first_point

  def check(self, point_id:str):
    if point_id:
      if point_id not in ('start', 'stop'):
        self.points[ point_id ] = self.tock()

  def __enter__(self):
    return self.start()

  def __exit__(self, type, value, trace):
    self.last_point = self.tock()
    self.points['stop'] = self.last_point

class ConPlace(object):
  """ Clears console output after with-block exit
  """

  CURSOR_UP     = '\x1b[1A'
  ERASE_LINE    = '\x1b[K'
  CLEAR_TO_END  = '\x1b[0J'

  __SAVE_CURSOR   = '\x1b[s'
  __LOAD_CURSOR   = '\x1b[u'

  def __init__(self, stream=sys.stdout, end_pause=1.0):
    """ end_pause - make pause after with-block complete, seconds
    """

    self.stream = stream
    self.pause  = end_pause

  def do(self, action):
    """ action - one of fields ConPlace-class
        Example: do( ConPlace.CURSOR_UP )
    """

    if action:
      self.stream.write( action )

  def __enter__(self):

    self.stream.write( self.__SAVE_CURSOR )
    self.stream.write( '\n' )
    return self

  def __exit__(self, type, value, trace):

    time.sleep( self.pause )
    self.stream.write( self.ERASE_LINE )
    self.stream.write( self.__LOAD_CURSOR )
    self.stream.write( self.CLEAR_TO_END )
    self.stream.flush()

class Indicator(object):
  """ Threaded single line indicator.
      This class allows you to create a text indicator of the progress in the with-block.
  """

  __SAVE_CURSOR  = '\x1b[s'
  __LOAD_CURSOR  = '\x1b[u'
  __CLEAR_TO_END = '\x1b[0J'
  __CLEAR_LINE   = '\x1b[K'
  __HIDE_CURSOR  = '\x1b[?25l'
  __SHOW_CURSOR  = '\x1b[?25h'

  __indicator = [ '[   ]','[.  ]', '[.. ]', '[...]', '[ ..]', '[  .]']
  __status    = 0
  __border    = len( __indicator )
  __interval  = 0.1
  __message   = ' Please wait for the process to complete...'
  __suffix    = ''

  __stream  = None
  __thread  = None
  __lock    = TLock()
  __exit    = Event()

  def __init__(self, message='', suffix='', stream=sys.stdout, interval_sec=0.1 ):
    """ Creates a textual progress bar.

        Parameters:
        message      (str)    - message displayed after the indicator;
        suffix       (str)    - variable part of the message used to display additional information,
                                if the suffix contains {elaps}, the elapsed time will be substituted;
        stream       (TextIO) - output file. e.g. sys.stdout;
        interval_sec (float)  - indicator drawing frequency, seconds;

        Returns:
        (self)
    """
    self.__interval = interval_sec
    self.__stream   = stream or sys.stdout
    self.__thread   = Thread( target=self.__shift, args=(), daemon=False )
    self.__message  = message or self.__message
    self.__elapsed  = False
    self.__started  = datetime.datetime.now()
    self.__suffix   = suffix

    self.__check_elaps( suffix )

    self.now = 0

  def __shift(self):

    while not self.__exit.wait( self.__interval ):

      self.__status = ( self.__status + 1 ) % self.__border

      if self.__elapsed:
        self.now = relativedelta( datetime.datetime.now(), self.__started )
        self.now = '{}:{:02d}:{:02d}:{:0^3.0f}'.format( self.now.hours, self.now.minutes, self.now.seconds, self.now.microseconds/1000 )

        self.__stream.write( ' ' + self.__indicator[ self.__status ] + self.__message + self.__suffix.format( elaps=self.now ) + self.__CLEAR_LINE + '\r' )
      else:
        self.__stream.write( ' ' + self.__indicator[ self.__status ] + self.__message + self.__suffix + self.__CLEAR_LINE + '\r' )

      self.__stream.flush()

    # cleaning
    self.__stream.write( ' ' * ( len( self.__indicator[0] ) + len( self.__message ) + 1 )  )
    self.__stream.write( '\r' )
    self.__stream.flush()

  def __check_elaps(self, suffix:str=''):

    if '{elaps}' in suffix:
      self.__elapsed = True
    else:
      self.__elapsed = False

  def set_message(self, text:str):

    with self.__lock:
      self.__message = text

  def set_suffix(self, text:str):
    self.__check_elaps( text )

    with self.__lock:
      self.__suffix = text

  def __enter__(self):

    self.__exit.clear()
    self.__stream.write( self.__SAVE_CURSOR )
    self.__stream.write( '\n' )
    self.__stream.write( self.__HIDE_CURSOR )
    self.__stream.flush()
    self.__thread.start()

    return self

  def __exit__(self, type, value, trace):

    self.__exit.set()
    self.__thread.join(1)

    self.__stream.write( self.__LOAD_CURSOR )
    self.__stream.write( self.__CLEAR_TO_END )
    self.__stream.write( self.__SHOW_CURSOR )
    self.__stream.flush()

class Context:
  """
    u : selinux-user
    r : selinux-role
    t : selinux-type(or domain)
    s : array of available levels (selinux-mls)
    c : array of available categories (selinux-mсs)
    full : full security label(context) as string
  """

  full = "file_not_found"
  u    = "_"
  r    = "_"
  t    = "_"
  s    = []
  c    = []

  def update(self):
    s = self.__convert_arrays_to_context_ranges()
    c = self.__convert_arrays_to_context_ranges(True)

    self.full = '{}:{}:{}:{}{}'.format(
      self.u,
      self.r,
      self.t,
      s if s else 's0',
      ':' + c if c else ''
    )
    return self

  @staticmethod
  def sequence(array:List[int], strict=0, cut=False):
    length = len(array)

    if length == 1:
      return [array.pop()] if cut else array
    elif length == 1:
      return []

    src = array if cut else array[:]
    result = [src.pop(0)]

    while src:
      if result[-1] < src[0]:
        if strict and src[0] - result[-1] != strict:
          return result
        result.append(src.pop(0))
      else:
        return result

    return result

  @staticmethod
  def transform_to_char_range(array:List[int], category=False):
    """
      array (list of integers) : values must be strictly sequential,
                                 in other words, their order is not important,
                                 but they can be used to form a continuous
                                 non-decreasing sequence
                                 Example:
                                  [1, 5, 2, 4, 3] -> [1, 2, 3, 4, 5] OK
                                  [1, 6, 2, 4, 3] -> WRONG (6-4 > 1)
    """

    _min = _max = None

    for val in array:
      if _min is None or val < _min:
        _min = val
      if _max is None or val > _max:
        _max = val

    if category:
      if _min == _max:
        result = 'c{}'.format(_min)
      else:
        result = 'c{}.c{}'.format(_min, _max)
    else:
      if _min == _max:
        result = 's{}'.format(_min, _max)
      else:
        result = 's{}-s{}'.format(_min, _max)

    return result

  def __convert_arrays_to_context_ranges(self, category=False):
    transformed = []

    if category:
      array = self.c[:]

      while array:
        transformed.append(
          Context.transform_to_char_range(
            Context.sequence(array, 1, cut=True),
            category=category
          )
        )

      return ','.join( transformed )

    else:
      return Context.transform_to_char_range(self.s)

class User(object):
  """ ! Root privileges required !

      This class allows you to create a user
      and delete it on close with-statement.
  """

  def __init__(self, name:str='', password:str='', uid:int=-1, home='', se_user:str='', se_range:str=''):

    if os.getuid() != 0:
      print( 'User-class: Insufficient privileges. This function only available for the root user.' )

    self.name            = name
    self.password        = password
    self.uid             = uid
    self.gid             = -1
    self.gecos           = ''
    self.home            = home
    self.shell           = ''
    self.dbus_session_id = ''
    self.groups          = []

    self.created  = False # if the specified user did not exist
    self.deleted  = False
    self.selected = False # if user exists or current is selected

    # selinux
    self.se_created = False
    self.se_deleted = False

    self.se_user  = se_user
    self.se_range = se_range
    self.context  = None

    if not name and uid < 0:
      self.current()

    elif self.exist():

      if uid >= 0:
        self.updByUid()
      else:
        self.updByName()

      self.updDbusSessionId()

      if uid == 0:
        self.updCon()

      self.selected = True

    elif self.create():

      if self.se_user and not self.se_login_exist():
        self.selinux_set_range()

    else:
      raise OSError( 'Cant create user {} | '.format( self.name ) )

  def __str__(self):

    out = ''
    out += 'name:    ' + self.name        + '\n'
    out += 'pass:    ' + self.password    + '\n'
    out += 'uid:     ' + str(self.uid)    + '\n'
    out += 'gid:     ' + str(self.gid)    + '\n'
    out += 'gecos:   ' + self.gecos       + '\n'
    out += 'home:    ' + self.home        + '\n'
    out += 'shell:   ' + self.shell       + '\n'

    if self.se_user:
      out += 'selinux: ' + self.context.full + '\n'

    return out

  def __enter__(self):
    return self

  def __exit__(self, clss, value, trace):
    self.delete()

  def __del__(self):
    """ not save, use with-statement instead"""

    self.delete()

  def create(self) -> bool:

    if not self.created and not self.deleted:

      flags = ''

      if self.password:
        encrypted_password = crypt.crypt( self.password, '22' )
        flags += ' --password {}'.format( encrypted_password )

      if self.home:
        flags += ' -d {}'.format( self.home )
      else:
        flags += ' -M'

      if self.se_user:
        flags += ' --selinux-user {}'.format( self.se_user )

      code, out = x( 'useradd{} {}'.format( flags, self.name ), Return.codeout )

      if code != 0:
        print( 'create(): creating user error {} - {}'.format( code, out ) )
        return False

      self.updByName()
      self.updCon()
      self.created = True; os.sync()
      return True

  def delete(self, quiet=False) -> bool:

    if self.name and self.created and not self.deleted:

      code, out = x( 'pkill -u {}'.format( self.name ), Return.codeout )

      # not necessary check
      # if code != 0 and not quiet:
      #   print( 'User::delete(): killing process error {} - {}'.format( code, out or 'no error message' ) )

      code, out = x( 'userdel {} -f --remove -Z '.format(  self.name ), Return.codeout )

      if code != 0:
        if not quiet:
          print( 'User::delete(): deleting error {} - {}'.format( code, out or 'no error message' ) )
        return False

      self.deleted = True; os.sync()
      return True

    return False

  def exist(self) -> bool:

    names = [ struct.pw_name for struct in pwd.getpwall()]
    uids  = [ struct.pw_uid  for struct in pwd.getpwall()]

    return self.name in names or self.uid in uids

  def se_login_exist(self) -> bool:

    out = x( 'semanage login -l', Return.stdout )

    for l in out.splitlines():
      if l and self.name in l.strip().split()[0]:
        return True

    return False

  def updByUid(self):

    users  = [ struct for struct in pwd.getpwall()]

    for u in users:
      if u[2] == self.uid:
        self.name, self.password, self.uid, self.gid, self.gecos, self.home, self.shell = u

  def updByName(self):

    users  = [ struct for struct in pwd.getpwall()]

    for u in users:
      if u[0] == self.name:
        self.name, self.password, self.uid, self.gid, self.gecos, self.home, self.shell = u

  def updDbusSessionId(self):
    cookie_dir = Path('{}/.dbus/session-bus'.format(self.home))
    if cookie_dir.exists():
      conf = next(cookie_dir.glob('*'))
      if conf.exists():
        with conf.open('r') as c:
          for l in c.readlines():
            if l.startswith('#'):
              continue
            if 'DBUS_SESSION_BUS_ADDRESS' in l:
              self.dbus_session_id = l.split('=', 1)[1].strip().strip("'")

  def current(self) -> bool:

    code, out = x( 'logname', Return.codeout )

    if code != 0:
      return False

    self.name = out.strip()
    self.updByName()

    if self.uid == 0:
      self.updCon()

    self.selected = True
    return True

  def updCon(self):

    self.context = Context()

    usr = 'user_u'
    rng = 's0'

    if self.se_login_exist() and self.se_user:
      usr = self.se_user
      rng = self.se_range or rng

    elif self.se_login_exist():
      usr = selinux.getseuserbyname( self.name )[1]
      rng = selinux.getseuserbyname( self.name )[2]

    self.context.u    = usr
    self.context.r    = self.get_se_default_roles_table()[ usr            ][-1:][0] # last role in list
    self.context.t    = self.get_se_default_types_table()[ self.context.r ][-1:][0]
    self.context.full = '{}:{}:{}:{}'.format( usr, self.context.r, self.context.t, rng  )
    self.context.s    = rangeGetFromCon( self.context.full, mcs=False )


    if len( rng.split(':') ) == 2:
      self.context.c = rangeGetFromCon( self.context.full, mcs=True  )

  def addGroup(self, group:str) -> bool:

    if (self.created or self.selected) and not self.deleted:
      code, res = x( 'usermod -a -G {} {}'.format( group, self.name ), Return.codeout )
      if code == 0:
        self.updGroups()
        return True

    return False

  def updGroups(self) -> bool:

    if (self.created or self.selected) and not self.deleted:
      if self.exist():
        code, res = x( 'groups ' + self.name, Return.codeout )

        if code == 0:
          self.groups = res.split(':')[1].split()
          return True

    return False

  @staticmethod
  def selinux_getcon() -> Context:
    """
      fields:
        str:str, u:str, r:str, t:str, s:list, c:list
    """
    context_str = selinux.getcon_raw()[1]
    context_lst = context_str.split(':')

    if len(context_lst) == 5:
      return Context(
                      context_str,
                      context_lst[0],
                      context_lst[1],
                      context_lst[2],
                      rangeGetFromCon( context_str, mcs=False ),
                      rangeGetFromCon( context_str, mcs=True )
                    )

    if len(context_lst) == 4:
      return Context(
                      context_str,
                      context_lst[0],
                      context_lst[1],
                      context_lst[2],
                      rangeGetFromCon( context_str, mcs=False ),
                      None
                    )

  def selinux_set_range(self) -> bool:

    if not self.se_created and not self.se_deleted:

      code, out = x( 'semanage login --modify {} -r "{}"'.format( self.name, self.se_range ), Return.codeout )

      if code == 0:

        # set the context of all files in the home directory
        code, out = x( 'chcon -R -l "{}" {}'.format( self.se_range, self.home ), Return.codeout )

        if code == 0:
          self.se_created = True
          self.updCon()
          return True

    print( 'selinux_set_range(): error {} - {}'.format( code, out ) )
    return False

  @staticmethod
  def get_se_default_roles_table():

    code, out = x( 'semanage user -l', Return.codeout )

    book = dict()

    if code == 0:
      table = out.splitlines()[4:]

      for rec in table:
        record = rec.split()
        book[ record[0] ] = record[4:]

    else:
      print( 'get_se_default_roles_table(): error {} - {}'.format( code, out ) )

    return book

  @staticmethod
  def get_se_default_types_table():

    # !!! at the time of writing the code, this config had 1:1 mappings without ranges
    # example: secadm_r:secadm_t
    cfg  = Path( '/etc/selinux/mls/contexts/default_type' )
    book = dict()

    if cfg.exists():

      with cfg.open( 'r' ) as f:
        for rec in f.readlines():
          record = rec.strip().split( ':' )
          book[ record[0] ] = record[1:]

    else:
      print( ' get_se_default_types_table(): config {} not exist'.format( cfg ) )

    return book

class SuspendGuard(object):
  """ This class avoids session termination due to inactivity.
      It uses dbus.
  """

  def __init__(self, appname:str, reason:str):

    self.__bus     = dbus.SessionBus()
    self.__proxy   = self.__bus.get_object('org.freedesktop.PowerManagement.Inhibit', '/org/freedesktop/PowerManagement/Inhibit')
    self.__appname = appname
    self.__reason  = reason
    self.__cookie  = -1

    self.__activate()

  def isActive(self) -> bool:
    return bool(self.__proxy.HasInhibit())

  def block(self):

    try:
      while not sleep(1): pass
    except KeyboardInterrupt as e:
      pass

  def __activate(self):
    self.__cookie = self.__proxy.Inhibit( self.__appname, self.__reason )

  def __deactivate(self):

    if self.__cookie > 0:
      self.__proxy.UnInhibit( self.__cookie )

  def __del__(self):
    self.__deactivate()

class IsoInfo(object):

  def __init__(self):

    self.iso_number       = ''
    self.release_name     = ''
    self.release_platform = ''

    self.update_iso_number()
    self.update_release()

  def update_iso_number(self):

    code, out = x( 'cat /etc/isonumber', Return.codeout )

    if code == 0:
      self.iso_number = out.strip()
    else:
      self.iso_number = '-'

  def update_release(self):

    code, out = x( 'lsb_release -r -d -s', Return.codeout )

    if code == 0:
      info                  = out.split('"')
      self.release_name     = info[1].strip()
      self.release_platform = info[2].strip()

    else:
      self.release_name     = '-'
      self.release_platform = '-'

class Actions(object):

  __CLEAR_TO_END  = '\x1b[0J'
  __SAVE_CURSOR   = '\x1b[s'
  __LOAD_CURSOR   = '\x1b[u'
  __HIDE_CURSOR   = '\x1b[?25l'
  __SHOW_CURSOR   = '\x1b[?25h'

  welcome = \
  """
  HelperScripts: вспомогательная утилита для подготовки образа к разработке.
  Отметьте нужные для выполнения пункты. Если не выбран ни один пункт, будет
  произведен корректный выход.
  Управление:
      Перемещение: {} Пометить\снять: {} Продолжить: {}
  ═══════════════════════════════════════════════════════════════════════════
  """.format( colored( ' ↑ ↓ ', 16, 244 ), colored( ' ↩ ', 16, 244 ), colored( ' Shift + ↩ ', 16, 244 ) )

  def __init__(self):

    self.__actions  = [ ] # [ [description, callable, checked_status], ... ]
    self.__selected = -1
    self.__Exit     = Event()

    self.__stream       = sys.stdout
    self.__lock         = RLock()
    self.__cursor_saved = False

  def start(self):

    print( self.welcome )
    self.__save_cursor()

    keyboard.add_hotkey('up', self.__move_up, suppress=True )
    keyboard.add_hotkey('down', self.__move_down, suppress=True )
    keyboard.add_hotkey('enter', self.__toggle, suppress=False, timeout=2 )

    self.__disable_stdin_echo()
    self.__paint()

    keyboard.wait( 'shift+enter', suppress=False )
    keyboard.unhook_all_hotkeys()

    self.__run()

  def __run(self):

    print( '\n  Запуск выбранных задач...\n' )

    for act in self.__actions:
      self.__enable_stdin_echo()

      if self.__Exit.is_set():
        break

      if act[2] == True:
        with self.__lock:
          act[1](self.__Exit)

      self.__disable_stdin_echo()

    time.sleep(1)
    self.__enable_stdin_echo()

  def __paint(self):

    with self.__lock:

      self.__load_cursor()

      for idx, act in enumerate(self.__actions):
        desc, _, status = act
        status          = '▣' if status else '□'
        line            = '  [ {} ] {}'.format( status, desc )

        if idx == self.__selected:
          print( line[0:7], colored( line[8:], 16, 244 ) )
        else:
          print( line )

  def __toggle(self):

    if self.__selected >= 0 and self.__selected < len( self.__actions ):
      if self.__actions[ self.__selected ] [ 2 ] == True:
        self.__actions[ self.__selected ] [ 2 ] = False
      else:
        self.__actions[ self.__selected ] [ 2 ] = True
      self.__paint()

  def __move_up(self):

    if self.__selected > 0:
      self.__selected -= 1
    else:
      self.__selected = len( self.__actions ) -1

    self.__paint()

  def __move_down(self):

    if self.__selected < len( self.__actions ) -1 :
      self.__selected += 1
    else:
      self.__selected = 0

    self.__paint()

  def __save_cursor(self):

    with self.__lock:
      if not self.__cursor_saved:
        self.__stream.write( self.__SAVE_CURSOR )
        self.__cursor_saved = True

  def __load_cursor(self):

    with  self.__lock:
      if self.__cursor_saved:
        self.__stream.write( self.__LOAD_CURSOR )
        self.__stream.flush()

  def __clear_to_end(self):

    with self.__lock:
      self.__stream.write( '\x1b[K' )
      self.__stream.write( self.__CLEAR_TO_END )
      self.__stream.flush()

  def __disable_stdin_echo(self):

    fd                  = sys.stdin.fileno()
    self.__old_settings = termios.tcgetattr( fd )
    new_settings        = termios.tcgetattr( fd )
    new_settings[3]     = new_settings[3] & ~ termios.ECHO

    termios.tcsetattr( fd, termios.TCSADRAIN, new_settings )

    self.__stream.write( self.__HIDE_CURSOR )
    self.__stream.flush()

  def __enable_stdin_echo(self):

    fd = sys.stdin.fileno()
    termios.tcflush( fd, termios.TCIOFLUSH )
    termios.tcsetattr( fd, termios.TCSADRAIN, self.__old_settings )

    self.__stream.write( self.__SHOW_CURSOR )
    self.__stream.flush()

  def add(self, desc:str, func:Callable, status=False):

    for act in self.__actions:
      _, exist_func, _ = act

      if func == exist_func:
        return

    self.__actions.append( [ desc, func, status ] )

  def pop_index(self, idx):

    if idx < len( self.__actions ) and idx >= 0:
      self.__actions.pop( idx )

  def pop_object(self, obj):

    for idx,act in enumerate( self.__actions ):
      if act[1] == obj:
        self.__actions.pop( idx )

class CleanException(Exception):
  def __init__(self, TestObject:object):
      super().__init__()
      self.object = TestObject

class SystemCtl:

  cmd     = 'systemctl'

  Service = namedtuple('Service', 'unit load active sub description')
  Process = namedtuple('Process', 'pid exec code status')
  Status  = namedtuple('Status',  'load path preset active since processes journal')

  def __init__(self, stream=sys.stdout):
    self.__stream = stream

  def start(self, service_name:str) -> int:
    cmd       = 'systemctl start {}'.format( service_name )
    code, out = x( cmd, Return.codeout )

    if code != 0:
      print( 'SystemCtl::start(): error {} - {}'.format( code, out ), file=self.__stream )
      return code

    return 0

  def stop(self, service_name:str) -> int:
    cmd       = 'systemctl stop {}'.format( service_name )
    code, out = x( cmd, Return.codeout )

    if code != 0:
      print( 'SystemCtl::stop(): error {} - {}'.format( code, out ), file=self.__stream )
      return code

    return 0

  def get_services(self, service_name='', all=False):

    cmd       = 'systemctl --type=service --full --plain --no-legend --no-pager{}'.format( ' --all' if all else '' )
    code, out = x( cmd, Return.codeout )

    if code != 0:
      print( 'SystemCtl::get_services(): error {} - {}'.format( code, out ), file=self.__stream )
      return

    services = []
    for line in out.splitlines():

      fields     = line.split( maxsplit=4 )
      fields[-1] = fields[-1].strip()

      if service_name and service_name.lower() in line.lower():
        return self.Service( *fields )

      else:
        services.append( self.Service( *fields ) )

    return services

  def status(self, service_name:str):

    cmd = 'systemctl status {} --no-pager'.format( service_name )
    code, out = x( cmd, Return.codeout )

    if code == 4:
      print( 'SystemCtl::status(): Unit {} could not be found'.format( service_name ), file=self.__stream )
      return None

    processes = []
    journal   = []

    pid = exe = code = status = main_pid  = ''
    load = path = preset = active = since = ''

    journal_begin = False
    for line in out.splitlines():
      line = line.lower()

      if journal_begin:
        journal.append( line )
        continue

      if '' == line:
        journal_begin = True
        continue

      elif 'process:' in line or 'main pid:' in line:
        # https://regex101.com/r/50OQ13/1/
        reg = re.findall( r'(?:(\d{1,6})(?: .*?=)?(.*) \(.*=(?:(.*), .*=(.*))\))|(\d{1,6})', line )
        if reg:
          pid, exe, code, status, main_pid = reg[0]
          process                          = self.Process( pid or main_pid, exe, code, status)
          processes.append( process )

      elif 'loaded:' in line:
        # https://regex101.com/r/LlIDXr/2
        # https://regex101.com/r/QfcT5p/1
        reg = re.findall( r'(?:Loaded: (\w*)) (?:(?:\()?(.*?);(?:(?:.*): (.*).*(?:\)))?)?', line, re.IGNORECASE )
        if reg:
          load, path, preset = reg[0]

      elif 'active:' in line:
        # https://regex101.com/r/ka3SW2/1
        # https://regex101.com/r/Jy1chn/1
        reg = re.findall( r'(?:Active: (\w*)(?:(?:.*since )(.*);)?)', line, re.IGNORECASE )
        if reg:
          active, since = reg[0]

    if not load or not active:
      print( 'SystemCtl::status(): looks like regular expressions need to be updated or service name is wrong.', file=self.__stream )
      return None

    status = self.Status( load, path, preset, active, since, processes, journal )

    return status

class Overlay(object):

  root          = None
  tree          = None
  working_root  = None

  short_storage = '/tmp/overlay-rosa-tests/'
  long_storage  = '/var/tmp/overlay-rosa-tests/'

  def __available_dirs(self, root='/') -> List:

    root_dir = Path( root )

    if not root_dir.exists():
      return []

    strict_mount_points    = [ ]
    available_mount_points = [ ]

    for part in psutil.disk_partitions(True):
      if part.fstype in [ 'tmpfs', 'proc', 'overlay' ]:
        strict_mount_points.append( part.mountpoint )

    for x in os.listdir( root_dir.as_posix() ):
      point = root_dir.as_posix() + x

      if point not in strict_mount_points:
        available_mount_points.append( point )

    return available_mount_points

  def __overleyed_dirs(self):

    overlay_mount_points = [ x.mountpoint for x in psutil.disk_partitions(True) if x.fstype == 'overlay' ]
    this_mount_points    = [ ]

    for p in overlay_mount_points:
      if p == self.tree.values():
        this_mount_points.append( p )

    return this_mount_points

  def __remove_working_tree(self):

    if self.root and self.root.exists():

      code, out = x( 'umount -n -t tmpfs {}'.format( self.root ), Return.codeout )

      if code != 0:
        raise OSError( 'Overlay::__make_working_tree(): error {} - {}'.format( code, out ) )

      if self.tree:
        shutil.rmtree( self.root.as_posix() )

  def __make_working_tree(self, clean_after_reboot=True):

    self.root = Path( self.short_storage if clean_after_reboot else self.long_storage )
    self.tree = dict()

    self.root.mkdir( parents=True, exist_ok=True )

    code, out = x( 'mount -n -t tmpfs tmpfs {}'.format( self.root ), Return.codeout )

    if code != 0:
      raise OSError( 'Overlay::__make_working_tree(): error {} - {}'.format( code, out ) )

    self.working_root = self.root / '.working'
    self.working_root.mkdir( parents=True, exist_ok=True )

    for _d in self.__available_dirs():

      d  = self.root / _d.lstrip('/')
      wd = self.working_root / _d.lstrip('/')

      if 'root' in _d:
        d.mkdir( 0o750, parents=True, exist_ok=True )
      else:
        d.mkdir( 0o755, parents=True, exist_ok=True )

      wd.mkdir( parents=True, exist_ok=True )

      self.tree[ _d ] = d

  def enable(self):

    self.__make_working_tree()

    if self.tree == None:
      raise OSError( 'Overlay::enable(): make_working_tree() may not have been called.' )

    overlayed = self.__overleyed_dirs()

    for readonly_dir,overlay_dir in self.tree.items():

      if overlay_dir in overlayed:
        continue

      cmd = 'mount -n -t overlay -o upperdir={},lowerdir={},workdir={} none {}'
      cmd = cmd.format( overlay_dir, readonly_dir, self.working_root/readonly_dir.lstrip('/'), readonly_dir )

      code, out = x( cmd, Return.codeout )

      if code != 0:
        raise OSError( 'Overlay::enable(): error {} - {}'.format( code, out ) )

  def disable(self, clear_storage=False):
    """ NOT WORK"""

    for d in self.__overleyed_dirs():

      cmd       = 'umount -t overlay ' + d
      code, out = x( cmd, Return.codeout )

      if code != 0:
        raise OSError( 'Overlay::disable(): error {} - {}'.format( code, out ) )

    if clear_storage:
      self.__remove_working_tree()

  @staticmethod
  def __str__() -> str:

    short_storage = Path( Overlay.short_storage )
    long_storage  = Path( Overlay.long_storage )

    if short_storage.exists():
      storage = short_storage
    elif long_storage.exists():
      storage = long_storage
    else:
      storage = None

    if storage != None:

      code, out = x( 'tree {}'.format( storage ), Return.codeout )

      if code != 0:
        raise OSError( 'Overlay::__str__(): error {} - {}'.format( code, out ) )

      return out

    return ''

  @staticmethod
  def __repr__() -> str:
    return Overlay.__str__()

class Action(object):

  def __init__(self, desc:str, func:Callable=None, checked=False, required:Tuple[Callable]=(), root=None, level=0 ):

    self.__root   = root
    self.level    = level
    self.desc     = desc
    self.checked  = checked
    self.catalog  = False
    self.complete = False
    self.symbol   = ('□','▣')

    if func == None:
      self.catalog = True
      self.symbol  = ('▽','▼')
    else:
      self.__func  = func

    self.__actions  = []
    self.__required = []

    if self.__root is not None:
      for func in required:
        self.require( func )

  def __iter__(self, ignore_unchecked_catalog=False):
    """ Left-hand traversal of the action tree. """

    stack   = deque()
    current = self
    stack.append( current )

    prev = deque( [self] )

    while len(prev) > 0:

      curr = prev.popleft()

      if ignore_unchecked_catalog and curr.catalog and not curr.checked:
        pass
      else:
        for act in curr.__actions[::-1]:
          prev.appendleft(act)

      yield curr

  def __str__(self) -> str:
      return '{:65} {:1} {:1} {:1} {} {}'.format( self.desc, self.checked, self.catalog, bool(self.__root), self.__actions, self.__required )

  def __repr__(self) -> str:
    return self.desc if self.catalog else self.__func.__name__

  def __len__(self) -> int:

    length = 0
    for _ in self:
       length += 1

    return length

  def get_actions_count(self):
    return len( self.__actions )

  def get_ignore_iterator(self, ignore_unchecked_catalog=False):
    return self.__iter__(ignore_unchecked_catalog)

  def add(self, desc:str, func:Callable=None, checked=False, required:Tuple[Callable]=()):

    self.__actions.append( Action(desc, func, checked, required, self, self.level + 1 ) )

    return self

  def last(self):
    return self.__actions[-1]

  def get_required_actions(self):
    return self.__required

  def get_depended_actions(self):
    depended = []

    for act in self.__root: # see self.__iter__
      if self in act.__required:
        depended.append( act )

    return depended

  def get_action(self, func:Callable):
    for act in self.__root:
      if not act.catalog:
        if act.__func == func:
          return act

  def execute(self, __exit):
    self.__func( __exit )

  def get_catalog(self, desc:str):
    for act in self.__root:
      if act.catalog:
        if desc in act.desc:
          return act

  def require(self, func:Callable):

    action = self.get_action( func )

    if action:
      self.__required.append( action )
    else:
      print( 'require(): action with {} function not found!'.format( func.__name__ ) )

  def check_required(self):
    for act in self.__required:
      if not act.checked:
        return False
    return True

  def pop_func(self, func:Callable):
    for act in self.__root or self:
      for idx,node in enumerate( act.__actions ):
        if not node.catalog and node.__func == func:
          act.__actions.pop( idx )
          return

  def pop_desc(self, desc:str):
    for act in self.__root or self:
      for idx,node in enumerate( act.__actions ):
        if node.desc == desc:
          act.__actions.pop( idx )
          return

class Actions(object):

  welcome = \
  """
  HelperScripts: вспомогательная утилита для подготовки образа к разработке.
  Отметьте нужные для выполнения пункты. Если не выбран ни один пункт, будет
  произведен корректный выход.
  Управление:
      Перемещение: {} Пометить\снять: {} Продолжить: {}
  ═══════════════════════════════════════════════════════════════════════════
  """.format( colored( ' ↑ ↓ ', 16, 244 ), colored( ' ↩ ', 16, 244 ), colored( ' Shift + ↩ ', 16, 244 ) )

  __CLEAR_TO_END  = '\x1b[0J'
  __SAVE_CURSOR   = '\x1b[s'
  __LOAD_CURSOR   = '\x1b[u'
  __HIDE_CURSOR   = '\x1b[?25l'
  __SHOW_CURSOR   = '\x1b[?25h'

  def __init__(self):

    self.__root_action  = None # actions tree

    self.__painted      = -1
    self.__selected     =  0
    self.__all          = -1
    self.__Exit         = Event()

    self.__stream       = sys.stdout
    self.__lock         = RLock()
    self.__cursor_saved = False

  def start(self):

    print( self.welcome )
    self.__save_cursor()

    keyboard.add_hotkey('up', self.__move_up, suppress=True )
    keyboard.add_hotkey('down', self.__move_down, suppress=True )
    keyboard.add_hotkey('enter', self.__toggle, suppress=False, timeout=2 )

    self.__disable_stdin_echo()
    self.__paint()

    keyboard.wait( 'shift+enter', suppress=False )
    keyboard.unhook_all_hotkeys()

    self.__run()

  def __run(self):

    print( '\n  Запуск выбранных задач...\n' )

    for idx,act in enumerate( self.__root_action ):

      if idx == 0 or act.catalog:
        continue

      self.__enable_stdin_echo()

      if self.__Exit.is_set():
        break

      if act.checked == True:
        print( '\n{:═<{width}}\n'.format( '═══ ' + act.desc + ' ', width=shutil.get_terminal_size().columns ) )

        with self.__lock:
          act.execute( self.__Exit )

      self.__disable_stdin_echo()

    time.sleep(1)
    self.__enable_stdin_echo()

  def __paint(self):

    with self.__lock:

      self.__load_cursor()

      self.__painted = 0

      idx = 0
      last_action = deque()

      for idx,act in enumerate( self.__root_action.get_ignore_iterator(True) ):
        if idx == 0:
          continue

        if act.checked:
          status = act.symbol[1]
        else:
          status = act.symbol[0]

        if act.catalog and act.checked:
          last_action.append( act.last() )

        # ~ tree art ~

        if len( last_action ) and act is last_action[0]:
          space = self.indent( act.level, size=3 )[:-2] + '└─'
          last_action.popleft()

        elif act.level > 1:
          space = self.indent( act.level, size=3 )[:-2] + '├─'
        else:
          space = self.indent( act.level, size=3 )

        # ~~~~~~~~~~~~

        line = '{space} {indicator} {text}'.format \
          (
            space=space,
            indicator=status,
            text=colored( act.desc, 16, 244 ) if idx == self.__selected else act.desc
          )

        print( '{: <{width}}'.format( line, width=shutil.get_terminal_size().columns ) )
        self.__painted += 1

      # fill
      for _ in range( self.__all - idx ):
        print( '{: <{width}}'.format( '', width=shutil.get_terminal_size().columns ) )

  def __toggle(self):

    if self.__selected >= 1 and self.__selected <= self.__painted:

      for idx,act in enumerate( self.__root_action.get_ignore_iterator(True) ):
        if idx == self.__selected:

          if act.checked:
            self.__uncheck_all( act )

            for req in act.get_depended_actions():
              req.checked = False

          else:
            for req in act.get_required_actions():
              req.checked = True
            act.checked = True


      self.__paint()

  def __move_up(self):

    if self.__selected > 1:
      self.__selected -= 1
    else:
      self.__selected = self.__painted

    self.__paint()

  def __move_down(self):

    if self.__selected < self.__painted :
      self.__selected += 1
    else:
      self.__selected = 1

    self.__paint()

  def __save_cursor(self):

    with self.__lock:
      if not self.__cursor_saved:
        self.__stream.write( self.__SAVE_CURSOR )
        self.__cursor_saved = True

  def __load_cursor(self):

    with  self.__lock:
      if self.__cursor_saved:
        self.__stream.write( self.__LOAD_CURSOR )
        self.__stream.flush()

  def __clear_to_end(self):

    with self.__lock:
      self.__stream.write( '\x1b[K' )
      self.__stream.write( self.__CLEAR_TO_END )
      self.__stream.flush()

  def __disable_stdin_echo(self):

    fd                = sys.stdin.fileno()
    self.old_settings = termios.tcgetattr( fd )
    new_settings      = termios.tcgetattr( fd )
    new_settings[3]   = new_settings[3] & ~ termios.ECHO

    termios.tcsetattr( fd, termios.TCSADRAIN, new_settings )

    self.__stream.write( self.__HIDE_CURSOR )
    self.__stream.flush()

  def __enable_stdin_echo(self):

    fd = sys.stdin.fileno()
    termios.tcflush( fd, termios.TCIOFLUSH )
    termios.tcsetattr( fd, termios.TCSADRAIN, self.old_settings )

    self.__stream.write( self.__SHOW_CURSOR )
    self.__stream.flush()

  def __uncheck_all(self, node:Action):
    for act in node:
      act.checked = False

  def add_actions(self, action_tree:List):
    """ Exampleaction_tree:

      [ "a1" , (f1, False),
        "a2" , (f2, False),
        "a3" , [ "a4" , (f4, False),
                 "a5" , (f5, False, f4)
               ], True
      ]
    -----------------------------------------
    a3   - catalog
    a5   - action
    "a5" - action description
    f5   - function connected to action "a4"
    false\True - checked status
    [ f5, false, ...(required functions)]
    """

    assert type(action_tree) is list
    assert type(action_tree[0]) is str
    assert type(action_tree[1]) is list or type(action_tree[1]) is tuple

    if not self.__root_action:
      self.__root_action = Action( 'root_action', checked=True )

      stack = deque()
      stack.append( [action_tree, self.__root_action] )

      while len( stack ) > 0:
        node_in, node_out = stack.popleft()

        while len( node_in ) > 0:
          desc = node_in.pop(0)
          val  = node_in.pop(0)

          if type(val) is tuple:
            assert len(val) >= 2
            node_out.add( desc, *val[:2], required=val[2:] )
            self.__all += 1

          elif type(val) is list:
            checked = node_in.pop(0)
            assert type(checked) is bool, 'Action::add_actions(): wrong tree format!'

            node_out.add( desc, checked=checked )
            self.__all += 1
            stack.append( [val, node_out.last()] )

      #self.__paint()

  def pop_func(self, func:Callable):
    self.__root_action.pop_func( func )

  def pop_desc(self, desc:str):
    self.__root_action.pop_desc( desc )

  def get_root_action(self):
    return self.__root_action

  def indent(self, level=1, symbol=' ', startwith='', size=2):
    return '{start:}{:{smb}<{width}}'.format( '', start=startwith, smb=symbol, width=size * level)

class ASCIIColor:
  begin = 0
  end = 0
  prefix = ''
  suffix = '\x1b[0m'
  raw = ''

  # color code: https://regex101.com/r/wUBkGY/1
  __reg = '(?!\\x1b\[0m)\\x1b\[(?:.*?)m'
  __reg_all = '\\x1b\[(?:.*?)m'

  def __init__(self, line:str):
    self.raw = line
    found = list(re.finditer(self.__reg, line, re.IGNORECASE))
    if found:
      self.begin = found[0].start()
      self.prefix = ''.join( [r.group() for r in found] )

    self.end = line.rfind('\x1b[0m')
    if self.end < 0:
      self.suffix = ''

  def cleaned(self):
    """without ascii codes"""
    return self.clean(self.raw)

  @staticmethod
  def walk(line:str):
    found = re.finditer(ASCIIColor.__reg_all, line, re.IGNORECASE)
    for p in found:
      yield p

  @staticmethod
  def clean(line:str, replace_by=''):
    found = re.finditer(ASCIIColor.__reg, line, re.IGNORECASE)
    for p in found:
      line = line.replace(p.group(), replace_by)
    line = line.replace(ASCIIColor.suffix, replace_by)
    return line

  def __len__(self):
    return len(self.prefix) + len(self.suffix)

class StreamTable:
  """This class provides the ability to print an ASCII table cell by cell.
     In theory, this can be used for logging.

     Example:
      t = StreamTable(30)
      t.split()
      t.cell( 'welcome!', '^' )
      t.split( 15 )
      t.cell( 'hello,', '>'), t.cell( 'Jack!', '<')
      t.split(end=True)
      --------------------------------
      ╔══════════════════════════════╗
      ║           welcome!           ║
      ╟──────────────┬───────────────╢
      ║       hello, │ Jack!         ║
      ╚══════════════╧═══════════════╝
  """

  def __init__( self, width = shutil.get_terminal_size().columns, stream=sys.stdout ):

    self.width       = width -2 # why? need investigation
    self.stream      = stream
    self.abc         = '╔╤╗╟┬╢╚╧╝║│┴┼'
    self.ascii_point = '[^]'

    self.history  = []

    # volatile
    self.__text_stack       = []
    self.__align_stack      = []
    self.__ascii_stack      = []
    self.__splitters        = []

    self.__incomplete_cell = 0

  def cell( self, text:str, align='^' or '<' or '>'):

    if self.__incomplete_cell:
      self.__text_stack.append( text )
      self.__align_stack.append( align )
      self.__incomplete_cell -= 1

    if not self.__incomplete_cell:
      self.__paint( self.__make_columns( self.__text_stack, self.__align_stack, self.__splitters ) )

      self.__text_stack.clear()
      self.__align_stack.clear()

    return self

  def line( self, text=' ', align='<' or '^' or '>'):
    text = str(text)
    text = text.replace( '\t', ' ' ) or ' '
    self.__paint( self.__make_columns( [text], [align], [] ), force=True )

    return self


  def split( self, *args, end=False ):

    for arg in args:
      assert type(arg) == int, 'split(): {}(type:{}) is not int!'.format( arg, type(arg) )

    head  = len( self.history ) == 0

    if head:
      pattern       = '╔{:═^{width}}╗'.format( '', width=self.width )
      pattern_split = '╤'
    elif end:
      pattern       = '╚{:═^{width}}╝\n'.format( '', width=self.width )
      pattern_split = '╧'
    else:
      pattern       = '╟{:─^{width}}╢'.format( '', width=self.width )
      pattern_split = '┴'

    self.__splitters       = sorted( set( args ) )
    self.__incomplete_cell = 1 + len( self.__splitters )

    if self.__splitter_range_check():

      pattern = self.__set_splitters(  pattern_split , pattern )

      self.__paint( pattern, True )

      return self

    else:
      print( 'paint_split(): wrong splitter position!' )

  def __paint( self, text:str, force=False ):
    if not self.__incomplete_cell or force:
      self.history.append( text or ' ' )
      self.stream.write( text + '\n' )

  def __get_splitters( self ):

    splitters = []

    if len( self.history ) > 0:

      i      = 0
      escape = False

      for ch in self.history[-1].splitlines()[-1]:

        if escape:
          if ch == 'm':
            escape = False

        else:

          if ch == '\x1b':
            escape = True

          else:

            if ch in '│╤┬':
              splitters.append( i )
            i += 1

    return splitters

  def __splitter_range_check( self ) -> bool:
    for split in self.__splitters:
      if split > self.width -2:
        return False
    return True

  def __extract_ascii_codes( self, line:str ):
    for p in ASCIIColor.walk(line):
      self.__ascii_stack.append(p.group())
    return ASCIIColor.clean(line, replace_by=self.ascii_point)

  def __wrap_lines( self, text:str, width:int, align='<' or '^' or '>' ) -> str:

    lines      = []
    max_length = (width -3)

    for line in text.splitlines():
      line = self.__extract_ascii_codes(line)
      while len(line) > 0:
        lines.append(line[:max_length])
        line = line[max_length:]

    wrapped_line = ''
    additional_len = 0
    for line in lines:
      additional_len = len(self.ascii_point) * line.count(self.ascii_point)
      wrapped_line += '  {: {align}{width}} \n'.format(
        line.lstrip(),
        align=align,
        width=max_length + additional_len
      )

    return wrapped_line

  def __align_fragments( self, wrapped_fragments:List[str] ):

    max_rows          = len( max( wrapped_fragments, key=lambda x: len(x) ) )
    aligned_fragments = []

    for f in wrapped_fragments:

      fragment_rows  = len(f)
      max_row_length = len( max( f, key=lambda x: len(x) ) )

      f.extend( [' ' * max_row_length] * (max_rows - fragment_rows) )
      aligned_fragments.append( f )

    return aligned_fragments

  def __framing( self, aligned_fragments:List[str]):

    result_line = ''
    buff        = ''

    while len(aligned_fragments) > 0:
      for i,f in enumerate( aligned_fragments ):

        if len(f) > 0:
          fragment = f.pop(0)
          while self.ascii_point in fragment:
            fragment = fragment.replace(self.ascii_point, self.__ascii_stack.pop(0), 1)

          if len( aligned_fragments ) == 1:
            buff += '║' + fragment[1:] + '║'

          elif i == 0:
            buff += '║' + fragment[1:]

          elif i == len(aligned_fragments)-1:
            buff += '│' + fragment[1:] + '║'

          else:
            buff += '│' + fragment[1:]

        else:
          aligned_fragments.pop( i )

      result_line +=  buff + '\n'
      buff = ''

    return result_line.rstrip()

  def __make_columns( self, fragments:List[str], align:List[str], splitters:List[int]  ):
    """ SUPER SLOW! """

    wrapped_fragments = []
    place             = 0
    place_index       = 0
    columns           = 1 + len(splitters)

    assert len(fragments) == columns == len(align), '{} == {} == {}'.format( len(fragments), columns, len(align) )

    splitters.sort()

    # fragment formation

    buff = splitters

    for i,f in enumerate( fragments ):
      if len(buff) > 0:
        place_index = buff.pop(0)
        place       = place_index - place
        wrapped_fragments.append( self.__wrap_lines( f, place, align[i] ).splitlines() )

    wrapped_fragments.append( self.__wrap_lines( fragments[-1], self.width - place_index+1, align[-1] ).splitlines() )

    # fragment alignment
    aligned_fragments = self.__align_fragments( wrapped_fragments )

    # printing fragments and separators
    framed = self.__framing( aligned_fragments )

    return framed

  def __set_splitters( self, split_char:str, text:str ):

    old_split_set = self.__get_splitters()
    new_split_set = self.__splitters
    buff          = ''

    i      = 0
    escape = False

    for ch in text:

      if escape:
        if ch == 'm':    # like as '\x1b[0m'
          escape = False

      else:

        if ch == '\x1b':
          escape = True

        else:

          if i in old_split_set and i in new_split_set:
            buff += '┼'

          elif i in new_split_set and split_char == '┴':
            buff += '┬'

          elif i in old_split_set or i in new_split_set:
            buff += split_char

          else:
            buff += ch

          i += 1

    return buff

  def set_cur( self, x:int, y:int ):
    cmd = '\x1b[{};{}R'.format( y, x )
    self.stream.write( cmd )

  def move_cur( self, x=0, y=0 ):
    cmd = ''

    if x > 0:
      cmd += '\x1b[{x}C '
    elif x < 0:
      cmd += '\x1b[{x}D '

    if y > 0:
      cmd += '\x1b[{y}B'
    elif y < 0:
      cmd += '\x1b[{y}A'

    cmd = cmd.format( y=abs(y), x=abs(x) )
    self.stream.write( cmd )

class Walker:
  """ This class allows you to go through all the files in the specified folders or count their number.
      For best performance, Walker uses its own cache to truncate stat() system calls
  """

  def __init__(self, include:Tuple[str], exclude:Tuple[str]=(), ignore_dirs=False) -> None:
    self.__included = include
    self.__excluded = exclude
    self.__ignore_dirs = ignore_dirs
    self.__stat_cache = dict()
    self.__count = 0

  def count(self, dirs_only=False):
    for include in self.__included:
      self.__count = 0
      for entry in self.__walk(include):
        if dirs_only and not stat.S_ISDIR(self.__stat_cache[entry].st_mode):
          continue
        if entry.path.startswith(self.__excluded):
          continue
        self.__count += 1
      yield include, self.__count

  def __walk(self, path:str):
    for entry in os.scandir(path):
      try:
        st = entry.stat(follow_symlinks=False)
        self.__stat_cache[entry] = st
        if stat.S_ISDIR(st.st_mode):
          yield from self.__walk(entry.path)
      except (PermissionError, FileNotFoundError):
        continue
      yield entry

  def __iter__(self):
    self.__stat_cache.clear()

    for include in self.__included:
      for entry in self.__walk(include):
        if stat.S_ISDIR(self.__stat_cache[entry].st_mode) and self.__ignore_dirs:
          continue
        if entry.path.startswith(self.__excluded):
          continue
        res = Path(entry)
        res.__cached_stat = self.__stat_cache[entry]
        res.stat = res.lstat = lambda : res.__cached_stat
        yield res

  def __next__(self):
    return next(self)

class CountDown:

  def __init__(self, start:float, delay=1.0):
    self.timeout = start
    self.timeleft = start
    self.elapsed = 0
    self.delay = delay

  def __iter__(self):
    while self.timeleft > 0:
      time.sleep(self.delay)
      self.elapsed += 1
      self.timeleft = self.timeout - self.elapsed
      yield self.timeleft
    else:
      return 0

  def __next__(self):
    return next(self)

class CtrlC:
  interrupted = False
  def __enter__(self):
    return self.interrupted
  def __exit__(self, clss, value, trace):
    self.interrupted = clss is not None and issubclass(clss, KeyboardInterrupt)
    return self.interrupted

class XDisplay:
  """This class allows you to disable x-server display access control.

    Example:
      with XDisplay.unlocked_display('root'):
        XDisplay.info()
        # some code
      XDisplay.info()
  """

  from Xlib.display import Display, request, rq
  from Xlib.X import EnableAccess, DisableAccess, HostInsert, HostDelete
  from contextlib import contextmanager

  # библиотека Xlib не дает использовать Family == ServerInterpreter == 5
  request.ChangeHosts._request = rq.Struct(
    rq.Opcode(109),
    rq.Set('mode', 1, (HostInsert, HostDelete)),
    rq.RequestLength(),
    rq.Set('host_family', 1, (0, 1, 2, 5)),
    rq.Pad(1),
    rq.LengthOf('host', 2),
    rq.List('host', rq.Card8Obj)
  )

  @staticmethod
  def unlock_display(display=None):
    """Disables display access control.

      Parameters:
        display (str) - specify a display, for example ":0"
    """
    display = XDisplay.Display(display)
    display.set_access_control(XDisplay.DisableAccess)
    display.flush() # нужно вызывать перед каждым запросом получающим информацию

  @staticmethod
  def lock_display(display=None):
    """Enables display access control.

      Parameters:
        display (str) - specify a display, for example ":0"
    """
    display = XDisplay.Display(display)
    display.set_access_control(XDisplay.EnableAccess)
    display.flush()

  @staticmethod
  def add_host(localuser:str, display=None):
    """Add user to list of users who are allowed to access display.

      Parameters:
        localuser (str) - user name;
        display   (str) - specify a display, for example ":0"
    """
    name = 'localuser\0{}'.format(localuser)
    name = [ord(c) for c in name]
    display = XDisplay.Display(display)
    display.change_hosts(XDisplay.HostInsert, 5, name)
    display.flush()

  @staticmethod
  def del_host(localuser:str, display=None):
    """Remove user from list of users who are allowed to access display.

      Parameters:
        localuser (str) - user name;
        display   (str) - specify a display, for example ":0"
    """
    name = 'localuser\0{}'.format(localuser)
    name = [ord(c) for c in name]
    display = XDisplay.Display(display)
    display.change_hosts(XDisplay.HostDelete, 5, name)
    display.flush()

  @staticmethod
  def info(display=None):
    """Print display information including security status information.

      Parameters:
        display (str) - specify a display, for example ":0"
    """
    display = XDisplay.Display(display)
    if not display.has_extension('SECURITY'):
      if display.query_extension('SECURITY') is None:
        print('SECURITY extension not supported')
    else:
      security_version = display.security_query_version()
      print('SECURITY version {}.{}'.format(
        security_version.major_version,
        security_version.minor_version
      ))
    data = display.list_hosts()
    print('AccessListEnabled: {:}'.format(data._data['mode']))
    display.flush()
    print('Hosts:')
    for host in data._data['hosts']:
      name = ''.join( [chr(c) for c in host['name']] ).replace('\0', '\\0')
      print('  {}(family:{})'.format(name, host['family']))

  @contextmanager
  def unlocked_display(localuser='', display=None):
    """Grant the specified user access to the display during the execution of \
      the with-statement block.

      Parameters:
        localuser (str) - user name;
        display   (str) - specify a display, for example ":0"
    """
    _display = XDisplay.Display(display)
    if localuser:
      XDisplay.add_host(localuser, display)
    else:
      _display.set_access_control(XDisplay.DisableAccess)
      _display.flush()
    yield

    if localuser:
      XDisplay.del_host(localuser, display)
    else:
      _display.set_access_control(XDisplay.EnableAccess)
      _display.flush()

class BackupDB():
  """The class allows you to quickly create backups of files.
  Backups are stored in json with the following structure:
  {
    /path/to/file.any = [ timestamp, "binary data"],
  }

  Example:
    bk = BackupDB()
    with bk('accessibility.conf'):
      # do something with conf
    # сonf will recover if an error occurs

    bk.restore('accessibility.conf')
  """
  transaction = None

  def __init__(self, db_path: Path = Path(__file__).absolute().parent/Path("backups.json")):
    """ Database initialization. Create base file.json if it doesn't exist.

      Parameters:
        db_path (str or pathlib.Path) - database file path
    """
    db_path = Path(db_path)
    self.db_path = db_path.absolute()
    if not self.db_path.exists():
      self.db_path.parent.mkdir(0o664, parents=True, exist_ok=True)
      with self.db_path.open('w') as f:
        json.dump({}, f, indent=2)

    with self.db_path.open('r') as f:
      self.backups = json.load(f)

  def add(self, path:Path, backup_exist_ok=False, missing_ok=False):
    """Add file to backup database.

      Parameters:
        path            (str or pathlib.Path) - path to the file to be backed up;
        backup_exist_ok (bool) - ignore the fact that the file is already backed up;
        missing_ok      (bool) - ignore the fact that the specified file does not exist

      Exceptions:
        IsADirectoryError
        FileExistsError
        ValueError

    """
    path = Path(path)
    path = path.absolute()
    if path.is_dir():
      raise IsADirectoryError('{} is a directory!'.format(path))

    if self.backups is not None:
      if path.exists():
        if path.as_posix() not in self.backups:
          with path.open('r') as f:
            mtime = path.stat().st_mtime
            mtime = datetime.datetime.fromtimestamp(mtime)
            self.backups[path.as_posix()] = [mtime.strftime(r'%d.%m.%Y %H:%M:%S'), f.read()]
            self.dump()
        elif not backup_exist_ok:
          raise FileExistsError('{} is already in the backup database!'.format(path))
      elif not missing_ok:
        raise FileNotFoundError('{} not found!'.format(path))
    else:
      raise ValueError('self.backups == None')

    return self

  def dump(self):
    """Saves changes to the backup database to disk.
    """
    if self.backups is not None:
      with self.db_path.open('w') as f:
        json.dump(self.backups, f, indent=2)
        f.flush()

  def restore(self, path:Path, missing_ok=False):
    """Recovers a file from the backup database.

      Parameters:
        path       (str or pathlib.Path) - path to the file that was backed up;
        missing_ok (bool) - ignore the fact that the file was not found in the backup database

      Exceptions:
        ValueError
        FileNotFoundError
    """
    path = Path(path)
    path = path.absolute()
    if self.backups is not None:
      if path.exists():
        if path.as_posix() in self.backups:
          old_date, data = self.backups[path.as_posix()]
          old_date = datetime.datetime.strptime(old_date, r"%d.%m.%Y %H:%M:%S")
          cur_date = datetime.datetime.fromtimestamp(path.stat().st_mtime)
          if old_date < cur_date:
            with path.open('w') as f:
              f.write(data)
              f.flush()
            del self.backups[path.as_posix()]
            self.dump()

          else: raise ValueError('Date error: "{old_date}" >= "{cur_date}"!'.format(old_date, cur_date))
        elif not missing_ok: raise ValueError('{} not found in backups!'.format(path))
      elif not missing_ok: raise FileNotFoundError('{} not found!'.format(path))
    else: raise ValueError('self.backups == None')

  def has(self, path:Path) -> bool:
    """Returns True if the file was found in the backup database.

      Parameters:
        path (str or pathlib.Path) - path to the file that was backed up

      Returns:
        (bool)
    """
    path = Path(path)
    path = path.absolute()
    if self.backups is not None:
      return path.as_posix() in self.backups

  def substitute(self, by: Path, target: Path = ''):
    """Substitutes the contents of the specified file in place of the contents of target.

      Parameters:
        by     (str or pathlib.Path) - place to get content from;
        target (str or pathlib.Path) - place to put the content, if not specified,
                                       content will be taken from the self.transaction
    """
    target = Path(target) if target != '' else None
    by = Path(by)
    if by.exists():
      if self.transaction:
        with by.open('rb') as b:
          with self.transaction.open('wb') as a:
            a.write(b.read())
            a.flush()
      elif target:
        with by.open('rb') as b:
          with target.open('wb') as a:
            a.write(b.read())
            a.flush()

  def __call__(self, path:Path, missing_ok=False, restore_before=False, restore_after=False):
    """Additional parameters when using with-statement.

      Parameters:
        path           (str or pathlib.Path) - path to the file to be backed up;
        missing_ok     (bool) - ignore the fact that the specified file does not exist;
        restore_before (bool) - restore the specified file before the start of
                                the with-statement block;
        restore_after  (bool) - restore the specified file after the with-statement block

      Returns:
        (self)
    """
    path = Path(path)
    self.transaction = path.absolute()
    self.missing_ok = missing_ok
    self.restore_before = restore_before
    self.restore_after = restore_after
    return self

  def __enter__(self):
    if self.restore_before:
      self.restore(self.transaction, missing_ok=True)
    self.add(self.transaction, missing_ok=self.missing_ok)
    return self

  def __exit__(self, type, value, traceback):
    if type != None or self.restore_after:
      self.restore(self.transaction)
      #print('{} restored!'.format(self.transaction), flush=True)
    else:
      self.dump()

def x(command, return_type=Return.errcode, ignore_output=False, log_and_lock={'log': [], 'Lock': None}, pause=0, timeout=0):
  """ Python shell runner. This is a "simple" wrapper.
      log_and_lock = { [ [errcode, output], ...], multiprocessing.Lock() }

      example: print( x('echo 123', return_type=Return.stdout) )

      if return_type == Return.codeout then x returns tuple(errcode,stdout\stderr )
      if return_type == Return.stdout  then x returns str
      if return_type == Return.errcode then x returns int
      if return_type == Return.process then x returns object of subrocess.Popen(...)
      if return_type == Return.ignore  then x returns 0

      pause         (float) - make pause before executing, in seconds.
      timeout       (float) - seconds. if timed out, return errcode(1). default: not limited.
      ignore_output (bool)  - adds suffix '> /dev/null' to command.
  """

  suffix    = ''
  waskilled = False
  output    = tempfile.SpooledTemporaryFile( mode='w+', encoding='UTF-8' )

  LnL = log_and_lock if type( log_and_lock['Lock'] ) is classLock else None

  if not command:
    print( 'x: command is empty' )
    return 1

  if ignore_output or return_type == Return.errcode:
    suffix = ' > /dev/null'

  if pause > 0:
    time.sleep(pause)

  process = Popen( command + suffix, shell=True, universal_newlines=True, stdout=output, stderr=STDOUT )
  process.stdout = output

  if timeout > 0:
    with Ticker( timeout ) as timer:
      while timer.tick() and process.poll() == None:
        sleep(0.2)

    if not timer.tick() and process.poll() == None:
      process.kill()
      process.returncode = 62 # ETIME Timer expired
      waskilled = True
  else:
    process.wait()

  process.stdout.seek(0)
  retcode = process.returncode

  if return_type != Return.errcode:
    if not waskilled:
      retout = process.stdout.read()
      process.stdout.seek(0)
      if LnL:
        retout = retout.replace('\n', ';') # if writing to log
    elif return_type == Return.stdout:
      retout = 'Timed out or stdout overloaded!'
    else:
      retout = 'Process was killed'


  if return_type == Return.stdout:
    if LnL:
      with LnL['Lock']:
        LnL['log'].append( [ retcode, retout ] )
    return retout

  if return_type == Return.codeout:
    if LnL:
      with LnL['Lock']:
        LnL['log'].append( [ retcode, retout ] )
    return retcode, retout

  elif return_type == Return.errcode:
    if LnL:
      with LnL['Lock']:
        LnL['log'].append( [ retcode, 'Command: ' + command ] )
    return process.returncode

  elif return_type == Return.process:
    if LnL:
      with LnL['Lock']:
        LnL['log'].append( [ retcode, retout ] )
    return process

  elif return_type == Return.ignore:
    if LnL:
      with LnL['Lock']:
        LnL['log'].append( [ OK, '[ignore errors] {}'.format( retout ) ] )
    return OK

def fileBackup(uri='', restore=False, replacement=''):

  backup = uri + '.bkp'
  path   = ''
  name   = ''

  if restore == False and not os.path.exists( uri ):
    raise Exception('There is nothing to back up. File {} not found!'.format( uri ))

  if replacement and os.path.exists( replacement ):
    path = os.path.split( uri )[0]
    name = os.path.split( replacement )[1]

  elif replacement and not os.path.exists( replacement ):
    raise Exception('Replacement does not exist. File {} not found!'.format( uri ))

  # "some_magic.was".[::-1].replace('was.','',1)[::-1]

  if restore:

    if os.path.exists ( backup ):

      if os.path.exists( uri ):
        os.remove( uri )

      os.rename( backup, uri )

    else:
      raise Exception('Backup of "{}" not found!'.format( backup ))

  else:

    if os.path.exists ( uri ):

      if os.path.exists( backup ):
        raise Exception('Backup file "{}" already exists!'.format( backup ))
      else:
        os.rename( uri, backup )

        if replacement:
          shutil.copy2( replacement, uri)

    else:
      raise Exception('File to save "{}" not found!'.format( uri ) )

  pass

def waitCPU(pid = -1, cpuload = 10, verbose=False, silent=False, stream=sys.stdout, end_pause = 1.0):
  """
    end_pause - seconds
  """

  import functools

  CURSOR_UP     = '\x1b[1A'
  ERASE_LINE    = '\x1b[K'
  SAVE_CURSOR   = '\x1b[s'
  LOAD_CURSOR   = '\x1b[u'
  CLEAR_TO_END  = '\x1b[0J'

  stream.write( SAVE_CURSOR )
  stream.write( '\n' )

  if not silent:
    if pid == 0:
        print( 'waitCPU(): Process with PID:{} not found!'.format( pid ), file=stream )
        return
    elif pid < 0:
        print( 'waitCPU(): Режим общей загрузки ЦП! Ожидание пока загрузка ЦП не станет < {}%.'.format( cpuload ), file=stream )

  p = psutil.Process( os.getpid() if pid < 0 else pid)

  if verbose and not silent:
      print( 'NOW: pid = {}, status = {}, user = {}'.format( p.pid, p.status(), p.username() ), file=stream )
      print( 'CMD line = {}'.format( p.cmdline() ), file=stream )
      print( 'RAM load = {} \n'.format( p.memory_percent() ), file=stream )

  if pid > 0:
    get_cpu_load = functools.partial( p.cpu_percent, 0.1 )
  else:
    get_cpu_load = functools.partial( psutil.cpu_percent, 0.1 )

  load = 100.0
  while ( True ):
    load = get_cpu_load()
    if not silent:
      print( 'Загрузка ЦП = {}%, ожидание...'.format( load ), end='\r', file=stream)

    if ( load < cpuload ):
      break

  if not silent:
    print( 'Загрузка ЦП = {}%, продолжение...'.format( load ), end='', file=stream, flush=True)
    time.sleep(1)

  stream.write( ERASE_LINE )
  stream.write( LOAD_CURSOR )
  stream.write( CLEAR_TO_END )
  stream.flush()
  time.sleep( end_pause )

  pass

def waitProcess(pid=-1, name='', stream=sys.stdout, end_pause=1.0, timeout=10, kill=False):
  """
    end_pause - seconds
    timeout   - seconds
    kill      - kill process after timeout

    Check if process with pid or name exists.

    return 1 - process exceeded timeout
    return 2 - process killed by timeout
    return 3 - process not found
    return 0 - process terminate self
  """

  indicator = [ '[   ]','[.  ]', '[.. ]', '[...]', '[ ..]', '[  .]']
  shift = len( indicator )
  i = 0
  process = None
  stopped = False

  t = Ticker( timeout=timeout )

  if stream == None:
    stream = StreamToNull()

  with ConPlace( end_pause=end_pause ) as CP:

    for proc in psutil.process_iter():
      try:
        if name:
          if name.lower() in proc.name().lower():
            process = proc
        else:
          if proc.pid == pid:
            name = str( pid )
            process = proc

      except ( psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess ) as e:
        pass
      else:
        pass

    if process:
      while t.tick():
        try:
          if process.is_running():
            print( ' {} Ожидание завершения процесса {}...'.format( indicator[i], name ), end='\r', file=stream, flush=True )
            i = (i + 1) % shift
            if kill and not t.tick():
              print( ' Процесс {} уничтожен!'.format( process.name() ), end='\r', flush=True, file=stream )
              process.kill()
              return 2
          else:
            CP.do( CP.ERASE_LINE )
            print( 'Процесс завершен.', flush=True, file=stream )
            return 0
        except ( psutil.ZombieProcess ):
          pass # mb late
        time.sleep(0.2)
      else:
        return 1 #timeout
    print( 'Процесс не найден! ', file=stream )
    return 3

def waitForPrinterIdle(check_interval = 0.1, waiting_timeout_sec = 10):
    """
        return True if any printer is busy
    """
    tick = time.time()

    with ConPlace():
      while (True):

        time.sleep(check_interval)
        out = check_output([ 'lpstat', '-o' ])
        tock = time.time()

        print('Waiting for printer..{}/{} sec\r'.format( int (tock-tick), waiting_timeout_sec), end=' ', file=sys.stdout)

        if len(out) <= 0:
            return True
        if tock - tick > waiting_timeout_sec:
            raise ValueError('Printer timed out!')

    # print 'waitForPrinterIdle: lpstat out: {}, return: {}'.format(out, len(out) > 0)


    return False

def backup_mozilla(thunderbird = False):

  time.sleep(5)
  mPath = os.path.dirname(os.path.realpath(__file__))

  if (not thunderbird):
      webDir    = os.path.join(mPath, '..', '..', 'test_data', 'web')
      ConfigNew = os.path.join(webDir, 'config.firefox')
      # nickel: "./mozilla/firefox" RED: "./mozilla"
      Config    = os.path.expanduser('~/.mozilla/firefox')
      ConfigOld = os.path.expanduser('~/.mozilla.bak')
  else:
      webDir    = os.path.join(mPath, '..', '..', 'test_data', 'web')
      ConfigNew = os.path.join(webDir, 'config.thunderbird')
      Config    = os.path.expanduser('~/.thunderbird')
      ConfigOld = os.path.expanduser('~/.thunderbird.bak')

  if not os.path.exists(Config):
      #os.makedirs(Config)
      shutil.copytree(ConfigNew, Config)

  elif not os.path.exists(ConfigOld):
      os.rename(Config, ConfigOld)
      backup_mozilla(thunderbird) # sorry for that

  else:
      shutil.rmtree(Config)
      os.rename(ConfigOld, Config)

def clone_mozilla_profile(delete = False, thunderbird = False):
  """
  return absolute path to test.profile for firefox
  """
  this        = os.path.dirname(os.path.realpath(__file__))

  if not thunderbird:
      profile     = os.path.join(this, '..', '..' 'test_data', 'web', 'config.firefox', 'test.profile')
      destination = os.path.expanduser('~/.mozilla/firefox')
      tmp_profile = os.path.join(destination,'test.profile')
  else:
      profile     = os.path.join(this, '..', '..', 'test_data', 'web', 'config.thunderbird', 'test.profile')
      destination = os.path.expanduser('~/.thunderbird')
      tmp_profile = os.path.join(destination,'test.profile')

  if not os.path.exists(tmp_profile):
      print('creating {}'.format(tmp_profile), file=sys.stdout)
      shutil.copytree(profile, tmp_profile)

  elif os.path.exists(tmp_profile) and not delete:
      print('recreating {}'.format(tmp_profile), file=sys.stdout)
      clone_mozilla_profile(True, thunderbird)
      clone_mozilla_profile(False, thunderbird)

  if delete:
      print('removing {}'.format(tmp_profile), file=sys.stdout)
      shutil.rmtree(tmp_profile)

  return tmp_profile

def usb_device_reset(deviceClass='Printer', sleepPerDevice=1):

    # (?:Bus.(\d+).+Dev.(\d+))(?=[\w\W]+Printer)
    # regwork = re.compile(r'(?:Bus.(\d+).+Dev.(\d+))(?=[\w\W]+{})'.format(deviceClass))

    # i.marochkin@rosalinu.ru
    # lsusb -t | grep 'Bus\|Printer' | tr '\n' 'x' | sed 's/x\//\n/g' | grep 'Printer' | cut -f1 -d',' | sed 's/[^0-9]/ /g'

    cmd = "lsusb -t | grep 'Bus\|Printer' | tr '\\n' 'x' | sed 's/x\//\\n/g' | grep '{}' | cut -f1 -d',' | sed 's/[^0-9]/ /g'".format(deviceClass)
    out = check_output(cmd, shell=True).splitlines()

    devs = []
    for d in out:
        devs.append( [int(i) for i in d.split()] )

    # print devs

    # UNBINDING
    BUS = DEV = err = 0
    for d in devs:
        BUS = d[0]
        DEV = d[2]
        # print >> sys.stderr, 'Printer found on BUS:{} DEV: {}'.format(BUS,DEV)
        # print >> sys.stderr, 'Unbind USB{} & sleep {}'.format(BUS, sleepPerDevice)

        err = call('echo \'{}-{}\' | tee /sys/bus/usb/drivers/usb/unbind'.format(BUS,DEV), stdout=PIPE, shell=True)

        if (err):
            print('Can\'t unbind {}-{}'.format(BUS,DEV), file=sys.stderr)

        time.sleep(sleepPerDevice)

    # BINDING
    for d in devs:
        BUS = d[0]
        DEV = d[2]
        #print >> sys.stderr, 'Rebind USB{} & sleep {}'.format(BUS, sleepPerDevice)

        err = call('echo \'{}-{}\' | tee /sys/bus/usb/drivers/usb/bind'.format(BUS,DEV), stdout=PIPE, shell=True)

        if (err):
            print('Can\'t bind {}-{}'.format(BUS,DEV), file=sys.stderr)

        time.sleep(sleepPerDevice)

    if (err):
        # raise ValueError('Not all ports have been restarted')
        return 1

    return 0

def cls():
    os.system('clear')

def processEx(name, kill=False) -> int:
  """
      process found     - return pid
      process killed    - return 0
      process not found - return -1
      error             - return -2

  """
  for proc in psutil.process_iter():
    try:
      if name.lower() in proc.name().lower():
        if ( kill ):
          proc.kill()
        return proc.pid
    except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
      return -2

  return -1

def whatlibs(Exit=True):

    pid  = os.getpid() # awk '{print $NF}' after
    deps = check_output("lsof -p {} | awk {} | grep -v 'NAME' | grep '.so'" \
                                   .format(pid, "'{print $NF}'"), shell=True).splitlines()
    print("Dependencies: \n", file=sys.stdout)
    for d in deps:
        print(d, file=sys.stdout)

    if (Exit):
        exit(0)

def groupCreate(name=''):

  if not name:
    print('Groupname is empty! Exit.', file=sys.stderr)
    exit(1)

  flags = ' {}'.format( name )

  res = x( 'groupadd' + flags, return_type=Return.process )

  if res.returncode:
    print('groupCreate error: {}'.format( res.stdout.read() ), file=sys.stderr)
    exit(res.returncode)

def groupDelete(name=''):

  if not name:
    print('Groupname is empty! Exit.', file=sys.stderr)
    exit(1)

  flags = ' {}'.format( name )

  res = x( 'groupdel' + flags, return_type=Return.process )

  if res.returncode:
    print('groupDelete error: {}'.format( res.stdout.read() ), file=sys.stderr)
    exit(res.returncode)

def chownR(path='', user='', group=''):

  if not path or not user or not group:
    print("One of parameters is empty! Exit.", file=sys.stderr)
    exit(1)

  uid = pwd.getpwnam( user ).pw_uid
  gid = grp.getgrnam( group ).gr_gid

  os.chown( path, uid, gid )

  for root, dirs, files in os.walk( path ):
    for d in dirs:
      os.chown( os.path.join( root, d ), uid, gid )

    for f in files:
      os.chown( os.path.join( root, f ), uid, gid )

def chmodR(path='', rights=''):

  if not path or not rights:
    print("One of parameters is empty! Exit.", file=sys.stderr)
    exit(1)

  mod = int( rights, base=8 )

  os.chmod( path, mod )

  for root, dirs, files in os.walk( path ):
    for d in dirs:
      os.chmod( os.path.join( root, d ), mod )

    for f in files:
      os.chmod( os.path.join( root, f ), mod )

def countdown(description='', suffix='', check_interval = 0.1, waiting = 10, unit=' sec'):
  """ Prints message %description% to stdout and counts down from %waiting%
  """
  with Ticker( waiting ) as timer:
    with ConPlace() as CP:
      print( description )

      while ( timer.tick() ):

        time.sleep( check_interval )
        tock = time.time()

        print( '{} {:.0f}/{}{}'.format( suffix, timer.tock(), waiting, unit ), end='\r' )
        sys.stdout.flush()

def gen_sha512(phrase=''):

  return hashlib.sha512(phrase).hexdigest()

def contextTest(runascon='user_u:user_r:user_t:s0'):

  print( 'before run-context: {}'.format( selinux.getexeccon_raw() ) )
  print( 'before usr-context: {}'.format( selinux.getcon_raw() ) )
  print( 'before: {}\n '.format( x("id", return_type=Return.stdout) ) )

  # not safe
  with ConRun():
    print( '\tnow context: {}'.format( selinux.getexeccon_raw() ) )
    print( '\tnow: {}\n'.format( x("id ", return_type=Return.stdout) ) )

  # safe
  # cr = ConRun(uid=500, gid=500, raw_context="user_u:user_r:user_t:s0-s3:c0.c1023")
  # if cr.start():
  #   print(f'\tnow context: {selinux.getexeccon_raw()}')
  #   print(f'\tnow: {x("id ", return_type=Return.stdout)}\n')
  # cr.stop()

  print( 'after run-context: {}'.format( selinux.getexeccon_raw() ) )
  print( 'after: {}'.format( x("id ", return_type=Return.stdout) ) )

  pass

def quest(question:str, prepared='', variants='') -> Union[bool, str]:
  """ answer is always case insensitive
      example: quest( 'Hello, ? {variants}', 'mike', 'Mike|Michael|Mihairu' )
  """

  variants = [ ans.strip().lower() for ans in variants.split('|') if ans != '' ]
  varmode  = False

  if '{variants}' in question:
    question = question.format( variants=','.join(variants) )

  if len(variants) > 0:
    if prepared and prepared not in variants:
      print( 'quest: prepared answer does not match any of variants' )
      return False

    varmode = True

  import readline

  readline.set_startup_hook( lambda: readline.insert_text( prepared ) )
  try:
    if varmode:

      variant = ''
      while variant not in variants:
        variant = input(question).strip().lower()

      return variant

    elif prepared in input(question).lower():
      return True

    else:
      return False

  except KeyboardInterrupt:
    return '' if varmode else False

  finally:
    readline.set_startup_hook()

def se_enforce(stream = sys.stdout):

  # static var
  if not hasattr(se_enforce, 'SE_ENFORCING_STATUS'):
    se_enforce.SE_ENFORCING_STATUS = selinux.security_getenforce()

  if se_enforce.SE_ENFORCING_STATUS != 1:
    if selinux.security_getenforce():
      selinux.security_setenforce( 0 )
      print( 'SElinux: отключение... [ теперь {} ]'\
                    .format( 'permissive' if selinux.security_getenforce()==0 else 'enforcing' )
                    ,file=stream )
    else:
      selinux.security_setenforce( 1 )
      print( 'SElinux: включение... [ теперь {} ]'\
                    .format( 'permissive' if selinux.security_getenforce()==0 else 'enforcing' )
                    ,file=stream )

def seMakeSuite(clear=False):

  if not clear:

    u = User(name='tester', password='tester', home='/home/tester', se_user='user_u', se_range='s0-s3')
    u.deleted = True

    files = {
      '/home/tester/s0.txt'   : 'user_u:object_r:user_home_t:s0',
      '/home/tester/s3.txt'   : 'user_u:object_r:user_home_t:s3',
      '/home/tester/s2c4.txt' : 'user_u:object_r:user_home_t:s0:c4'
    }
    print(files)
    for path, con in files.items():
      with open( path, 'w' ) as f:
        f.write('text')
      selinux.setfilecon_raw( path, con )

  else:
    u = User(name='tester')
    u.delete(force=True)

def rangeGetFromCon(context:str, mcs=False):

  if   not context:            return []
  elif not context[3]:         return []
  elif mcs and not context[4]: return []

  _ranges = context.split(':')[4] if mcs else context.split(':')[3]
  _ranges = _ranges.replace( 'c' if mcs else 's', '')
  _ranges = _ranges.split(',')


  int_ranges = []
  for rng in _ranges:

    if '.' in rng:
      sep = '.'
    elif '-' in rng:
      sep = '-'
    else:
      sep = ''

    if sep:
      rng = [ int(_) for _ in rng.split( sep ) ]
      rng = [ _ for _ in range( rng[0], rng[-1:][0]+1) ]
      int_ranges.extend( rng )

    else:
      int_ranges.append( int(rng) )

  int_ranges.sort()
  return int_ranges

def fileConGet(path:str) -> Context:

  P = Path( path )

  if P.is_symlink():
    P = Path( os.readlink( P.as_posix() ) )

  if not P.exists():
    return Context()

  if not os.path.exists(path): return Context()
  try:
    context_str = selinux.getfilecon_raw( path )[1]
    context_lst = context_str.split(':')
  except:
    return Context()

  if len(context_lst) == 5:
    return Context(
                    context_str,
                    context_lst[0],
                    context_lst[1],
                    context_lst[2],
                    rangeGetFromCon( context_str, mcs=False ),
                    rangeGetFromCon( context_str, mcs=True )
                  )

  if len(context_lst) == 4:
    return Context(
                    context_str,
                    context_lst[0],
                    context_lst[1],
                    context_lst[2],
                    rangeGetFromCon( context_str, mcs=False ),
                    None
                  )

def kernelParamCheck(param_name, need_value='y|m', config = '') -> int:
  """
  return 0 - all is ok
  return 1 - kernel parameter has unexpected value
  return 2 - kernel parameter not found
  """

  if not config:
      for f in os.listdir( '/boot' ):
        if 'config' in f:
          config = '/boot/' + f
          break

  found = False
  with open( config, 'r' ) as kconf:
    for l in kconf.readlines():

      if l.startswith('#'): continue

      if param_name in l:
        found = True

        need_value = need_value.split('|')

        if l.strip('\n').split('=')[1] not in need_value:
          return 1
        else:
          break

  if not found:
    return 2
  else:
    return 0

def packageCheck(pkg_name:str):
  headers = rpm.TransactionSet().dbMatch()
  for h in headers:
    if pkg_name in h['name']:
      return True

  return False

def lineUpdate(file:Path, old_line:str, new_line:str, all=False, delete=0, strict=True) -> bool:
  """ The function updates the line in the file.

      Parameters:
      file     (str)  - path to the file where you want to replace the line
      old_line (str)  - a fragment of a line in need of replacement, case sensitive
      new_line (str)  - new line fragment to replace old_line fragment, case sensitive
      all      (bool) - if true, then all lines matching the old_line will be replaced,
                        otherwise the function will return after the first replacement
      delete   (int)  - if > 0 and new_line is '', will delete an integer number of
                        lines starting from the line containing old_line. You must
                        specify a number including old_line
      strict   (bool) - if false the whole string will be replaced with new_line, not
                        part of it. Don't forget about end of line.

      Returns:
      (bool) : True  if all replacements are successful
             : False if no file or line is found, or an error occurred while replacing
  """

  replaced = -1 if delete > 0 else 0
  deleted  = 0

  if not file.exists():
    return False

  new = new_line or '$16_#@23*&' # (marker) necessary for correct replacement
  old = old_line

  if len(old) > 0:

    # replacing
    with open( file.as_posix(), 'r' ) as f:
      content = f.readlines()

      for i,l in enumerate(content):

        if old in l:

          if strict:
            content[i] = l.replace( old, new )
          else:
            content[i] = new

          replaced += 1

          if not all: break

    # deleting and writing
    with open( file.as_posix(), 'r+' ) as f:
      for i,l in enumerate( content ):

        f.truncate()

        if new in l and delete > 0:
          f.write( '\n' if '\n' in l else '' )
          deleted = 1

        elif deleted != 0 and deleted < delete:
          f.write( '\n' if '\n' in l else '' )
          deleted += 1

        else:
          f.write( l )


  if replaced == 0 and deleted == 0:
    #print( f'  Файл {file} не был обновлён.' )
    return False

  #print( f'  Файл {file} успешно обновлён. Заменено: {replaced}, Удалено: {deleted}.' )
  return True

def socket_test():

  import socket

  socket_f = Path( '/tmp/testfolder/test.socket' )

  if socket_f.exists() and socket_f.is_socket():
    socket_f.unlink()

  with socket.socket( socket.AF_UNIX, socket.SOCK_STREAM ) as sock:
    sock.bind( socket_f.as_posix() )
    sock.listen()

    while True:
      conn, addr = sock.accept()
      data = conn.recv(150)

      print( 'recieved: ' + data.decode('utf-8') )

      conn.send( 'Hello, neighbor!\n'.format( addr ).encode() )
      conn.close()

def dmesgCheck(txt:str) -> bool:

  code, out = x( 'dmesg', Return.codeout )

  return code == 0 and txt in out

def dmesgClear() -> bool:

  code, out = x( 'dmesg -c', Return.codeout )

  if code != 0:
    print( 'dmesgClear: error {} - {}'.format( code, out ) )
    return False

  return True

def hasInet(host="8.8.8.8", port=53, timeout=5) -> bool:

  try:
    socket.setdefaulttimeout( timeout )
    socket.socket( socket.AF_INET, socket.SOCK_STREAM ).connect( (host, port) )
    return True

  except socket.error as e:
    print( 'socket: ' + str(e) )
    return False

def getInetDevices():

  devs = []

  code, out = x( 'ifconfig | grep BROADCAST | grep UP', Return.codeout )

  if code != 0:
    print( 'getInetDevices: ethernet devices not found!' )
    return None

  for line in out.splitlines():
    devs.append( line.split(':')[0] )

  return devs

def etherdog( interval:float=5.0 ):

  devs = getInetDevices()

  if devs is None:
    exit(1)

  try:
    while not sleep( interval ):

      if not hasInet():
        print( 'trying to restart network...')

        for d in devs:
          print( 'restarting: ' + d )

          # code, out = x( 'ip link set {i} down; ip link set {i} up'.format( i=d ), Return.codeout )
          # if code != 0:
          #   print( 'etherdog: ' + out )

          # code, out = x( 'dhclient -r {i}; dhclient {i}'.format( i=d ), Return.codeout )
          # if code != 0:
          #   print( 'etherdog: ' + out )

          code, out = x( 'systemctl restart networkmanager.service', Return.codeout )
          if code != 0:
            print( 'etherdog: ' + out )



  except KeyboardInterrupt:
    os.system( 'clear' )
    print( 'Network monitoring process stopped.' )

def save_env( path:Path ):

  env = os.environ.copy()

  with path.open( 'a+', encoding='UTF-8' ) as f:
    for key, value in env.items():
      f.write( '{}={}\n'.format( key,value ) )
      f.flush()

def load_env( path:Path ):

  with path.open( 'r' ) as f:
    for line in f.readlines():
      n = line.strip().split('=')
      os.environ[ n[0] ] = n[1]

def collect_selinux_allows( sec:float, group=False, exclude:List[str]=[], include:List[str]=[] ) -> Union[Dict, List]:
  """ This function allows you to collect "allow" rules for selinux.

      Parameters:
      sec     (float)           : lifetime(in seconds) of the log entries for searchable rules,
                                  in other words, how many seconds ago the first rule might have appeared;
      group   (bool)            : if True then return a dictionary, where the key is the subject type;
      include (list of strings) : only accept lines that contain at least one substring from include;
      exclude (list of strings) : do not accept lines that contain at least one substring from exclude;

      Returns:
      Dict : if group is True
      List : if group is False(default)

      Example:
        >> collect_selinux_allows(10.0)
        >> [ 'allow auditadm_t sysadm_t:unix_stream_socket { read write };' ]
        >>
        >> collect_selinux_allows(10.0, exclude=['123', 'socket'])
        >> []
        >>
        >> collect_selinux_allows(10.0, True)
        >> { 'auditadm_t' : ['sysadm_t', 'unix_stream_socket', ['read','write'] ] }
        >>
        >> collect_selinux_allows(10.0, True, exclude=['adm_t'])
        >> {}
  """

  # allow sysadm_t rosatest_user_object_t:file { getattr unlink };
  # allow sysadm_t rosatest_sysadm_object_t:file unlink;
  reg   = r'(?:allow (?P<subject_type>\w*) (?P<object_type>\w*):(?P<object>\w*)(?: ?{? ?)(?P<action>.*?) ?}?);'
  rules = dict() if group else list()

  code, out = x( 'journalctl --since "{} sec ago" | audit2allow'.format( round(sec) ), Return.codeout )

  if code == 0:
    for line in out.splitlines():
      if len(line) <= 1:
        continue
      # if all substrings are not included in line
      if include and all(include_str not in line for include_str in include):
        continue
      # if there is no substring in line
      if exclude and any(strict_substr in line for strict_substr in exclude):
        continue
      if group:
        rule = re.findall( reg, line, re.IGNORECASE )
        if rule:
          subject_type, object_type, object, actions = rule[0]
          if subject_type in rules.keys():
            rules[subject_type].append( [ object_type, object, actions.split() ] )
          else:
            rules[subject_type] = [[ object_type, object, actions.split() ]]
      else:
        rules.append( line.strip() )

  return rules

def encrypt( data:bytes, key:str ) -> str:
  """init_vec must be authenticated by the receiver and it should be picked randomly
  """

  key_length = len(key)

  init_vec = Random.get_random_bytes( AES.block_size )

  if key_length < 32:
    print( 'encrypt(): selected key is too weak!' )
    key = '{:^{len}}'.format( key, len=32 )

  elif key_length > 32:
    print( 'encrypt(): selected key is too large, it should not be more than 32 bytes!' )
    return None

  cph     = AES.new( key, AES.MODE_CFB, init_vec )
  crypted = cph.encrypt( data )

  iv = b64encode( cph.IV ).decode( 'utf-8' )
  ct = b64encode( crypted).decode( 'utf-8' )

  return json.dumps({ 'iv':iv, 'crypted':ct })

def decrypt( json_dump, key:str ):
  """init_vec must be authenticated by the receiver and it should be picked randomly
  """

  key_length = len(key)

  if key_length < 32:
    key = '{:^{len}}'.format( key, len=32 )

  try:
    b64     = json.loads( json_dump )
    iv      = b64decode( b64[ 'iv' ] )
    crypted = b64decode( b64[ 'crypted' ] )

  except (ValueError, KeyError):
    print( "decrypt(): incorrect decryption!" )
    return None

  cph   = AES.new( key, AES.MODE_CFB, iv )
  plain = cph.decrypt( crypted )

  return bytearray(plain)

def encrypt_folder( path:Path, key:str ):

  buffer     = bytearray()
  special    = bytearray()
  ctrl_chars = '›ᚫᛕ‹'.encode()

  if path.exists() and path.is_dir():
    for f in list( path.glob('**/*') ):

      if f.is_dir():
        buffer += '››{}ᛕ'.format( f.relative_to(path).as_posix() ).encode()
      else:
        buffer += '››{}ᚫ'.format( f.relative_to(path).as_posix() ).encode()

        with f.open('rb') as data:
          for byte in data.read():

            if byte in ctrl_chars:
              special.append( byte )

              if len(special) == 5 and special in ctrl_chars :
                print( 'encrypt_folder(): {} already contains special characters!'.format( f ) )
                return None

            buffer.append( byte )
      buffer += '‹‹'.encode()

    return encrypt( bytes(buffer), key )

  else:
    print( 'encrypt_folder(): {} is not directory!'.format( path ) )
    return None

def decrypt_folder( path_to_encrypted:Path, destination:Path, key:str ):

  decrypted = bytearray()
  jsn       = None

  if path_to_encrypted.exists():
    with path_to_encrypted.open( 'r' ) as f:
      jsn = json.load( f )

    decrypted.extend( decrypt( json.dumps(jsn), key ) )
    content    = { } # filename : data
    buff       = bytearray()

    for entry in decrypted.split( '‹‹'.encode() )[:-1]:

      entry = entry.strip( '››'.encode() )

      if entry.find( 'ᚫ'.encode() ) > 0:
        buff = entry.split( 'ᚫ'.encode() )

        if len(buff) > 2:
          print( 'decrypt_folder(): data corrupted, control character ᚫ is no longer unique!' )
          return None

        content[ buff[0].decode(errors='ignore') ] = buff[1]

      elif entry.find( 'ᛕ'.encode() ) > 0:
        buff = entry.split( 'ᛕ'.encode() )
        content[ buff[0].decode(errors='ignore') ] = None

      else:
        print( 'decrypt_folder(): data corrupted, missing control characters!' )
        return None

   # recreating file hierarchy

    for entry,data in content.items():
      e = destination / Path( entry )

      if data == None:
        e.mkdir( parents=True, exist_ok=True )
      else:
        e.parent.mkdir( parents=True, exist_ok=True )
        with e.open( 'wb' ) as f:
          f.write( data )

  else:
    print( 'decrypt_folder(): {} not exists!'.format( path_to_encrypted ) )
    return None

def runtty(cmd:str, comm_stack:List[str]):
  """ This function allows you to execute a command on
      a dedicated tty and get the output history.

      Parameters:
      cmd        (list of strings) : shell command;
      comm_stack (list of strings) : list of shell commands that will be sequentially
                                     entered into the dedicated tty;
      Example:
      runtty('/bin/bash', ['id\n', 'exit\n'])

      Returns:
      (list of strings) : all that has been written to stdout in a dedicated tty
  """
  old_tty = termios.tcgetattr(sys.stdin)
  tty.setraw(sys.stdin.fileno())

  master, slave = pty.openpty()

  p = Popen(
    cmd,
    shell=True,
    preexec_fn=os.setsid,
    close_fds=True,
    universal_newlines=True,
    stdin=slave,
    stdout=slave,
    stderr=slave
  )
  os.close(slave)

  write_queue = [comm.encode() for comm in comm_stack]
  history = []
  try:
    while p.poll() is None:
      r, w, e = select.select([master], [master], [], 1)

      if master in w and master in r and write_queue:
        os.write(master, write_queue.pop(0))
      elif master in r:
        out = os.read(master, 512)
        history.append(out.decode())

  finally:
    p.kill()
    os.close(master)
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_tty)
    return history

def get_file_mode( file:Path ) -> str:

  try:
    return oct( file.lstat().st_mode & 0o7777 )[2:]
  except (PermissionError, FileNotFoundError) as e:
    return '000'

def profile(saveto:str):
  """ This is a decorator that allows you to collect statistics on calls and time spent on the decorated function and
      save it to a file.
  """
  def decorator(func):
    def wrapper(*args, **kwargs):
      with cProfile.Profile() as profiler:
        rc = func(*args, **kwargs)

      data = io.StringIO()
      profiler_stats = pstats.Stats(profiler, stream=data)
      profiler_stats.sort_stats(pstats.SortKey.CUMULATIVE)
      if saveto:
        profiler_stats.dump_stats(saveto)
      else:
        profiler_stats.print_stats()
      return rc
    return wrapper
  return decorator

def steal_x_cookie(from_user:str, to_user:str, overwrite=False):
  """ Copies one user's x-server cookies so that another can access the display.
      Root privileges required.

    Parameters:
      from_user (str)  : user login name;
      to_user   (str)  : user login name;
      overwrite (bool) : if true and another user's cookie already exists then
                         it will be overwritten.
    Example:
      steal_x_cookie('ivanzero','admin55')

    Returns:
      None : function can throw exceptions
  """
  users  = [ struct for struct in pwd.getpwall()]

  l_user_cookie = None
  r_user_cookie = None

  for u in users:
    if l_user_cookie and r_user_cookie:
      break

    if not l_user_cookie and from_user == u[0]:
      l_user_cookie = Path('{}/.Xauthority'.format(u[5]))

    if not r_user_cookie and to_user == u[0]:
      r_user_cookie = Path('{}/.Xauthority'.format(u[5]))
      r_user_uid = u[2]
      r_user_gid = u[3]

  if not l_user_cookie.exists():
    raise FileNotFoundError('File {} does not exist!'.format(l_user_cookie))

  if r_user_cookie.exists() and not overwrite:
    raise Warning('Target file {} already exists'.format(r_user_cookie))

  elif r_user_cookie.exists():
    if not filecmp.cmp(l_user_cookie.as_posix(), r_user_cookie.as_posix(), shallow=False):
      shutil.copyfile(l_user_cookie.as_posix(), r_user_cookie.as_posix())

  else:
    shutil.copyfile(l_user_cookie.as_posix(), r_user_cookie.as_posix())

  os.chown(r_user_cookie.as_posix(), r_user_uid, r_user_gid)

def test_json_raw_or_serialized(self):
  """
    legacy json size: 492.68 Mb
    legacy json read to ram:  6.7836 sec
    serialized json size: 403.77 Mb
    serialized json read to ram:  5.028 sec
    difference: 88.90 Mb (18.05%)
  """
  legacy_json = Path('/mnt/hgfs/1SH/nvdcpematch-1.0.json')
  legacy_size = legacy_json.stat().st_size
  serialized_json = Path('/mnt/hgfs/1SH/nvdcpematch-new.json')
  serialized_size = serialized_json.stat().st_size

  # run once for raw json (raw - human-readable)
  # with legacy_json.open('r') as j:
  #   with serialized_json.open('w') as new_j:
  #     json.dump(json.load(j), new_j)

  os.sync()
  with Ticker(high_resolution=True, precision=4) as timer1:
    with legacy_json.open('r') as j:
      print('legacy json size: {:.2f} Mb'.format(legacy_size / 1024 / 1024))
      legacy_json_in_ram = json.load(j)
  print('legacy json read to ram: ', timer1.last_point, 'sec')

  del legacy_json_in_ram

  with Ticker(high_resolution=True, precision=4) as timer2:
    with serialized_json.open('r') as j:
      print('serialized json size: {:.2f} Mb'.format(serialized_size / 1024 / 1024))
      serialized_json_in_ram = json.load(j)
  print('serialized json read to ram: ', timer2.last_point, 'sec')

  del serialized_json_in_ram

  diff = (legacy_size - serialized_size)
  percent = diff / (legacy_size / 100.0)

  print('difference: {:.2f} Mb ({:.2f}%)'.format(diff / 1024 / 1024, percent))

@contextmanager
def get_display_privileges():
  user = User()
  with ConRun(user.uid, user.gid):
    XDisplay.add_host('root')
  yield

  XDisplay.del_host('root')

if __name__ == '__main__':
  # whatlibs()
  #cls()

  # t = StreamTable(width=50)

  # t.split(16,21)
  # t.cell( 'Test name and lot of text', '<')
  # t.cell( 'Проверка сервисов и прочая фигня', '<')
  # t.cell( 'Правильные паттерны = неправильное мышление?', '<')
  # t.split(25, 39)
  # t.cell(' Почти готово' )
  # t.cell(' hmm' )
  # t.cell( 'OK!')
  # t.split(end=True)

  # t = StreamTable(30)
  # t.split()
  # t.cell( 'welcome!', '^' )
  # t.split( 15 )
  # t.cell( 'hello,', '>'), t.cell( 'Jack!', '<')
  # t.split(end=True)


  # t = StreamTable(39)
  # t.split().cell( 'Test name', '^' )
  # t.split().cell( 'Проверка обеспечения функционирования средств ' \
  #                 'графического интерфейса пользователя на соврем' \
  #                 'енном оборудовании.', '<' )
  # t.split().cell( 'Базовая проверка', '^' )
  # t.split(29)\
  #   .cell( 'Проверка конфигурации ядра', '<' )\
  #   .cell( 'OK', color=97 )
  # t.split(29)\
  #   .cell( 'Запуск сервисов и всякая фигня', '<', color=97 )\
  #   .cell( '--' )

  # t.split(29).cell('...').cell('...')
  # t.split().cell( 'Начинается проверка прерывания процесса...\n'
  #                 'Запускается программа, имитирующая длительный процесс...\n'
  #                 'Обнаружение pid программы имитирующей длительный процесс...\n'
  #                 'Запускается программа принудительного прерывания процесса...\n'
  #                 'Подтверждение закрытия процесса...\n'
  #                 'Проверка прерывания процесса пройдена успешно.', '<' )

  # t.split(end=True)

  # b = StreamTable(41)
  # b.split().line( '', '<')

  # ctx = Context()
  # ctx.u = 'user_u'
  # ctx.r = 'user_r'
  # ctx.t = 'user_t'
  # ctx.s = [0]
  # ctx.c = []
  # print(ctx.update().full)

  # for line in collect_selinux_allows(1000):
  #   print(line)
  # print('----')
  # for k,v in collect_selinux_allows(1000, True).items():
  #   print(k, v)

  # con = "aib_u:sysadm_r:sysadm_t:s0:c8,c11,c3.c5"
  # con = selinux.context_new(con)
  # print(selinux.context_user_get(con))
  # print(selinux.context_role_get(con))
  # print(selinux.context_type_get(con))
  # print(selinux.context_range_get(con))

  # con = "aib_u:sysadm_r:sysadm_t:s0:c8,c11,c3.c5"
  # print(rangeGetFromCon(con, True))
  # with Ticker(high_resolution=True, precision=4) as timer1:
  #   time.sleep(5)
  # print('first ', timer1.last_point)
  # with Ticker(high_resolution=True, precision=4) as timer2:
  #   time.sleep(4)
  # print('second ', timer2.last_point)

  # print('delta ', timer1 - timer2)

  # with User('vasya', 't3stSimple_', home='/home/vasya') as u:
  #   steal_x_cookie('test', 'vasya')
  #   input()
  #colorMap()
  # with User() as u:
  #   with ConRun(u.uid, u.gid) as mem:
  #     proc = Process(
  #     target=run,
  #     args=(['soffice', '--writer'],),
  #     daemon=True
  #     )
  #     proc.start()
  #     proc.join()
  #save_env(Path('/tmp/eee'))
  # halo = False

  # def keyboard_interrupt(signal, stack_frame):
  #   halo=True
  #   print('lol')
  #   raise KeyboardInterrupt
  # import signal
  # signal.signal(signal.SIGINT, keyboard_interrupt)

  # for _ in CountDown(10):
  #   pass

  # reg = '\\x1b\[(?:.*?)m'
  # line = 'asdas\x1b[38;5;13m\x1b[1m\x1b[48;5;44masdasd\x1b[0m'

  # o = ASCIIColor(line)

  # print(o.prefix)


  # t = StreamTable(30)
  # t.split()
  # t.cell( 'welcome!{}'.format(colored('hey!', 10) ), '^' )
  # t.line(colored('ha haassssssssssssssssssssssdasdasdas', 200), '>')
  # t.line('dasdasdas', '>')
  # t.split( 15 )
  # t.cell( 'hello,guyyyyyyyyyyyyyyyyyyyyyyyyyyyys', '>'), t.cell( 'Jack!sadasdasd Just do it!', '<')
  # t.split( 8, 25)
  # t.cell('lala').cell('b').cell(colored('h', 112))
  # t.split()
  # t.cell('looooooooooool')
  # t.split(end=True)

  # with get_display_privileges():
  #   x('kolourpaint')
  #   XDisplay.info()

  # conf = '/usr/share/rosa-tests/test_data/accessibility.conf'
  # target = '/usr/share/defaults/at-spi2/accessibility.conf'
  # bdb = BackupDB()
  # if not bdb.has(target):
  #   bdb.add(target)
  #   bdb.substitute(conf, target)
  # else:
  #   bdb.restore(target)

  mem = mmap(-1, 8192, flags=MAP_SHARED)
  print(777, mem.tell())
  sys.stdout = StreamToMem(mem)
  print(123)
  print(321)
  print(sys.stdout.read(), file=sys.stderr)
  sys.stdout.close()


"""╔═════════════════════════════════════╗
   ║            Test name                ║
   ╟─────────────────────────────────────╢
   ║      Проверка обеспечения функциони ║
   ║ рования средств графического интерф ║
   ║ ейса пользователя на современном об ║
   ║ орудовании                          ║
   ╟─────────────────────────────────────╢
   ║         Базовая проверка            ║
   ╟────────────────────────────┬────────╢
   ║ Проверка конфигурации ядра │   OK   ║
   ╟────────────────────────────┼────────╢
   ║ Запуск сервисов и          │        ║
   ║ всякая фигня               │   --   ║
   ╟────────────────────────────┼────────╢
   ║ ...                        │  ...   ║
   ╟────────────────────────────┴────────╢
   ║          Ход тестирования           ║
   ╟─────────────────────────────────────╢
   ║ Начинается проверка прерывания проц ║
   ║есса...                              ║
   ║ Запускается программа, имитирующая  ║
   ║длительный процесс...                ║
   ║ Обнаружение pid программы имитирующ ║
   ║ей длительный процесс...             ║
   ║ Запускается программа принудительно ║
   ║го прерывания процесса...            ║
   ║ Подтверждение закрытия процесса...  ║
   ║ Проверка прерывания процесса пройде ║
   ║на успешно.                          ║
   ╟─────────────────────────────────────╢
   ║    Завершение базовой проверки      ║
   ╟────────────────────────────┬────────╢
   ║ Остановка сервисов         │   --   ║
   ╟───────────────────┬────────┴────────╢
   ║ Затраченное время │   15.084 сек.   ║
   ╟───────────────────┼─────────────────╢
   ║ Заключение        │    Пройден      ║
   ╚═══════════════════╧═════════════════╝
"""
