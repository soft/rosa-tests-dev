#!/usr/libexec/python3.8
# -*- coding: utf-8 -*-

# kernelplv@gmail.com

import os
import sys
import argparse


from pathlib import Path
from hashlib import md5

from subprocess import run
from sys import stderr, stdin, stdout
from typing import Dict, Tuple, Union


self = Path(__file__)

def x( command:str, direct=False ) -> Union[int, Tuple[int, str]]:
  if direct:
    process = run( command, shell=True, encoding='UTF-8', stdout=stdout, stderr=stderr, stdin=stdin )
    return process.returncode
  else:
    process = run( command, shell=True, encoding='UTF-8', capture_output=True )
  return (process.returncode, process.stdout)

def I( string:str, foreground:int, background:int=-1) -> str:
  """ The function returns a string with added ansi colors.

      Parameters:
      string     (str)  - source string to be colored
      foreground (int)  - 256-bit color code
      background (int)  - 256-bit color code

      Returns:
      (str) : colored line
  """

  RESET = '\033[0m'

  res     = ''
  content = string.splitlines()

  for part in content:
    buf = '\033[38;5;{fg}m{content}{reset}'.format( fg=foreground, content=part, reset=RESET  )

    if background > 0:
      buf = '\033[48;5;{bg}m'.format( bg=background ) + buf

    if part != content[-1]:
      res += buf + '\n'
    else:
      res += buf

  return res

def quest(question:str, prepared='', variants='') -> Union[bool, str]:
  """ answer is always case insensitive
      example: quest( 'Hello, ? {variants}', 'mike', 'Mike|Michael|Mihairu' )
  """

  variants = [ ans.strip().lower() for ans in variants.split('|') if ans != '' ]
  varmode  = False

  if '{variants}' in question:
    question = question.format( variants=','.join(variants) )

  if len(variants) > 0:
    if prepared and prepared not in variants:
      print( 'quest: prepared answer does not match any of variants' )
      return False

    varmode = True

  import readline

  readline.set_startup_hook( lambda: readline.insert_text( prepared ) )
  try:
    if varmode:

      variant = ''
      while variant not in variants:
        variant = input(question).strip().lower()

      return variant

    elif prepared in input(question).lower():
      return True

    else:
      return False

  except KeyboardInterrupt:
    return '' if varmode else False

  finally:
    readline.set_startup_hook()

def save_to_db( path:Path, ignore_dirs=[ '.git' ] ):

  db = dict()

  for folder, dirs, file_name in os.walk( path, followlinks=False ):
    dirs[:] = [d for d in dirs if d not in ignore_dirs]

    for name in file_name:

      entry = Path( '{}/{}'.format( folder, name ) )

      try:
        with entry.open('rb') as f:
          content              = f.read()
          relative_entry       = entry.relative_to( self.parent )
          file_hash            = md5( content ).hexdigest()
          db[ relative_entry ] = { 'hash':file_hash, 'content':content }
          #print( file_hash, ' ', relative_entry )

      except (PermissionError, FileNotFoundError) as e:
        self.fail( 'permissions error {}'.format( entry.as_posix() ) )

  return db

def check( path:Path, db:Dict, ignore_dirs=[ '.git', '.vscode', '__pycache__' ] ):

  for folder, dirs, file_name in os.walk( path, followlinks=False ):
    dirs[:] = [d for d in dirs if d not in ignore_dirs]

    for name in file_name:

      entry = Path( '{}/{}'.format( folder, name ) )

      try:
        with entry.open('rb') as f:
          content              = f.read()
          relative_entry       = entry.relative_to( path )
          file_hash            = md5( content ).hexdigest()

          if relative_entry in db.keys():
            if db[ relative_entry ]['hash'] != file_hash:
              print( file_hash, ' ', I(relative_entry.as_posix(),12) )

              if quest( 'Update this file? (y,n): ', variants='y|n' ) == 'y':
                absolute_entry = (self.parent / relative_entry).resolve()

                with absolute_entry.open( 'wb' ) as updated:
                  updated.write( content )

                print( f'{absolute_entry} updated!' )
          else:
            print( 'new file: {} '.format( I(entry.as_posix(), 48) ) )

            if quest( 'Add this file? (y,n): ', variants='y|n') == 'y':
              absolute_entry = (self.parent / relative_entry).resolve()

              with absolute_entry.open( 'wb' ) as new:
                new.write( content )
              print( f'{absolute_entry} added!' )

      except (PermissionError, FileNotFoundError) as e:
        self.fail( 'permissions error {}'.format( entry.as_posix() ) )

if __name__ == "__main__":

  parser             = argparse.ArgumentParser( )
  parser.description = \
    """
    This tool should be located in the root of the git project.
    It allows you to update the project source codes to the actual ones by updating from the deployed project
    """

  parser.add_argument( 'target', type=str, default='' )

  cmdline = parser.parse_args( sys.argv[1:] )

  source_dir = self.parent
  target_dir = Path( cmdline.target )

  if not target_dir.is_dir():
    print( 'no directory specified. exit.' )
    exit(1)

  db = save_to_db( source_dir )
  check( target_dir, db )


