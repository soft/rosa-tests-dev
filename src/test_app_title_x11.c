#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

static void sendCloseEvent(Display *display, Window rootWindow, Window window) {
    XEvent ev;

    memset(&ev, 0, sizeof(ev));
    Atom closeWindowAtom = XInternAtom(display, "_NET_CLOSE_WINDOW", False);
    ev.xclient.type = ClientMessage;
    ev.xclient.window = window;
    ev.xclient.message_type = closeWindowAtom;
    ev.xclient.format = 32;
    ev.xclient.data.l[0] = CurrentTime;
    ev.xclient.data.l[1] = rootWindow;
    XSendEvent(display, rootWindow, False, SubstructureRedirectMask, &ev);
}

static bool windowHasName(Display *display, Window win,
                          const char *name, const char *altName) {
    XTextProperty wmName;

    int status = XGetWMName(display, win, &wmName);
    bool result = false;
    if (status) {
        if (wmName.value) {
            if (wmName.nitems) {
                char **list;
                int i;
                status = Xutf8TextPropertyToTextList(display, &wmName, &list, &i);
                if (status >= Success && *list) {
                    result = (strcmp(*list, name) == 0) ||
                             (altName != NULL && strcmp(*list, altName) == 0);
                    XFreeStringList(list);
                }
            }
            XFree(wmName.value);
        }
    } else result = (name == NULL);
    return result;
}

static Window findWindow(Display *display, Window currentWindow,
                         const char *targetWindowName, const char *targetWindowAltName) {
    Window parent;
    Window *children;
    unsigned int nNumChildren;

    if (windowHasName(display, currentWindow, targetWindowName, targetWindowAltName)) {
        return currentWindow;
    }

    int status = XQueryTree(display, currentWindow, &currentWindow, &parent, &children, &nNumChildren);
    if (!status || nNumChildren == 0) {
        return None;
    }

    int i;
    Window result = 0;
    for (i = 0; i < nNumChildren; i++) {
        result = findWindow(display, children[i], targetWindowName, targetWindowAltName);
        if (result != None) break;
    }
    XFree((char*) children);
    return result;
}

static int error_handler(Display *display, XErrorEvent *error) {
    return 0;
}

int main(int argc, char **argv) {
    Display *display = XOpenDisplay(0);
    bool done = false;
    if (argc < 2) {
        fprintf(stderr, "Internal Error! Invalid parameters.\n");
        return 1;
    }
    if (display) {
        XSetErrorHandler(error_handler);
        int t;
        Window rootWindow = XDefaultRootWindow(display);
        for (t = 0; t < 100; t++) {
            Window window = findWindow(display, rootWindow, argv[1], argc < 3 ? NULL : argv[2]);
            if (window != None) {
                printf("Target window found. Closing it...\n");
                usleep(300 * 1000);
                sendCloseEvent(display, rootWindow, window);
                done = true;
                break;
            }
            usleep(100 * 1000);
        }
        if (!done) {
            if (argc < 3)
                fprintf(stderr, "Couldn't find a window with the caption '%s'.\n", argv[1]);
            else
                fprintf(stderr, "Couldn't find a window with the caption '%s' ('%s').\n", argv[1], argv[2]);
        }
        XCloseDisplay(display);
    } else {
        fprintf(stderr, "Couldn't open X display.\n");
    }
    return done ? 0 : 1;
}
