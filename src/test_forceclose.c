#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

static XKeyEvent createKeyEvent(Display *display, Window win,
                           Window winRoot, bool press,
                           int keycode, int modifiers) {
    XKeyEvent event;

    event.display     = display;
    event.window      = win;
    event.root        = winRoot;
    event.subwindow   = None;
    event.time        = CurrentTime;
    event.x           = 1;
    event.y           = 1;
    event.x_root      = 1;
    event.y_root      = 1;
    event.same_screen = True;
    event.keycode     = XKeysymToKeycode(display, keycode);
    event.state       = modifiers;
    event.type = press ? KeyPress : KeyRelease;
    return event;
}

static void sendCloseEvent(Display *display, Window rootWindow, Window window) {
    XEvent ev;

    memset(&ev, 0, sizeof(ev));
    Atom closeWindowAtom = XInternAtom(display, "_NET_CLOSE_WINDOW", False);
    ev.xclient.type = ClientMessage;
    ev.xclient.window = window;
    ev.xclient.message_type = closeWindowAtom;
    ev.xclient.format = 32;
    ev.xclient.data.l[0] = CurrentTime;
    ev.xclient.data.l[1] = rootWindow;
    XSendEvent(display, rootWindow, False, SubstructureRedirectMask, &ev);
}

static bool windowHasName(Display *display, Window win, const char *name) {
    XTextProperty wmName;

    int status = XGetWMName(display, win, &wmName);
    bool result = false;
    if (status) {
        if (wmName.value) {
            if (wmName.nitems) {
                char **list;
                int i;
                status = Xutf8TextPropertyToTextList(display, &wmName, &list, &i);
                if (status >= Success && *list) {
                    result = (strcmp(*list, name) == 0);
                    XFreeStringList(list);
                }
            }
            XFree(wmName.value);
        }
    } else result = (name == NULL);
    return result;
}

static bool windowHasPropertyValues(Display *display, Window win,
                                     const char *propName, const char *val0, const char* val1) {
    Atom type;
    int format;
    unsigned long nItem, bytesAfter;
    unsigned char *properties = NULL;

    Atom prop = XInternAtom(display, propName, True);
    int status = XGetWindowProperty(display, win, prop, 0, (~0L), False, AnyPropertyType,
                        &type, &format, &nItem, &bytesAfter, &properties);
    if (status != Success) return false;

    Atom aVal0 = XInternAtom(display, val0, True);
    bool result = false;
    if (nItem == 1) {
        result = (((long*)properties)[0] == aVal0);
    } else if (nItem == 2) {
        Atom aVal1 = XInternAtom(display, val1, True);
        result = ((((long*)properties)[0] == aVal0 && ((long*)properties)[1] == aVal1) ||
                  (((long*)properties)[0] == aVal1 && ((long*)properties)[1] == aVal0));
    }
    XFree(properties);
    return result;
}

static bool windowHasNumChildren(Display *display, Window win, int numChildren) {
    Window parent, tmpWindow;
    Window *children;
    unsigned int nNumChildren;

    int status = XQueryTree(display, win, &tmpWindow, &parent, &children, &nNumChildren);
    if (!status) return false;
    XFree((char*) children);
    return (numChildren == nNumChildren);
}

static Window findWindow(Display *display, Window currentWindow, const char *targetWindowName) {
    Window parent;
    Window *children;
    unsigned int nNumChildren;

    if (windowHasName(display, currentWindow, targetWindowName)) {
        return currentWindow;
    }

    int status = XQueryTree(display, currentWindow, &currentWindow, &parent, &children, &nNumChildren);
    if (!status || nNumChildren == 0) {
        return None;
    }

    int i;
    Window result = 0;
    for (i = 0; i < nNumChildren; i++) {
        result = findWindow(display, children[i], targetWindowName);
        if (result != None) break;
    }
    XFree((char*) children);
    return result;
}

static int error_handler(Display *display, XErrorEvent *error) {
    return 0;
}

int main(int argc, char **argv) {
    Display *display = XOpenDisplay(0);
    bool done = false;
    if (argc != 2) {
        fprintf(stderr, "Internal Error! Invalid parameters.\n");
        return 1;
    }
    if (display) {
        XSetErrorHandler(error_handler);
        int status = Success;
        int t;
        Window rootWindow = XDefaultRootWindow(display);
        Window tmpWindow = None, parent = None;
        Window *rootChildren;
        unsigned int nNumRootChildren;
        bool targetWindowClosing = false;
        for (t = 0; t < 100; t++) {
            Window window = findWindow(display, rootWindow, argv[1]);
            if (window != None) {
                printf("Target window found. Closing it...\n");
                usleep(300 * 1000);
                status = XQueryTree(display, rootWindow, &tmpWindow, &parent, &rootChildren, &nNumRootChildren);
                if (status) {
                    sendCloseEvent(display, rootWindow, window);
                    targetWindowClosing = true;
                } else {
                    fprintf(stderr, "Couldn't get X tree (%d).\n", status);
                }
                break;
            }
            usleep(100 * 1000);
        }
        if (targetWindowClosing) {
            Window *children;
            unsigned int nNumChildren;
            for (t = 0; t < 300 && status >= Success && !done; t++) {
                status = XQueryTree(display, rootWindow, &tmpWindow, &parent, &children, &nNumChildren);
                if (!status) {
                    fprintf(stderr, "Couldn't get X tree (%d).\n", status);
                    break;
                }
                int i0, i, j;
                Window cWindow;
                for (i = 0; i < nNumChildren && !done; i++) {
                    cWindow = children[i];
                    bool cWindowFound = false;
                    for (i0 = 0; i0 < nNumRootChildren && !cWindowFound; i0++) {
                        cWindowFound = (cWindow == rootChildren[i0]);
                    }
                    if (!cWindowFound) {
                        //printf("new window found: %d\n", cWindow);
                        Window *childrenWin;
                        unsigned int nNumChildrenWin;
                        status = XQueryTree(display, cWindow, &tmpWindow, &parent, &childrenWin, &nNumChildrenWin);
                        if (!status) {
                            fprintf(stderr, "Couldn't get X tree (%d).\n", status);
                            break;
                        }
                        Window winChild;
                        for (j = 0; j < nNumChildrenWin && !done; j++) {
                            winChild = childrenWin[j];
                            if (windowHasPropertyValues(display, winChild, "_NET_WM_WINDOW_TYPE", "_NET_WM_WINDOW_TYPE_DIALOG", NULL) &&
                                windowHasPropertyValues(display, winChild, "_NET_WM_STATE", "_NET_WM_STATE_MODAL", "_NET_WM_STATE_DEMANDS_ATTENTION") &&
                                windowHasName(display, winChild, NULL) && windowHasNumChildren(display, winChild, 3)
                            ) {
                                // OpenBox WM force close dialog (it has no caption so we have only loose indications)
                                printf("The force close dialog (OpenBox) found. Choosing termination...\n");
                                usleep(100 * 1000);

                                XKeyEvent event;
                                event = createKeyEvent(display, winChild, rootWindow, true, XK_Tab, 0);
                                status = XSendEvent(event.display, event.window, True, KeyPressMask, (XEvent *)&event);

                                event = createKeyEvent(display, winChild, rootWindow, false, XK_Tab, 0);
                                status = XSendEvent(event.display, event.window, True, KeyPressMask, (XEvent *)&event);
                                XFlush(display);
                                usleep(1000 * 1000);

                                event = createKeyEvent(display, winChild, rootWindow, true, XK_space, 0);
                                status = XSendEvent(event.display, event.window, True, KeyPressMask, (XEvent *)&event);

                                event = createKeyEvent(display, winChild, rootWindow, false, XK_space, 0);
                                status = XSendEvent(event.display, event.window, True, KeyPressMask, (XEvent *)&event);

                                done = true;
                                break;
                            } else {
                                Window warningWindow = findWindow(display, cWindow, "Предупреждение — Диспетчер окон");
                                if (warningWindow == None) {
                                    warningWindow = findWindow(display, cWindow, "Warning – Window Manager");
                                }
                                if (warningWindow != None) {
                                    //KWin Killer
                                    char **list;
                                    int i;
                                    status = XGetCommand(display, warningWindow, &list, &i);
                                    if (status && i && *list) {
                                        int cmdLen = strlen(*list);
                                        const char *kwinTitle = "/kwin_killer_helper";
                                        int kwLen = strlen(kwinTitle);
                                        if ((cmdLen >= kwLen) && (strcmp(*list + cmdLen - kwLen, kwinTitle) == 0)) {
                                            printf("The force close dialog (KWin) found. Choosing termination...\n");
                                            usleep(1000 * 1000);

                                            XKeyEvent event;
                                            event = createKeyEvent(display, warningWindow, rootWindow, true, XK_space, 0);
                                            status = XSendEvent(event.display, event.window, True, KeyPressMask, (XEvent *)&event);

                                            event = createKeyEvent(display, warningWindow, rootWindow, false, XK_space, 0);
                                            status = XSendEvent(event.display, event.window, True, KeyPressMask, (XEvent *)&event);

                                            done = true;
                                            break;
                                        }
                                        XFreeStringList(list);
                                    }
                                }
                            }
                        }
                        status = Success;
                        XFree((char*) childrenWin);
                    }
                }
                XFree((char*) children);
                usleep(100 * 1000);
            }
            if (!done) {
                fprintf(stderr, "Force close dialog not found.\n");
            }
            XFree((char*) rootChildren);
        } else {
            fprintf(stderr, "Couldn't find window with the caption '%s' and request a close.\n", argv[1]);
        }
        XCloseDisplay(display);
    } else {
        fprintf(stderr, "Couldn't open X display.\n");
    }
    return done ? 0 : 1;
}
