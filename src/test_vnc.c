#include <rfb/rfbclient.h>

static int gotInitFB = 0;
static int gotUpdateFB = 0;
static unsigned char *frameBuffer = NULL;

static rfbBool initFB(rfbClient* client) {
    gotInitFB++;
    if (frameBuffer) free(frameBuffer);
    frameBuffer = malloc(client->width * client->height * client->format.bitsPerPixel / 8);
    client->frameBuffer = frameBuffer;
    return TRUE;
}

static void updateFB(rfbClient* cl, int x, int y, int w, int h) {
    gotUpdateFB++;
}

static void cleanup(rfbClient* cl) {
    if (cl) rfbClientCleanup(cl);
    if (frameBuffer) free(frameBuffer);
}

static char* password = NULL;
static char* read_password(rfbClient* client) {
  if (password) return strdup(password);
  return strdup("");
}

#define NUM_MESSAGES 5

int main(int argc,char** argv) {
    rfbClient* cl;
    int i, j;
    int result = 0;

    for (i = 1, j = 1; i < argc; i++) {
        if (!strcmp(argv[i], "-password") && i < argc - 1) {
            password = argv[++i];
        } else {
            if (i != j) argv[j] = argv[i];
            j++;
        }
    }
    argc = j;
    if (argc < 2) {
        fprintf(stderr, "Invalid parameters (hostname required).\n");
        return 1;
    }

    cl = rfbGetClient(8, 3, 4);
    if (!cl) {
        fprintf(stderr, "Couldn't get RFB client structure.\n");
        return 2;
    }
    cl->MallocFrameBuffer = initFB;
    cl->canHandleNewFBSize = TRUE;
    cl->GotFrameBufferUpdate = updateFB;
    cl->GetPassword = read_password;

    if(rfbInitClient(cl, &argc, argv)) {
        for (i = 1; i < NUM_MESSAGES; i++) {
            j = WaitForMessage(cl, 500);
            if (j < 0) {
                fprintf(stderr, "Couldn't get an RFB message.\n"); result = 11;
                break;
            }
            if (j) {
                if (!HandleRFBServerMessage(cl)) {
                    fprintf(stderr, "Couldn't handle an RFB message.\n"); result = 12;
                    break;
                }
                if (gotUpdateFB) break;
            } else i--;
        }
        if (result == 0 && gotInitFB == 0) {
            fprintf(stderr, "Frame buffer was not initialized as expected.\n"); result = 21;
        }
        if (result == 0 && gotUpdateFB == 0) {
            fprintf(stderr, "Frame buffer was not updated as expected.\n"); result = 22;
        }
        cleanup(cl);
    } else {
        fprintf(stderr, "Couldn't init RFB client.\n"); result = 3;
    }
    return result;
}
