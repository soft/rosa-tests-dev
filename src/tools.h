#pragma once

#include <string>
#include <iostream>
#include <chrono>

namespace std {

template<typename CHRONO_TIMESIZE = chrono::milliseconds>
class chronobot
{
    chrono::high_resolution_clock::time_point start = chrono::high_resolution_clock::now() ;
    ostream& out = cout ;

public:
    chronobot(ostream& out = cout) {};
    ~chronobot() { check() ; }
    auto check(bool quiet = true, string msg = "Your work is done for: " ) {
        auto end = chrono::high_resolution_clock::now() ;
        auto elapsed = chrono::duration_cast<CHRONO_TIMESIZE>(end - start).count() ;
        if ( not quiet )
            out << msg << elapsed << endl ;
        return elapsed;
    } 
};

#define ERR(msg) error(string(msg) + string(""), __LINE__)

inline void error( const string msg = "", int line = __LINE__, ostream& out = cerr ) {
    if   ( msg.empty() ) out << "ERROR on line: "    << line << endl;
    else                 out << line << ": ERROR = " << msg  << endl;
    exit(EXIT_FAILURE);
}

}