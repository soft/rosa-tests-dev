/*
 ┌───────────────────────────────────────────────────────────────────────────┐
 │                                                                           │
 │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
 │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
 │   @@@@@:::::::::@@@@@@@@@@@::::::@@@@@@@@@@@::::@@@@@@@@@@@@@:@@@@@@@@@   │
 │   @@@:------------::@@@@:----------::@@@@:--------::@@@@@@@:---:@@@@@@@   │
 │   @@@:---@@@@@@@@:--:@@---:@@@@@@@---:@@---:@@@@@:@@@@@@@@:-----@@@@@@@   │
 │   @@@:---@@@@@@@@:--:@:--:@@@@@@@@:---@@---:@@@@@@@@@@@@@@--:@:--@@@@@@   │
 │   @@@:---@@@@@@@:---@@:--:@@@@@@@@:---@@@:------:::@@@@@@---@@@--:@@@@@   │
 │   @@@:-----------::@@@:--:@@@@@@@@:---@@@@@@:::::---@@@@:--:@@@:--:@@@@   │
 │   @@@:---@@@@@:--:@@@@:--:@@@@@@@@:--:@@@@@@@@@@@:--:@@:-----------:@@@   │
 │   @@@:---@@@@@@:---:@@@:--:::@@@::--:@@::-:::@@::---@@:--:@@@@@@@---:@@   │
 │   @@@:::-@@@@@@@@::::@@@@:::-----::@@@@@@:::----::@@@::::@@@@@@@@:::-@@   │
 │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
 │   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   │
 │                                                                           │
 │                  Additional tools for ROSA Tests                          │
 │                                                                           │
 │   Copyright © 2020 LLC "NTC IT ROSA"                                      │
 │   License: GPLv3                                                          │
 │   Authors:                                                                │
 │       Michael Mosolov     <m.mosolov@rosalinux.ru>       2020             │
 │                                                                           │
 │   This program is free software; you can redistribute it and/or modify    │
 │   it under the terms of the GNU General Public License as published by    │
 │   the Free Software Foundation; either version 3, or (at your option)     │
 │   any later version.                                                      │
 │                                                                           │
 │   This program is distributed in the hope that it will be useful,         │
 │   but WITHOUT ANY WARRANTY; without even the implied warranty of          │
 │   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            │
 │   GNU General Public License for more details                             │
 │                                                                           │
 │   You should have received a copy of the GNU General Public License       │
 │   License along with this program; if not, write to the                   │
 │   Free Software Foundation, Inc.,                                         │
 │   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.           │
 │                                                                           │
 ╘═══════════════════════════════════════════════════════════════════════════╛
 ┌───────────────────────────────────────────────────────────────────────────┐
 │  This program is trying to modify a file opened by another process.       │
 ╘═══════════════════════════════════════════════════════════════════════════╛
*/

#include <iostream>
#include <string>
#include <fstream>
#include <filesystem>
#include <thread>
#include <boost/program_options.hpp>

#include <unistd.h>
//#include <fcntl.h>
//#include <sys/file.h>


using namespace std;
namespace po = boost::program_options;
namespace fs = std::filesystem;

bool DEBUG = false;

void AppInit( int argc, char *argv[] )
{
  po::options_description desc( "file-locking test [ 0.1 ]" );
  string epilog = "m.mosolov.rosalinux.ru // www.rosalinux.ru © 2020";

  desc.add_options()
    ( "help,h",  "Produce help message" )
    ( "debug,g", "Use debug output" )
  ;

  po::variables_map vars;
  po::store( po::parse_command_line( argc, argv, desc ), vars );
  po::notify( vars );

  if ( vars.count( "help" ) ) {
    cout << desc   << endl
         << epilog << endl;
    exit(0);
  }

  if ( vars.count( "debug" ) )
    DEBUG = true;

}

string fileChange( fs::path path, string text = "" )
{
  static fstream file;
  string         line {};

  if ( fs::exists( path ) ) file.open( path, ios::out | ios::in );
  else                      file.open( path, ios::out | ios::in | ios::trunc );

  if ( file.is_open() )
  {
    if ( text.compare( "==" ) == 0 ) {

      file.clear(); // clear eofbit

      getline( file, line, '\n' );
      file.seekg( ios::beg );
    }
    else if ( text.length() == 0 ) {
      file << "Dracula welcomes you!" << endl;
      file.seekg( ios::beg );
      file.seekp( ios::beg );

      getline( file, line, '\n' );
      file.seekg( ios::beg );

    } else {
      file.clear(); // clear eofbit

      file.seekp( 8 );
      file << text << flush;
      file.seekg( ios::beg );

      getline( file, line, '\n' );
      file.seekg( ios::beg );
    }
  }
  else {
    if ( DEBUG )
      cerr << "Can't open file!" << endl;
    return line;
  }

  return line;
}

int main( int argc, char *argv[] )
{
  AppInit( argc, argv );

  fs::path p = "/var/tmp/testfile.locked";

  pid_t pid = fork();

  if ( pid > 0 )
  {
    string parent_before = fileChange( p );

    if ( DEBUG )
      cout  << "parent before:\t"
            << parent_before
            << endl;

    this_thread::sleep_for( 5s );

    string parent_after = fileChange( p , "==" );

    if ( DEBUG )
      cout  << "parent after:\t"
            << parent_after
            << endl;

    if ( parent_after.compare( parent_before ) == 0 )
      return 1;

  }
  else if ( pid == 0 )
  {
    this_thread::sleep_for( 1s );

    string child = fileChange( p, "hates   " );

    if ( DEBUG )
      cout  << "after child:\t"
            << child
            << endl
            << "file " << p << " deleted by child:\t"
            << boolalpha
            << fs::remove( p )
            << endl;

    exit(0);
  }
  else
  {
    if ( DEBUG )
      cerr << "fork() failed" << endl;

    return 2;
  }

  if ( DEBUG )
    cout  << "file " << p << " deleted by parent:\t"
          << boolalpha << fs::remove( p )
          << endl;

  return 0;
}