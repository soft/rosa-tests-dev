# -*- coding: utf-8 -*-

from pathlib import Path
import unittest
import logging
import jsonconfig
from modules.kplpack.utils import Ticker, Indicator, collect_selinux_allows, colored, BackupDB

log = logging.getLogger( "rts.libtests" )

#! code refactoring strongly required

# 51, 93, 98, 134(лишнее начало базовой проверки)

def loggger_log_wrapper( log_level, text ):
  return log.info( text )


class BaseTest(unittest.TestCase):

  CURRTEST = jsonconfig.Test()  # sets in rosa-tests.py
  CMDLINE  = None               # ArgumentParser:    sets in rosa-tests.py
  TABLE    = None               # class StreamTable: sets in rosa-tests.py
  BACKUP   = None

  def setUp(self):

    self.timer                = Ticker(precision=3)
    self.base_tasks           = self.CURRTEST.has_tasks()
    self.base_tasks_desc_size = 35

    log.info    = self.TABLE.line
    log.debug   = self.TABLE.line
    log.warning = self.TABLE.line
    log.log     = loggger_log_wrapper

    self.BACKUP = BackupDB(Path('/var/log/rosa-tests/backups.json'))

    self.timer.start()

    # if self.CMDLINE.need_safe:
    #   self.overlay = Overlay()
    #   self.overlay.enable()

    self.TABLE.split()
    self.TABLE.cell( 'Тест {} - {}'.format( self.CURRTEST.id, self.CURRTEST.name ) )

    self.TABLE.split()
    self.TABLE.cell( self.CURRTEST.description, '<' )

    if self.base_tasks == 1 and self.CURRTEST.dmesg:
      pass
    elif self.base_tasks:
      self.TABLE.split()
      self.TABLE.cell( 'Начало базовой проверки' )


    if len( self.CURRTEST.kconfig ) > 0 :

      self.TABLE.split( self.base_tasks_desc_size )
      self.TABLE.cell( 'Проверка конфигурации ядра', '<')

      err = self.CURRTEST.check_kconfig()
      self.assert_( err < 0, 'Проверка конфигурации ядра не пройдена: параметр - {}'
                             .format( self.CURRTEST.kconfig[err] ) )

      self.TABLE.cell( colored('OK', 10) )


    if len( self.CURRTEST.package ) > 0:

      self.TABLE.split( self.base_tasks_desc_size )
      self.TABLE.cell( 'Проверка установленных пакетов', '<')

      err = self.CURRTEST.check_package()
      self.assert_( err < 0, 'Проверка установленных пакетов не пройдена: параметр - {}'
                             .format( self.CURRTEST.package[err] ) )

      self.TABLE.cell( colored('OK', 10) )

    if len( self.CURRTEST.kmodule ) > 0:

      self.TABLE.split( self.base_tasks_desc_size )
      self.TABLE.cell( 'Проверка наличия модулей ядра', '<' )

      err = self.CURRTEST.check_kmodule()
      self.assert_( err < 0, 'Проверка наличия модулей ядра не пройдена: параметр - {}'
                             .format( self.CURRTEST.kmodule[err] ) )

      self.TABLE.cell( colored('OK', 10) )


    if len( self.CURRTEST.service ) > 0:

      self.TABLE.split( self.base_tasks_desc_size )
      self.TABLE.cell( 'Проверка доступных сервисов', '<')

      err = self.CURRTEST.check_service()
      self.assert_( err < 0, 'Проверка доступных сервисов не пройдена: параметр - {}'
                             .format( self.CURRTEST.service[err] ) )

      self.TABLE.cell( colored('OK', 10) )


    if len( self.CURRTEST.kmodule ) > 0:

      self.TABLE.split( self.base_tasks_desc_size )
      self.TABLE.cell( 'Подключение модулей ядра', '<')

      self.CURRTEST.clear_dmesg()
      err = self.CURRTEST.run_kmodules()
      self.assert_( err < 0, 'Подключение модулей ядра вызвало ошибки: параметр - {}'
                             .format( self.CURRTEST.kmodule[err] ) )

      self.TABLE.cell( colored('OK', 10) )


    if len( self.CURRTEST.service ) > 0:

      self.TABLE.split( self.base_tasks_desc_size )
      self.TABLE.cell( 'Запуск сервисов', '<' )

      err = self.CURRTEST.run_services()
      self.assert_( err < 0, 'Запуск сервисов вызвал ошибки: параметр - {}'
                             .format( self.CURRTEST.service[err] ) )

      self.TABLE.cell( colored('OK', 10) )


    self.TABLE.split()
    self.TABLE.cell( 'Ход тестирования' )
    self.TABLE.split()

    self.addCleanup( self.endStage )

  def endStage(self):

    if len( self.CURRTEST.kmodule ) > 0 \
    or len( self.CURRTEST.dmesg )   > 0 \
    or len( self.CURRTEST.service ) > 0:

      self.TABLE.split()
      self.TABLE.cell( 'Завершение базовой проверки' )


    if len( self.CURRTEST.kmodule ) > 0:

      self.TABLE.split( self.base_tasks_desc_size )
      self.TABLE.cell( 'Отключение модулей ядра', '<' )

      err = self.CURRTEST.rem_kmodules()
      self.assert_( err < 0, 'Отключение модулей ядра вызвало ошибки: параметр - {}'
                             .format( self.CURRTEST.kmodule[err] ) )

      self.TABLE.cell( colored('OK', 10) )


    if len( self.CURRTEST.dmesg ) > 0:

      self.TABLE.split( self.base_tasks_desc_size )
      self.TABLE.cell( 'Проверка сообщений dmesg', '<' )

      err = self.CURRTEST.check_dmesg()
      self.assert_( err < 0, 'Ошибка при проверке dmesg: параметр - {}'
                             .format( self.CURRTEST.dmesg[err] ) )

      self.TABLE.cell( colored('OK', 10) )


    if len( self.CURRTEST.service ) > 0:

      self.TABLE.split( self.base_tasks_desc_size )
      self.TABLE.cell( 'Остановка сервисов', '<' )

      err = self.CURRTEST.stop_services()
      self.assert_( err < 0, 'Остановка сервисов вызвала ошибки: параметр - {}'
                             .format( self.CURRTEST.service[err] ) )

      self.TABLE.cell( colored('OK', 10) )

    if self.CMDLINE.need_selinux_rules:

      self.TABLE.split().cell( 'Сбор разрешений SElinux' )

      with Indicator( '  Сбор разрешений SElinux' ):
        for rule in collect_selinux_allows( sec=self.timer.tock() ):
          log.info( '  ' + rule )

    # if self.CMDLINE.need_safe:
    #   log.info( '\n {:┄^50}\n '.format(' Безопасный режим ') )

    #   for l in self.overlay.__str__().splitlines():
    #     log.info( '  ' + l )

    end_time = '{} сек.'.format( self.timer.tock() )
    self.TABLE.split( self.base_tasks_desc_size )\
      .cell( 'Выполнение теста длилось' )\
      .cell( end_time )

    self.TABLE.split( self.base_tasks_desc_size )\
      .cell( 'ЗАКЛЮЧЕНИЕ' )

  def runTest(self):
    log.info( '  Generic-тест. Дополнительные проверки не проводятся.' )