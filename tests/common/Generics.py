# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

class testATM(BaseTest): pass
class testServiceRouting(BaseTest): pass
class testSupportMPLS(BaseTest): pass
class testX25FrameRelayHDLC(BaseTest): pass
class testCLI(BaseTest): pass
class testServiceSmartCard(BaseTest): pass
class testServiceVconsole(BaseTest): pass
class testWiFi(BaseTest): pass
class testMail(BaseTest): pass
class testSPI(BaseTest): pass
class testFilemanager(BaseTest): pass
class testImageConverter(BaseTest): pass
class testCoreutils(BaseTest): pass
class testStorageManage(BaseTest): pass
class testUserAccountsManage(BaseTest): pass
class testProcStat(BaseTest): pass
class testPackageManager(BaseTest): pass
class testHardwareInfo(BaseTest): pass
class testOSConfigTools(BaseTest): pass
class testDhcpNfsEtc(BaseTest): pass
class testRdp(BaseTest): pass
class testSLAB(BaseTest): pass
class testServiceInit(BaseTest): pass
class testServiceCron(BaseTest): pass
class testServiceAtd(BaseTest): pass
class testServiceCups(BaseTest): pass
class testServiceSshd(BaseTest): pass
class testServiceVncServer(BaseTest): pass
class testServiceLdapClient(BaseTest): pass
class testServiceLocalMsgBus(BaseTest): pass
class testServiceNtpd(BaseTest): pass
class testServiceNfsExtAttr(BaseTest): pass
class testServiceVsftpd(BaseTest): pass
class testServiceSnmp(BaseTest): pass
class testServiceRsync(BaseTest): pass
class testServiceSyslog(BaseTest): pass
class testServiceTMask(BaseTest): pass
class testServicePcscd(BaseTest): pass
class testInstalledOfficeWriter(BaseTest): pass
class testInstalledOfficeCalc(BaseTest): pass
class testInstalledOfficeImpress(BaseTest): pass
class testInstalledOfficeDraw(BaseTest): pass
class testInstalledScanApp(BaseTest): pass
class testInstalledMediaPlayer(BaseTest): pass
class testInstalledPdfViewerConverter(BaseTest): pass
class testInstalledArk(BaseTest): pass
class testInstalledJavaRE(BaseTest): pass
class testInstalledWine(BaseTest): pass
class testInstalledMailClient(BaseTest): pass
class testInstalledBackup(BaseTest): pass
class testInstalledCalculator(BaseTest): pass
class testInstalledConsole(BaseTest): pass
class testInstalledFileManager(BaseTest): pass
class testInstalledControlCenter(BaseTest): pass
class testInstalledXmlTool(BaseTest): pass
class testDiagnosticSupport(BaseTest): pass
class testMailServer(BaseTest): pass
class testFsProgs(BaseTest): pass

