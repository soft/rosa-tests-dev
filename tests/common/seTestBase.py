# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import selinux
import sys
import os

from modules.kplpack.utils import StreamToLog

class seTestBase(BaseTest):
  """
  Базовый класс тестов selinux
  """

  se_enforce    = selinux.security_getenforce()
  stream_backup = sys.stderr

  def setUp(self):
      super(seTestBase, self).setUp()
      sys.stderr = StreamToLog(log, logging.DEBUG)

  def runcon(self, context, program, *args):
      try:
          errcode = selinux.setexeccon(context)
      except OSError as e:
          log.error('Не установлен контекст безопасности: {}, {}'.format( context, e.errno ) )
          errcode = e.errno
      if errcode:
          return errcode
      progargs = (program,) + args
      try:
          errcode = os.spawnv(os.P_WAIT, program, progargs)
      except OSError as e:
          log.error('Потомок не запущен ' + str(progargs) + ': ' +
                        e.message)
          errcode = e.errno
      return errcode

  def __del__(self):
    sys.stderr = self.stream_backup
