# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import multiprocessing
import subprocess
from modules.kplpack.atspi import AT
from modules.kplpack.utils import SystemCtl
from modules.kplpack.utils import ConRun
from modules.kplpack.utils import processEx, StreamToLog, User


class testApache(BaseTest):

  site_path_www   = '/var/www/html'
  test_page       = '/usr/share/rosa-tests/test_data/web/test.html'
  orig_page       = '/var/www/html/index.html'
  test_util       = '/usr/lib64/rosa-tests/xtool'
  test_util_flags = ' "TEST PASSED" -t 15000'

  def runTest(self):

    log.info('Начинается тестирование сервера Apache')

    log.info('  Создается резервная копия страницы index.html на сервере...')
    with self.BACKUP(self.orig_page, restore_after=True):
      self.BACKUP.substitute(self.test_page)

      log.info('  Перезапуск сервера Apache...')
      sctl = SystemCtl(StreamToLog(log))
      sctl.stop('httpd')
      if sctl.start('httpd') != 0:
        self.fail('Не удалось перезапустить httpd.')

      u = User()
      with ConRun(u.uid, u.gid) as mem:
        AT.force_atspi()
        test()

    if mem[0] == '0':
      log.info('Тест успешной пройден.')
    else:
      self.fail('Тест не получил ожидаемый вывод.')


  def tearDown(self):
    pass

def test():
  log.info('  Запускается браузер Chromium...')

  proc = multiprocessing.Process(
    target=subprocess.run,
    args=(['chromium-browser', 'http://127.0.0.1', '--no-sandbox'],)
  )
  proc.start()

  app = AT('Chromium', app_init_time=10, timeout=20)

  frame = app.el('frame', 'TEST PASSED', wait=20, in_breadth=True)

  if not frame:
    log.info('Тестовая страница не была открыта!')
    print(1)
    return
  else:
    log.info('  Тестовая страница успешно открыта.')

  log.info('  Закрытие браузера Chromium...')
  processEx('chrome', kill=True)

  print(0)
  return