# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import sys
import shutil
import os

from modules.kplpack.utils import Terminal, StreamToLog, User, get_display_privileges, processEx

class testApacheComplex(BaseTest):

  url             = 'http://localhost'
  site_path_www   = '/var/www/html/'
  test_page       = '/usr/share/rosa-tests/test_data/web/index.py'
  test_conf       = '/usr/share/rosa-tests/test_data/web/apache.conf'

  orig_page       = '/var/www/html/index.html'
  orig_conf       = '/etc/httpd/conf/httpd.conf'

  test_values     = ['hello tester!','time to done your work','Bye!']
  test_user       = 'tester'
  test_pass       = 'TopSecret'
  test_db         = 'testdb'
  test_table      = 'testtable'
  test_util       = '/usr/lib64/rosa-tests/xtool'
  stream_backup   = sys.stderr

  postgresql      = '' # see below
  username        = 'rosa-test-user'

  def genUrlGET(self):

    out = self.url + '/?'

    out += 'db={}&'.format( self.test_db )
    out += 'u={}&'.format( self.test_user )
    out += 'p={}&'.format( self.test_pass )
    out += 'h=localhost&'

    for i, v in enumerate( self.test_values ):
      out += 'f{}={}&'.format( i, v ).replace(' ', '+')
    out = out[:-1] # delete last coma

    return out

  def genInsertInto(self):

    out = 'insert into {}(id, message) values '.format( self.test_table )

    for i, v in enumerate( self.test_values ):
      out += '({},\'{}\'),'.format( i, v )
    out = out[:-1] # delete last coma

    return out

  def runTest(self):

    log.info('Начинается комлексное тестирование веб-сервера Apache + СУБД PostgreSQL + Python')

    for s in self.CURRTEST.service:
      if 'postgres' in s:
        self.postgresql = s

    log.info('  Создание стартовой страницы веб-сервера...')
    shutil.copy2( self.test_page, self.site_path_www)
    os.chmod( self.site_path_www + os.path.split( self.test_page )[1], int( '755', base=8) )

    log.info('  Создание тестового пользователя {}...'.format(self.username))
    with User(self.username, password='Gb220_kJs#65', home='/home/{}'.format(self.username)) as user:

      log.info('  Создание резервной копии конфигурации веб-сервера...')
      with self.BACKUP(self.orig_conf, restore_after=True):
        self.BACKUP.substitute(self.test_conf)

        log.info('  Получение доступа к дисплею для root...')
        with get_display_privileges():
          plan = Terminal( StreamToLog( log ), flush=False)

          plan.add( 'systemctl restart {}'.format(self.postgresql)) \
              .info( '  Перезапуск СУБД PostgreSQL [{}]...\n'.format(self.postgresql))

          plan.add( 'echo "create user {} with password \'{}\'" | sudo -u postgres psql'
                    .format( self.test_user, self.test_pass )) \
              .info('  Создание тестового пользователя базы данных...\n')

          # plan.add( 'echo "\du" | sudo -u postgres psql'
          #           .format( self.test_user, self.test_pass ))

          plan.add( 'echo "create database {}" | sudo -u postgres psql'
                    .format( self.test_db )) \
              .info('  Создание тестовой базы данных...\n')

          plan.add( 'echo "grant all privileges on database {} to {}" | sudo -u postgres psql'
                    .format( self.test_db, self.test_user )) \
              .info('  Предоставление всех привилегий над базой данных тестовому пользователю...\n')

          plan.add( 'systemctl restart {}'.format(self.postgresql)) \
              .info( '  Перезапуск базы данных PostgreSQL [{}]...\n'.format(self.postgresql))

          plan.add( 'echo "create table {}(id int PRIMARY KEY, message TEXT)" | sudo -u postgres psql -h localhost {} {}'
                    .format( self.test_table, self.test_db, self.test_user )) \
              .info( '  Создание тестовой таблицы...\n')

          plan.add( 'echo "{}" | sudo -u postgres psql -h localhost {} {}'
                    .format( self.genInsertInto(), self.test_db, self.test_user )) \
              .info( '  Создание тестовых данных...\n')

          plan.add( 'systemctl restart httpd.service' ).info( '  Перезагрузка веб-сервера...\n')
          plan.add( 'systemctl restart httpd' )
          plan.add( 'systemctl restart {}'.format(self.postgresql) )

          plan.add( 'chromium-browser "{}" --no-sandbox'
                    .format( self.genUrlGET() ), separated=True, ignore_output=True, pause_before=7 ) \
              .info( '  Запуск браузера для проверки веб-сервера...\n')

          plan.add( self.test_util + ' "TEST PASSED!" -t 5000', separated=True, pause_before=15 ) \
              .info( '  Запуск проверяющей утилиты в режиме ожидания 20 сек...\n')

          plan.run()

    for l in plan.log:
      self.assertEqual( l[0], 0, 'Во время тестирования произошла ошибка:\n{}'.format( l[1] ) )


  def tearDown(self):

    log.info('  Принудительное завершение работы браузера (если требуется)...')
    processEx('chrome', kill=True )

    log.info('  Удаление тестовой базы данных и тестового пользователя...')
    planb = Terminal( stream=sys.stderr )

    planb.add( 'echo "drop database {}" | sudo -u postgres psql'
               .format( self.test_db ))

    planb.add( 'echo "drop user {}" | sudo -u postgres psql'
               .format( self.test_user ))

    planb.run()

    log.info('  Удаление тестовой стартовой страницы веб-сервера...')
    os.remove( self.site_path_www + os.path.split( self.test_page )[1] )