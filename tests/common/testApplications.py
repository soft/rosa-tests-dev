# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os

from subprocess import PIPE, Popen, STDOUT, run
from modules.kplpack.utils import ConRun, CountDown, Indicator, User
from modules.kplpack.utils import colored as I
from pathlib import Path


# %f - A single file name (including the path), even if multiple files are selected.
#      The system reading the desktop entry should recognize that the program in question
#      cannot handle multiple file arguments, and it should should probably spawn and
#      execute multiple copies of a program for each selected file if the program is not
#      able to handle additional file arguments. If files are not on the local file system
#      (i.e. are on HTTP or FTP locations), the files will be copied to the local file
#      system and %f will be expanded to point at the temporary file. Used for programs
#      that do not understand the URL syntax.
# %F - A list of files. Use for apps that can open several local files at once. Each file
#      is passed as a separate argument to the executable program.
# %u - A single URL. Local files may either be passed as file: URLs or as file path.
# %U - A list of URLs. Each URL is passed as a separate argument to the executable program.
#      Local files may either be passed as file: URLs or as file path.
# %i - The Icon key of the desktop entry expanded as two arguments, first --icon and then
#      the value of the Icon key. Should not expand to any arguments if the Icon key is
#      empty or missing.
# %c - The translated name of the application as listed in the appropriate Name key in the
#      desktop entry.
# %k - The location of the desktop file as either a URI (if for example gotten from the
#      vfolder system) or a local filename or empty if no location is known.

# %d, %D, %n, %v, %m - Deprecated.
# A command line may contain at most one %f, %u, %F or %U field code. If the application
# should not open any file the %f, %u, %F and %U field codes must be removed from the
# command line and ignored.

# Field codes must not be used inside a quoted argument, the result of field code expansion inside a quoted argument is undefined. The %F and %U field codes may only be used as an argument on their own.


# PS:
#    .desktop файлы могут содержать аргументы в строке Exec, обычно эти .desktop файлы
#    являются элементами контекстного меню, поэтому запуск без агрументов таких файлов
#    проверяется, но без учета синтаксических ошибок (например usage,wrong arguments).
#    При запуске таких файлов строка Exec очищается от аргументов.

class testApplications(BaseTest):

  apps_dir    = '/usr/share/applications/'
  field_codes = [ r'%f', r'%F', r'%u', r'%U', r'%i', r'%c', r'%k',
                  # deprecated:
                  r'%d', r'%D', r'%n', r'%v', r'%m',
                  # unknown:
                  r'-caption', '""' ]

  se_users    = [ 'user_u', 'auditor_u', 'sysadm_u', 'aib_u' ]

  white_list  = [ 'XFdrake' ]

  def runTest(self):

    execs = set()

    for folder, _, file_name in os.walk( Path( self.apps_dir ).as_posix(), followlinks=False ):
      for name in file_name:

        if '.desktop' not in name:
          continue

        entry = Path( '{}/{}'.format( folder, name ) )
        if entry.stem in self.white_list:
          log.info( '  Обнаружено приложение из белого списка: {}. Пропускаем.'.format( entry.stem ) )
          continue
        else:
          log.info( '  Обнаружено приложение: {}'.format( entry.stem ) )

        try:
          with entry.open('rb') as f:
            execs.update( self.extract( f.read() ) )

        except (PermissionError, FileNotFoundError) as e:
          self.fail( 'Нет доступа к ярлыку приложения {}'.format( entry.as_posix() ) )

    apps_number = len(execs)
    log.info( '  Всего обнаружено {} приложений.'.format( apps_number ) )

    FAILS = 0

    for seusr in self.se_users:
      with User( 'appuser', 't3stThere_', home='/home/appuser', se_user=seusr, se_range='s0' ) as usr:
        with Indicator( ' Проверка пользовательских приложений [{}] : '.format( seusr ) )   as progress:

          for i,entry in enumerate( execs ):
            progress.set_suffix( '[{}] {} из {}'.format( I(entry,33), i+1, apps_number ) + ' | {elaps}' )

            errors = self.runapp( entry, usr )
            if len(errors) > 0:
              log.info('  Возникли проблемы: ')

              for n,e in enumerate(errors):
                log.info( '    {}. {} '.format( n, e ) )

              FAILS += 1

            else:
              log.info( '  Приложение отработало нормально.' )


    self.assert_( FAILS == 0, 'Кол-во проблемных приложений: {}'.format( FAILS ) )

  def remove_field_codes(self, line:str):

    cleaned = line.strip()

    for code in self.field_codes:
      cleaned = cleaned.replace( code, '' ).strip()

    return cleaned

  def extract(self, content:bytes):
    """ extract exec-lines from .desktop file """

    exec_lines = []
    pattern    = b'Exec='

    for line in content.splitlines():
      if line.startswith( pattern ):
        decoded_line = line[len(pattern):].decode('UTF-8')
        cleaned_line = self.remove_field_codes( decoded_line )
        exec_lines.append( cleaned_line )

    return exec_lines

  def runapp(self, entry:str, user:User):

    errors  = []

    log.info( '  Испытание {} | {}'.format( I(entry,33), user.context.full ) )

    with ConRun( uid=user.uid, gid=user.gid, raw_context=user.context.full, memory_bytes=3000, wait=False ) as mem:
      process   = Popen( entry, shell=True, universal_newlines=True, stdout=PIPE, stderr=STDOUT )
      stdout, _ = process.communicate() # waiting process
      # см описание выше - PS:
      print( 'ERR:Работа {} была неожиданно прервана! {} - {}'.format( entry, process.returncode, stdout ) )

    for _ in CountDown(15):
      pass # process works

    run( [ 'pkill','-KILL', '-u', user.name ], check=True )

    for l in mem[1:]: # first element is pid
      if l.strip() == '':
        continue

      errors.append( l )

    return errors

  def tearDown(self):
    pass
