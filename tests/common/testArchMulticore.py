# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os

from subprocess import Popen, PIPE, STDOUT, getstatusoutput
from time import sleep

class testArchMulticore(BaseTest):
    """
    Тест поддержки 64-разрядных архитектур х86_64, в т. ч.
    многоядерных и многопроцессорных конфигураций
    """
    samplename = "python"

    def runTest(self):
        if getCPUsNum() == 1:
            self.fail("В системе есть только одно процессорное ядро.")

        cpu_mask = 2
        proc = Popen("taskset %d %s" % (cpu_mask, self.samplename),
                                stdout=PIPE,
                                stderr=STDOUT, shell=True)
        sleep(1)
        errcode, output = getstatusoutput("taskset -p %s" % str(proc.pid))
        proc.kill()

        self.assertEqual(errcode, 0, "Получить привязку процесса к CPU не удалось.\n" + output )

        log.debug("  Привязка процесса к CPU: %s" % output )
        cpu_mask_cur = int(output.split(": ")[1])

        self.assertEqual(cpu_mask_cur, cpu_mask,
                         "Маска привязки процесса к ядрам (%d)"
                         " отличается от заданной (%d)." %
                         (cpu_mask_cur, cpu_mask))

def getCPUsNum():
    """ Возвращает число процессоров, установленных в системе """
    if hasattr(os, "sysconf"):
        if "SC_NPROCESSORS_ONLN" in os.sysconf_names:
            n = os.sysconf("SC_NPROCESSORS_ONLN")
            if isinstance(n, int) and n > 0:
                return n
    return 1
