# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import re

from subprocess import getstatusoutput
from modules.kplpack.utils import x, Return

class testAutologinApmdz(BaseTest):
    """
    Проверка PAM-модуля автовхода с устройством АПМДЗ
    """

    def runTest(self):
        from modules.pexpect import run
        log.info('Начинается проверка работы модуля автовхода.')
        pamModule = 'pam_apmdz.so'
        kdePamCfg = '/etc/pam.d/kde'
        self.assertTrue(os.path.exists(kdePamCfg),
                        'Не найден файл конфигурации %s.' % kdePamCfg)
        with open(kdePamCfg, 'r') as f:
            pamConfig = f.read()
            modFound = None
            for line in pamConfig.split('\n'):
                modFound = re.search( r'\s+' + pamModule.replace('.', '\\.') + r'\s*(.*)$', line )
                if modFound:
                    modParams = modFound.group(1)
                    break
            self.assertTrue( modFound is not None, 'В файле конфигурации %s не подключен модуль %s.'
                            (kdePamCfg, pamModule))
        pamModulePath = '/lib/security/' + pamModule
        if not (os.path.exists(pamModulePath)):
            pamModulePath = '/lib64/security/' + pamModule
            self.assertTrue(os.path.exists(pamModulePath),
                            'Модуль %s в системе не найден.' % pamModule)
        errcode, output = getstatusoutput("%s %s" % (pamModulePath, modParams))
        self.assertEqual(errcode, 0,
                         'При обращении к PAM-модулю %s произошла ошибка.\n%s' %
                         (pamModulePath, output))
        username = output
        log.info('PAM-модуль возвратил имя пользователя: %s.' % output)
        log.info('Выполняется тестирование аутентификации...')
        (output, errcode) = run('pamtester kde "%s" authenticate' % username,
                                withexitstatus=True,
                                events={'(?i)password:': '\x04\n'})
        self.assertEqual(errcode, 0,
                         ('Аутентификация с модулем автовхода не выполнена '
                          '(код состояния %d).\n' % errcode) +
                         'Сообщения программы проверки:\n' + output)

        log.info('Проверка работы PAM-модуля автовхода '
                    'с устройством АПМДЗ пройдена успешно.')
