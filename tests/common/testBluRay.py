# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os

from modules.kplpack.utils import x, Return
from subprocess import getstatusoutput
from tempfile import mkdtemp


class testBluRay(BaseTest):
    """
    Тест поддержки поддержки устройств чтения с оптических носителей Blu-Ray
    """

    def setUp(self):
        super(testBluRay, self).setUp()

        mPath = os.path.dirname(os.path.realpath(__file__))
        self.iso = os.path.join(mPath, '../../test_data', 'udf', 'udf_2.50.iso')
        self.mntDir = mkdtemp()

    def runTest(self):
        log.info( '  Начинается проверка файловой системы UDF 2.50, применяемой на дисках Blu-ray.' )
        log.debug( '  Подключение ISO-образа с файловой системой UDF 2.50...' )

        errcode, output = getstatusoutput( 'mount -o loop -t udf %s %s' % (self.iso, self.mntDir) )

        self.assertEqual(errcode, 0, 'Подключить образ диска с UDF 2.50 не удалось.\n' + output)

        log.debug('  Чтение тестового файла...')
        with open(os.path.join(self.mntDir, 'README'), 'r') as f:
            file_content = f.read()

        log.debug( '  Содержимое тестового файла:\n' + file_content )
        log.info( 'Проверка поддержки файловой системы UDF 2.50 пройдена успешно.' )

    def tearDown(self):
        getstatusoutput( 'umount %s' % self.mntDir )
