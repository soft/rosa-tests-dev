# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from tempfile import mkdtemp
from modules.kplpack.utils import x, Return, Indicator

class testCdRom(BaseTest):
  """
  Тест поддержки устройств чтения с оптических носителей CD+/-, DVD+/-
  """

  def setUp(self):
    super(testCdRom, self).setUp()
    self.mntDir = mkdtemp()

  def runTest(self):

    with Indicator( ' Тестирование устройств чтения оптических дисков...' ):
      log.info('  Поиск доступного устройства чтения оптических дисков...')

      roms = dict()

      code, out = x( "cat /proc/sys/dev/cdrom/info | grep 'drive name'", Return.codeout )
      self.assert_( code == 0, 'При поиске устройства чтения оптических дисков произошла ошибка [{}]: {} '.format( code, out ) )

      for name in out.split( ':' )[-1].split():
        roms[ '/dev/' + name ] = 'noinfo'

      self.assert_( len(roms) > 0, '  Ни одного устройства чтения оптических дисков не обнаружено')

      for r in roms.keys():
        code, out = x( "udisksctl info -b {} | grep 'Drive'".format( r ), Return.codeout )
        self.assert_( code == 0, 'При получении информации об устройстве "{}" произошла ошибка [{}]: {} '.format( r, code, out ) )

        roms[r] = out.split( '/' )[-1]

        log.info( '  Обнаружено устройство чтения оптических дисков: {} : {}'.format( r, roms[r] ) )
        log.info( '  Производится попытка монтирования в {}...'.format( self.mntDir ) )

        code, out = x( 'mount {} {}'.format( r, self.mntDir ), Return.codeout )
        self.assert_( code == 0, 'При монтировании устройства произошла ошибка [{}]: {} '.format( code, out ) )

        log.info( '  Производится размонтирование устройства...' )

        code, out = x( 'umount {}'.format( self.mntDir ), Return.codeout )
        self.assert_( code == 0, 'При размонтировании устройства произошла ошибка [{}]: {} '.format( code, out ) )

        log.info( '  Устройство прошло проверку.' )

        #bonus
        x( 'eject' )
