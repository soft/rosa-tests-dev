# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os

from modules.kplpack.utils import Terminal, StreamToLog

class testCupsFilters(BaseTest):
  """
  Проверка работоспособности фильтров Cups
  """
    # TODO:
    # try to test these ones, as well as other files in /usr/lib/cups/filter/ installed by cups itself:
    #
    # /usr/lib/cups/filter/commandtoescpx
    # /usr/lib/cups/filter/commandtopclx
    # /usr/lib/cups/filter/gstopxl
    # /usr/lib/cups/filter/gstoraster
    # /usr/lib/cups/filter/urftopdf
    # /usr/lib/cups/filter/rastertoescpx
    # /usr/lib/cups/filter/pdftoijs
    # /usr/lib/cups/filter/pdftoippprinter
    # /usr/lib/cups/filter/pdftoopvp
    # /usr/lib/cups/filter/textonly

  def setUp(self):
    super(testCupsFilters, self).setUp()

    mPath     = os.path.dirname(os.path.realpath(__file__))
    test_data = os.path.join(mPath, '../../test_data')

    self.pdf    = os.path.join( test_data, 'document.pdf')
    self.ps     = os.path.join( test_data, 'document.ps')
    self.raster = os.path.join( test_data, 'document.raster')
    self.txt    = os.path.join( test_data, 'document.ps')
    self.image  = os.path.join( test_data, 'images', 'test.png')
    self.banner = os.path.join( test_data, 'print', 'standard.banner')

  def runTest(self):
    log.info('Начинается проверка работоспособности фильтров Cups.')

    #Опции - произвольные, на работоспособность фильтра это влиять не должно
    with Terminal( StreamToLog( log ) ) as t:
      t.error('  Во время тестирования cups-фильтров произошла ошибка [{code}]: {msg}.')
      t.add( '/usr/lib/cups/filter/pdftops 1 2 3 1 5 {}'.format( self.pdf ), ignore_output=True)\
        .info('  PDF -> PS\n')
      t.add( '/usr/lib/cups/filter/pdftopdf 1 2 3 1 5 {}'.format( self.pdf ) , ignore_output=True)\
        .info('  PDF -> PDF\n')
      t.add( '/usr/lib/cups/filter/pdftoraster 1 2 3 1 5 {}'.format( self.pdf ), ignore_output=True)\
        .info('  PDF -> RASTER\n')
      t.add( '/usr/lib/cups/filter/imagetops 1 2 3 1 5 {}'.format( self.image ), ignore_output=True)\
        .info('  IMAGE -> PS\n')
      t.add( '/usr/lib/cups/filter/imagetopdf 1 2 3 1 5 {}'.format( self.image ), ignore_output=True)\
        .info('  IMAGE -> PDF\n')
      t.add( '/usr/lib/cups/filter/imagetoraster 1 2 3 1 5 {}'.format( self.image ), ignore_output=True)\
        .info('  IMAGE -> RASTER\n')
      t.add( '/usr/lib/cups/filter/bannertopdf 1 2 3 1 5 {}'.format( self.banner ), ignore_output=True)\
        .info('  BANNER -> PDF\n')
      t.add( '/usr/lib/cups/filter/rastertopdf 1 2 3 1 5 {}'.format( self.raster ), ignore_output=True)\
        .info('  RASTER -> PDF\n')
      t.add( '/usr/lib/cups/filter/rastertopclx 1 2 3 1 5 {}'.format( self.raster ), ignore_output=True)\
        .info('  RASTER -> PCLX\n')
      t.add( '/usr/lib/cups/filter/pstotiff 1 2 3 1 5 {}'.format( self.ps ), ignore_output=True)\
        .info('  PS -> TIFF\n')
      t.add( '/usr/lib/cups/filter/texttops 1 2 3 1 5 {}'.format( self.txt ), ignore_output=True)\
        .info('  TEXT -> PS\n')
      t.add( '/usr/lib/cups/filter/texttopdf 1 2 3 1 5 {}'.format( self.txt ), ignore_output=True)\
        .info('  TEXT -> PDF\n')

    self.assertEqual( t.fail[0], 0, 'Произошла ошибка:{}'.format( t.fail[1] ) )

    log.info('Проверка работоспособности фильтров Cups пройдена успешно.')

