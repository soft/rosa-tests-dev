# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from pathlib import Path
from subprocess import getstatusoutput
from tempfile import mkdtemp

class testFS(BaseTest):
  """
  Тест поддержки журналируемой файловой системы
  """

  def setUp(self):
    super(testFS, self).setUp()

    self.diskFile = Path('/var/tmp/rosa-tests/testFSF')
    self.device = None
    self.mntpoint = None

  def runTest(self):

    self.diskFile.parent.mkdir( parents=True, exist_ok=True )
    self.diskFile.touch( exist_ok=True )

    errcode, output = getstatusoutput( "dd if=/dev/zero of={} bs=1024 count=30720".format( self.diskFile.as_posix() ) )

    self.assertEqual(errcode, 0, output)
    # find and change free loop device
    errcode, self.device = getstatusoutput("losetup -f")
    self.assertEqual(errcode, 0, self.device)
    # attach loopback device with disk file
    errcode, output = getstatusoutput("losetup %s %s" %
                                      (self.device, self.diskFile))
    self.assertEqual(errcode, 0, output)
    # create a EXT3 file system with 1% reserved block count
    #  on the loopback device
    errcode, output = getstatusoutput("mkfs -t ext4 -m 1 -v %s" %
                                      self.device)
    self.assertEqual(errcode, 0,
                      "Ошибка создания файловой системы:\n%s" % output)
    # create a directory (as mount point)
    self.mntpoint = mkdtemp()
    # mount the loopback device (disk file) to mntpoint
    errcode, output = getstatusoutput("mount -t ext4 %s %s" %
                                      (self.device, self.mntpoint))
    self.assertEqual(errcode, 0, "Ошибка монтирования:\n%s" % output)

  def release(self):
    if self.mntpoint:
      getstatusoutput("umount %s" % self.mntpoint)
      self.mntpoint = None
    if self.device:
      getstatusoutput("losetup -d %s" % self.device)
      self.device = None
    try:
      self.diskFile.unlink()
    except OSError as e:
      log.exception(e.message)

  def tearDown(self):
    self.release()
