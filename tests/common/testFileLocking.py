# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from modules.kplpack.utils import x, Return

class testFileLocking(BaseTest):
  """ #536 """

  tool = '/usr/lib64/rosa-tests/file-locking' + ' -g'

  def runTest(self):

    res = x( self.tool, Return.process )

    log.info( '  Вывод проверяющей утилиты:' )
    for l in res.stdout.readlines():
      log.info( '    ' + l.strip('\n') )

    self.assert_( res.returncode == 0, 'Родительский процесс потерял доступ к открытому файлу.' )

    log.info( '  Родительский процесс смог прочитать свой открытый файл после модификации и удаления его процессом-потомком.' )
