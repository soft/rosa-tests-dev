# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

class testI2C(BaseTest):

  def runTest(self):
    code, out = super().CURRTEST.run_kmodule(0)
    self.assert_( code == 0, 'При запуске модуля ядра {} произошла ошибка [{}]: {}'
                             .format( super().CURRTEST.kmodule[0], code, out ) )
    log.info( '  Модуль {} отработал без ошибок.'.format( super().CURRTEST.kmodule[0] ) )
