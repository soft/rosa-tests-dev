# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from modules.kplpack.utils import x, Return

class testIP(BaseTest):

  tool1  = 'ping'
  flags1 = '-c 2 127.0.0.1'

  tool2  = 'ping6'
  flags2 = '-c 2 0:0:0:0:0:0:0:1'

  def runTest(self):

    code, out = x( self.tool1 + ' ' + self.flags1, Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

    code, out = x( self.tool2 + ' ' + self.flags2, Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )
