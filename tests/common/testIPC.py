# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from pathlib import Path
from modules.kplpack.utils import x, Return

class testIPC(BaseTest):

  tools = [
            [ Path('/usr/lib64/rosa-tests/netlinktest') , 'hello' ],
            [ Path('/usr/lib64/rosa-tests/signaltest')  , '' ],
            [ Path('/usr/lib64/rosa-tests/shmemtest')   , '' ],
            [ Path('/usr/lib64/rosa-tests/semtest')     , '' ],
          ]

  def runTest(self):

    for t in self.tools:
      self.assert_( t[0].exists(), 'Утилита необходимая для проверки не обнаружена: {}'.format( t[0].as_posix() ) )

      code, out = x( t[0].as_posix() + ' ' + t[1], Return.codeout )

      for l in out.splitlines():
        log.info( '  {}'.format( l ) )

      self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )
