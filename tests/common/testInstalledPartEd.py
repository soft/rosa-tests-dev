# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from modules.kplpack.utils import x, Return

class testInstalledPartEd(BaseTest):

  tool  = 'which'
  flags = 'diskdrake'

  def runTest(self):

    code, out = x( self.tool + ' ' + self.flags, Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )