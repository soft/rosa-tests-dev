# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import shutil

from modules.kplpack.utils import x, Return
from subprocess import getstatusoutput

class testIntegrity(BaseTest):
  """
  Тест целостности средств защиты информации
  """
  testcfg = '/etc/rosa-test.cfg'

  def runTest(self):
    log.debug('  Создание тестового файла...')

    with open(self.testcfg, 'w') as f:
      f.write('test config')

    log.debug('  Инициализация базы данных AIDE ...')

    errcode, output = getstatusoutput('/usr/sbin/aide --init --limit /etc')
    self.assertEqual(errcode, 0, 'Проинициализировать базу данных AIDE не удалось.\n' + output)

    if os.path.isfile('/var/lib/aide/aide.db'):
      log.debug('  Удаление старой базы данных AIDE...')
      try:
        os.unlink('/var/lib/aide/aide.db')
      except OSError as e:
        self.assertEqual(e.errno, 0, 'Удалить старую базу данных AIDE не удалось. Ошибка %d: %s'
                                      % (e.errno, e.message))

    log.debug('  Замена базы данных AIDE...')
    try:
      shutil.move('/var/lib/aide/aide.db.new', '/var/lib/aide/aide.db')
    except OSError as e:
      self.assertEqual(e.errno, 0, 'Заменить базу данных AIDE не удалось. Ошибка %d: %s'
                                    % (e.errno, e.message))

    log.debug('  Изменение тестового файла...')

    with open(self.testcfg, 'w') as f:
      f.write('test config updated')

    log.debug('  Проверка базы данных AIDE ...')

    errcode, output = getstatusoutput('/usr/sbin/aide --check --limit /etc')
    self.assertEqual(errcode, 4, 'При проверке базы данных AIDE получен неверный код ошибки (%d).\n%s'
                                      % (errcode, output))

    #errcode, output = getstatusoutput('sync; echo 3 > /proc/sys/vm/drop_caches' + extraParam)

    changeDetected = False
    for line in output.split('\n'):
      if self.testcfg in line:
        changeDetected = True
        break

    self.assertTrue(changeDetected, 'Изменение файла %s не было выявлено. Сообщения программы проверки:\n%s'
                                      % (self.testcfg, output))

    log.info('Проверка целостности средств защиты информации пройдена успешно.')

  def tearDown(self):
    if os.path.isfile(self.testcfg):
      log.debug('  Удаление тестового файла...')
      os.remove(self.testcfg)
