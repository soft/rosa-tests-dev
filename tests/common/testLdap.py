# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import ldap
import os
import re
import shutil

from subprocess import getstatusoutput
from time import sleep

class testLdap(BaseTest):
  """
  Проверка обеспечения изделием централизованного хранения учетных данных
  пользователей в локальной вычислительной сети
  """
  service = 'ldap'
  serviceStopped = False
  ldapDbDir = '/var/lib/ldap'
  ldapDbBakDir = '/var/lib/ldap/bak'
  ldapDbMoved = False
  slapdConf = '/etc/openldap/slapd.conf'
  slapdConfBak = '/etc/openldap/slapd.conf.backup'
  sysconfLdap = '/etc/sysconfig/ldap'
  sysconfLdapBak = '/etc/sysconfig/ldap.backup'
  configCopied = False
  sysconfigCopied = False

  @staticmethod
  def san(text:str, indent=2, words=('by',)):
    """ multiline-string sanation works as opposed to textwrap.dedent() """
    res = ''
    text = text.strip('\n')
    buf = ''
    for s in text.splitlines():
        buf = s.strip(' ').strip('\t') + '\n'
        if buf.startswith(words):
          res += (' ' * indent) + buf
        else:
          res += buf
    return res

  def runTest(self):
    log.debug( '  Проверка состояния службы {}...'.format( self.service ) )
    errcode, output = getstatusoutput( 'service {} status'.format( self.service ) )

    if errcode == 0:
      self.serviceStopped = True
      log.debug( '  Служба {} была запущена до начала проверки, она будет остановлена.'.format( self.service ) )
      errcode, output = getstatusoutput( 'service {} stop'.format( self.service ) )

      self.assert_( errcode == 0, 'Остановить службу {} не удалось.\n{}'.format( self.service, output ) )

      sleep(1)

    self.prepareConfig()
    log.debug( '  Перенос исходной базы данных LDAP...' )
    shutil.rmtree( self.ldapDbBakDir, True )
    os.mkdir( self.ldapDbBakDir )

    for dbfile in os.listdir( self.ldapDbDir ):
      if not os.path.isfile( os.path.join( self.ldapDbDir, dbfile ) ):
        continue
      os.rename( os.path.join( self.ldapDbDir, dbfile ),
                 os.path.join(self.ldapDbBakDir, dbfile ) )

    self.ldapDbMoved = True
    log.debug( '  Запуск службы {}...'.format( self.service ) )
    errcode, output = getstatusoutput( 'service {} start'.format( self.service ) )

    self.assert_( errcode == 0, 'Запустить службу {} не удалось: {}'.format( self.service, output ) )

    sleep(1)
    log.debug('  Запрос контекста именования...')

    errcode, output = getstatusoutput( 'ldapsearch -LLL -x -b "" -s base namingContexts supportedLDAPVersion' )

    self.assertEqual( errcode, 0, 'При запросе контекста именования произошла ошибка.\n' + output )

    nc = None
    for line in output.split('\n'):
      ncre = re.search(r'^namingContexts:\s+(.*)$', line)
      if ncre:
        nc = ncre.group(1).strip()
        break
    if nc is None:
      self.fail( 'Получить контекст именования не удалось.' )
    log.debug( 'Получен контекст именования: {}'.format( nc ) )
    if self.setAccess:
      self.setAccess()
    log.debug( '  Наполнение каталога...' )

    cmd = '''
            ldapadd -Y EXTERNAL -H ldapi:/// <<EOS
            dn: {c}
            o: LDAP testing directory
            description: Root entry for the test directory.
            objectClass: top
            objectclass: dcObject
            objectclass: organization

            # Admin user.
            dn: cn=admin,{c}
            objectClass: simpleSecurityObject
            objectClass: organizationalRole
            cn: admin
            description: LDAP administrator
            userPassword: 5ecretP@ssw0rd

            dn: ou=Group,{c}
            objectClass: organizationalUnit
            ou: Group

            dn: cn=testers,ou=Group,{c}
            objectClass: posixGroup
            cn: testers
            gidNumber: 10000

            dn: ou=People,{c}
            objectClass: organizationalUnit
            ou: People

            dn: uid=rosa-test-user,ou=People,{c}
            objectClass: inetOrgPerson
            objectClass: posixAccount
            objectClass: shadowAccount
            uid: rosa-test-user
            sn: Tester
            givenName: Tester
            cn: Rosa Tester
            displayName: Rosa Tester
            uidNumber: 1000
            gidNumber: 10000
            userPassword: password
            gecos: Rosa Tester
            loginShell: /bin/bash
            homeDirectory: /home/rosa-test-user
            shadowExpire: -1
            shadowFlag: 0
            shadowWarning: 7
            shadowMin: 8
            shadowMax: 999999
            shadowLastChange: 10877
            mail: rosa-test-user@no.mail
            mobile: +7 xxx xxx xx xx
            title: System Tester
            initials: RT
            EOS
            exit $?
          '''.format( c=nc )

    errcode, output = getstatusoutput( self.san( cmd ) )

    self.assert_( errcode == 0, 'Добавить в каталог данные не удалось.\n' + output )

    log.debug( '  Проверка подключения пользователя...' )
    errcode, output = getstatusoutput( 'ldapwhoami -D "uid=rosa-test-user,ou=People,{}" -w password'.format( nc ) )

    self.assert_( errcode == 0, 'Пользователь не смог подключиться к хранилищу LDAP.\n' + output )

    log.debug( '  Смена пароля пользователя...' )
    errcode, output = getstatusoutput( 'ldappasswd -Y EXTERNAL -H ldapi:/// "uid=rosa-test-user,ou=People,{}" -s newP@ssw0rd'.format( nc ) )

    self.assert_( errcode == 0, 'Сменить пароль пользователя не удалось.\n' + output )

    log.debug( '  Проверка альтернативного подключения с новым паролем...' )


    l = ldap.initialize( 'ldap://localhost/' )
    l.simple_bind_s( 'uid=rosa-test-user,ou=People,{}'.format( nc ), 'newP@ssw0rd' )
    l.unbind_s()
    log.info( 'Проверка поддержки централизованного хранилища учётных данных пройдена успешно.' )

  def prepareConfig(self):
    log.debug('  Подготовка конфигурации sysconf/ldap...')
    shutil.copy2(self.sysconfLdap, self.sysconfLdapBak)
    with open(self.sysconfLdap, 'r') as conf:
      contents = conf.read()
    with open(self.sysconfLdap, 'w+') as conf:
      for line in contents.split('\n'):
        if re.search(r'^#\s*SLAPD URL', line):
          line = line + '\n' + '\nSLAPDURLLIST="ldap:/// ldaps:/// ldapi:///"\n'
        if re.search(r'^\s*SLAPDURLLIST=', line):
          line = '#' + line
        conf.write(line)
        conf.write('\n')
    self.sysconfigCopied = True

    log.debug('  Подготовка конфигурации openldap/slapd.conf...')
    shutil.copy2(self.slapdConf, self.slapdConfBak)
    with open(self.slapdConf, 'r') as conf:
      contents = conf.read()
    with open(self.slapdConf, 'w+') as conf:
      for line in contents.split('\n'):
        if re.search(r'^\s*(TLSCertificateFile|'
                      r'TLSCertificateKeyFile|'
                      r'TLSCACertificateFile\s+)', line):
          line = '# ' + line
        if re.search(r'^# Global ACL definitions', line):
          line = line + '\n' \
                      + self.san(
                                  '''
                                    access to *
                                      by dn.base="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" manage
                                      by self write
                                      by * read

                                    access to attr=userPassword
                                      by dn.base="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" write
                                      by self write
                                      by anonymous auth
                                      by * none
                                  ''')
        conf.write(line)
        conf.write('\n')
    self.configCopied = True

  def setAccess(self):
    pass

  def tearDown(self):
    log.debug('  Остановка службы LDAP...')
    errcode, output = getstatusoutput( 'service {} stop'.format( self.service ) )
    if self.ldapDbMoved:
      log.debug( '  Восстановление исходной базы данных LDAP...' )

      for dbfile in os.listdir( self.ldapDbDir ):
        if not os.path.isfile( os.path.join( self.ldapDbDir, dbfile ) ):
          continue
        os.remove(os.path.join(self.ldapDbDir, dbfile))

      for dbfile in os.listdir(self.ldapDbBakDir):
        os.rename(os.path.join(self.ldapDbBakDir, dbfile),
                  os.path.join(self.ldapDbDir, dbfile))

      os.rmdir(self.ldapDbBakDir)
    self.restoreConfig()
    if self.serviceStopped:
      log.debug( '  Запуск службы {}...'.format( self.service ) )
      errcode, output = getstatusoutput( 'service {} start'.format( self.service ) )

      self.assert_( errcode == 0, 'Запустить службу {} не удалось.\n'.format( self.service, output ) )

  def restoreConfig(self):
    if self.sysconfigCopied:
      log.debug('  Восстановление конфигурации sysconf/ldap.')
      with open(self.sysconfLdapBak, 'r') as confbak:
        confContents = confbak.read()
      with open(self.sysconfLdap, 'w+') as conf:
        conf.write(confContents)
    if self.configCopied:
      log.debug('  Восстановление конфигурации openldap/slapd.conf.')
      with open(self.slapdConfBak, 'r') as confbak:
        confContents = confbak.read()
      with open(self.slapdConf, 'w+') as conf:
        conf.write(confContents)
