# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import shutil

from modules.kplpack.utils import x, Return
from modules.kplpack.utils import User
from modules.kplpack.utils import chmodR

class testLftpWithSftp(BaseTest):
  """
    Проверка возможности передачи файлов средствами lftp и sftp
  """
  #sys.path.append('/modules/utils.py')
  #from modules.utils import Terminal as X

  localRoot     = '/var/tmp'
  localDirName  = 'sftpserver'
  localFileName = 'welcome.txt'
  testContent   = 'hello, lftp!'
  user          = 'sftpuser'
  password      = 'topsecret'

  localDirPath  = '{}/{}'.format( localRoot, localDirName )
  localFilePath = '{}/{}'.format( localDirPath, localFileName )

  userDirPath  = '/home/{}'.format( user )
  userFilePath = '{}/{}/{}'.format( userDirPath, localDirName, localFileName )

  def runTest(self):

    log.debug('  Создание тестового пользователя \'{}\'...'.format( self.user ) )

    with User( name=self.user, password=self.password, home=self.userDirPath ) as u:

      if os.path.isdir( self.localDirPath ):
        log.debug('  Удаление старой тестовой директории...')
        shutil.rmtree( self.localDirPath )

      log.debug('  Создание тестовой директории и файла...')
      os.mkdir( self.localDirPath )

      with open( self.localFilePath , 'w') as f:
          f.write( self.testContent )

      chmodR(self.localFilePath, '777')
      chmodR(self.localDirPath, '777')

      log.debug('  Загрузка тестовой директории {} с SFTP-сервера...'.format( self.localDirName ) )

      res = x( "lftp -c 'open -u {u},{p} sftp://127.0.0.1{lr}; mirror --verbose -c -P5 {ld} {td}/' " \
                .format( u=self.user, p=self.password, lr=self.localRoot, ld=self.localDirName, td=self.userDirPath ),
                return_type=Return.process )

      self.assert_( res.returncode == 0, 'Получить директорию с SFTP-сервера не удалось.\n' + res.stdout.read() )

      log.debug('  Выполняется чтение {} полученного с SFTP-сервера...'.format( self.userFilePath ) )
      with open( self.userFilePath, 'r') as f:
        res = f.readlines()[0]

      self.assertEqual( self.testContent, res, 'Содержимое тестового файла отличается от исходного ("{}" != "{}").' \
                                                .format( self.testContent, res ) )

  def tearDown(self):

    if os.path.isdir( self.localDirPath ):
      log.debug('  Удаление тестовой директории...')
      shutil.rmtree( self.localDirPath )
