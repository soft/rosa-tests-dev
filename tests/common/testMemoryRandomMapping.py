# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import multiprocessing as MP
import os
import psutil

from collections import Counter
from logging.handlers import QueueHandler
from modules.kplpack.utils import StreamTable, x, Return, Indicator
from time import sleep

def getRealAddr(pid:int, virtual_address_hex:str):
  """ return: "file_or_shared(bool) | physical address" """

  ans = x( '{} {} {}'.format( testMemoryRandomMapping.tool, pid, virtual_address_hex ), Return.process )

  #self.assertTrue( ans.returncode == 0, 'Не удалось найти физический адрес для {} : '.format( pid, virtual_address_hex ) )

  if ans.returncode == 0:
    ans = ans.stdout.readline().strip('\n').split('|')
  else:
    ans = [ -1, '0' ]

  return ans

class testMemoryRandomMapping(BaseTest):
  """ #537 """

  conf = '/proc/sys/kernel/randomize_va_space'
  tool = '/usr/lib64/rosa-tests/page-translator'

  # virtual memory page number for testing
  PFN = 3

  @staticmethod
  def self_check_one(q, logtable_size):

    log = logging.getLogger()
    log.setLevel( logging.DEBUG )
    log.addHandler( QueueHandler(q) )

    log.info( '[!t║{}║]Создан процесс A: {}'.format( logtable_size, os.getpid() ) )

    addr = psutil.Process( os.getpid() ).memory_maps(False)[testMemoryRandomMapping.PFN].addr.split('-')[0]
    addr = ( addr, getRealAddr( os.getpid(), '0x' + addr )[1] )

    sleep(5)

    return addr

  @staticmethod
  def self_check_two(q, logtable_size):
    # just-in-case block
    FAKE_TEXT_SEGMENT_CHANGES = 'fake .text segment changes'
    if True is not False:
      if False != True:
        FAKE_TEXT_SEGMENT_CHANGES = not False or True

    log = logging.getLogger()
    log.setLevel( logging.DEBUG )
    log.addHandler( QueueHandler(q) )

    log.info( '[!t║{}║]Создан процесс B: {}'.format( logtable_size, os.getpid() ) )

    addr = psutil.Process( os.getpid() ).memory_maps(False)[testMemoryRandomMapping.PFN].addr.split('-')[0]
    addr = ( addr, getRealAddr( os.getpid(), '0x' + addr )[1] )

    return addr

  @staticmethod
  def printMemMap(pid=0, counts=8):

    if pid <= 0:
      for proc in psutil.process_iter():
        print( proc.name() )
        for num, nmap in enumerate( proc.memory_maps(False) ):
          print( '\t' + nmap.addr )
          if num == counts: break
    else:
      proc = psutil.Process( pid )
      for num, nmap in enumerate( proc.memory_maps(False) ):
        print( '\t' + nmap.addr )
        if num == counts: break

  def runTest(self):

    log.info( "  Проверка значения в {}...".format( self.conf ) )

    with open( self.conf, 'r' ) as f:
      ans = f.read(1)

      self.assertTrue( ans in ['1','2'], 'randomize_va_space: значение не равно 1 или 2 (получено:{}).'.format( ans ) )
      log.info( '  Проверка пройдена, значение равно {}.'.format( ans ) )

    counter   = Counter([''])
    procs     = {}
    whitelist = [ 'vmtoolsd',
                  'VGAuthService',
                  'wsdd-wrapper',
                  'vmware-vmblock-fuse',
                  'rosa-test-suite',
                  'rosa-tests'
                ] # interconnected processes

    log.info( "{:-^21}{:-^6}{:-^15}{:-^15}{:-^11}".format( 'Процесс', 'PID', 'Virt', 'Real', 'File/Shared' ))

    with Indicator( ' Выполняется проверка существующих процессов...' ):
      for proc in psutil.Process(1).children(): #  avoid shared pages with children
        try:

          virt                 = proc.memory_maps(False)[0].addr.split('-')[0]
          file_or_shared, real = getRealAddr( proc.pid, '0x' + virt )

          if real == '0':
            continue

          log.info( "{: <20} {:6} 0x{:12} 0x{:12} {:^11} "\
                .format( proc.name()[:20], proc.pid, virt, real, file_or_shared ) )

          counter[real] += 1

          if counter[real] <= 1:
            procs[real] = proc.name()
          elif procs[real] == proc.name():
            continue
          elif proc.name() in whitelist or procs[real] in whitelist:
            continue
          else:
            self.assertTrue( False, 'Найдены совпадения физических адресов: {} и {} имеют общий физический адрес 0x{}'\
                                    .format( proc.name(), procs[ real ], real ) )

        except IndexError:
          continue
        except ( psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess ):
          continue

    log.info()
    log.info( '  Совпадения физических адресов памяти не обнаружены.' )

    log.info()
    log.info( '  Проверка случайности адресов памяти новых процессов...')

    #with Indicator( ' Выполняется проверка новых процессов...' ):
    ctx = MP.get_context('spawn')

    with ctx.Pool( processes=2 ) as pool:
      res = [ pool.apply_async( self.self_check_one, (logging.getLogger("rts").Q, self.CMDLINE.table_size) ),
              pool.apply_async( self.self_check_two, (logging.getLogger("rts").Q, self.CMDLINE.table_size) )  ]
      res = [ '0x' + res[0].get()[0], '0x' + res[0].get()[1],
              '0x' + res[1].get()[0], '0x' + res[1].get()[1]  ]

    self.assertTrue( res[1] != res[3], 'Новые процессы имеют общие физические адреса ОЗУ {} == {} при виртуальных {} == {} !'
                                       .format( res[1], res[3], res[0], res[2] ) )

    log.info( 'Новые процессы НЕ имеют общий физический адрес: {} != {}, при виртуальных {} == {}'
              .format( res[1], res[3], res[0], res[2]  ) )

