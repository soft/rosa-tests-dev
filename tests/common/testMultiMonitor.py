# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import re
import os

from modules.kplpack.utils import steal_x_cookie
from subprocess import getstatusoutput


class testMultiMonitor(BaseTest):
  """
  Проверка обеспечения изделием работы в многомониторном режиме
  """

  def runTest(self):
    display_owner = os.getlogin()
    log.info('Добавление x-server cookie от пользователя {}...'.format(display_owner))
    steal_x_cookie(display_owner, 'root', overwrite=True)

    log.debug('  Получение информации о подключённых мониторах от расширения X RandR...')
    errcode, output = getstatusoutput('xrandr')

    self.assertEqual(errcode, 0, 'Не удалось получить информацию расширения X RandR.')

    outputs = []
    for line in output.split('\n'):
      cre = re.search(r'(.*?)\s+connected\s+', line)
      if cre:
        outputs.append(cre.group(1))

    self.assertTrue(outputs, 'Не удалось получить сведения ни об одном подключённом мониторе.')

    if len(outputs) > 1:
      log.debug('  К системе подключены мониторы: ' + ', '.join(outputs) + '.')
    else:
      log.warning( 'В настоящее время к системе подключён один монитор. '
                   'Подключите дополнительный монитор и проверьте работу '
                   'в многомониторном режиме после перезагрузки.')
