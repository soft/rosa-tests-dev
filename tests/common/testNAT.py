# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import psutil

from subprocess import call, PIPE, STDOUT

class testNAT(BaseTest):
    """
    Тест поддержки механизма преобразования сетевых адресов NAT
    (RFC 1631, RFC 3022)
    """
    def runTest(self):

        log.info('Тест поддержки механизма преобразования сетевых адресов NAT.')

        test_util = '/usr/lib64/rosa-tests/test_NAT'

        cmd_setup_pull = [
            'ip netns add R',
            'ip netns add V1',
            'ip netns add V2',

            'ip link add name con1a type veth peer name con1b',
            'ip link add name con2a type veth peer name con2b',

            'ip link set dev con1a netns R',
            'ip link set dev con2a netns R',
            'ip link set dev con1b netns V1',
            'ip link set dev con2b netns V2',

            'ip netns exec R ip address add 192.168.1.1/24 dev con1a',
            'ip netns exec R ip address add 192.168.2.1/24 dev con2a',
            'ip netns exec V1 ip address add 192.168.1.2/24 dev con1b',
            'ip netns exec V2 ip address add 192.168.2.2/24 dev con2b',

            'ip netns exec R ip link set dev con1a up',
            'ip netns exec R ip link set dev con2a up',
            'ip netns exec V1 ip link set dev con1b up',
            'ip netns exec V2 ip link set dev con2b up',

            'ip netns exec V1 ip route add default via 192.168.1.1',
            'ip netns exec V2 ip route add default via 192.168.2.1',

            'ip netns exec R sysctl -w net.ipv4.ip_forward=1 -q',

            'ip netns exec R iptables -F',
            'ip netns exec R iptables -P FORWARD DROP'
        ]
        cmd_nat_ON = [
            'ip netns exec R iptables -A FORWARD -o con1a -i con2a -j ACCEPT',
            'ip netns exec R iptables -A FORWARD -i con1a -o con2a -j ACCEPT'
        ]

        # setup
        log.debug("Настройка виртуальной сетевой инфраструктуры")
        for cm in cmd_setup_pull:
            log.debug("Executing: " + cm)
            self.assertEqual( call(cm, shell=True), 0, 'Ошибка при настройке виртуальной сетевой инфраструктуры!')

        # check line with NAT OFF
        log.debug("Проверка работы виртуальной сетевой инфраструктуры с отключенной NAT")
        p = psutil.Popen( test_util, shell=True, stdout=PIPE, stderr=STDOUT )
        self.assertEqual( p.wait(), 1, 'Проверка не пройдена! NAT уже включена!')
        log.info( 'Вывод тестирующей утилиты: \n\n{}\nУСПЕШНО!'.format( p.stdout.read().decode("utf-8") ) )

        # NAT turn ON
        log.debug("Проверка работы виртуальной сетевой инфраструктуры со включенной NAT")
        for cm in cmd_nat_ON:
            log.debug("Executing: " + cm)
            self.assertEqual( call(cm, shell=True), 0, 'Ошибка при настройке iptables!')

        # check line with NAT ON
        p = psutil.Popen( test_util, shell=True, stdout=PIPE, stderr=STDOUT )
        self.assertEqual( p.wait(), 0, 'Проверка не пройдена! Нет соединения!')
        log.info( 'Вывод тестирующей утилиты: \n\n{}\nУСПЕШНО!'.format( p.stdout.read().decode("utf-8") ) )

        log.info('Проверка поддержки механизма преобразования сетевых адресов NAT пройдена успешно.')

    def tearDown(self):

        log.debug("Удаление виртуальной сетевой инфраструктуры")
        cmd_clear = ['ip netns del R', 'ip netns del V1', 'ip netns del V2']

        for cm in cmd_clear:
            log.debug("Executing: " + cm)
            self.assertEqual( call(cm, shell=True), 0, 'Ошибка при очистке виртуальной сетевой инфраструктуры!')
