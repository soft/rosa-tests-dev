# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from modules.kplpack.utils import steal_x_cookie
from modules.kplpack.utils import User
from modules.kplpack.utils import x, Return

class testNvidiaAti(BaseTest):
  """
  Тест возможности использования драйверов для современных
  высокопроизводительных графических ускорителей (ATI, Nvidia)
  """

  def runTest(self):
    steal_x_cookie(User().name, 'root', overwrite=True)

    code, out = x('glxinfo -B', Return.codeout)
    out = out.lower()

    self.assert_(code == 0, "Error[{}]: {}".format(code, out))

    vendor = False
    renderer = False
    accelerated = False

    # "ATI" and "NVIDIA" are for proprietary drivers
    # "nouveau" is a free NVIDIA driver
    # As for free drivers for ATI cards, different card/drivers can
    # return different values and we try to check them all here

    for l in out.splitlines():
      if "vendor string" in l:
        for item in ["ati", "amd", "nvidia", "nouveau", "radeon", "x.org"]:
          if item in l:
            vendor = True
      if "renderer string" in l:
        for item in ["ati", "amd", "nvidia", "radeon", "nv"]:
          if item in l:
            renderer = True
      if "accelerated" in l:
        if "yes" in l:
          accelerated = True

    self.assert_(
      vendor and (renderer or accelerated),
      'Возможность использования драйверов для современных'
      'высокопроизводительных графических ускорителей (ATI, Nvidia) в '
      'данной конфигурации оборудования отсутствует.'
    )
