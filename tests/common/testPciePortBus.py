# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from subprocess import getstatusoutput

class testPciePortBus(BaseTest):
  """
  Тест поддержки высокоскоростного периферийного интерфейса PCI Express
  """

  pass_phrase = 'pcie_test: device has PCI-E capabilities.'

  def runTest(self):
    log.debug('  Получение списка PCI-устройств...')
    errcode, output = getstatusoutput('lspci -n -mm')
    logIfError(errcode, output)

    self.assertEqual(errcode, 0, 'Получить список PCI-устройств не удалось:\n%s' % output)

    pcie_found = False
    for line in output.split('\n'):
      devinfo = line.split(' ')
      vnd = devinfo[2].replace('"', '')
      dev = devinfo[3].replace('"', '')
      log.debug('  Проверка устройства {}:{}...'.format( vnd, dev ) )

      self.assert_( super().CURRTEST.clear_dmesg(), 'Очистить буфер dmesg не удалось:\n%s' % output)

      code, out = super().CURRTEST.run_kmodule(0,'vendor_id="0x{}" device_id="0x{}"'.format( vnd, dev ) )

      # no need
      # self.assert_( code == 0, 'При запуске модуля {} произошла ошибка [{}]: {}'
      #                           .format( CURRTEST.kmodule[0], code, out ) )

      if super().CURRTEST.check_dmesg( self.pass_phrase ) < 0:
        pcie_found = True
        log.debug( '  Устройство {}:{} использует интерфейс PCI Express.'.format( dev, vnd ) )

      if code == 0:
        code, out = super().CURRTEST.rem_kmodule(0)

        self.assert_( code == 0, 'При выгрузке модуля {} произошла ошибка [{}]: {}'
                                 .format( super().CURRTEST.kmodule[0], code, out ) )

      if pcie_found:
        break

    self.assert_( pcie_found == True, 'В системе не найдено ни одного устройства PCI-E.' )

def logIfError(errcode, msg):
  if errcode:
    log.error(msg)
