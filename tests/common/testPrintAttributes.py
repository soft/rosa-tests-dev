# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import selinux

from time import sleep
from subprocess import getstatusoutput
from modules.kplpack.utils import x, Return

class testPrintAttributes(BaseTest):
    """
    Проверка возможности предоставления программного интерфейса для
    автоматического заполнения учетных атрибутов документа при выводе
    на печатающее устройство
    """

    def runTest(self):
        contextS0 = self.selinux_user + ':' + self.selinux_role + ':' + \
                    self.program_type + ':' + 's0'
        contextS1 = self.selinux_user + ':' + self.selinux_role + ':' + \
                    self.program_type + ':' + 's1'

        mPath = os.path.dirname(os.path.realpath(__file__))

        log.debug('   Настройка печати маркера...')
        confstring = "\nClassification mandatory # rosa-tests\n"
        cupspath = "/etc/cups/cupsd.conf"
        confexists = False
        errcode, username = getstatusoutput("getent passwd `cat /proc/self/loginuid ` | cut -d: -f1")
        self.assertEqual(errcode, 0, 'Не удалось получить имя пользователя.\n' +
                         username)
        f = open(cupspath, "a+")
        fstring = f.readlines()
        for line in fstring:
            if "rosa-tests" in line:
                confexists = True
                break
        if not confexists:
            f.write(confstring)
        f.close()
        log.debug('  Перезапуск службы cups...')
        errcode, output = getstatusoutput('killall -9 cupsd')
        errcode = selinux.setexeccon("system_u:system_r:cupsd_t:s0-s1")
        self.assertEqual(errcode, 0, 'Не удалось включить контекст'
                                     ' безопасности s0.')
        errcode, output = getstatusoutput('/usr/sbin/cupsd')
        self.assertEqual(errcode, 0, 'Перезапустить службу cups не удалось.\n' +
                         output)
        sleep(2)
        log.debug('   Добавление принтера cups-pdf...')
        errcode, output = getstatusoutput('lpadmin -p cups-pdf -v cups-pdf:/ -E -P /usr/share/cups/model/CUPS-PDF.ppd')
        self.assertEqual(errcode, 0, 'Добавить принтер не удалось.\n' + output)

        errcode = selinux.setexeccon(contextS0)
        self.assertEqual(errcode, 0, 'Не удалось включить контекст'
                                     ' безопасности s0.')
        log.debug('  Печать документа на уровне безопасности 0...')
        errcode, output = getstatusoutput(
            'lpr -U "%s" -P cups-pdf "%s"' % (username, os.path.join(mPath, 'test_data', 'print', 'markertest0.ps')))
        self.assertEqual(errcode, 0,
                         'При отправке документа на печать произошла ошибка.\n'
                         'Сообщения программы печати:\n' + output)

        errcode = selinux.setexeccon(contextS1)
        self.assertEqual(errcode, 0, 'Не удалось включить контекст'
                                     ' безопасности s1.')
        log.debug('  Печать документа на уровне безопасности 1...')
        errcode, output = getstatusoutput(
            'lpr -U "%s" -P cups-pdf "%s"' % (username, os.path.join(mPath, 'test_data', 'print', 'markertest1.ps')))
        self.assertEqual(errcode, 0,
                         'При отправке документа на печать произошла ошибка.\n'
                         'Сообщения программы печати:\n' + output)

    def tearDown(self):
        errcode = selinux.setexeccon("\n")
        logIfError(errcode, 'Восстановить контекст безопасности не удалось.')

def logIfError(errcode, msg):
    if errcode:
        log.error(msg)