# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import shutil

from subprocess import getstatusoutput
from time import sleep

class testPrintMarkers(BaseTest):
    """
    Проверка настройки вида маркеров документов
    """

    def setUp(self):
        self.tagsDir = '/etc/cups/tags'
        self.tagsBakDir = '/var/tmp/cups_tags.bak'
        shutil.rmtree(self.tagsBakDir, True)
        shutil.copytree(self.tagsDir, self.tagsBakDir)

    def runTest(self):
        mPath = os.path.dirname(os.path.realpath(__file__))
        log.debug('  Подстановка специальных тегов разметки...')
        self.copyFiles(os.path.join(mPath, 'test_data', 'print', 'tags'),
                       self.tagsDir)
        log.debug('  Печать документа...')
        errcode, output = getstatusoutput('lp "%s"' % os.path.join(mPath,
                                                                   'test_data', 'print',
                                                                   'printer_test0.pdf'))
        self.assertEqual(errcode, 0,
                         'При отправке документа на печать произошла ошибка.\n'
                         'Сообщения программы печати:\n' + output)
        sleep(5)  # Пауза для подготовки задания с подставленными тегами
        log.debug('  Печать завершена.')

    def copyFiles(self, src, dest):
        for sfile in os.listdir(src):
            shutil.copy2(os.path.join(src, sfile), dest)

    def tearDown(self):
        self.copyFiles(self.tagsBakDir, self.tagsDir)
