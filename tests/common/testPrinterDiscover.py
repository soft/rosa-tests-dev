# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import re

from modules.kplpack.utils import waitForPrinterIdle, usb_device_reset
from subprocess import getstatusoutput
from time import sleep

class testPrinterDiscover(BaseTest):
    """
    Проверка режима автоматического поиска и настройки принтеров
    """

    def runTest(self):

        log.debug('  Получение сведений о подключенных USB-принтерах...')
        errcode, output = getstatusoutput('lpstat -v')
        self.assertEqual(errcode, 0, 'Получить сведения о подключенных '
                                     'принтерах не удалось.\n' + output)

        log.debug('Waiting the printers to complete their work..')

        waitForPrinterIdle(check_interval=1, waiting_timeout_sec=40)

        printersFound = 0
        lines = output.split('\n')
        for line in lines:
            usr = re.search(r'^\s*(устройство для|device for)\s+([^:]+):\s*'
                            '(usb://|hp:/usb/)', line)
            if usr:
                printersFound += 1
                printer = usr.group(2)
                log.debug('  Удаляется принтер %s...' % printer)
                errcode, output = getstatusoutput('lpadmin -x "%s"' % printer)
                self.assertEqual(errcode, 0,
                                 ('Удалить подключённый принтер "%s" не удалось.\n' % printer) +
                                 output)

        if not printersFound:
            log.warning('При запуске теста в системе не выявлено'
                           'ни одного подключенного USB-принтера.')
        else:
            log.debug('  При запуске теста выявлено USB-принтеров: %d.',
                         printersFound)
        try:
            os.remove('/var/run/udev-configure-printer/usb-uris')
        except OSError:
            pass
        log.debug('  Перезапуск службы cups...')
        errcode, output = getstatusoutput('systemctl restart cups')
        self.assertEqual(errcode, 0, 'Перезапустить службу cups не удалось.\n' +
                         output)
        sleep(1.5)

        usb_device_reset()

        log.debug('  Инициирование операции добавления USB-принтеров...')
        udeverr, udevexe = getstatusoutput('udevadm trigger --action=add '
                                          '--subsystem-match=usb --attr-match=bInterfaceClass=07 '
                                          '--attr-match=bInterfaceSubClass=01 --verbose')

        # udevadm control --reload-rules
        errcode, output = getstatusoutput('systemctl restart cups')
        self.assertEqual(errcode, 0, 'Перезапустить службу cups не удалось.\n' +
                         output)

        log.debug('  udevadm: OUT: %s  ERR: %s' % (udevexe, udeverr))

        log.debug('  Получение сведений об обнаруженных USB-принтерах...')

        printersDetected = 0
        if udevexe and not udeverr:
            printersDetected = len(udevexe.splitlines())

        # for t in xrange(1, 60):
        #     printersDetected = 0
        #     errcode, output = getstatusoutput('lpstat -v')
        #     lines = output.split('\n')
        #     for line in lines:
        #         usr = re.search(r'^\s*(устройство для|device for)\s+([^:]+):\s*'
        #                         '(usb://|hp:/usb/)', line)
        #         if usr:
        #             printersDetected += 1
        #     if (printersFound == printersDetected or
        #             (printersFound == 0 and printersDetected > 0)):
        #         break
            sleep(0.5)

        self.assertNotEqual(printersDetected, 0, 'В процессе проверки'
                                                 ' не удалось обнаружить ни одного принтера.')
        log.debug('  В процессе проверки обнаружено принтеров: %d.' %
                     printersDetected)
