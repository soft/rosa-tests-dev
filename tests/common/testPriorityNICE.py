# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import multiprocessing as MP

from time import sleep
from modules.kplpack.utils import x as X, Return, Indicator, Ticker

class testPriorityNICE(BaseTest):
  """ #522 """

  def setUp(self):
    super(testPriorityNICE, self).setUp()

    self.cores = os.cpu_count()


  @staticmethod
  def race( i:int=100 ):

    for x in range( i ):
      for y in range( x + 1 ):
       ( x + 1 ) / ( y + 1 ) * ( x + y )

  def renice(self, nice:int, pid:int):

    code, out = X( 'renice {} {} '.format( nice, pid ), Return.codeout )
    self.assert_( code == 0, 'Не удалось изменить приоритет процесса. NICE={}: {}'
                             .format( nice, out ) )

  @staticmethod
  def payload_cpu( cores:int, duration:int ):
    X( 'stress --cpu {} --timeout {}'.format( cores, duration ) )

  def do(self, delay:int, renice:int, duration:int = 10000 ):

    if delay > 0:
      sleep( delay )

    P = MP.Process( target=self.race, args=( duration, ) )
    P.start()

    while not P.pid : pass
    self.renice( renice, P.pid )

    P.join()

  def runTest(self):

    log.info( '  Нагрузка свободных вычислительных мощностей ( {} ядра(ер) cpu )'.format( self.cores ) )
    payload = MP.Process( target=self.payload_cpu, args=( self.cores, 40 ), daemon=True )
    payload.start()

    self.renice( 5, os.getpid() )

    with Indicator( ' Работа процесса с приоритетом(низкий) NICE=20...' ):
      log.info( '  Процесс NICE=20 начал свою работу.' )

      with Ticker( timeout=15, high_resolution=True, precision=4 ) as timer1:
        self.do( 0, 0, 10000 )
      log.info( '  Процесс NICE=20 завершил свою работу за {} сек.'.format( timer1.last_point) )

    with Indicator( ' Работа процесса с приоритетом(высокий) NICE=-20...' ):
      log.info( '  Процесс NICE=-20 начал свою работу c задержкой 1 сек.' )

      with Ticker( timeout=15, high_resolution=True, precision=4 ) as timer2:
        self.do( 1, -20, 10000 )
      log.info( '  Процесс NICE=-20 завершил свою работу за {} сек.'.format( timer2.last_point ) )

    delta = timer1 - timer2
    self.assert_( delta > 1.0, 'Не удалось проверить рекцию системы на приоритет NICE. {} - {} <= 1.0 сек.'
                                           .format( timer1.last_point, timer2.last_point ) )

    log.info( '  Процесс с высоким приоритетом NICE завершил работу быстрее на {} секунд(ы).'.format( delta ) )


  def tearDown(self):
    self.renice( -5, os.getpid() )
