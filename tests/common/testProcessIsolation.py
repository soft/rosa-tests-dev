# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import psutil
import os
import multiprocessing as MP

from collections import Counter
from logging.handlers import QueueHandler
from modules.kplpack.utils import x, Return, Indicator, Damper

class testProcessIsolation(BaseTest):
  """ #535 """

  tool = '/usr/lib64/rosa-tests/process-intervention'

  @classmethod
  def self_check(self, table_size, q):
    _log = logging.getLogger()
    _log.setLevel( logging.DEBUG )
    _log.addHandler( QueueHandler(q) )


    _log.info( '[!t║{}║]Создан процесс: {}'.format( table_size, os.getpid() ) )

    addr = psutil.Process( os.getpid() ).memory_maps(False)[0].addr.split('-')[0]
    return addr

  def runTest(self):

    with Indicator(message=' Выполняется тестирование...'):
      log.info('  Запуск проверяющей утилиты...')
      self.assertTrue( 0 == x( self.tool ), 'Изоляция процессов не соблюдается, обнаружены изменения в переменных после работы дочернего процесса.')
      log.info('  Попытка дочернего процесса изменить данные родительского - провалена.')

    log.info( '  Проверка адресов новых процессов...')

    with Damper():
      with MP.Pool( processes=2 ) as pool:
        res = [ pool.apply_async(self.self_check, ( self.CMDLINE.table_size, logging.getLogger("rts").Q) ) for _ in range(2) ]
        res = [ '0x' + w.get() for w in res ]

    self.assertTrue( res[0] == res[1], 'Новые процессы не имеют общий виртуальный адрес {} != {}, изоляция не доказана !'
                                       .format( res[0], res[1] ) )

    log.info('  Новые процессы имеют общий виртуальный адрес.')

    log.info('  Проверка уникальности имен существующих процессов в системе...')

    counter = Counter([''])
    procs   = {}

    with Indicator(message=' Выполняется проверка имён процессов...'):

      for proc in psutil.process_iter(): #  avoid shared pages with children
        try:
          counter[proc.pid] += 1

          if counter[proc.pid] <= 1:
            procs[proc.pid] = proc.name()
          else:
            self.assertTrue( False, 'Найдены совпадения имен процессов: {} и {} имеют общий физический адрес {}'\
                                    .format( proc.name(), procs[ proc.pid ], proc.pid ) )

        except IndexError:
          continue
        except ( psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess ):
          continue

    log.info('  Совпадений имен процессов не обнаружено.' )
