# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os

from time import sleep
from pathlib import Path
from modules.kplpack.utils import x, Return, User, Terminal, StreamToLog, Indicator, ConRun

class testQuota(BaseTest):
  """ #540 """

  tfs = Path( '/var/tmp/testpart.tfs' )
  mnt = Path( '/mnt/tfs' )

  username = 'rosa-tests-user'
  password = 'testThere_'
  home = '/home/{}'.format(username)

  def runTest(self):

    log.info( '  Создание нового пользователя {} ..."'.format( self.username ) )
    self.U = User(self.username, self.password, home=self.home)

    with Terminal( StreamToLog( log ) ) as term:
      term.add( 'dd if=/dev/zero bs=1M count=1 of={}'.format( self.tfs.as_posix() ) )\
          .info( '  Выделение места под тестовую файловую систему...\n')
      term.add( 'mkfs -t ext4 {}'.format( self.tfs.as_posix() ) )\
          .info( '  Создание тестовой файловой системы(tfs)...\n' )
      term.add( 'mkdir {}'.format( self.mnt.as_posix() ) )\
          .info( '  Создание точки монтирования({})...\n'.format( self.mnt.as_posix() ) )
      term.add( 'mount -t auto -o loop,usrquota,grpquota {} {}'.format( self.tfs.as_posix(), self.mnt.as_posix() ) )\
          .info( '  Монтирование тестовой файловой системы(tfs) в {} \n'.format( self.mnt.as_posix() ) )
      term.add( 'mount -o remount,rw {}'.format( self.mnt.as_posix() ) )\
          .info( '  Переключение тестовой файловой системы(tfs) в режим чтения и записи.\n' )
      term.add( 'chown {u}:{u} {p}'.format( u=self.username, p=self.mnt.as_posix() ) )\
          .info( '  Смена владельца файловой системы\n' )
      term.add( 'quotacheck -cuvg {p}; quotaon {p}'.format( p=self.mnt.as_posix() ) )\
          .info( '  Активация механизма квотирования...\n'.format( self.mnt.as_posix() ) )
      term.add( 'setquota -u {} 0 4 2 4 -a {}'.format( self.username, self.mnt.as_posix() ) )\
          .info( '  Установка квот на {}: 1Кб, 4 файла\n'.format( self.mnt.as_posix() ) )
      term.add( 'setquota -t 5 5 {}'.format( self.mnt.as_posix() ) )\
          .info( '  Установка срока действия квот(soft) на {}: 2 файла, 5 секунд\n'.format( self.mnt.as_posix() ) )

    self.assert_( term.fail[0] == 0, 'При создании тестовой файловой системы и настройке квот произошла ошибка [{}]: {}'
                                      .format( term.fail[0], term.fail[1] ) )

    log.info( '  Запуск тестирования квот...' )

    with Indicator( ' Тестирование квот...'):
      with ConRun(self.U.uid, self.U.gid) as mem:

        # testing the quota for the number of files
        try:
          for n in range( 4 ):
            with open( self.mnt.as_posix() + "/{}.file".format( n ), 'w+' ) as f: pass
          raise Exception('Действие квоты не зафиксировано.')

        except OSError as e:
          if e.errno == 122:
            print( 'OK' )
          else:
            print( 'error: {}'.format(e) )

        try:
          os.unlink( self.mnt.as_posix() + "/2.file" )
        except OSError as e:
          print( 'error: {}'.format(e) )

        log.info( '  Протестирована квота на ограничение кол-ва файлов.')

        # testing the file size quota
        try:
          with open( self.mnt.as_posix() + "/large.file", 'a+' ) as f:
            for _ in range( 4096 ):
              f.write( 'b' )
          raise Exception('Действие квоты не зафиксировано.')

        except OSError as e:
          if e.errno == 122:
            print( 'OK' )
          else:
            print( 'error: {}'.format(e) )

        try:
          os.unlink( self.mnt.as_posix() + "/large.file" )
        except OSError as e:
          print( 'error: {}'.format(e) )

        log.info( '  Протестирована квота на объем файла.')

        # testing temporary quotas
        try:
          sleep(5.1)
          with open( self.mnt.as_posix() + "/extra.file", 'w+' ) as f:
            pass
          raise Exception('Действие квоты не зафиксировано.')

        except OSError as e:
          if e.errno == 122:
            print( 'OK' )
          else:
            print( 'error: {}'.format(e) )

        log.info( '  Протестирована soft квота.')

    for m in mem:
      if m not in [ 'OK', '' ]:
        self.fail( 'Во время проверки механизма квотирования произошла ошибка: {}'.format( m ) )


  def tearDown(self):

    if self.mnt.exists():
      log.info( '  Попытка размонтировать тестовую файловую систему(tfs)...' )
      code, out = x( 'umount {}'.format( self.mnt.as_posix() ), Return.codeout )

      self.assert_( code == 0, 'При размонтировании тестовой файловой системы произошла ошибка [{}]: {}'.format( code, out ) )

      self.mnt.rmdir()

    if self.tfs.exists():
      self.tfs.unlink()

    log.info( '  Удаление тестового пользователя ...' )
    self.U.delete( quiet=True )
