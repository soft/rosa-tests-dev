# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os

from modules.kplpack.utils import x, Return
from subprocess import getstatusoutput
from time import sleep

class testRAID(BaseTest):
  """
  Тест возможности программной организации дисковых разделов в RAID
  уровней 0, 1, 5 и их комбинаций, а также томов LVM
  http://www.anthonyldechiaro.com/blog/2010/12/19/lvm-loopback-how-to/
  """

  def setUp(self):
    super(testRAID, self).setUp()

    self.diskFiles = ['/var/tmp/virtpart1', '/var/tmp/virtpart2', '/var/tmp/virtpart3']
    self.devices = []
    self.raidDev = '/dev/md0'

    for df in self.diskFiles:
      errcode, output = getstatusoutput( 'dd if=/dev/zero of={} bs=1024 count=30720'.format( df ) )
      self.assertEqual(errcode, 0, output)
      # find and change free loop device
      errcode, device = getstatusoutput( 'losetup -f' )
      self.assertEqual(errcode, 0, device)
      self.devices.append(device)
      # attach loopback device with disk file
      errcode, output = getstatusoutput( 'losetup {} {}'.format( device, df ) )
      self.assertEqual(errcode, 0, output)
    pass

  def delRaid(self, dev):

    errcode, output = getstatusoutput( 'mdadm -S {}'.format( dev ) )
    self.assertEqual( errcode, 0, 'Ошибка остановки устройства RAID:\n{}'.format( output ) )
    for device in self.devices:
      errcode, output = getstatusoutput( 'mdadm --zero-superblock {}'.format( device ) )
      self.assertEqual( errcode, 0, 'Ошибка размонтирования раздела RAID:\n{}'.format( output ) )
    pass

  def runTest(self):

    for level in [1, 0]:
      errcode, output = getstatusoutput(  'mdadm --create {} '.format( self.raidDev )
                                           + '--metadata=0.90 --level={} '.format( level )
                                           + '--raid-devices=2 {} {}'.format( self.devices[0], self.devices[1] )
                                        )

      self.assertEqual( errcode, 0, 'Ошибка создания RAID-массива уровня {}.'.format( level ) )
      sleep(1)

      for line in x('cat /proc/mdstat', return_type=Return.stdout).split(';'):
        log.info( '  {}'.format( line ) )

      self.delRaid(self.raidDev)

    errcode, output = getstatusoutput( 'mdadm --create {} --raid-devices=3 '.format( self.raidDev )
                                       + '--level=5 {} {} {}'\
                                         .format( self.devices[0], self.devices[1], self.devices[2] )
                                      )

    self.assertEqual(errcode, 0, "Ошибка создания RAID массива уровня 5.")
    sleep(1)

    for line in x('cat /proc/mdstat', return_type=Return.stdout).split(';'):
      log.info( '  {}'.format( line ) )

    self.delRaid( self.raidDev )


  def tearDown(self):
    log.info('  Восстановление исходных параметров...')
    errcode, output = getstatusoutput( 'mdadm -S {}'.format( self.raidDev ) )
    #log.info(f'  {output}')
    sleep(1)
    for df in self.devices:
      errcode, output = getstatusoutput( 'losetup -d {}'.format( df ) )
      logIfError(errcode, output)
      self.assertEqual(errcode, 0, output)
    for filepath in self.diskFiles:
      try:
        os.remove(filepath)
      except OSError as e:
        log.exception(str(e))
    pass

def logIfError(errcode, msg):
    if errcode:
        log.error(msg)