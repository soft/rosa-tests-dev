# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from time import sleep
from subprocess import getstatusoutput, Popen, PIPE


class testRemoteDesktop(BaseTest):
    """
    Проверка возможности изделия поддерживать механизмы доступа
    к удаленному рабочему столу
    """
    vncserver = None
    client    = '/usr/lib64/rosa-tests/test_vnc'

    def runTest(self):
        log.debug('  Подготовка к запуску VNC-сервера...')
        password = '123321'
        port = 55990
        errcode, output = getstatusoutput('echo -e "%(pass)s\n%(pass)s" |'
                                          ' vncpasswd /var/tmp/vncpass' % {'pass': password})
        self.assertEqual(errcode, 0, 'Не удалось задать пароль'
                                     ' для подключения.\n(%s)' % output)
        log.debug('  Запуск VNC-сервера...')
        self.vncserver = Popen(['x0vncserver',
                                           '--passwordFile=/var/tmp/vncpass',
                                           '--rfbport=%d' % port],
                                          stdout=PIPE, stderr=PIPE)
        sleep(1)
        if self.vncserver.poll() is not None:
            self.fail('Ошибка при запуске VNC-сервера.\n' +
                      'Сообщения сервера:\n' +
                      self.vncserver.stderr.read())
        log.debug('  Проверка подключения VNC-клиента...')
        errcode, output = getstatusoutput('%s localhost:%d -password %s' %
                                          (self.client, port, password))
        self.assertEqual(errcode, 0,
                         'При проверке VNC-подключения произошла ошибка.\n' +
                         'Сообщения клиента:\n' + output)

    def tearDown(self):
        finishProcess(self.vncserver, 'VNC-сервера')

def finishProcess(proc, procName):
    if proc and proc.poll() is None:
        try:
            log.debug('  Завершение работы %s...' % procName)
            proc.terminate()
        except OSError:
            pass
