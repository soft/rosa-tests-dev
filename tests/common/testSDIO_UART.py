# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from subprocess import getstatusoutput
from tempfile import mkdtemp

class testSDIO_UART(BaseTest):
  """
  Тест поддержки стандарта обмена данными с периферийными устройствами
  через слот расширения формата SD SDIO
  """

  def setUp(self):
    super(testSDIO_UART, self).setUp()

    self.sdDev = '/dev/sdc1'
    self.mntDir = mkdtemp()

  def getExecutables(self):
    return ['mount -t vfat %s %s' % (self.sdDev, self.mntDir)]

  def tearDown(self):
    getstatusoutput('umount %s' % self.mntDir)
