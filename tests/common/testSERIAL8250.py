# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import re

from struct import pack, unpack
from subprocess import Popen
from time import sleep

class testSERIAL8250(BaseTest):
    """
    Тест поддержки универсального асинхронного приѐмопередатчика UART
    в стандартах RS232/RS485
    """

    def setUp(self):
        super(testSERIAL8250, self).setUp()
        self.stremout = open("/var/tmp/testSERIAL8250F", 'w+b')
        self.ttyss = ['/dev/ttySm0', '/dev/ttySm1']
        self.proc = None

    def tearDown(self):
        self.stremout.close()
        for item in self.ttyss:
            try:
                os.unlink(item)
                if self.proc is not None:
                    self.proc.terminate()
            except OSError as e:
                log.exception(e.message)

    def runTest(self):
        self.proc = Popen('socat -d -d pty,raw,echo=0 pty,raw,echo=0',
                                     stdout=self.stremout,
                                     stderr=self.stremout,
                                     shell=True)
        sleep(3)
        curPos = self.stremout.tell()
        self.stremout.seek(0)
        content = self.stremout.readlines()
        self.stremout.seek(curPos)
        i = 0
        for line in content:
            reg = re.search(r"/dev/pts/\d+", line.decode("utf-8"))
            if reg:
                pts = reg.group()
                os.symlink(pts, self.ttyss[i])
                i += 1

        with open(self.ttyss[0], 'wb') as f1:
            content1 = 'a'
            f1.write(pack('c'.encode(), content1.encode()))
        sleep(1)
        with open(self.ttyss[1], 'rb') as f2:
            content2 = unpack('c', f2.read(1))[0]
        self.assertEqual(content1, content2.decode())
