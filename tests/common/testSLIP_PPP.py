# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import re

from modules.kplpack.utils import x, Return, Indicator
from subprocess import Popen, PIPE, getstatusoutput
from time import sleep

class testSLIP_PPP(BaseTest):
  """
  Тест поддержки управления коммутируемыми линиями SLIP, PPP средствами ядра
  """

  socat = None
  sl0   = None
  sl1   = None
  ppp0  = None
  ppp1  = None

  def runTest(self):

    toggleRosaDeviceManager()

    with Indicator( ' Тестирование SLIP...' ):

      log.info( 'Начинается проверка управления линиями SLIP...' )
      log.debug( '  Связывание последовательных портов (socat)...' )

      self.socat = Popen( [ 'socat PTY,link=/dev/ttyS10 PTY,link=/dev/ttyS11' ],
                                      stdout=PIPE,
                                      stderr=PIPE,
                                      shell=True )
      sleep(3); os.sync()

      if self.socat.poll() is not None:
        self.fail( 'Ошибка при запуске socat.\nСообщения программы:\n' + self.socat.stderr.read() )

      log.debug( '  Подключение сетевого интерфейса к последовательному порту 0 (slattach)...' )

      self.sl0 = Popen( [ 'slattach -p slip /dev/ttyS10' ],
                                    stdout=PIPE,
                                    stderr=PIPE,
                                    shell=True)

      log.debug( '  Подключение сетевого интерфейса к последовательному порту 1 (slattach)...' )

      self.sl1 = Popen( ['slattach -p slip /dev/ttyS11'],
                                    stdout=PIPE,
                                    stderr=PIPE,
                                    shell=True )

      sleep(3); os.sync()
      log.debug( '  Настройка IP...' )
      errcode, output = getstatusoutput('ifconfig sl0 10.255.0.100 && ifconfig sl1 10.255.0.101')

      self.assertEqual( errcode, 0, 'Назначить IP-адреса устройствам sl0/sl1 не удалось.\n{}'.format( output ) )

      log.debug( '  Проверка соединения...' )
      errcode, output = getstatusoutput( 'ping -c 1 -r -W 1 10.255.0.101 -I sl0' )
      log.debug( '  Получение информации об интерфейсе sl1...' )

      errcode, output = getstatusoutput( 'ifconfig sl1' )
      self.assertEqual( errcode, 0, 'Получить информацию об интерфейсе sl1 не удалось. {}'.format( output ) )

      lere = None
      rxre = None

      # sl1: flags=4305<UP,POINTOPOINT,RUNNING,NOARP,MULTICAST>  mtu 296
      # inet 10.255.0.101  netmask 255.255.255.255  destination 10.255.0.101
      # slip  txqueuelen 10  (Serial Line IP)
      # RX packets 24  bytes 5228 (5.1 KiB)
      # RX errors 0  dropped 0  overruns 0  frame 0
      # TX packets 22  bytes 5060 (4.9 KiB)
      # TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

      for line in output.split( '\n' ):
        if not (lere):
          lere = re.search( r'slip\s+.*\((.*?)\)\s*$', line )
          if not lere:
            lere = re.search( r'^sl1\s+Link encap:(.*?)\s*$', line )
          if lere:
            self.assertEqual( lere.group(1), 'Serial Line IP', 'Интерфейс sl1 использует неверную инкапсуляцию ({}).'
                                                              .format( lere.group(1) ) )
        if not (rxre):
          rxre = re.search( r'\s*RX packets?\s*(\d+)', line )
          if rxre:
            self.assert_( rxre.group(1) != '0', 'Интерфейс sl1 не получил пакетов.' )

      self.assertTrue( lere is not None, 'Получить тип инкапсуляции интерфейса sl1 не удалось.' )
      self.assertTrue( rxre is not None, 'Получить число пакетов для интерфейса sl1 не удалось.' )

      log.info( 'Проверка управления линиями SLIP пройдена успешно.' )
      finishProcess( self.sl0, 'slattach (0)' )
      finishProcess( self.sl1, 'slattach (1)' )
      finishProcess( self.socat, 'socat' )

    with Indicator( ' Тестирование PPP...' ):

      sleep(1)
      log.info( 'Начинается проверка управления линиями PPP...' )
      log.debug( '  Связывание последовательных портов (socat)...' )
      self.socat = Popen( [ 'socat PTY,link=/dev/ttyS10 PTY,link=/dev/ttyS11' ],
                                      stdout=PIPE,
                                      stderr=PIPE,
                                      shell=True )
      sleep(1)
      if self.socat.poll() is not None:
          self.fail('Ошибка при запуске socat.\nСообщения программы:\n' + self.socat.stderr.read() )

      log.debug('  Установление принимающей стороны соединения PPP через последовательный порт 0 (pppd)...')

      self.ppp0 = Popen( [ 'pppd -detach /dev/ttyS10 10.255.1.1: nomagic' ],
                                    stdout=PIPE,
                                    stderr=PIPE,
                                    shell=True )

      log.debug( '  Установление вызывающей стороны соединения PPP через последовательный порт 1 (pppd)...' )

      self.ppp1 = Popen( ['pppd -detach /dev/ttyS11 10.255.1.2: nomagic'],
                                    stdout=PIPE,
                                    stderr=PIPE,
                                    shell=True )
      iface = None

      sleep(5)
      while not iface:
        pline = self.ppp1.stdout.readline().decode("utf-8")
        if pline:
          ifre = re.search(r'^Using interface\s+(.*)$', pline)
          if ifre:
            iface = ifre.group(1).strip()
        else:
          break

      self.assertTrue( iface is not None, 'Получить название интерфейса для вызывающей стороны соединения не удалось.\n'
                                          'Получено сообщение: ' + pline )

      log.debug( '  Получение информации об интерфейсе {}...'.format( iface ) )

      errcode, output = getstatusoutput( 'ifconfig {}'.format( iface ) )

      self.assertEqual( errcode, 0, 'Получить информацию об интерфейсе {} не удалось.\n{}'.format( iface, output ) )

      lere = None
      adre = None
      for line in output.split( '\n' ):
        if not (lere):
          lere = re.search( r'ppp\s+.*\((.*?)\)\s*$', line )
          if not lere:
            lere = re.search( '^' + iface + r'\s+Link encap:(.*?)\s*$', line )
          if lere:
            self.assertEqual( lere.group(1), 'Point-to-Point Protocol', 'Интерфейс {} использует неверную инкапсуляцию ({}).'
                                                                          .format( iface, lere.group(1) ) )
        if not (adre):
          adre = re.search( r'^\s*inet\s*([\d\.]+).*destination\s*([\d\.]+)', line )
          if not adre:
            adre = re.search( r'^\s*inet addr:([\d\.]+)\s+P-t-P:([\d\.]+)', line )
          if adre:
            self.assertTrue( adre.group(2) == '10.255.1.1', 'Через интерфейс {} установлено соединение с неверным адресом ({}).'
                                                            .format( iface, adre.group(2) ) )

      self.assertTrue( lere is not None, 'Получить тип инкапсуляции интерфейса {} не удалось.'.format( iface ) )
      self.assertTrue( adre is not None, 'Получить адреса интерфейса {} не удалось.'.format( iface ) )

      log.info('Проверка управления линиями PPP пройдена успешно.')

  def tearDown(self):
    finishProcess(self.sl0, 'slattach (0)')
    finishProcess(self.sl1, 'slattach (1)')
    finishProcess(self.ppp0, 'pppd (0)')
    finishProcess(self.ppp1, 'pppd (1)')
    finishProcess(self.socat, 'socat')

    log.info( '  Перезагрузка NetworkManager...' )
    code, out = x( 'systemctl restart networkmanager.service', Return.codeout )

    if code != 0:
      log.info( 'Не удалось перезагрузить NetworkManager [{}]: {}'.format( code, out ) )

    toggleRosaDeviceManager()

def toggleRosaDeviceManager():

  if not hasattr(toggleRosaDeviceManager, 'ON'):
    toggleRosaDeviceManager.ON = True

  if toggleRosaDeviceManager.ON:
    log.info( 'rosa-device-manager: отключение блокирующего режима...' )

    code, out = x( 'rosa-device-manager -d; rosa-device-manager -n', Return.codeout )
    if code != 0:
      log.info( 'Отключить rosa-device-manager не удалось: {}.'.format( out ) )

    log.info( 'rosa-device-manager: OK' )
    toggleRosaDeviceManager.ON = False

  else:
    log.info( 'rosa-device-manager: включение блокирующего режима...' )

    code, out = x( 'rosa-device-manager -e; rosa-device-manager -b', Return.codeout )
    if code != 0:
      log.info( 'Включить rosa-device-manager не удалось: {}.'.format( out ) )

    log.info( 'rosa-device-manager: OK' )
    toggleRosaDeviceManager.ON = True

def finishProcess(proc, procName):
    if proc and proc.poll() is None:
        try:
            log.debug('  Завершение работы %s...' % procName)
            proc.terminate()
        except OSError:
            pass
