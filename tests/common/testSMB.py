# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import shutil

from modules.kplpack.utils import x, Return, fileBackup, groupCreate, User, chmodR, chownR, groupDelete

class testSMB(BaseTest):

  _conf    = '/etc/samba/smb.conf'
  old_conf = '/etc/samba/smb.conf.bkp'
  new_conf = '/usr/share/rosa-tests/test_data/samba_conf.conf'

  smb_dir       = '/var/tmp/smb'
  smb_share_dir = smb_dir + '/test_secure_share'

  test_file_name = 'welcome.txt'
  test_file_path = '{}/{}'.format( smb_share_dir, test_file_name )
  test_content_l = 'Hello, samba client!'
  test_content_r = '\nHello, samba server!'

  user      = 'samba_user'
  user_home = '/home/{}/'.format( user )
  password  = 'topsecret'

  def runTest(self):
    log.info('Начинается тестирование samba...')

    log.info('  Создается резервная копия конфигурации samba...')
    fileBackup( self._conf, replacement=self.new_conf)

    log.info('  Создается тестовая группа...')
    groupCreate('test_smbgrp') # in /test_data/samba_conf.conf

    log.info('  Создается тестовый пользователь...')
    self.U = User( name=self.user, password=self.password, home=self.user_home, se_user='user_u', se_range='s0' )
    self.U.addGroup( 'test_smbgrp' )

    log.info('  Импорт тестового пользователя в базу samba...')
    res = x( 'echo -e "{p}\n{p}" | smbpasswd -s -a {u}'.format( p=self.password, u=self.user ), return_type=Return.process)

    self.assertEqual( res.returncode, 0, 'При импорте тестового пользователя произошла ошибка: {}'
                                         .format( res.stdout.read() ) )

    log.info('  Создаются тестовые файлы для сервера samba...')
    os.mkdir( self.smb_dir )
    os.mkdir( self.smb_share_dir )

    with open( self.test_file_path, 'w' ) as f:
      f.write( self.test_content_l )

    log.info('  Изменяются права на тестовые файлы для сервера samba...')
    chmodR( self.smb_dir, '777' )
    chownR( self.smb_share_dir, 'root', 'test_smbgrp')

    log.info('  Перезагружается сервер samba...')
    res = x( 'systemctl restart smb.service', return_type=Return.process )
    if res.returncode:
      log.error('При перезагрузке сервера samba произошла ошибка:\n'.format( res.stdout.read() ) )

    log.info('  Получение файла с сервера samba...')

    res = x( 'smbclient -U {u}%{p} //127.0.0.1/Secure -c "get {rf} {f}"'
             .format( u=self.user, p=self.password, rf=self.test_file_name, f=self.user_home + self.test_file_name ),
             return_type=Return.process )

    self.assert_( res.returncode == 0, 'При получении файла с сервера samba произошла ошибка: {}'
                                       .format( res.stdout.read() ) )

    log.info('  Проверка содержимого полученного файла.')
    with open( self.user_home + self.test_file_name, "r") as f:
      welcome = f.readlines()[0]

    self.assertEqual( welcome, self.test_content_l, 'Содержимое файла полученного с сервера samba "{}" не соответствует "{}". '
                                                    .format( welcome, self.test_content_l ) )

    log.info('  Добавление нового контента в полученный файл.')
    with open( self.user_home + self.test_file_name, "a") as f:
      f.write( self.test_content_r )

    log.info('  Отправка файла на сервер samba...')
    res = x( 'smbclient -U {u}%{p} //127.0.0.1/Secure -c "put {f} {rf}" '
             .format( u=self.user, p=self.password, f=self.user_home + self.test_file_name, rf=self.test_file_name ),
             return_type=Return.process )

    self.assert_( res.returncode == 0, 'При отправке файла на сервер samba произошла ошибка: {}'.format( res.stdout.read() ) )


    log.info('  Проверка содержимого отправленного файла на сервере samba.')
    with open( self.test_file_path, "r") as f:
      welcome = f.readlines()[1]

    self.assertEqual( welcome, self.test_content_r.replace( '\n', '' ), 'Содержимое файла на сервере samba "{}" не соответствует "{}". '
                                                                        .format( welcome, self.test_content_r.replace( '\n', '' ) ) )


  def tearDown(self):
    log.info('  Восстановление исходной конфигурации сервера samba...')
    fileBackup( self._conf, restore=True)

    log.info('  Удаление тестовой группы test_smbgrp...')
    groupDelete( 'test_smbgrp' )

    log.info('  Удаление тестовой директории "{}" ...'.format( self.smb_dir ) )
    if os.path.exists( self.smb_dir ):
      shutil.rmtree( self.smb_dir )

    log.info('  Удаление тестового пользователя ...' )
    self.U.delete( quiet=True )

    log.info('Удаление отладочной информации завершено успешно.'.format( self.user ) )
