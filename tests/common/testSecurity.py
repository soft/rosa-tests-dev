# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import audit
import selinux

class testSecurity(BaseTest):
  """
  Тест поддержки работы средств защиты информации от НСД
  """

  def setUp(self):
    try:
        self.audit_fd = audit.audit_open()
    except OSError as e:
        log.exception(e.message)
        self.assertEqual(0, e.errno,
                          'Подключиться к подсистеме аудита не удалось: ' +
                          e.message)

  def runTest(self):

    #se_enforce( StreamToLog( log ) )

    self.assertEqual( selinux.is_selinux_enabled(),          1, 'SELinux выключен.' )
    self.assertEqual( selinux.security_getenforce(),         1, 'SELinux не в режиме enforcing.' )
    self.assertEqual( audit.audit_is_enabled(self.audit_fd), 1, 'Модуль Audit выключен.' )


  def tearDown(self):

    #se_enforce( StreamToLog( log ) )

    try:
        audit.audit_close(self.audit_fd)
    except OSError as e:
        log.exception(e.message)
