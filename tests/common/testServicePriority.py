# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import multiprocessing as MP
import os
import psutil
import time

from pathlib import Path
from logging.handlers import QueueHandler
from modules.kplpack.utils import x, Return, Indicator

class testServicePriority(BaseTest):
  """ #522 """

  CPU_CORES = psutil.cpu_count()
  TMP_FILE  = '/var/tmp/testfile.stress'

  def set_param(self, param:str, val:str):

    res = x( 'cgset -r {}="{}" test'.format( param, val ), Return.process )

    log.info( '  Установлен параметр {}={}.'.format( param, val ) )

    self.assert_( res.returncode == 0, 'Не удалось установить параметр: {}={} .'.format( param, val ) )

  def set_group(self, pid:int):

    log.info( '  Добавление процесса[{}] в контролирующую группу /test...'.format( pid ) )
    code, out = x( 'cgclassify -g cpu,cpuset,memory,blkio:/test --sticky {}'.format( pid ), Return.codeout )

    self.assert_( code == 0, 'Не удалось добавить процесс[{}] в группу /test: {}'.format( pid, out ) )

  def unset_group(self, pid:int):

    log.info( '  Удаление процесса[{}] из контролирующей группы /test...'.format( pid ) )
    code, out = x( 'cgclassify -g cpu,cpuset,memory,blkio:/ {}'.format( pid ), Return.codeout )

    self.assert_( code == 0, 'Не удалось удалить процесс[{}] из группы /test: {}'.format( pid, out ) )

  def setUp(self):

    super(testServicePriority, self).setUp()

    log.info( 'Для правильной работы теста необходимо закрыть все пользовательские приложения кроме rosa-tests!' )

    log.info( '  Создание контролирующей группы /test ...' )
    res = x( 'cgcreate -g cpu,cpuset,memory,blkio:/test', Return.process )

    self.assert_( res.returncode == 0, 'Не удалось создать группы [{}]:{}'.format( res.returncode, res.stdout.read() ) )

    self.set_param( 'cpuset.cpus', '0-{}'.format( self.CPU_CORES -1 ) ) # -1 important
    self.set_param( 'cpuset.mems', '0' )

  @classmethod
  def challenge(self, q, payload):
    _log = logging.getLogger()
    _log.setLevel( logging.DEBUG )
    _log.addHandler( QueueHandler(q) )

    _log.info( '[!t║{}║]Создан процесс A: {}'.format( self.CMDLINE.table_size, os.getpid() ) )
    _log.info( '[!t║{}║]Генерация нагрузки...'.format( self.CMDLINE.table_size ) )

    payload()

    return 0

  @staticmethod
  def payload_cpu():
    x( 'stress --quiet --cpu {} --timeout 10'.format( testServicePriority.CPU_CORES ) )

  @staticmethod
  def payload_ram():
    x( 'stress --quiet --vm 1 --vm-bytes 1G --vm-hang 0 --vm-keep --timeout 15' )

  @staticmethod
  def payload_io():
    #X( 'stress --hdd 30 --hdd-bytes 4096 -t 10s' )
    x( 'timeout -s SIGKILL 5s dd if=/dev/zero of={} oflag=direct,dsync,sync bs=1M count=4000'.format( testServicePriority.TMP_FILE ) )

  def get_loaded_cores(self):

    active_cores = [ False ] * self.CPU_CORES
    counter      = 0

    for _ in range(3):
      cores = psutil.cpu_percent( interval=0.1, percpu=True )

      for i,c in enumerate(cores):
        if c > 75.0: active_cores[i] = True

    for c in active_cores:
      if c == True: counter += 1

    return counter

  def get_loaded_ram(self, pid, measurements=4):

    results = []

    for _ in range( measurements ):
      time.sleep(1)
      percent =  psutil.Process( pid ).memory_percent()

      for proc in psutil.Process( pid ).children( recursive=True ):
        percent +=  proc.memory_percent()
      results.append( percent )

    return sum( results ) / len( results )

  def get_io_serviced(self, pid):
    results = []

    b =  psutil.Process( pid ).io_counters().read_count + psutil.Process( pid ).io_counters().write_count
    results.append( b )

    for proc in psutil.Process( pid ).children( recursive=True ):
      b +=  proc.io_counters().read_count + proc.io_counters().write_count
    results.append( b )

    return sum( results ) / len( results )

  def Go(self, payload, sleep=5):

    ctx     = MP.get_context('fork')
    process = ctx.Process( target=self.challenge, args=( logging.getLogger("rts").Q, payload ) )
    #process.daemon = True
    process.start()

    if sleep:
      time.sleep( sleep ) # heating-up

    return process

  def runTest(self):

    with Indicator( ' Проведение тестов с нагрузкой CPU...'):
      log.info( '\n  Начинается проверка ограничений CPU...' )

      p           = self.Go( self.payload_cpu )
      experiment1 = self.get_loaded_cores()
      log.info( '  Определено кол-во нагруженных(>75%) вычислительных ядер: {}'.format( experiment1 ) )
      p.join()

      self.set_param( 'cpu.cfs_quota_us',  '10000'  ) # < 10% per core
      self.set_param( 'cpu.cfs_period_us', '100000' )

      self.set_group( os.getpid() )

      p           = self.Go( self.payload_cpu )
      experiment2 = self.get_loaded_cores()
      log.info( '  Определено кол-во нагруженных(>75%) вычислительных ядер: {}'.format( experiment2 ) )
      p.join()

    self.unset_group( os.getpid() )
    self.assert_( experiment1 > experiment2, "Не удалось ограничить процессу использование вычислительных ядер CPU.")

    # ------------------------------------------------------------------------------------------------------------------

    with Indicator( ' Проведение тестов с нагрузкой RAM...' ):
      log.info( '\n  Начинается проверка ограничений RAM...' )

      p = self.Go( self.payload_ram )
      experiment1 = self.get_loaded_ram( p.pid )
      log.info( '  Определено кол-во используемой RAM: {}'.format( experiment1 ) )
      p.join()


      self.set_param( 'memory.limit_in_bytes', '100M'  )
      self.set_param( 'memory.memsw.limit_in_bytes', '100M'  )
      # self.set_param( 'memory.soft_limit_in_bytes', '10M'  )
      self.set_param( 'memory.swappiness', '0' )
      self.set_param( 'memory.oom_control', '1' )
      # self.set_param( 'memory.use_hierarchy', '1' )
      self.set_group( os.getpid() )

      try:
        p = self.Go( self.payload_ram )
        experiment2 = self.get_loaded_ram( p.pid )
        log.info( '  Определено кол-во используемой RAM: {}'.format( experiment2 ) )
        p.join()

      except OSError as e:
        p.terminate()
        if e.errno == 12:
          experiment2 = 0
          log.info( '  Определено кол-во используемой RAM: {}'.format( experiment2 ) )
        else:
          self.fail( 'Произошла непредвиденная ошибка [{}]: {}'.format( e.strerror, e.errno ) )

    self.unset_group( os.getpid() )
    self.assert_( experiment1 - experiment2 > 10, "Не удалось ограничить процессу использование RAM.")

    # ------------------------------------------------------------------------------------------------------------------

    with Indicator( ' Проведение тестов с нагрузкой IO...' ):
      log.info( '\n  Начинается проверка ограничений IO...' )

      p = self.Go( self.payload_io )
      p.join()

      pth = Path( self.TMP_FILE )
      experiment1 = pth.stat().st_size
      pth.unlink()
      log.info( '  Определено кол-во записанных байт IO: {}'.format( experiment1 ) )

      self.set_param( 'blkio.throttle.read_bps_device',  '1:5 10000000' ) # /dev/zero
      self.set_param( 'blkio.throttle.read_iops_device', '1:5 1000000' )

      self.set_param( 'blkio.throttle.write_bps_device', '8:0 10000000' ) # /dev/sda
      self.set_param( 'blkio.throttle.write_iops_device', '8:0 1000000' )

      self.set_param( 'blkio.weight', '100' ) # min
      self.set_param( 'blkio.weight_device', '1:5 100' ) # min

      self.set_group( os.getpid() )

      p = self.Go( self.payload_io, 0 )
      p.join()

      pth = Path( self.TMP_FILE )
      experiment2 = pth.stat().st_size
      pth.unlink()
      log.info( '  Определено кол-во записанных байт IO: {}'.format( experiment2 ) )

    self.unset_group( os.getpid() )
    self.assert_( experiment1 - experiment2 > 30000000, "Не удалось ограничить процессу использование IO.")


  def tearDown(self):

    self.set_param( 'memory.force_empty', '0' )

    code, out = x( 'cgdelete cpu,cpuset,memory,blkio:/test', Return.codeout )

    self.assert_( code == 0, 'Не удалось удалить группы [{}]:{}'.format( code, out ) )
