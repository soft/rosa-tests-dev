# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import re

from modules.kplpack.utils import x, Return
from subprocess import getstatusoutput

class testServiceSftp(BaseTest):
    """
    Проверка наличия службы передачи файлов по протоколу SFTP
    """

    def runTest(self):
        self.assertTrue(os.path.exists("/etc/ssh/sshd_config"),
                        "Файл конфигурации службы ssh не найден.")
        errcode, output = getstatusoutput(r"grep -E '^Subsystem\s+sftp\s+' "
                                          "/etc/ssh/sshd_config")
        config_file_path = ''
        if errcode == 0:
            m = re.search(r'^Subsystem\s+sftp\s+([^\0]+)', output)
            if m:
                config_file_path = m.group(1).strip()
            if not os.path.exists(config_file_path):
                self.fail("Ошибка в тесте службы SFTP: файл %s не найден" %
                          config_file_path)
        else:
            self.fail("Подсистема sftp не настроена в конфигурации службы ssh.")
