# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from time import sleep
from modules.kplpack.utils import Indicator, StreamToNull, SystemCtl, colored as I

# active (running) -	Service or daemon is running in the background.
#                     For example, sshd or nginx/apache web server and listing for incoming traffic.
# active (exited)  -	Service successfully started from the config file.
#                     Typically one time services configuration read before Service was exited.
#                     For example, AppArmor or Firewall service.
# active (waiting) -	Our service is running but waiting for an event such as CPUS/printing event.
# inactive         - 	Service is not running.
# enabled          - 	Service is enabled at boot time.
# disabled         -	Service is disbled and will not be started at Linux server boot time.
# static           -	Service cannot be enabled on Linux, but mostly started by another systemd unit automatically.
#                     In other words, the unit file is not enabled and has no provisions for allowing in the [Install] unit file section.
# masked           -	Service is completely disabled and any start operation on it always fails.
# alias            - 	Service name is an alias. It means service is symlink to another unit file.
# linked           -	Made available through one or more symlinks to the unit file
#                     (permanently in /etc/systemd/system/ or transiently in /run/systemd/system/),
#                     even though the unit file might reside outside of the unit file search path.

class testServices(BaseTest):

  # без 'not-found' т.к. верные ссылки могут быть указаны в других сервисах
  load_errors   = [ 'bad-setting', 'failed' ]

  active_errors = [ 'failed' ]
  sub_errors    = [ 'failed' ]
  all_errors    = [ 'error' ] + load_errors + active_errors + sub_errors

  whitelist     = [ 'emergency.service',
                    'rescue.service',
                    'getty@tty1.service',
                    'plymouth-quit.service',
                    'plymouth-quit-wait.service',

                    # debug
                    # 'lm_sensors.service',
                    # 'smartd.service',
                    # 'sssd.service'
                  ]

  sctl = None

  def lamp_state(self, service:SystemCtl.Service) -> str:

    if 'inactive' == service.active:
      return I( '●', 243 )

    elif 'active' == service.active:
      return I( '●', 10 )

    elif 'failed' == service.active:
      return I( '●', 9 )

    else:
      return I( '●', 27 )

  def colored(self, state:str ) -> str:

    if state in self.all_errors:
      return I( state, 9 )

    return state

  def print_info(self, status:SystemCtl.Status, service:SystemCtl.Service):

    log.info( '    LOAD: {} | ACTIVE: {} | SUB: {}'
          .format( service.load, service.active, service.sub ))

    if status != None and service.load not in self.load_errors:

      if len( status.processes ) > 0:
        for p in status.processes:
          log.info( '    Обнаружен процесс: {}, статус: {}, код: {}'.format( p.exec or p.pid, p.code or 'в работе', p.status ) )

      log.info( '    Journal:' )

      if len( status.journal ) > 0:
        for l in status.journal:
          log.info( '      ' + l )

      else:
        log.info( '      Записей нет.' )

  def check(self, service:SystemCtl.Service) -> bool:

    status = self.sctl.status( service.unit )
    failed = False

    if status:

      if status.active == 'active':
        return True

      if status.load not in self.load_errors:
        if status.active not in self.active_errors:
          for p in status.processes:

            try:
              code = int( p.status.split('/')[0] if p.status else 0 )
            except ValueError:
              code = 1

            if code != 0:
              failed = True

          if not failed:
            return True

    self.print_info( status, service )
    return False


  def runTest(self):

    self.sctl = SystemCtl( StreamToNull() )
    services = self.sctl.get_services( all=True )

    FAILS = 0

    log.info( \
    """
    LOAD   = отображает, правильно ли был загружен unit-файл.
    ACTIVE = обобщенный статус активации сервиса.
    SUB    = (substate) статус активации зависимый от типа unit'a.
    """ )

    with Indicator( ' Проверка сервиса: ' ) as progress:

      for n,s in enumerate(services):
        progress.set_suffix( '{:27s}{:3s} {:3d}/{}'.format( s.unit[:27], '...' if len(s.unit) > 27 else '', n, len(services) ) + ' {elaps}' )

        log.info( '\n  Проверка: {} {} - {}'.format( self.lamp_state(s), I(s.unit, underline=True), s.description ) )

        if s.unit in self.whitelist:
          log.info( '    Сервис находится в доверенном списке, проверка не производится.' )
          continue

        if s.load == 'not-found':
          log.info( '    Сервис не предназначен для ручного запуска, проверка не производится.' )
          continue

        if s.load not in self.load_errors:
          if s.active == 'inactive':
            log.info( '    Попытка запуска сервиса...' )
            self.sctl.start( s.unit )
            sleep(5)

        if not self.check( s ):
          FAILS += 1
        elif s.active == 'inactive':
          log.info( I('OK', 10) )

    self.assert_( FAILS == 0, 'Кол-во проблемных сервисов: {}'.format( FAILS ) )

  def tearDown(self):
    pass