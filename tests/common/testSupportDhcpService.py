# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import fcntl
import socket

from modules.kplpack.utils import x, Return, Indicator
from struct import pack
from subprocess import call, Popen, PIPE, getstatusoutput
from time import sleep

class testSupportDhcpService(BaseTest):
    """
    Проверка обеспечения взаимодействия компьютеров в сети по протоколам
    DHCP и BOOTP (RFC 2131, 2132)
    """
    dhcpServer = None
    dhcpClient = None
    dummyLoaded = False

    def runTest(self):

        toggleRosaDeviceManager()

        log.debug('  Назначение IP-адреса...')
        errcode = call( 'ip link add name dummy0 type dummy', shell=True )
        errcode = call( 'ip address add 10.255.0.1/24 dev dummy0', shell=True )
        errcode = call( 'ip link set dev dummy0 up', shell=True )
        self.assertEqual(errcode, 0, 'Назначить IP-адрес фиктивному сетевому интерфейсу (dummy0) не удалось.\n')

        open('/var/tmp/dhcp.server.lease', 'w').close()
        with open('/var/tmp/dhcp.test.conf', 'w') as f:
            f.write('subnet 10.255.0.0 netmask 255.255.255.0 {range dynamic-bootp 10.255.0.101 10.255.0.101;}')

        log.debug('  Запуск DHCP-сервера...')
        self.dhcpServer = Popen(['dhcpd', '-d', '-f',
                                            '-lf', '/var/tmp/dhcp.server.lease',
                                            '-cf', '/var/tmp/dhcp.test.conf',
                                            'dummy0'],
                                           stdout=PIPE,
                                           stderr=PIPE )
        sleep(1)
        if self.dhcpServer.poll() is not None:
            self.fail('Ошибка при запуске DHCP-сервера.\n Сообщения сервера:\n' +
                      self.dhcpServer.stderr.read())

        #errcode, output = getstatusoutput('killall dhclient')

        log.debug('  Сброс аренды DHCP-клиента...')
        errcode, output = getstatusoutput('dhclient -r dummy0')
        self.assertEqual(errcode, 0, 'Сбросить аренду DHCP-клиента не удалось.\n' + output)

        log.debug('  Запуск DHCP-клиента...')
        self.dhcpClient = Popen( ['dhclient', '-d', '-1', 'dummy0'],
                                           stdout=PIPE,
                                           stderr=PIPE )

        log.info( '  Сброс адреса интерфейса dummy0.' )
        if x( 'ip address flush dummy0' ):
          self.fail( 'Не удалось сбросить адрес интерфейса dummy0.' )

        with Indicator( ' Ожидание смены ip сервером DHCP:' ) as indicator:
          wait = 20
          errcode = 0
          gotip = False
          while not (gotip) and wait > 1:
              if self.dhcpClient.poll() is not None:
                  break
              sleep(1)
              wait -= 1
              ip = get_ip_address('dummy0')
              indicator.set_suffix( ' осталось {} сек, текущий ip {}'.format( wait, ip ) )
              log.debug('  sec {} | ip: {}'.format(wait, ip))
              if ip == '10.255.0.101':
                  gotip = True

        if self.dhcpClient.poll() is None:
            try:
                self.dhcpClient.terminate()
            except OSError:
                pass
        output = self.dhcpClient.communicate()[1]
        errcode = self.dhcpClient.returncode
        self.assertTrue( gotip, 'Ожидаемый IP-адрес не был получен.\n'
                               'Сообщения DHCP-клиента:\n' + output.decode("utf-8") )

        log.info('\n  IP-адрес получен. Сообщения DHCP-клиента:\n' + output.decode("utf-8") )
        log.info('Проверка поддержки DHCP пройдена успешно.')

    def tearDown(self):
        finishProcess(self.dhcpServer, 'DHCP-сервера')
        toggleRosaDeviceManager()

def toggleRosaDeviceManager():

  if not hasattr(toggleRosaDeviceManager, 'ON'):
    toggleRosaDeviceManager.ON = True

  if toggleRosaDeviceManager.ON:
    log.info( 'rosa-device-manager: отключение блокирующего режима...' )

    code, out = x( 'rosa-device-manager -d; rosa-device-manager -n', Return.codeout )
    if code != 0:
      log.info( 'Отключить rosa-device-manager не удалось: {}.'.format( out ) )

    log.info( 'rosa-device-manager: OK' )
    toggleRosaDeviceManager.ON = False

  else:
    log.info( 'rosa-device-manager: включение блокирующего режима...' )

    code, out = x( 'rosa-device-manager -e; rosa-device-manager -b', Return.codeout )
    if code != 0:
      log.info( 'Включить rosa-device-manager не удалось: {}.'.format( out ) )

    log.info( 'rosa-device-manager: OK' )
    toggleRosaDeviceManager.ON = True

def finishProcess(proc, procName):
    if proc and proc.poll() is None:
        try:
            log.debug('  Завершение работы %s...' % procName)
            proc.terminate()
        except OSError:
            pass

def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            pack('256s'.encode(), ifname[:15].encode())
        )[20:24])
    except IOError:
        return '0.0.0.0'
