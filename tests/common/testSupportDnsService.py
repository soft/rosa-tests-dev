# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from subprocess import getstatusoutput

class testSupportDnsService(BaseTest):
  """ #455 """

  def runTest(self):

    errcode, output = getstatusoutput( 'dig localhost.localdomain @127.0.0.1 +short' )

    self.assertEqual( errcode, 0, 'При проверке поддержки DNS произошла ошибка.\n{}'.format( output ) )
    self.assertEqual( output.strip(), '127.0.0.1', 'При проверке поддержки DNS получен неверный ответ.\n'
                                      .format( output ) )

    log.debug( '  Имя localhost.localdomain преобразовано в адрес: {}'.format( output ) )
    errcode, output = getstatusoutput( 'dig ptr 1.0.0.127.in-addr.arpa @127.0.0.1 +short' )

    self.assertEqual( errcode, 0, 'При проверке поддержки DNS произошла ошибка.\n{}'.format( output ) )
    self.assertTrue( output.startswith( 'localhost' ), 'При проверке поддержки DNS получен неверный ответ.\n'
                                                       .format( output ) )

    log.debug( '  Адрес 127.0.0.1 преобразован в имя: {}'.format( output ) )
