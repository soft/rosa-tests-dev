# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from modules.kplpack.utils import chownR
from modules.kplpack.utils import x, Return
from pathlib import Path
from shutil import rmtree

class testSupportFileTransferServices(BaseTest):
  """
  Проверка обеспечения передачи файлов по протоколам TFTP, FTP
  (RFC 1350, 959, 1094, 913)
  """
  removeTestFiles = []
  remove_tftp_dir = False
  remove_ftp_dir = False
  turnOffTftp = False
  stopVsftpd = False
  nfsSX = False

  content = 'Test file content :)'

  def setUp(self):
    self.tftp_share_dir = Path('/var/lib/tftpboot')
    if not self.tftp_share_dir.exists():
      self.tftp_share_dir.mkdir(parents=True)
      self.remove_tftp_dir = True
      chownR(self.tftp_share_dir.as_posix(), 'root', 'root')
      self.tftp_share_dir.chmod(0o755)

    self.ftp_share_dir = Path('/var/ftp/pub')
    if not self.ftp_share_dir.exists():
      self.ftp_share_dir.mkdir(parents=True)
      self.remove_ftp_dir = True
      self.ftp_share_dir.chmod(0o755)

    super().setUp()

  def runTest(self):
    shared_file = self.tftp_share_dir / Path('test_file')

    log.info('  Начинается проверка поддержки TFTP...')
    log.info('  Создание тестового файла...')
    with shared_file.open('w') as f:
      f.write( self.content )

    shared_file.chmod(0o444)

    self.removeTestFiles.append(shared_file)
    log.info('  Загрузка тестового файла с TFTP-сервера...')
    code, out = x('curl -m 5 -s -S tftp://127.0.0.1/test_file', Return.codeout)
    log.info('  Содержимое исходного  файла:{} размер {}'.format(self.content, len(self.content)))
    log.info('  Содержимое полученого файла:{} размер {}'.format(out, len(out)))

    self.assert_(code == 0, 'Получить тестовый файл с TFTP-сервера не удалось.\n' + out)
    self.assert_(self.content == out,
      'Содержимое тестового файла отличается от исходного ("{}" != "{}").'.format(self.content, out))

    log.info('  Начинается проверка поддержки FTP...')
    log.info('  Создание тестового файла...')
    shared_file = self.ftp_share_dir / Path('test_file')
    with shared_file.open('w') as f:
      f.write(self.content)

    shared_file.chmod(0o444)

    self.removeTestFiles.append(shared_file)
    log.info('  Загрузка тестового файла с FTP-сервера...')
    code, out = x('curl -m 3 -s -S ftp://127.0.0.1/pub/test_file', Return.codeout)
    log.info('  Содержимое исходного  файла:{} размер {}'.format(self.content, len(self.content)))
    log.info('  Содержимое полученого файла:{} размер {}'.format(out, len(out)))

    self.assert_(code == 0, 'Получить тестовый файл с FTP-сервера не удалось.\n' + out)
    self.assert_(self.content == out,
      'Содержимое тестового файла отличается от исходного ("{}" != "{}").'.format(self.content, out))

    log.info('Проверка передачи файлов по протоколам TFTP, FTP пройдена успешно.')

  def tearDown(self):
    if self.removeTestFiles:
      log.info('  Удаление тестовых файлов...')
      try:
        for f in self.removeTestFiles:
          f.unlink()
        if self.remove_tftp_dir:
          rmtree(self.tftp_share_dir.as_posix())
        if self.remove_ftp_dir:
          rmtree(self.ftp_share_dir.as_posix())
      except OSError as e:
        log.exception(str(e))

