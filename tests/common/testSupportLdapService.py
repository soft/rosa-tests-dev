# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import re
import shutil

from modules.kplpack.utils import x, Return
from subprocess import getstatusoutput
from time import sleep

class testSupportLdapService(BaseTest):
  """
  Проверка обеспечения поддержки службы каталогов LDAP
  """

  service      = 'ldap'
  slapdConf    = '/etc/openldap/slapd.conf'
  slapdConfBak = '/etc/openldap/slapd.conf.backup'
  tool         = 'slaptest'
  configCopied = False

  def runTest(self):
      if self.service:
        errcode, output = getstatusoutput('service %s status' % self.service)
        if errcode == 0:
          log.debug('  Служба %s была запущена до начала проверки.' % self.service)
        elif errcode == 3:
          log.debug('  Подготавливается конфигурация службы %s...' % self.service)
          # Обход проблемы с /etc/pki/tls/private/ldap.pem
          # (этот файл содержит только закрытый ключ)
          shutil.copy2(self.slapdConf, self.slapdConfBak)
          with open(self.slapdConf, 'r') as conf:
            contents = conf.read()
          with open(self.slapdConf, 'w') as conf:
            for line in contents.split('\n'):
              if re.search(r'^\s*(TLSCertificateFile|TLSCertificateKeyFile|TLSCACertificateFile\s+)', line):
                line = '# ' + line

              conf.write(line)
              conf.write('\n')
          self.configCopied = True

          log.debug('  Запускается служба %s...' % self.service)
          errcode, output = getstatusoutput('service %s start' % self.service)

          self.assertEqual(errcode, 0, 'Запустить службу %s не удалось.\n%s' % (self.service, output))
          self.serviceStarted = True
          sleep(2)
        else:
          self.fail('Служба %s находится в неизвестном состоянии (%d).' % (self.service, errcode))

      log.info( '  Запускается проверка поддержки LDAP...' )
      errcode, output = getstatusoutput('ldapsearch -LLL -x -b "" -s base namingContexts supportedLDAPVersion')
      self.assertEqual(errcode, 0, 'При проверке поддержки LDAP произошла ошибка.\n' + output )

      ncFound = False
      vFound = False
      for line in output.split('\n'):
        ncre = re.search(r'^namingContexts:\s+(.*)$', line)
        if ncre:
          log.debug('  Получен контекст именования LDAP: %s' % ncre.group(1))
          ncFound = True
        vre = re.search(r'^supportedLDAPVersion:\s+(.*)$', line)
        if vre:
          log.debug('  Получена версия LDAP: %s' % vre.group(1))
          vFound = True

      self.assertTrue(ncFound, 'Не удалось получить контекст именования '
                                'от службы LDAP.\nПолучен ответ:\n' + output)
      self.assertTrue(vFound, 'Не удалось получить номер версии '
                              'от службы LDAP.\nПолучен ответ:\n' + output)

      code, out = x( self.tool, Return.codeout )

      for l in out.splitlines():
        log.info( '  {}'.format( l ) )

      self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

      log.info('Проверка поддержки LDAP пройдена успешно.')

  def tearDown(self):
    if self.configCopied:
      log.debug('  Восстанавливается исходная конфигурация.')
      with open(self.slapdConfBak, 'r') as confbak:
        confContents = confbak.read()
      with open(self.slapdConf, 'w') as conf:
        conf.write(confContents)
