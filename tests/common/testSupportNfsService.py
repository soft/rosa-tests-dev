# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import shutil

from pathlib import Path
from subprocess import getstatusoutput

class testSupportNfsService(BaseTest):
  """
  Проверка обеспечения поддержки сетевой файловой системы
  """
  service = 'nfs'
  nfsExported = False
  nfsMounted = False

  export_dir = Path('/var/tmp/exported')
  import_dir = Path('/var/tmp/imported')

  def prepareNfsService(self):
    log.debug('  Подготовка экспортируемого каталога...')
    shutil.rmtree( self.export_dir.as_posix(), True )
    self.export_dir.mkdir( 0o777, True, True )
    os.sync()

    log.debug('  Экспорт каталога...')
    errcode, output = getstatusoutput( 'exportfs -o fsid=0,rw,no_root_squash localhost:{}'.format( self.export_dir.as_posix() ) )
    self.assertEqual(errcode, 0, 'Экспортировать каталог не удалось.\n' + output)
    self.nfsExported = True

    log.debug('  Подготовка каталога монтирования...')
    shutil.rmtree( self.import_dir.as_posix() , True)
    self.import_dir.mkdir( 0o777, True, True )
    os.sync()

    # log.debug('  Запуск сервиса nfs-server...')
    # code, out = X( 'systemctl start nfs-server', Return.codeout )
    # self.assert_( code == 0, 'При старте сервиса nfs-server произошла ошибка [{}]:{}'.format( code, out  ) )

    log.debug('  Монтирование каталога NFS...')
    errcode, output = getstatusoutput( 'mount.nfs4 localhost:/ {}'.format( self.import_dir.as_posix() ) )
    self.assertEqual(errcode, 0, 'Смонтировать каталог NFS не удалось.\n' + output)
    self.nfsMounted = True

  def runTest(self):
    self.prepareNfsService()
    log.debug('  Запись пробного файла в смонтированный каталог...')
    with open( '{}/test_file'.format( self.import_dir.as_posix() ), 'w'):
      pass

    log.debug('  Проверка наличия файла в целевом каталоге NFS...')
    self.assertTrue(os.path.isfile('{}/test_file'.format( self.export_dir.as_posix() ) ),
                    'Проверочный файл не найден в целевом каталоге NFS.')
    log.info('Проверка поддержки NFS пройдена успешно.')

  def tearDown(self):
    if self.nfsMounted:
      log.debug('  Размонтирование каталога...')
      errcode, output = getstatusoutput( 'umount {}'.format( self.import_dir.as_posix() ) )
      if errcode != 0:
        log.info('Размонтировать каталог не удалось.\n' + output)
    if self.nfsExported:
      log.debug('  Отмена экспорта каталога...')
      errcode, output = getstatusoutput('exportfs -u 127.0.0.1:{}'.format( self.export_dir.as_posix() ) )
      if errcode != 0:
        log.info('Отменить экспорт каталога не удалось.\n' + output)

    try:
      shutil.rmtree( self.export_dir.as_posix() )
      shutil.rmtree( self.import_dir.as_posix() )

    except OSError as e:

      if e.__class__.__name__ == 'OSError' and e.errno == 116:
        log.info( '  Дескриптор директории импорта устарел, удаление не требуется.')

      else:
        raise e