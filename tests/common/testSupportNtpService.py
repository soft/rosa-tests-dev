# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import re

from subprocess import getstatusoutput

class testSupportNtpService(BaseTest):
    """
    Проверка обеспечения поддержки системы сетевого времени
    по протоколу NTP (RFC 1305)
    """
    #service = 'ntpd'

    def runTest(self):
        #self.runTestPrepare()
        log.debug('  Запускается программа синхронизации времени по NTP...')
        errcode, output = getstatusoutput('ntpdate -q 127.0.0.1')
        lines = output.split('\n')
        srvFound = False
        for line in lines:
            srv = re.search(r'^server\s+127\.0\.0\.1,\s+stratum ([0-9]+)', line)
            if srv and int(srv.group(1)) > 0:
                # Если сервер недоступен, возвращается stratum 0
                srvFound = True
                log.debug('  От сервера получены данные:\n    ' + line)
                break
        self.assertTrue( srvFound, 'Получить данные от локального сервера времени NTP не удалось.\n' + output )
