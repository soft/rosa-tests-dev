# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from subprocess import getstatusoutput
from modules.kplpack.utils import x, fileBackup

class testSupportSnmpRmonService(BaseTest):
  """
  Проверка обеспечения поддержки средств сетевого мониторинга по протоколам
  SNMP (RFC 1157) и RMON (RFC 1757)
  """
  server = "127.0.0.1"
  client = '/usr/lib64/rosa-tests/test_snmp'

  new_config = '/usr/share/rosa-tests/test_data/snmpd.conf'
  old_config = '/etc/snmp/snmpd.conf'

  def runTest(self):

    log.info('  Сохранение существующей конфигурации snmpd, создание новой...')
    fileBackup( self.old_config, replacement=self.new_config )

    #self.startService("snmpd")

    log.info('  Перезапуск сервера snmpd...')
    if x('systemctl restart snmpd.service'):
      self.assert_(False, 'Перезапустить snmpd не удалось!')


    log.info('  Запускается проверка поддержки SNMP (с сервером {})...'.format( self.server ) )
    errcode, output = getstatusoutput('{} {}'.format( self.client, self.server ) )

    self.assertEqual( errcode, 0, 'При проверке поддержки SNMP произошла ошибка [{}]: {}'.format( errcode, output ) )

    log.info( 'Проверка поддержки SNMP пройдена успешно.\n  Сообщение проверяющей утилиты:' )
    for l in output.splitlines(): log.info( '    ' + l )

  def tearDown(self):

    log.info('  Восстановление предыдущей конфигурации snmpd...')
    fileBackup( self.old_config, restore=True )
