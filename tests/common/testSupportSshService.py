# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import paramiko
import time

from modules.kplpack.utils import x, Return, User, Indicator
from paramiko.ssh_exception import NoValidConnectionsError

class testSupportSshService(BaseTest):
  """
  Проверка обеспечения удаленного входа пользователя в ОС
  по протоколу SSH (RFC 4251)
  """

  def init(self):

    self.service      = 'sshd'
    self.userName     = 'sshtester'
    self.userPassword = 'PAssw0rd'

    self.u = User( name=self.userName, password=self.userPassword, home='/home/' + self.userName )

    self.testFile      = self.u.home + '/test.file'
    self.cmdCreateFile = 'touch "{}"; sync'.format( self.testFile )

  def runTest(self):
    self.init()

    log.info('  Перезапуск сервера sshd...')
    if x('systemctl restart sshd.service'):
      self.assert_(False, 'Перезапустить sshd не удалось!')

    if os.path.isfile( self.testFile ):
      os.remove( self.testFile )

    log.debug( '  Выполняется тестовое подключение SSH...' )

    with Indicator( ' Выполняется тестовое подключение...' ):
      try:
        ssh = paramiko.SSHClient()
        ssh.load_system_host_keys()
        ssh.set_missing_host_key_policy( paramiko.WarningPolicy() )

        try:
          ssh.connect('localhost', username=self.u.name, password=self.userPassword )

        except NoValidConnectionsError as e:
          self.fail( 'При подключении произошла ошибка: {}'.format( e ) )

        stdin, stdout, stderr = ssh.exec_command( self.cmdCreateFile )
        log.info( '  Создание файла...{}'.format( stderr.read().decode('UTF-8') ) )

      except paramiko.SSHException as e:
        log.error( 'ssh failed: {}'.format( e ) )
        self.fail( 'Установить SSH-подключение не удалось: {}.'.format( e ) )

      time.sleep(5) # time to cache sync
    log.debug( '  Тестовое подключение установлено. \n  Команда создания проверочного файла исполнена.' )

    self.assertTrue( os.path.isfile( self.testFile ) , 'Проверочный файл не обнаружен.' )

    log.info( 'Проверка удалённого входа по протоколу SSH пройдена успешно.' )

  def tearDown(self):
    if os.path.isfile( self.testFile ):
      log.debug('  Удаляется тестовый файл...')
      os.remove( self.testFile )
    log.debug('  Удаление тестового пользователя...' )
    self.u.delete()
