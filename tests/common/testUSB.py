# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from pathlib import Path
from modules.kplpack.utils import x, Return

class testUSB(BaseTest):

  tool1  = 'test'
  flags1 = '-d /sys/bus/usb/devices'

  def runTest(self):
    log.info( '  Запуск проверяющей утилиты...' )
    code, out = x( self.tool1 + ' ' + self.flags1, Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )
