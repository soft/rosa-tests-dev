# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os

from modules.kplpack.utils import x, Return

class testUnicode(BaseTest):
  """
  Проверка поддержки кодировки Unicode и средств локализации
  """

  command = 'locale -a | grep ru_RU.utf8'

  def setUp(self):
      super(testUnicode, self).setUp()

      self.pre_lang = os.environ['LANG']
      if 'LANGUAGE' in os.environ:
          self.pre_language = os.environ['LANGUAGE']

  def runTest(self):
      import gettext

      testStr = "The quick brown fox jumps over the lazy dog."

      log.debug("  Проверка локализации (ru_RU.utf8)...")
      os.environ['LANG'] = 'ru_RU.UTF-8'
      if 'LANGUAGE' in os.environ:
          os.environ['LANGUAGE'] = 'ru_RU.UTF-8:ru'
      mPath = os.path.dirname(os.path.realpath(__file__))
      gettext.install('test_l10n', os.path.join(mPath, '../../test_data', 'l10n'))
      localizedBytes = bytearray(_(testStr), 'UTF-8')
      with open( os.path.join( mPath, '../../test_data', 'l10n', 'test_utf8.txt' ), 'rb' ) as f:
          utf8RuBytes = f.read()
      self.assertEqual(localizedBytes, utf8RuBytes,
                        'Локализованная строка не совпадает с ожидаемой.')
      log.debug("  Проверка локализации (en_US.utf8)...")
      os.environ['LANG'] = 'en_US.UTF-8'
      if 'LANGUAGE' in os.environ:
          os.environ['LANGUAGE'] = 'en_US.UTF-8:en'
      gettext.install('test_l10n', os.path.join(mPath, '../../test_data', 'l10n'))
      self.assertEqual(_(testStr), testStr,
                        'Строка изменена при переводе на исходный язык.')

      code, out = x( self.command, Return.codeout )

      self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )

      for l in out.splitlines():
        log.info( '  {}'.format( l ) )

  def tearDown(self):
      os.environ['LANG'] = self.pre_lang
      if 'LANGUAGE' in os.environ:
          self.pre_language = os.environ['LANGUAGE']
