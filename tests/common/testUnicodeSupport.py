# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import re

from modules.kplpack.utils import x, Return
from subprocess import Popen, PIPE

class testUnicodeSupport(BaseTest):
    """
    Проверка возможности использования изделием кодировки Unicode
    (наличие шрифтов, содержащих требуемые блоки символов Unicode)
    """

    def runTest(self):
        log.debug("  Получение списка установленных шрифтов...")
        fclist = Popen(['fc-list', '-v'], stdout=PIPE)
        fontFamily = None
        charsetList = False
        fontBlocks = {0x04: 'Кириллица',
                      0x1e: 'Дополнительная расширенная латиница',
                      0x22: 'Математические операторы',
                      0x23: 'Разнообразные технические символы',
                      0x2b: 'Разнообразные символы и стрелки'}
        blocksFound = []
        while True:
            line = fclist.stdout.readline().decode("utf-8")
            if line:
                if re.search(r'^Pattern has \d+ elts', line):
                    fontFamily = None
                else:
                    if fontFamily is None:
                        fm = re.search(r'^\s+family:\s*"([^"]*)"', line)
                        if fm:
                            fontFamily = fm.group(1)
                            charsetList = False
                    else:
                        if re.search(r'^\s*charset:\s*$', line):
                            charsetList = True
                        else:
                            if charsetList:
                                csm = re.search(r'^\s*([0-9a-f]{4}):'
                                                r'\s+([0-9a-f]{8})\s+([0-9a-f]{8})\s+([0-9a-f]{8})\s+([0-9a-f]{8})'
                                                r'\s+([0-9a-f]{8})\s+([0-9a-f]{8})\s+([0-9a-f]{8})\s+([0-9a-f]{8})',
                                                line)
                                if csm:
                                    blockNum = int(csm.group(1), 16)
                                    if (blockNum in fontBlocks and
                                            not (blockNum in blocksFound)):
                                        blocksFound.append(blockNum)
                                else:
                                    charsetList = False
            else:
                break

        for k in fontBlocks:
            self.assertTrue(k in blocksFound,
                            'Ни в одном из установленных шрифтов нет'
                            ' блока символов Unicode «%s».' % fontBlocks[k])
        log.debug('  Установленные шрифты содержат'
                     ' требуемые блоки символов Unicode: \n    ' +
                     '\n    '.join(list(fontBlocks.values())) + '.')
