# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from subprocess import getstatusoutput

class testUrpmiCustomCurl(BaseTest):
    """
    Проверка работы urpmi с произвольным Curl - в частности,
    с Curl с поддержкой шифрования по ГОСТ
    """

    def runTest(self):
        log.info('Начинается проверка работы urpmi с произвольным '
                    'Curl.')
        # Запускаем urpmi, выставив downloader в curl_gost, а curl_gost_path - в фэйковый curl,
        # в роли которого заиспользуем собственно rosa-tests.py
        # Делаем вид, что хотим установить пакет из сети - http://fake_host/fake.rpm
        # (чтобы urpmi попробовал вызвать curl и скачать пакет)
        # Указываем '--debug', чтобы urpmi печатал, как именно вызывается curl,
        # и проверяем, что вызывается наш rosa-tests.py
        errcode, output = getstatusoutput("urpmi --auto --debug "
                                          "--downloader=curl_gost "
                                          "--curl_gost_path=/usr/share/rosa-tests/rosa-tests.py "
                                          "--test http://fake_host/fake.rpm 2>&1 "
                                          "| grep -q '/usr/share/rosa-tests/rosa-tests.py' ")
        self.assertEqual(errcode, 0, 'Проверка работы urpmi с произвольным Curl '
                                     'не пройдена.\n')

        # То же самое для urpmi.addmedia - проверим, что при попытке добавить
        # удаленный репозиторий urpmi.addmedia вызовет наш rosa-tests.py
        # при попытке закачать метаданные
        errcode, output = getstatusoutput("urpmi.addmedia --auto --debug "
                                          "--downloader=curl_gost "
                                          "--curl_gost_path=/usr/share/rosa-tests/rosa-tests.py "
                                          "fake_repo http://fake_repo 2>&1 "
                                          "| grep -q '/usr/share/rosa-tests/rosa-tests.py' ")
        self.assertEqual(errcode, 0, 'Проверка работы urpmi с произвольным Curl '
                                     'не пройдена.\n')
        log.info('Проверка работы urpmi с произвольным Curl пройдена успешно.')
