# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.seTestBase import seTestBase
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import selinux
import os
import time
import sys

from modules.kplpack.utils import x, Return, User, StreamToLog, se_enforce, Terminal


class testDACMAC(seTestBase):
  """
  Тест дискреционного и мандатного принципов разграничения доступа
  """
  user             = '__default__'
  user_lvls_backup = ''

  se_user   = 'user_u'
  se_user_r = 'user_r'
  se_user_d = 'user_t'      # for processes
  se_user_t = 'user_home_t' # for files

  se_user_context = '{}:{}:{}:'.format( se_user, se_user_r, se_user_d )

  reachable_lvls  = selinux.getseuserbyname( se_user )[2].split('-')

  se_user_lvl_max = reachable_lvls[-1:][0]
  se_user_lvl_min = reachable_lvls[0]

  default_user   = User()
  rbac_file_path = os.path.expanduser( '~{}/rbac-self-test-'.format( default_user.name ) )
  rbacHelperPath = '/usr/lib64/rosa-test-suite/rbac-self-test-helper'

  def runTest(self):

    if x( 'semanage permissive -a rosatest_t' ):
      self.assertFalse(True, '  Неудачная попытка расширить права тестирующему пакету!')

    if x( 'semanage user aib_u -m -r "s0-s3"' ):
      self.assertFalse(True, '  Неудачная попытка изменить диапазон доступа пользователю aib_u!')

    if self.reachable_lvls[0] in self.reachable_lvls[-1:]:
      log.info( '  Для пользователя {} существует только уровень {}.'.format( self.se_user, self.reachable_lvls[0] )\
                + '\n  Изменение уровней доступа для пользователя {}...'.format( self.se_user ) )

      self.user_lvls_backup = selinux.getseuserbyname( self.se_user )[2]

      if x( 'semanage login {} -m -r "s0-s3"'.format( self.user ) ):
        self.assertFalse(True, '  Неудачная попытка изменить диапазон доступа пользователю user_u!')

      self.reachable_lvls  = selinux.getseuserbyname( self.se_user )[2].split('-')

      time.sleep(5)
      self.se_user_lvl_max = self.reachable_lvls[-1:][0]
      self.se_user_lvl_min = self.reachable_lvls[0]

      self.contextHigh = self.se_user_context + self.se_user_lvl_max
      self.contextLow  = self.se_user_context + self.se_user_lvl_min

      log.info('  Диапазон уровней доступа успешно изменен')

    se_enforce( StreamToLog( log ) )


    log.info('  Обнаружены следующие уровни: минимальный {}, максимальный {}.'.format( self.se_user_lvl_min, self.se_user_lvl_max ) )

    self.__mlsfile_test( usr_lvl= self.se_user_lvl_max, op='r', owner_lvl= self.se_user_lvl_min, expectfail=False )
    self.__mlsfile_test( usr_lvl= self.se_user_lvl_max, op='w', owner_lvl= self.se_user_lvl_min, expectfail=True  )

    self.__mlsfile_test( usr_lvl= self.se_user_lvl_min, op='r', owner_lvl= self.se_user_lvl_max, expectfail=True )
    self.__mlsfile_test( usr_lvl= self.se_user_lvl_min, op='w', owner_lvl= self.se_user_lvl_max, expectfail=True )

    self.__mlsfile_test( usr_lvl= self.se_user_lvl_max, op='r', owner_lvl= self.se_user_lvl_max, expectfail=False )
    self.__mlsfile_test( usr_lvl= self.se_user_lvl_max, op='w', owner_lvl= self.se_user_lvl_max, expectfail=False )

    self.__mlsfile_test( usr_lvl= self.se_user_lvl_min, op='r', owner_lvl= self.se_user_lvl_min, expectfail=False )
    self.__mlsfile_test( usr_lvl= self.se_user_lvl_min, op='w', owner_lvl= self.se_user_lvl_min, expectfail=False )


  def __mlsfile_test(self, op=False, usr_lvl='', owner_lvl='', expectfail=False):

      owner_con = self.se_user_context + owner_lvl
      usr_con   = self.se_user_context + usr_lvl

      if 'w' in op:
        testopname = 'write'
        testop = 'w'
      elif 'r' in op:
        testopname = 'read'
        testop = 'r'

      se_enforce( StreamToLog( log ) )
      log.info( 'Создание файла с контекстом [{}]'.format( self.rbac_file_path + owner_con ) )
      selinux.setexeccon(None)
      testrc = self.runcon(owner_con, self.rbacHelperPath, owner_con, User().name, 'w')
      se_enforce( StreamToLog( log ) )

      created = selinux.getfilecon_raw( '{}/rbac-self-test-{}'.format( self.default_user.home, owner_con ) )[1]
      log.info('\nСоздан файл с контекстом "{}" : {}'.format( created, self.rbac_file_path + owner_con ) )

      self.assertEqual(0, testrc, 'Не завершено создание файла с контекстом ' + owner_con)

      log.info('Пользователь с контекстом "{}" {} файл \n{}'.format( usr_con, 'читает' if 'r' in op else 'пишет в',
      'Произошел ожидаемый отказ в доступе!\n' if expectfail else '') )
      testrc = self.runcon(usr_con, self.rbacHelperPath, owner_con, self.default_user.name, testop)

      if testrc:
        # log.error('Утилита вернула: {}'.format( testrc ) )
        testrc = 1

      msg = '\nТест метки НЕ ПРОЙДЕН:\n[владелец] {}\n[пользователь] {} операция: {}\n'.format(owner_con, usr_con, testopname )

      if expectfail:
        testrc = 0 if testrc == 1 else 1
        msg += 'Ожидаемая ошибка не была получена!\n'

      if self.runcon(owner_con, self.rbacHelperPath, owner_con, self.default_user.name, 'd'):
        log.error('Не завершено удаление файла с контекстом ' + owner_con)

      if testrc:
          log.error(msg)
      self.assertEqual(0, testrc, msg)


  def tearDown(self):

    se_enforce( StreamToLog( log ) )

    with Terminal( stream=sys.stderr ) as restore:
      restore.error('\nВо время восстановления параметров безопасности произошла ошибка [{code}]:\n   {msg}')
      restore.add( 'semanage login {} -m -r "{}"'.format( self.user, self.user_lvls_backup ) )\
            .info('  Попытка восстановить диапазон доступа пользователя user_u...')
      restore.add('semanage user aib_u --modify --range "s0" -R "sysadm_r"', quiet=True, timeout=30)\
            .info('  Попытка восстановить диапазон доступа для пользователя aib_u...')
      restore.add('semanage permissive -d rosatest_t', quiet=True, timeout=30)\
            .info('  Отмена расширения прав для пакета тестирования')


    errcode = selinux.setexeccon(None)
    logIfError(errcode, 'Восстановить контекст безопасности не удалось.')


def logIfError(errcode, msg):
  if errcode:
    log.error(msg)