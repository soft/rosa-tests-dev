# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.seTestBase import seTestBase
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import selinux
import sys

from modules.kplpack.utils import Indicator, Terminal, StreamToLog, User

class testNetlabelTrafficAccessControl(seTestBase):
  """
  Проверка контроля доступа сетевого трафика с метками IPsec
  ОТМЕНЁН
  """

  user             = '__default__'
  user_lvls_backup = ''

  se_user   = 'user_u'
  se_user_r = 'user_r'
  se_user_d = 'user_t' # for processes
  se_user_t = 'home_t' # for files

  se_user_context = '{}:{}:{}:'.format( se_user, se_user_r, se_user_d )
  contextHigh     = se_user_context + 's3'
  contextLow      = se_user_context + 's0'

  server_tool = '/usr/lib64/rosa-test-suite/getpeercon_server'
  server_port = '22459'

  client_tool = 'nc -d -4'

  testuser = None

  def rotate_sec(self, _con, con_, fail=True):

    log.info('  Попытка обмена данными между клиентом и сервером:')

    _lvl = _con.split(':')[3]
    lvl_ = con_.split(':')[3]

    # sudo -u tester_a runcon user_u:user_r:user_t:s3 nc -l 23
    # sudo -u tester_a runcon user_u:user_r:user_t:s3 /usr/lib64/rosa-test-suite/netlabel_server
    # sudo -u tester_a runcon user_u:user_r:user_t:s3 /usr/lib64/rosa-test-suite/netlabel_client 127.0.0.1

    run_server = 'sudo -u {} runcon "{}" {} {}'          .format( self.testuser.name, con_, self.server_tool, self.server_port )
    run_client = 'sudo -u {} runcon "{}" {} 127.0.0.1 {}'.format( self.testuser.name, _con, self.client_tool, self.server_port  )

    with Terminal( StreamToLog( log ) ) as plan:

      plan.add( run_server, separated=True )\
          .info('    Запуск сервера: [{}]...\n'.format( con_ ) )
      plan.add( run_client, pause_before=3 )\
          .info('    Запуск клиента: [{}]...\n'.format( _con ) )
      plan.error('  {msg}')

    peer_context = None

    for l in plan.log:
      if 'NO_CONTEXT' in l:
        self.fail( '  Трафик не был помечен!' )
      if '|' in l[1]:
        peer_context = l[1].replace('\t','')

    if peer_context:
      log.info('  Сообщения [от сервера|от клиента]: {}'.format( peer_context ) )

    if fail:
      self.assertTrue( plan.fail[0] != 0, 'Ошибка выполнения {} -> {}:\n  Ожидаемый отказ не был получен!'.format( _lvl, lvl_ ) )
      log.info('  Ожидаемый отказ в доступе получен!')
    else:
      self.assertTrue( plan.fail[0] == 0, 'Ошибка выполнения {} -> {}'.format( _lvl, lvl_ ) )

    log.info('\n  OK \n')

  def runTest(self):

    self.testuser = User( 'tester', 'tester', home='/home/tester', se_user='user_u', se_range='s0-s3' )

    with Terminal( stream=sys.stderr ) as setup:
      setup.add('netlabelctl cipsov4 add pass doi:1 tags:1')\
           .info('  Включение протокола маркирования пакетов cipsov4...\n')
      setup.add('netlabelctl map del default')
      setup.add('netlabelctl map add default protocol:cipsov4,1')
      setup.add('netlabelctl unlbl accept off')\
           .info('  Блокировка немаркированного трафика включена.\n')
      setup.error('\nВо время настройки меток произошла ошибка [{code}]:\n   {msg}')

    with Indicator( ' Идет проверка меток трафика: ' ) as progress:

      progress.set_suffix( self.contextHigh + ' -> ' + self.contextHigh )
      self.rotate_sec( self.contextHigh, self.contextHigh, fail=False )

      progress.set_suffix( self.contextHigh + ' -> ' + self.contextLow )
      self.rotate_sec( self.contextHigh, self.contextLow,  fail=True  )

      progress.set_suffix( self.contextLow + ' -> ' + self.contextHigh )
      self.rotate_sec( self.contextLow,  self.contextHigh, fail=False  )

      progress.set_suffix( self.contextLow + ' -> ' + self.contextLow )
      self.rotate_sec( self.contextLow,  self.contextLow,  fail=False )


  def tearDown(self):

    with Terminal( stream=sys.stderr ) as restore:
      restore.error('\nВо время восстановления настроек сети произошла ошибка [{code}]:\n   {msg}')
      restore.add('netlabelctl unlbl accept on')\
             .info('  Восстановление исходных настроек сети...\n')
      restore.add('netlabelctl map del default')
      restore.add('netlabelctl map add default protocol:unlbl')
      restore.add('netlabelctl cipsov4 del doi:1')

    errcode = selinux.setexeccon(None)
    logIfError(errcode, '\nВосстановить контекст безопасности не удалось.')



def logIfError(errcode, msg):
  if errcode:
    log.error(msg)