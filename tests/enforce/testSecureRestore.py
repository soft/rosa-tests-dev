# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import selinux

from subprocess import getstatusoutput

class testSecureRestore(BaseTest):
  """
  Проверка обеспечения изделием надежного восстановления
  """
  testfile = 'rosa-test-restore.txt'
  filepath = '/var/tmp/' + testfile
  tarpath = '/var/tmp/' + 'rosa-test-restore.tar'

  def runTest(self):

    #se_enforce( StreamToLog( log ) )

    log.info('Начинается проверка надёжного восстановления.')
    if os.path.isfile(self.filepath):
        log.debug('  Удаление ранее созданного тестового файла...')
        os.remove(self.filepath)
    log.debug('  Создание тестового файла...')
    with open(self.filepath, 'w') as f:
        f.write('test string')
    newcon = 'user_u:object_r:user_tmp_t:s0'
    log.debug('  Назначение контекста безопасности...')
    selinux.setfilecon(self.filepath, newcon)
    log.debug('  Текущий контекст файла {}: {}'.format( self.filepath, selinux.getfilecon_raw(self.filepath)[1] ))
    if os.path.isfile(self.tarpath):
        log.debug('  Удаление ранее созданного архива...')
        os.remove(self.tarpath)
    log.debug('  Создание архива с учетом контекста...')
    errcode, output = getstatusoutput('tar --selinux -cvf %s -C /var/tmp %s' %
                                      (self.tarpath, self.testfile))
    self.assertEqual(errcode, 0,
                      'Заархивировать тестовый файл не удалось.\n' + output)
    log.debug('  Удаление тестового файла...')
    os.remove(self.filepath)
    log.debug('  Восстановление файла из архива...')
    errcode, output = getstatusoutput('cd /var/tmp && tar --selinux -xvf %s' %
                                      self.tarpath)
    self.assertEqual(errcode, 0,
                      'Восстановить тестовый файл не удалось.\n' + output)
    log.debug('  Проверка контекста безопасности...')

    fcon = selinux.getfilecon_raw(self.filepath)[1]

    self.assertEqual(fcon, newcon, 'Контекст восстановленного файла (%s) '
                                    'отличается от ожидаемого (%s).' % (fcon, newcon))
    log.info('Проверка надёжного восстановления пройдена успешно.')

  def tearDown(self):

    #se_enforce( StreamToLog( log ) )

    if os.path.isfile(self.filepath):
        log.debug('  Удаление тестового файла...')
        os.remove(self.filepath)
    if os.path.isfile(self.tarpath):
        log.debug('  Удаление архива...')
        os.remove(self.tarpath)
