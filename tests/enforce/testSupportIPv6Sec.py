# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.seTestBase import seTestBase
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import selinux
import sys

from modules.kplpack.utils import processEx, Terminal

class testSupportIPv6Sec(seTestBase):
  """
  Проверка обеспечения передачи сетевых меток по протоколам
  IPSec (RFC 2401 - 2412)
  """

  se_lvl = 's0'

  server_host = '::1'
  port        = '5006'
  server_util = '/usr/lib64/rosa-tests/getpeercon_server ' + port
  client_util = 'nc -6 -d {} {}'.format( server_host, port )

  def runTest(self):

    log.info('Запускается проверка поддержки IPSec для протокола IPv6...')

    processEx('getpeercon_serv', kill=True) # serv! not server

    self.se_lvl = selinux.getcon_raw()[1].split(':')[3]

    with Terminal( stream=sys.stderr ) as plan:

      plan.add('! fuser {}/tcp'.format( self.port ) )\
          .info('  Проверка порта {} на доступность...\n'.format( self.port ) )

      plan.add('netlabelctl calipso add pass doi:2')\
          .info('  Включение протокола маркирования пакетов CALIPSO DOI:2\n')

      plan.add('netlabelctl map del default')
      plan.add('netlabelctl map add default address:0.0.0.0/0 protocol:unlbl')
      plan.add('netlabelctl map add default address:::0/0 protocol:unlbl')
      plan.add('netlabelctl map add default address:::1 protocol:calipso,2')

      plan.add( self.server_util, separated=True, timeout=10 )\
          .info('  Запуск сервера {}:{} {}...\n'.format( self.server_host, self.port, self.server_util ))

      plan.add( self.client_util, pause_before=5 )\
          .info('  Запуск клиента... [{}]\n'.format( selinux.getcon_raw()[1] ) )

      plan.error('\nВо время настройки меток произошла ошибка [{code}]:\n   {msg}')

    peer_l = None
    for line in plan.log:
      self.assert_( 'not available' not in line[1] and
                             'null' not in line[1] and
                       'NO_CONTEXT' not in line[1] and
                                    line[0] == 0 ,
                    'При проверке поддержки IPSec для протокола IPv6 произошла ошибка.\n  ' + line[1] )
      if '|' in line[1]:
        log.info( '  Обнаружены метки: {}'.format( line[1] ) )
        peer_l = line[1].replace('\t','').split('|')[1].replace(';','').split(':')[3]

    self.assertEqual( peer_l, self.se_lvl,
                      'Ошибка: MLS\MCS уровни отправителя и получателя трафика не совпадают! {} != {}\n'.format( peer_l, self.se_lvl ))

    log.info('  MLS\MCS уровни отправителя и получателя трафика совпадают!')
    log.info('Проверка поддержки IPSec для протокола IPv6 пройдена успешно.\n')


  def tearDown(self):

    processEx('getpeercon_serv', kill=True)

    with Terminal() as plan_clean:
      #plan_clean.add('netlabelctl unlbl accept on')
      plan_clean.add('netlabelctl map del default')
      plan_clean.add('netlabelctl map add default protocol:unlbl')
      plan_clean.add('netlabelctl calipso del doi:2')

    for l in plan_clean.log:
      self.assertEqual( l[0], 0, 'Во время отката временных изменений произошла ошибка:\n{}\nТребуется перезагрузка!'.format( l[1] ) )
