# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import sys

from modules.kplpack.utils import Damper

with Damper():
  import gi
  gi.require_version( 'Gtk', '3.0' )
  from gi.repository import Gtk

class testCyrillicSupport(BaseTest):
  """
  Проверка обеспечения изделием возможности ввода и вывода информации
  в символах кириллицы
  """
  class TestDialog(Gtk.MessageDialog):
    def __init__(self, parent):
      Gtk.Dialog.__init__( self, "Проверка ввода и вывода кириллицы", parent, 0, (Gtk.STOCK_OK, Gtk.ResponseType.OK) )

      self.set_default_response( Gtk.ResponseType.OK )
      self.set_default_size(100, 200)
      self.set_resizable(False)

      self.label = Gtk.Label( 'В этом окне проверяется возможность ввода и '
                              'вывода информации в символах кириллицы.\n\n'
                              'Если вы читаете этот текст введите «да» и нажмите «OK».'
                            )
      self.label.set_margin_left(10)
      self.label.set_margin_right(10)
      self.label.set_margin_top(10)
      self.label.set_margin_bottom(10)
      self.label.set_justify(Gtk.Justification.CENTER)
      self.label.set_line_wrap(True)


      self.entry = Gtk.Entry()
      self.entry.set_activates_default(True)
      self.entry.set_max_length(10)
      self.entry.set_margin_left(10)
      self.entry.set_margin_right(10)

      box = self.get_content_area()
      box.add( self.label )
      box.add( self.entry )

      self.show_all()

  def runTest(self):

    log.debug("  Выводится диалоговое окно для проверки...")

    with Damper( sys.stderr ):
      dialog = self.TestDialog(None)
      response = dialog.run()

      if response == Gtk.ResponseType.OK:
        text = dialog.entry.get_text().strip().lower()
        log.info( "  Получен ответ от пользователя: {}".format( text ) )
      else:
        log.info( '  Диалог был закрыт без ответа.' )

      dialog.destroy()

    log.debug( "  Диалоговое окно закрыто." )
    self.assertEqual( text, 'да', 'Ожидаемый текст не был введён.' )
