# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from modules.kplpack.utils import User, processEx, waitCPU, ConRun, Terminal, StreamToLog

class testCheckingForBrowser(BaseTest):
  """
  Проверка на наличие программы просмотра html-страниц (firefox)
  """

  def runTest(self):
    log.info('  Начинается проверка наличия программы обработки гипертекстовых страниц (браузера)')

    log.info('  Создание тестового пользователя "rosa-test-user"...')
    with User( name='rosa-test-user', password='rosa-test-user', home='/home/rosa-test-user', se_user='user_u', se_range='s0-s3' ) as u:

      processEx('chrome', kill=True )
      waitCPU()

      with ConRun( u.uid, u.gid, memory_bytes=3000 ) as mem:
        test()

      self.assertEqual( mem[0], '0', 'Проверка наличия программы обработки гипертекстовых страниц (браузера) не пройдена! ')

  def tearDown(self):
    processEx('chrome', kill=True )

def test():

  tool = '/usr/lib64/rosa-tests/xtool'

  log.info('\tЗапуск браузера...')

  with Terminal( stream=StreamToLog( log ) ) as T:
    T.error( 'Во время тестирования произошла ошибка [{code}]: {msg}\n' )

    T.add( '{} "TEST PASSED" -t 30000'.format( tool ), separated=True )\
      .info( '\tПроверка браузера...' )

    T.add( 'chromium-browser /usr/share/rosa-tests/test_data/web/test.html', separated=True, timeout=15 )\
      .info( '\tЗапуск браузера Chromium...\n' )

  if T.fail[0] != 0:
    print( T.fail[0], flush=True )

  print( 0, flush=True )
  return