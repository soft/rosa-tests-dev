# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from modules.kplpack.utils import x, Return, ConRun, User

class testDesktopThemes(BaseTest):
  """
  Тест графического интерфейса KDE
  """

  def runTest(self):

    log.info( 'Начинается проверка возможности унификации меню, основных диалогов, иконок и горячих клавиш.' )

    log.info( '  Создание тестового пользователя "rosa-test-user"...' )
    with User( name='rosa-test-user', password='rosa-test-user', home='/home/rosa-test-user', se_user='user_u', se_range='s0-s3' ) as u:
      with ConRun( u.uid, u.gid, memory_bytes=3000 ) as mem:
        testDesktopThemesDX
        x( 'sync' )

      if mem[0] != '0':
        log.info( '  \n'.join( mem ) )
        self.fail( 'Во время проверки произошла ошибка : {}'.format( mem ) )

def testDesktopThemesSX(theme_name):

  waitCPU(cpuload=5, verbose=True)

  def replaceAll(textFile, searchExp, replaceExp):
    for line in fileinput.input(textFile, inplace=1):
      if searchExp in line:
          line = line.replace(searchExp, replaceExp)
      sys.stdout.write(line)

  config_tpl = (
      '[GTK]\n'
      'sNet/ThemeName=%s \n'
  ) % theme_name

  config_path = os.path.expanduser('~/.config/lxsession/LXDE')
  config_file = os.path.join(config_path, 'desktop.conf')

  if not os.path.exists(config_file):
    try:
        os.makedirs(config_path)
    except OSError:
        pass
    with open(config_file, 'w') as f:
        f.write(config_tpl)
    subprocess.getstatusoutput('lxsession -r')
    time.sleep(5)
    os.remove(config_file)
    subprocess.getstatusoutput('lxsession -r')
  else:
    status, output = subprocess.getstatusoutput('grep -E "^sNet/ThemeName" ' +
                                              config_file)
    if status != 0:
      print('В файле конфигурации LXDE нет настройки темы.', file=sys.stderr)
    else:
      m = re.search(r'sNet/ThemeName=([^\0]+)', output)
      if m:
        prevWidgetStyle = m.group(1)
        replaceAll(config_file, prevWidgetStyle, theme_name)
        subprocess.getstatusoutput('lxsession -r')
        time.sleep(5)
        replaceAll(config_file, theme_name, prevWidgetStyle)
        subprocess.getstatusoutput('lxsession -r')

def testDesktopThemesDX(theme_name='cde'):
    from PyKDE4.kdecore import KConfig, KConfigGroup
    from PyKDE4.kdeui import KGlobalSettings
    from PyQt4 import QtCore

    #waitCPU(cpuload=5, verbose=True)

    def setStyle(ws):
        kg.writeEntry("widgetStyle", ws)
        kg.sync()
        kgs.emitChange(KGlobalSettings.StyleChanged)

    app = QtCore.QCoreApplication([])
    kc = KConfig("kdeglobals")
    kg = KConfigGroup(kc, "General")
    kgs = KGlobalSettings.self()
    prevWidgetStyle = kg.readEntry("widgetStyle")
    QtCore.QTimer.singleShot(5, lambda : setStyle(theme_name))
    QtCore.QTimer.singleShot(5000, lambda : [setStyle(prevWidgetStyle),
                                             app.exit(0)])
    return app.exec_()
