# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import cairo

from modules.kplpack.utils import User, ConRun, get_display_privileges
from time import sleep

#with get_display_privileges():

class testGuiDevices(BaseTest):
  """
  Проверка функционирования средств графического интерфейса пользователя
  """

  def runTest(self):
    with User() as u:
      with ConRun(u.uid, u.gid) as mem:
        # при импорте библиотек gtk происходит обращение к xorg...
        # если код объявить вне ConRun с правами root, то gtk начнет обращаться
        # к дисплею как root и, логично, не получит доступа. Если дать доступ
        # к дисплею для root, то ATSPI будет запускаться от root, что сломает
        # все тесты работающие с ATSPI, т.к. они потеряют доступ...
        import gi
        gi.require_version( 'Gtk', '3.0' )
        gi.require_version( 'PangoCairo', '1.0' )
        from gi.repository import Gtk, Gdk, Pango, PangoCairo

        class GraphicsTest:
          def __init__(self):
            self.state = None
            self.width = 768
            self.height = 620
            self.win = Gtk.Window()
            self.win.connect("delete_event", self.close)
            self.win.set_default_size(self.width, self.height + 80)
            self.win.set_resizable(False)
            self.win.set_title('Проверка графического оборудования')
            vbox = Gtk.VBox(False, 0)
            self.win.add(vbox)

            self.drawing_area = Gtk.DrawingArea()
            self.drawing_area.set_size_request(self.width, self.height)
            self.drawing_area.connect('draw', self.draw)
            #self.pangolayout = self.drawing_area.create_pango_layout("")
            vbox.add(self.drawing_area)

            labelt = Gtk.Label(
              'В этом окне проверяются средства отображения графического '
              'интерфейса и граф. манипулятор. Если вы видите все 32 градации '
              'цветов, отображённые выше, выполните следующие действия '
              '(в противном случае закройте окно):'
            )
            labelt.set_line_wrap(True)
            labelt.set_size_request(self.width, 40)
            labelt.set_max_width_chars(1)
            labelt.set_hexpand(True)
            vbox.add(labelt)

            hbox = Gtk.HBox(False, 0)
            self.labeld = Gtk.Label('')
            self.labeld.set_use_markup(True)
            hbox.pack_start(self.labeld, False, False, 0)
            vbox.add(hbox)
            self.setState(1)

            self.win.set_events(
              Gdk.EventMask.POINTER_MOTION_MASK |
              Gdk.EventMask.BUTTON_PRESS_MASK |
              Gdk.EventMask.SCROLL_MASK
              )

            self.win.connect('event', self.handler)
            self.win.show_all()

          def setState(self, state):
            sleep(0.2)
            if state == 1:
              self.labeld.set_label('<b>1. Щёлкните в окне левой кнопкой мыши.</b>')
            elif state == 2:
              self.labeld.set_label('<b>2. Переместите указатель мыши в окне.</b>')
            elif state == 3:
              self.labeld.set_label('<b>3. Щёлкните в окне правой кнопкой мыши.</b>')
            elif state == 4:
              self.labeld.set_label('<b>4. Прокрутите колёсико прокрутки.</b>')
            elif state == 5:
              self.labeld.set_label('<b>Проверка завершена.</b>')
            self.state = state

          def getState(self):
            return self.state

          def run(self):
            Gtk.main()

          def close(self, widget, event, data=None):
            self.win.destroy()
            Gtk.main_quit()
            return False

          def handler(self, widget, event):
            if self.state == 1:
              if event.type == Gdk.EventType.BUTTON_PRESS and event.button.button == 1:
                self.setState(2)
            elif self.state == 2:
              if event.type == Gdk.EventType.MOTION_NOTIFY:
                self.setState(3)
            elif self.state == 3:
              if event.type == Gdk.EventType.BUTTON_PRESS and event.button.button == 3:
                self.setState(4)
            elif self.state == 4:
              if event.type == Gdk.EventType.SCROLL:
                self.setState(5)
                while Gtk.events_pending():
                  Gtk.main_iteration()
                sleep(1)
                self.close(self.win, event)

          def draw(self, area, cc):
            cc.rectangle(0,0, area.get_allocated_width(), area.get_allocated_height())
            cc.fill()

            CON = 65535

            colors = [ Gdk.RGBA(   0,   0, CON, 1 ),
                        Gdk.RGBA(   0, CON,   0, 1 ),
                        Gdk.RGBA( CON,   0,   0, 1 ),
                        Gdk.RGBA(   0, CON, CON, 1 ),
                        Gdk.RGBA( CON,   0, CON, 1 ),
                        Gdk.RGBA( CON, CON,   0, 1 ),
                        Gdk.RGBA( CON, CON, CON, 1 )
                      ]

            bh = self.height / len(colors)
            sh = 20
            numlevels = 32
            y = 20

            cc.set_line_width(14)
            cc.set_line_join(cairo.LINE_JOIN_MITER)
            for color in colors:
              for j in range(1, numlevels + 1):
                cc.set_source_rgba( color.red,
                                    color.green,
                                    color.blue,
                                    color.alpha * j / numlevels
                                  )
                cc.rectangle( (j - 1) * (self.width / numlevels),
                              y,
                              self.width / numlevels,
                              bh - sh
                            )
                cc.fill()
              y += bh

            layout = PangoCairo.create_layout(cc)
            font = Pango.font_description_from_string("Consolas 10")
            layout.set_font_description(font)
            cc.set_source_rgb(204,204,204) #grey80
            for j in range(1, numlevels + 1):
              layout.set_text(str(j),2)
              cc.move_to((j - 1) * (self.width / numlevels) + 4, 4)
              PangoCairo.show_layout(cc,layout)

        log.debug('  Вывод диалогового окна для проверки...')
        gt = GraphicsTest()
        gt.run()
        print(gt.state)
        log.debug('  Диалоговое окно закрыто.')
    self.assert_(mem[0] == '5', 'Пройдены не все этапы проверки.\nПоследний этап: {}.'.format(mem[0]))
    log.debug('  Пройдены все этапы проверки.')
