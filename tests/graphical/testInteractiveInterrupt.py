# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import sys
import time

import multiprocessing
from modules.kplpack.utils import User, ConRun, Terminal, StreamToLog, steal_x_cookie, waitProcess

from PyQt5.QtWidgets import QApplication, QDialog, QHBoxLayout, QLabel, QSizePolicy
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import *
from PyQt5.QtCore import QSize

class testInteractiveInterrupt(BaseTest):
  """
  Проверка возможности прерывания выполняющегося процесса для всех функций
  интерактивного интерфейса
  """

  def runTest(self):

    tool = '/usr/lib64/rosa-tests/xtool'

    log.info('  Начинается проверка прерывания процесса...')

    u = User()
    steal_x_cookie(u.name, 'root', overwrite=True)

    with ConRun(u.uid, u.gid, memory_bytes=3000, wait=False) as mem:
      log.debug('  Запускается программа, имитирующая длительный процесс...')
      ctx = multiprocessing.get_context('spawn')
      P   = ctx.Process(target=self.stupid_app, daemon=True)
      P.start()

    with Terminal(StreamToLog(log)) as term:
      term.error('Во время работы утилиты для прерывания произошла ошибка [{code}]: {msg}\n')

      term.add('{} "Проверка прерывания процесса" -t 15000 -p'.format(tool))\
        .info('  Обнаружение pid программы имитирующей длительный процесс...\n')
      term.add('{} "Проверка прерывания процесса" -t 15000'.format(tool), separated=True, pause_before=1)\
        .info('  Запускается программа принудительного прерывания процесса...\n' )
      term.add('{} "Предупреждение" -t 15000 -k Return'.format(tool), separated=True)\
        .info('  Подтверждение закрытия процесса...\n')

    errcode = term.fail[0]
    self.assertEqual(errcode, 0, 'Принудительное прерывание не выполнено.\n Сообщения программы:\n' + term.fail[1])

    pid = int(term.log[1][1].strip(';'))
    if pid != 0:
      errcode = waitProcess(pid, kill=True, timeout=20, end_pause=2)

    self.assertNotEqual(errcode, 1, 'Процесс {} превысил свое время выполнения! Корректное прерывание не выполнено.\n'.format(pid))
    self.assertNotEqual(errcode, 2, 'Процесс {} уничтожен по истечению времени! Корректное прерывание не выполнено.\n'.format(pid))

    log.info('Проверка прерывания процесса пройдена успешно.')

  @staticmethod
  def stupid_app():
    app = QApplication(sys.argv)
    dg = Warning()
    sys.exit(app.exec_())

class Warning(QDialog):

  def __init__(self, parent=None):
    super().__init__(parent)

    self.setWindowTitle('Проверка прерывания процесса!')
    self.setMinimumSize(400, 70)
    self.setFixedSize(400, 70)
    self.setWindowFlag(Qt.WindowStaysOnTopHint, True)

    self.layout = QHBoxLayout()
    self.setLayout(self.layout)

    self.icon         = QIcon.fromTheme('dialog-warning')
    self.image        = self.icon.pixmap(QSize(50,50))
    self.image_widget = QLabel('')
    self.image_widget.setAlignment(Qt.AlignRight)
    self.image_widget.setPixmap(self.image)

    self.text = QLabel('Это окно демонстрирует процесс, выполнение которого должно быть прервано.\n'
                       'ПОЖАЛУЙСТА, ДОЖДИТЕСЬ ОКОНЧАНИЯ ТЕСТА')

    self.text.setWordWrap(True)
    self.text.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))

    self.setWindowIcon(self.icon)
    self.layout.addWidget(self.text)
    self.layout.addWidget(self.image_widget)

    self.show()

    self.timer = QTimer()
    self.timer.timeout.connect(self.doom)
    self.timer.setSingleShot(True)
    self.timer.start(1000)

  def doom(self):
    time.sleep(300)

if __name__ == "__main__":
  pass

