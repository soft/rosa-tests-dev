# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import multiprocessing
import subprocess
from modules.kplpack.utils import User, ConRun, Indicator, x
from modules.kplpack.atspi import AT
from pathlib import Path

class testLOImpress(BaseTest):
  """
  Проверка возможности использования табличного процессора
  """

  test_object_name = 'LibreOffice Impress'
  test_file_name = 'test_lo_impress'
  test_file_content = 'Тест работы с {}'.format(test_object_name)
  formats = ['ppt', 'pptx', 'odp']

  def runTest(self):

    log.info('  Начинается проверка работы {}'.format(self.test_object_name))
    self.user = User()
    self.test_file_path = self.user.home + '/' + self.test_file_name

    warning = ('Во время проверки не двигайте мышью и не переключайте окна, это'
      ' может нарушить работу теста.')

    with Indicator(' Идет тестирование {}. {}'.format(self.test_object_name, warning)):
      with self.user as u:
        with ConRun(u.uid, u.gid) as mem:
          AT.force_atspi()
          for fmt in self.formats:
            self._test_creating(fmt)
            self._test_loading(fmt)
            x('sync')

      for line in mem:
        line = line.strip()
        if line == '':
          continue
        if line != '0':
          log.info('  Лог процесса:')
          log.info('    \n'.join(mem))
          self.fail('Проверка работы с {} не пройдена!'.format(self.test_object_name))

  def tearDown(self):
    for fmt in self.formats:
      trash = Path('{}.{}'.format(self.test_file_path, fmt))
      log.info('  Удаление остаточных файлов: {}'.format(trash))
      if trash.exists:
        trash.unlink()

  def check_still_alive(self, process):
    process.join(10.0)

    if process.is_alive():
      process.terminate()
      log.info('{} не был корректно закрыт.'.format(self.test_object_name))
      print(2)
      return True
    else:
      return False

  def _test_creating(self, extension='txt'):
    log.info('  Запуск {}...\n'.format(self.test_object_name))

    proc = multiprocessing.Process(
      target=subprocess.run,
      args=(['soffice', '--impress', '--norestore'],),
      daemon=True
    )
    proc.start()

    app = AT('soffice', app_init_time=10, timeout=20)
    impress = app.el('frame', self.test_object_name)

    dialog = app.el('dialog', 'Совет дня', wait=3)
    if dialog:
      app.el('push button', 'ОК', dialog).click()

    templates_dialog = app.el('dialog', 'Выберите шаблон')
    if templates_dialog:
      cancel_button = app.el('push button', 'Отменить', templates_dialog)
      cancel_button.click()

    log.info('  Проверка работы {} с форматом {}...\n'.format(self.test_object_name, extension))
    root_pane = app.el('root pane', self.test_object_name, impress)
    title = app.el('panel', 'Заглавие презентации', root_pane)
    paragraph = app.el('paragraph', '', title)
    paragraph.set_text_contents(self.test_file_content)

    menu_bar = app.el('tool bar', 'Стандартная', impress)
    button_save = app.el('push button', 'Сохранить', menu_bar, in_breadth=True)

    button_save.press()

    dlg = AT('lo_kde5filepicker', timeout=20)
    save_dialog = dlg.el('file chooser', 'Сохранить')
    file_name_editbox = dlg.el('text', node=save_dialog)
    file_name_editbox.set_text_contents(
      '{}.{}'.format(self.test_file_name, extension)
    )
    combobox = dlg.el('combo box', '(*.', node=save_dialog)
    combobox.ShowMenu()
    item = dlg.el('list item', '(*.{})'.format(extension), node=combobox, in_breadth=True)
    AT.mouse_click(item)
    dlg.el('push button', 'Сохранить', node=save_dialog).Press()

    alert = app.el('alert', 'Вопрос', in_breadth=True)
    label = app.el('label', '', alert, in_breadth=True)

    if alert:
      if 'PowerPoint 97–2003' in label.name:
        app.el('push button', 'PowerPoint 97–2003', alert, in_breadth=True).click()
      elif 'PowerPoint 2007–365' in label.name:
        app.el('push button', 'PowerPoint 2007–365', alert, in_breadth=True).click()

    app.mouse_close(impress)

    if self.check_still_alive(proc):
      return

    print( 0, flush=True )
    return

  def _test_loading(self, extension='txt'):
    log.info('  Запуск {}...\n'.format(self.test_object_name))

    proc = multiprocessing.Process(
      target=subprocess.run,
      args=(['soffice', '--impress', '--norestore', '{}.{}'.format(self.test_file_path, extension)],),
      daemon=True
    )
    proc.start()

    app = AT('soffice', app_init_time=10, timeout=20)
    impress = app.el('frame', self.test_object_name)

    dialog = app.el('dialog', 'Совет дня', wait=3)
    if dialog:
      app.el('push button', 'ОК', dialog).click()

    templates_dialog = app.el('dialog', 'Выберите шаблон')
    if templates_dialog:
      cancel_button = app.el('push button', 'Отменить', templates_dialog)
      cancel_button.click()

    log.info('  Проверка чтения {} формата {}...\n'.format(self.test_object_name, extension))

    root_pane = app.el('root pane', self.test_object_name, impress)
    paragraph = app.el('paragraph', '', root_pane, in_breadth=True)

    log.info('  Сохраненный контент: "{}"'.format(self.test_file_content))
    loaded_content = paragraph.get_text(0,-1)
    log.info('  Загруженный контент: "{}"'.format(loaded_content))

    if self.test_file_content not in loaded_content:
      log.info('Сохраненный и загруженный контент различаются!')
      print(3)
      return

    app.mouse_close(impress)

    alert = app.el('alert', 'Предупреждение')
    if alert:
      app.el('push button', 'Сохранить', node=alert).click()

    if self.check_still_alive(proc):
      return

    print( 0, flush=True )
    return