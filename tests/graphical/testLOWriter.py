# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import multiprocessing
import subprocess
from modules.kplpack.utils import User, ConRun, Indicator, x
from modules.kplpack.atspi import AT
from pathlib import Path


class testLOWriter(BaseTest):
  """
  Проверка возможности использования текстового процессора
  """

  test_object_name = 'LibreOffice Writer'
  test_file_name = 'test_lo_writer'
  test_file_content = 'Тест работы с {}'.format(test_object_name)
  formats = ['txt', 'rtf', 'odt', 'doc', 'docx']

  def runTest(self):

    log.info('  Начинается проверка работы {}'.format(self.test_object_name))
    self.user = User()
    self.test_file_path = self.user.home + '/' + self.test_file_name

    warning = ('Во время проверки не двигайте мышью и не переключайте окна, это'
      ' может нарушить работу теста.')

    with Indicator(' Идет тестирование {}. {}'.format(self.test_object_name, warning)):
      with self.user as u:
        #with ConRun(u.uid, u.gid, stdout=sys.stdout, stderr=sys.stdout) as mem:
        with ConRun(u.uid, u.gid) as mem:
          AT.force_atspi()
          for fmt in self.formats:
            self._test_creating(fmt)
            self._test_loading(fmt)
            x('sync')

      for line in mem:
        line = line.strip()
        if line == '':
          continue
        if line != '0':
          log.info('  Лог процесса:')
          log.info('    \n'.join(mem))
          self.fail('Проверка работы с {} не пройдена!'.format(self.test_object_name))

  def tearDown(self):
    for fmt in self.formats:
      trash = Path('{}.{}'.format(self.test_file_path, fmt))
      log.info('  Удаление остаточных файлов: {}'.format(trash))
      if trash.exists:
        trash.unlink()

  def check_still_alive(self, process):
    process.join(10.0)

    if process.is_alive():
      process.terminate()
      log.info('{} не был корректно закрыт.'.format(self.test_object_name))
      print(2)
      return True
    else:
      return False

  def _test_creating(self, extension='txt'):
    log.info('  Запуск {}...\n'.format(self.test_object_name))

    proc = multiprocessing.Process(
      target=subprocess.run,
      args=(['soffice', '--writer', '--norestore'],),
      daemon=True
    )
    proc.start()

    app = AT('soffice', app_init_time=10, timeout=20)
    writer = app.el('frame', self.test_object_name)

    dialog = app.el('dialog', 'Совет дня', wait=3)
    if dialog:
      app.el('push button', 'ОК', dialog).click()

    log.info('  Проверка работы {} с форматом {}...\n'.format(self.test_object_name, extension))

    document = app.el('document text', desc='Просмотр документа', node=writer)
    paragraph = app.el('paragraph', node=document)
    paragraph.set_text_contents(self.test_file_content)
    toolbar = app.el('tool bar', 'Стандартная', node=writer)
    app.el('push button', 'Сохранить', node=toolbar).press()

    dlg = AT('lo_kde5filepicker', timeout=20)
    save_dialog = dlg.el('file chooser', 'Сохранить')
    file_name_editbox = dlg.el('text', node=save_dialog)
    file_name_editbox.set_text_contents(
      '{}.{}'.format(self.test_file_name, extension)
    )
    combobox = dlg.el('combo box', '(*.', node=save_dialog)
    combobox.ShowMenu()
    item = dlg.el('list item', '(*.{})'.format(extension), node=combobox, in_breadth=True)
    AT.mouse_click(item)
    dlg.el('push button', 'Сохранить', node=save_dialog).Press()

    alert = app.el('alert', 'Вопрос', in_breadth=True)
    label = app.el('label', '', alert, in_breadth=True)

    if alert:
      if '«Текст»' in label.name:
        app.el('push button', 'Использовать формат Текст', alert,).click()
      elif '«Word 97–2003»' in label.name:
        app.el('push button', 'Word 97–2003', alert).click()
      elif '«Word 2007–365»' in label.name:
        app.el('push button', 'Word 2007–365', alert).click()
      elif '«Текст с форматированием»' in label.name:
        app.el('push button', 'Текст с форматированием', alert).click()

    app.mouse_close(writer)

    if self.check_still_alive(proc):
      return

    print( 0, flush=True )
    return

  def _test_loading(self, extension='txt'):
    log.info('  Запуск {}...\n'.format(self.test_object_name))

    proc = multiprocessing.Process(
      target=subprocess.run,
      args=(['soffice', '--writer', '--norestore', '{}.{}'.format(self.test_file_path, extension)],),
      daemon=True
    )
    proc.start()

    app = AT('soffice', app_init_time=10, timeout=20)
    writer = app.el('frame', self.test_object_name)

    dialog = app.el('dialog', 'Совет дня', wait=3)
    if dialog:
      app.el('push button', 'ОК', dialog).click()

    log.info('  Проверка чтения {} формата {}...\n'.format(self.test_object_name, extension))

    document = app.el('document text', desc='Просмотр документа', node=writer)
    paragraph = app.el('paragraph', node=document)

    log.info('  Сохраненный контент: "{}"'.format(self.test_file_content))
    loaded_content = paragraph.get_text(0,-1)
    log.info('  Загруженный контент: "{}"'.format(loaded_content))

    if self.test_file_content not in loaded_content:
      log.info('Сохраненный и загруженный контент различаются!')
      print(3)
      return

    app.mouse_close(writer)

    alert = app.el('alert', 'Предупреждение', in_breadth=True)
    if alert:
      app.el('push button', 'Сохранить', node=alert).click()

    if self.check_still_alive(proc):
      return

    print( 0, flush=True )
    return