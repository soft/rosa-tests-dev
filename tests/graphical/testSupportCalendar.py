# -*- coding: utf-8 -*-

# - required for every test --------------
from time import sleep
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import multiprocessing
import subprocess
from modules.kplpack.utils import User, ConRun, x
from modules.kplpack.atspi import AT

class testSupportCalendar(BaseTest):
  """
  Проверка возможности использования календаря
  """

  def runTest(self):

    log.info( '  Начинается проверка работы с календарем' )

    with User() as u:
      with ConRun( u.uid, u.gid, memory_bytes=3000 ) as mem:
        test()
        x('sync')

      if mem[0] != '0':
        log.info( '  \n'.join( mem ) )
        self.fail( 'Проверка работы с календарем не пройдена!' )


def test():
  log.info('\tЗапуск thunderbird с режимом календаря...\n')

  AT.force_atspi()

  proc = multiprocessing.Process(
    target=subprocess.run,
    args=(['thunderbird'],),
    daemon=True
  )
  proc.start()

  app = AT('Thunderbird', timeout=20)

  log.info('  Проверка календаря...\n')

  w_settings = app.el('frame', 'Настройка', wait=10)
  if w_settings:
    log.info('  Обнаружен диалог настройки имеющейся учетной записи.')
    app.el('push button', 'Отмена', w_settings, wait=10).press()

  app.el('push button', 'Переключиться на вкладку календаря', wait=5).press()

  if not app.el('page tab', 'Календарь', wait=5):
    log.info('Вкладка с календарём не была открыта!')
    print(1)
    return
  else:
    log.info('Вкладка календаря была обнаружена.')

  log.info('Выход из приложения...')
  menu = app.el('menu', 'Файл').click()
  app.el('menu item', 'Выход', menu).click()

  proc.join(10.0)

  if proc.is_alive():
    proc.terminate()
    log.info('Календарь не был корректно закрыт.')
    print(2)
    return

  print( 0, flush=True )
  return