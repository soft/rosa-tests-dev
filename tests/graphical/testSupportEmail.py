# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import multiprocessing
import subprocess
from modules.kplpack.utils import User, ConRun, x
from modules.kplpack.atspi import AT

class testSupportEmail(BaseTest):
  """
  Проверка возможности использования электронной почты
  """

  def runTest(self):
    log.info( '  Начинается проверка работы с электронной почтой.' )

    with User() as u:
      with ConRun(u.uid, u.gid) as mem:
        AT.force_atspi()
        test()
        x('sync')

      if mem[0] != '0':
        log.info('  \n'.join(mem))
        self.fail('Проверка работы с электронной почтой не пройдена!')


def test():
  log.info('  Открытие электронного письма в почтовой программе...\n')

  proc = multiprocessing.Process(
    target=subprocess.run,
    args=(['thunderbird', '-file', '/usr/share/rosa-tests/test_data/web/test.eml'],),
    daemon=True
  )
  proc.start()

  app = AT('Thunderbird', timeout=20)

  log.info('  Проверка почтовой программы...\n')

  if app:
    log.info('  Thunderbird обнаружен!')

  master = app.el('dialog', name='Мастер учётных записей')
  if master:
    log.info('  Обнаружен мастер учетных записей.')
    app.el('push button', 'Отмена', master, wait=10).press()

  master2 = app.el('dialog', 'Мастер учётных записей', desc='Если')
  if master2:
    log.info('  Обнаружен мастер учетных записей<2>.')
    app.el('push button', 'Отмена', master2, wait=10).press()

  document = app.el('document web', 'Test message', wait=5)
  text = app.el('section', node=document).get_text(0, -1)

  if 'Это сообщение для проверки электронной почты' in text:
    log.info('  Тестовое письмо прочитано.')
  else:
    log.info('  Не удалось прочитать тестовое письмо!')
    print(1)
    return

  app.el('menu', 'Файл').click()
  app.el('menu item', 'Закрыть').click()

  proc.join(10.0)

  if proc.is_alive():
    proc.terminate()
    log.info('kolourpaint не был корректно закрыт.')
    print(2)
    return

  print(0, flush=True)
  return