# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import shutil
import multiprocessing
import subprocess
import time
from pathlib import Path
from modules.kplpack.utils import Return, processEx, User, chownR, chmodR
from modules.kplpack.atspi import AT
from modules.kplpack.utils import x
from modules.kplpack.utils import ConRun

class testSupportGraphicFiles(BaseTest):
  """
  Проверка возможности просмотра, базового редактирования и сохранения
  графических файлов в форматах BMP, JPEG, PNG и GIF
  """
  # Тест содержит множество задержек(sleep) т.к. тестируемое приложение
  # kolourpaint некорректно отвечает dbus на "нажатия" кнопок интерфейса
  # вызвающих диалоги.

  tmpdir    = Path('/var/tmp')
  images    = Path(tmpdir / 'images')
  src       = Path('/usr/share/rosa-tests/test_data/images/')

  # если нужен доступ к dbus из под root
  # dbus_conf = Path('/usr/share/rosa-tests/test_data/dbus_share.conf')

  def runTest(self):
    log.info('Начинается проверка работы с графическими файлами.')

    u = User()

    shutil.copytree(self.src.as_posix(), self.images.as_posix())
    chownR(self.images.as_posix(), u.name, u.name)
    chmodR(self.images.as_posix(), '777')

    with ConRun(u.uid, u.gid) as mem:
      AT.force_atspi('qt')
      code = self._test()

    if mem[0] != '0':
      log.info('mem={}'.format(mem))
      self.fail('Проверка работы с графическими файлами не пройдена.\n')

  def _test(self):
    for i in self.images.iterdir():
      F = i.as_posix()
      N = i.name

      log.info('\tЗапуск графического редактора с файлом {}.'.format(F))
      proc = multiprocessing.Process(target=subprocess.run, args=(['kolourpaint', F],))
      proc.daemon = True
      proc.start()

      app = AT('kolourpaint', timeout=20)

      def save_dialog():
        dlg_save = app.el('dialog', 'Сохранение')
        if dlg_save:
          dlg_btn_save = app.el('push button', 'Сохранить', dlg_save)
          dlg_btn_save.Press()

        if N.endswith('.jpeg'):
          dlg_save_lost = app.el('dialog', 'Формат с потерей')

          if dlg_save_lost:
            app.el('push button', 'Сохранить', dlg_save_lost).Press()

          # без задержки диалог сохранения kolourpaint будет вызван повторно
          # т.к. перед закрытием он проверяет состояние файла на диске
          time.sleep(2)

      btn_flip_right = app.el('menu item', 'Повернуть вправо')
      btn_inverse = app.el('menu item', 'Инвертировать цвета')

      log.info('\tСчитывание информации о {}...\n'.format(F))
      code, out_before = x('sync; identify -format "#OLD %wx%h %[mean]" {}'.format(F), Return.codeout)

      log.info('\tПоворот изображения...\n')
      btn_flip_right.Press()

      log.info('\tИнвертирование цветов изображения...\n')
      btn_inverse.Press()

      if N.endswith( '.gif' ):
        log.info('\tОтмена изменений для gif...\n')
        app.el('menu item', 'Отменить действие:').Press()
        #app.el('menu item', 'Отменить действие:').Press()

        log.info('\tЗакрытие графического редактора...\n')
        app.el('menu item', 'Выход').Press()
        continue
      else:
        log.info('\tСохранение изменений...\n')

        # kolourpaint fail response
        try:
          app.el('menu item', 'Сохранить', in_breadth=True).Press()
        except: pass

        save_dialog()

      try:
        app.el('menu item', 'Выход').Press()
      except: pass

      if proc.is_alive():
        dlg_warn = app.el('dialog', 'Предупреждение')

        if dlg_warn:
          log.info('\tЗакрытие диалога предупреждения...')
          btn = app.el('push button', 'Сохранить', node=dlg_warn)
          btn.Press()
          log.info('\tЗакрыт')

          save_dialog()

      if proc.is_alive():
        log.info('kolourpaint не был корректно закрыт.')
        print(1)
        return

      log.info('\tСчитывание информации сохраненного изображения...\n')
      code, out_after = x('sync; identify -format "#NOW %wx%h %[mean]" {}'.format( F ), Return.codeout)

      params_old = out_before.split()[1:]
      params_now = out_after.split()[1:]

      if params_old[0] == params_now[0]:
        log.info('Изображение не было развернуто! Было разрешение: {} Стало: {} ' \
                 .format( params_old[0], params_now[0]))
        print(2)
        return

      if params_old[1] == params_now[1]:
        log.info('Цвета изображения не были изменены! Был mean: {} Стал: {}' \
                 .format( params_old[1], params_now[1] ) )
        print(3)
        return

    print(0, flush=True)
    return

  def tearDown(self):
    shutil.rmtree('/var/tmp/images', ignore_errors=True)
    processEx('kolourpaint', kill=True)
