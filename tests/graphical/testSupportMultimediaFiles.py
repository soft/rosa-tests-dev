# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import shutil
import time
from modules.kplpack.utils import User, ConRun, Terminal, StreamToLog

class testSupportMultimediaFiles(BaseTest):
  """
  Проверка возможности воспроизведения мультимедийных (аудио- и видео-) файлов
  основных форматов
  """

  def runTest(self):
    log.info('  Начинается проверка воспроизведения мультимедийных файлов.')

    self.noroot = User()
    with ConRun(self.noroot.uid, self.noroot.gid):
      with Terminal() as T:
        T.add('pulseaudio -k')

    log.info('  Создание тестового пользователя "rosa-test-user"...')
    with User(name='rosa-test-user', password='t3stThere_', home='/home/rosa-test-user') as u:
      with ConRun(u.uid, u.gid) as mem:
        test()

      # error parse
      if mem[0] != '0':
        self.fail(mem[1])

  def tearDown(self):
    if hasattr(self, 'noroot'):
      with ConRun(self.noroot.uid, self.noroot.gid, output=[1]):
        with Terminal() as T:
          T.add('pulseaudio --start', ignore_output=True)

def test():

  rmpConfig    = os.path.expanduser('~/.config/rosamp')
  tool         = '/usr/lib64/rosa-tests/xtool'
  rmpConfigOld = None

  if os.path.exists(rmpConfig):
    rmpConfigOld = os.path.expanduser('~/.config/rosamp.bak')

    if os.path.exists(rmpConfigOld):
      shutil.rmtree(rmpConfigOld)
    os.rename(rmpConfig, rmpConfigOld)

  try:

    formats = {'mp4', 'mkv', 'mpg', 'wmv', 'avi'}

    for f in formats:

      log.info('\tПроверка поддержки формата {}... '.format(f))

      search  = 'Video Play Test' if f=='mpg' else 'bbb'

      mPath = os.path.dirname(os.path.realpath(__file__))
      arg1  = os.path.join(mPath, '../../test_data', 'media', '{}.{}'.format(search, f))

      with Terminal(stream=StreamToLog(log)) as T:
        T.error('\tВо время тестирования медиа-файлов произошла ошибка [{code}]: {msg}')

        T.add('{} "Video Play Test" -t 15000'.format(tool), separated=True, quiet=True)\
          .info('\tОжидание окна "Video Play Test" в течение 7 секунд...')
        T.add('rosa-media-player "{}"'.format(arg1), separated=True, quiet=True)\
          .info('\tЗапуск медиа: {}...'.format(arg1))
        T.add('pkill rosa-media-player', ignore_errors=True, pause_before=1)

      if T.fail[0]:
        print("{}\n{}".format(*T.fail), end='')
        return
      else:
        log.info('\tOK')

    with Terminal(stream=StreamToLog(log)) as T:
      T.error('') # supress err msg

      T.add('pulseaudio --start')\
        .info('\tЗапуск звукового сервера Pulseaudio...')
      T.add('audacious -q /usr/share/rosa-tests/test_data/media/LvB_s5.mp3', separated=True, quiet=True)\
        .info('\tЗапуск Audacious c файлом LvB_s5.mp3...')
      T.add('{} "Audio Play Test" -r -t 7000'.format(tool), pause_before=5)\
        .info('\tПроверка поддержки формата mp3... ')
      T.add('pulseaudio -k')\
        .info('\tОстановка звукового сервера Pulseaudio...')

    if T.fail[0]:
      print(
        "{}\n{} Возможно, вы запускаете тест внутри виртуальной"
        " среды(конфликт со звуковой картой).".format(*T.fail), end=''
      )
      return
    else:
      print(0)
      return

  finally:
    time.sleep(1)
    shutil.rmtree(rmpConfig)

    if rmpConfigOld:
      os.rename(rmpConfigOld, rmpConfig)