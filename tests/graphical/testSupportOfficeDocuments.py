# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import re
import time
import psutil

from pathlib import Path
from subprocess import Popen, PIPE, STDOUT
from modules.kplpack.utils import User, processEx, x, Return, ConRun, Terminal, StreamToLog


class testSupportOfficeDocuments(BaseTest):
  """
  Проверка возможности создания, редактирования, сохранения и просмотра
  документов в форматах TXT, ODF, DOC, DOCX, RTF, XLS, XLSX, ODS, ODP,
  PPT и PPTX
  """
  atspi_bin = '/usr/libexec/at-spi-bus-launcher'
  xtool = '/usr/lib64/rosa-tests/xtool'
  atspi_tests = '/usr/lib64/rosa-tests'

  def preventRootDbusSession(self):
    """ Наличие в системе процесса dbus с правами root может мешать работе
        atspi. Эта функция убивает такие процессы. Временное решение.
    """
    for proc in psutil.process_iter():
      try:
        if 'dbus-daemon' in proc.name().lower():
          if 'root' == proc.username():
            proc.kill()
            log.info('  Обнаружен dbus-daemon принадлежащий root, завершаем...')
            time.sleep(5)
      except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
        pass

  def runTest(self):
    self.preventRootDbusSession()

    log.info('  Создание тестового пользователя "rosa-test-user"...')
    with User( name='rosa-test-user', password='t3stThere_', home='/home/rosa-test-user') as u:
      pid = processEx( 'at-spi-bus-launcher' )
      if pid > 0:
        x('kill -15 {}'.format(pid), return_type=Return.stdout)

      with ConRun(u.uid, u.gid) as mem:
        spi_daemon = Popen([self.atspi_bin], universal_newlines=True, stderr=STDOUT)
        if spi_daemon.poll() == None:
          test()
        else:
          if spi_daemon.returncode != 0:
            log.info('Процесс atspi завершился с ошибкой: {}'.format(
              spi_daemon.stdout.read()
            ))
          else:
            log.info('Процесс atspi заершился до начала тестирования: {}'.format(
              spi_daemon.stdout.read()
            ))

    self.assert_(mem[0] == '0', 'Проверка офисных приложений не пройдена.')

def runOfficeApp(appname, binname, ext):

  log.info('\tТестирование приложения {} с файлом формата {}...'.format(appname, ext))

  xtool   = Path(testSupportOfficeDocuments.xtool)
  tools   = Path(testSupportOfficeDocuments.atspi_tests)
  atspi_test = Path(tools / 'test_{}'.format(binname))

  testfile = os.path.expanduser('~/Пробный_документ.' + ext)

  with Terminal(StreamToLog(log)) as term:
    term.error('Во время тестирования произошла ошибка [{code}]: {msg}')

    term.add('{} --nologo --norestore'
             .format(binname), separated=True, quiet=True)\
             .info('\tЗапуск {}...\n'.format(binname))
    term.add('{} "{}" "Пробный документ в формате {}"'
             .format(atspi_test.as_posix(), testfile, ext), pause_before=5)\
             .info('\tЗапуск тестирующей программы {}...\n'.format(atspi_test.as_posix()))
    term.add('{} --nologo --norestore "{}"'
             .format(binname, testfile), separated=True, quiet=True)\
             .info('\tОткрытие созданного файла "{}"...\n'.format(testfile))
    term.add('{} "Пробный_документ.{} - {}" -t 15000'
             .format(xtool, ext, appname), separated=True, pause_before=1)\
             .info('\tПроверка созданного файла...\n')

  return term.fail[0]

def test():

  os.environ['SAL_USE_VCLPLUGIN'] = 'gtk'
  os.environ['GTK_MODULES'] = 'gail:atk-bridge'
  os.environ['GNOME_ACCESSIBILITY'] = '1'
  os.environ['NO_AT_BRIDGE'] = '0'


  tests = [
    ('LibreOffice Writer', 'lowriter', 'txt'),
    ('LibreOffice Writer', 'lowriter', 'rtf'),
    ('LibreOffice Writer', 'lowriter', 'odt'),
    ('LibreOffice Writer', 'lowriter', 'doc'),
    ('LibreOffice Writer', 'lowriter', 'docx'),
    ('LibreOffice Calc', 'localc', 'xls'),
    ('LibreOffice Calc', 'localc', 'xlsx'),
    ('LibreOffice Calc', 'localc', 'ods'),
    #too old('LibreOffice Calc', 'localc', 'sxc'),
    ('LibreOffice Impress', 'loimpress', 'odp'),
    ('LibreOffice Impress', 'loimpress', 'ppt'),
    ('LibreOffice Impress', 'loimpress', 'pptx'),
  ]

  p = None

  try:

    errcode = processEx('at-spi2-registryd')

    if errcode > 0:
      log.info('\tСлужба at-spi-registryd уже запущена.')

    else:
      log.info('\tЗапуск службы at-spi-registryd...')
      at_spi_cfg = '/usr/share/dbus-1/accessibility-services/org.a11y.atspi.Registry.service'

      if not os.path.exists(at_spi_cfg):
        log.info('\tНе найден конфигурационный файл {}.'.format(at_spi_cfg))
        return 101

      at_spi_bin = None
      with open(at_spi_cfg, 'r') as f:
        at_spi_reg = f.read()
        for line in at_spi_reg.split('\n'):
          m = re.search(r'^Exec\s?=\s?(.*)\s(.*)', line)
          if m:
            at_spi_bin = m.group(1)
            at_spi_options = m.group(2)
            break

      if not at_spi_bin:
        log.info(' \tОпределить местоположение at-spi-registryd не удалось.')
        return 102

      at_env = os.environ.copy()
      p = Popen(
        [at_spi_bin, at_spi_options],
        start_new_session=True,
        env=at_env,
        universal_newlines=True,
        stdout=PIPE, stderr=STDOUT
      )

      time.sleep(1)
      if p.poll() is not None:
        log.info('\tОшибка при запуске at-spi-registryd.Сообщения программы: {}'.format(p.stdout.read()))
        return 103

    for test in tests:
      ret = runOfficeApp(*test)
      if ret != 0:
        print(ret, flush=True)
        return

  finally:
    if p and p.poll() is None:
      try:
        log.info('Завершение работы at-spi-registryd...')
        p.terminate()
      except OSError:
        pass

  print(0, flush=True)
  return

