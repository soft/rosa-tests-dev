# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------


from pathlib import Path
from multiprocessing import Process
from subprocess import run
from modules.kplpack.utils import User, ConRun
from modules.kplpack.atspi import AT

class testSupportPdfDocuments(BaseTest):
  """
  Проверка возможности просмотра документов в формате PDF
  """

  test_data = Path( '/usr/share/rosa-tests/test_data' )
  doc       = test_data / 'document.pdf'

  def runTest(self):
    log.info('  Начинается проверка просмотра документов в формате PDF.')

    with User() as u:
      with ConRun( u.uid, u.gid, memory_bytes=3000 ) as mem:
        AT.force_atspi()
        test()

      self.assertEqual(mem[0], '0', 'Проверка просмотра PDF не пройдена')

def test():

  proc = Process(
    target=run,
    args=(['okular', testSupportPdfDocuments.doc.as_posix()],),
    daemon=True
  )
  log.info('  Запуск программы для просмотра pdf - okular...')
  proc.start()

  app = AT('okular', timeout=20)
  frame = app.el('frame', 'Документ в формате PDF')

  if not frame:
    log.info('Открытый pdf-документ не обнаружен!')
    print(1, flush=True)
    return

  log.info('  Тестовый pdf-документ обнаружен.')

  menu_file = app.el('menu item', 'Файл', frame)
  menu_file.ShowMenu()

  log.info('  Закрытие okular...')
  menu_exit = app.el('menu item', 'Выход', frame)
  menu_exit.Press()

  proc.join(10.0)

  if proc.is_alive():
    proc.terminate()
    log.info('okular не был корректно закрыт.')
    print(2)
    return

  print(0, flush=True)
  return