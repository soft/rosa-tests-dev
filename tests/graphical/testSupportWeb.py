# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import ssl
import http.server
import threading
import multiprocessing
import subprocess

from modules.kplpack.utils import User, ConRun, processEx
from modules.kplpack.atspi import AT

class HttpTestRequestHandler(http.server.BaseHTTPRequestHandler):
    proto = 'HTTP'

    def do_GET(self):
        global web_request_served
        self.send_response( 200 )
        self.send_header('Content-type', 'text-html')
        self.end_headers()
        self.wfile.write(( '<html><head><title>{} Test Page : TEST PASSED</title></head>'
                            '<body>It works (with {}).</body></html>'
                            .format( self.proto, self.proto)
                          ).encode())
        web_request_served[self.proto] = True

    def log_message(self, format, *args):
        return

class HttpsTestRequestHandler(HttpTestRequestHandler):
    proto = 'HTTPS'

class HThread(threading.Thread):
    def __init__(self, ws, proto):
        global web_request_served
        threading.Thread.__init__(self)
        self.daemon = True
        self.ws = ws
        self.proto = proto
        web_request_served[self.proto] = False

    def run(self):
        global web_request_served
        while not (web_request_served[self.proto]):
            self.ws.handle_request()

class testSupportWeb(BaseTest):
  """
  Проверка возможности работы в сети по протоколам HTTP, HTTPS
  """
  httpt = None
  httpst = None

  def runTest(self):
    global web_request_served
    log.info('  Начинается проверка работы с веб-протоколами.')

    httpd  = http.server.HTTPServer(('localhost', 50080), HttpTestRequestHandler)
    httpsd = http.server.HTTPServer(('localhost', 50443), HttpsTestRequestHandler)

    mPath = os.path.dirname(os.path.realpath(__file__))
    cert = os.path.join(mPath, '../../test_data', 'web', 'localhost.pem')

    httpsd.socket = ssl.wrap_socket(httpsd.socket, server_side=True, certfile=cert, cert_reqs=ssl.CERT_NONE)
    web_request_served = {}
    self.httpt  = HThread(httpd, 'HTTP')
    self.httpt.start()
    self.httpst = HThread(httpsd, 'HTTPS')
    self.httpst.start()

    log.info('  Начинается проверка наличия программы обработки гипертекстовых страниц (браузера)')

    with User() as u:
      with ConRun(u.uid, u.gid) as mem:
        AT.force_atspi()
        test()

    self.assertEqual(mem[0], '0', 'Проверка работы с веб-протоколами не пройдена! ')

  def tearDown(self):
    if self.httpt and self.httpt.isAlive():
      try:
        self.httpt._Thread__stop()
      except:
        pass
    if self.httpst and self.httpst.isAlive():
      try:
        self.httpst._Thread__stop()
      except:
        pass

    log.info('  Закрытие Chromium...')
    processEx('chrome', kill=True)


# Из-за старого atspi(1.32) в системе, некорректно обновляется дерево элементов в
# хромиуме. Поэтому приходится закрывать окно через убийство процесса.
def test():
  log.info('  Запускается браузер Chromium...')

  proc = multiprocessing.Process(
    target=subprocess.run,
    args=(['chromium-browser', 'http://localhost:50080/'],)
  )
  proc.start()

  app = AT('Chromium', app_init_time=10, timeout=20)

  frame = app.el('frame', 'TEST PASSED', wait=20, in_breadth=True)

  if not frame:
    log.info('Тестовая страница не была открыта!')
    print(1)
    return
  else:
    log.info('  Тестовая страница успешно открыта.')
    #app.el('push button', 'Закрыть', node=app.el('title bar')).press()

  print(0)
  return