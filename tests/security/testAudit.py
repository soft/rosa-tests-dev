# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------


# Тест аудита сокращен вследствие новых ограничений по безопасности, которые
# повлекли за собой отсутствие возможности изменять правила аудита.

# import os
# import shutil

# from modules.kplpack.utils import x, Return
# from pathlib import Path
# from time import sleep, asctime, localtime
# ~


from modules.kplpack.utils import SystemCtl, colored

class testAudit(BaseTest):
  normal_status = '0/success'
  bad_words = ['fail', 'err', 'stopped', 'not permitted']

  def print_journal(self, status):
    log.info('')
    log.info('Выдержка из journal(ключевые слова подсвечены):')
    for line in status.journal:
      for w in self.bad_words:
        line = line.replace(w, colored(w, 9))
      log.info(line)

  def runTest(self):
    log.info('Проверка состояния сервиса аудита.')
    status = SystemCtl().status('auditd.service')

    for proc in status.processes:
      # Main Pid
      if proc.exec == proc.code == proc.status == '':
        continue

      log.info('Процесс[{}]: exec={} code={} status={}.'.format(*proc))

      if proc.status != self.normal_status:
        msg = 'Состояние сервиса {} аудита не соответвует нормальному: {}!' \
              .format(proc.status, self.normal_status)
        log.info(msg)
        self.fail(msg)
      else:
        log.info('Cостояние процесса {} соответствует нормальному: {}.' \
                 .format(proc.pid, self.normal_status))
        log.info()

    # checking journal msg
    for line in status.journal:
      if any(word in line for word in self.bad_words):
        self.print_journal(status)
        log.info('Обнаружены подозрительные записи в выдержке из журнала!')
        #self.fail('Обнаружены подозрительные записи в выдержке из журнала!')

  def tearDown(self):
    pass


# class testAudit(BaseTest):
#   """
#   Тест: аудит системных событий
#   №54
#   """
#   secret_dir = Path( '/var/tmp/secret' )
#   audit_log  = Path( '/var/log/audit/audit.log' )
#   rule_key   = 'kabOom'

#   def checkLogAudit(self):

#     with open( self.audit_log.as_posix(), "r" ) as f:
#       content = f.readlines()

#     # read, write, access, rule : 4 times rule_key
#     count = 0
#     for line in content:
#       if self.rule_key in line:
#         count += 1

#     log.info('Количество прочтенных строк: {}'.format(count))
#     return count == 3

#   def runTest(self):

#     with open( "/var/log/audit/audit.log", "w+" ) as f: pass
#     log.info( '  Лог аудита очищен.')

#     log.info( '  Создание секретной директории {}...'.format( self.secret_dir.as_posix() ) )
#     if not self.secret_dir.exists():
#       self.secret_dir.mkdir()

#     code, out = x( 'auditctl -w {} -p rwa -k {}'.format( self.secret_dir.as_posix(), self.rule_key ), Return.codeout )

#     if 'immutable' in out:
#       code = 1

#     self.assert_( code == 0, 'Не удалось добавить правило аудита [{}]: {}'.format( code, out ) )
#     log.info( '  Добавлено правило аудита для директории {}'.format( self.secret_dir.as_posix() ) )

#     mtime1 = self.audit_log.stat().st_mtime
#     log.info( "  Изменен журнал аудита: {}".format( asctime( localtime( mtime1 ) ) ) )

#     sleep(1)

#     log.info( '  Осуществление доступа к секретной директории {}...'.format( self.secret_dir.as_posix() ) )

#     with open( self.secret_dir.as_posix() + '/file.txt', 'w+' ) as f:
#       f.write( 'w' )
#     with open( self.secret_dir.as_posix() + '/file.txt', 'r' ) as f:
#       f.read()

#     os.sync()

#     self.assertTrue( self.checkLogAudit(), "Сообщения audit не были зафиксированы." )

#     mtime2 = self.audit_log.stat().st_mtime
#     log.info( "  Изменен журнал аудита: {}".format( asctime( localtime( mtime2 ) ) ) )

#     self.assert_( mtime1 != mtime2, 'Файл аудита не был изменен, возможно, не были обновлены inode attributes.' )


#   def tearDown(self):
#     code, out = x( 'auditctl -W {} -p rwa -k {}'.format( self.secret_dir.as_posix(), self.rule_key ), Return.codeout )
#     self.assert_( code == 0, 'Не удалось удалить правило аудита [{}]: {}'.format( code, out ) )

#     log.info( '  Удалено правило аудита для директории'.format( self.secret_dir.as_posix() ) )

#     if self.secret_dir.exists():
#       shutil.rmtree( self.secret_dir.as_posix() )
