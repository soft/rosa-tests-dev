# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import subprocess
import crypt
import sys
import modules.kplpack.utils as utils

# trying google-style

class testAuthentication(BaseTest):

  username = 'passwordtester'

  def set_password(self, old_pass, new_pass:str):
    p = subprocess.Popen(['passwd'],
                         universal_newlines=True,
                         stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT)

    stdout, _ = p.communicate('{o}\n{p}\n{p}\n'.format(o=old_pass, p=new_pass))

    return p.returncode, stdout

  def generate_shadow_password_hash(self, password:str, hashtype:int, salt:str):
    password = crypt.crypt(password, '${}${}$'.format(hashtype, salt))
    return password

  def runTest(self):
    log.info('Создается пользователь {}...'.format(self.username))

    with utils.User(self.username, password='Gb220_kJs#65', home='/home/{}'.format(self.username)) as user:
      new_wrong_pass = [
        ('Gb220_kJs#65', 'текущий пароль',                                   False),
        ('zB2A20kJs#65', 'правильный пароль',                                 True),
        ('Gb_65#',       'меньше 8 символов',                                False),
        ('Gbbbb_65#',    'больше 3х повторяющихся символов',                 False),
        ('qwerty',       'содержится в базе cracklib',                       False),
        ('SuperSecret_', 'основан на слове из словаря cracklib',             False),
        # отдается на управление FreeIPA
        #('Gb220_kJs#65', 'пароль уже использовался',                         False),
        ('GB220_KJS#65', 'нет символов нижнего регистра',                    False),
        ('gb220_kjs#65', 'нет символов верхнего регистра',                   False),
        ('GbtIos_kJHs#', 'нет цифр',                                         False),
        ('Gb220kJs65',   'нет специальных символов или символов пунктуации', False)
      ]
      #with utils.Indicator(' Идет тестирование паролей...'):
      with utils.ConRun(user.uid, user.gid, direct=True) as mem:
        last_pass = 'Gb220_kJs#65'
        for password, description, good in new_wrong_pass:

          log.info('Устанавливается пароль "{}" c признаком "{}"...'.format(password, description))
          code, out = self.set_password(last_pass, password)
          #log.info('{} --> {} {}'.format(user.password, code, out))

          if not good and code == 0:
            print('{} -1'.format(password))
            break

          elif good and code == 0:
            last_pass = password

          elif good and code == 1:
            print('{} -2'.format(password))
            break

          elif code > 1:
            print('{} {}'.format(code, out))
            break

      # user process finished
      data = mem[0].split() if mem else []
      password, code = data if len(data) > 1 else ('ok','ok')

      if code == '-1':
        self.fail('Пароль НЕ соответствующий требованиям был принят. Пароль: "{}"'.format(password))
      elif code == '-2':
        self.fail('Пароль соответствующий требованиям был отклонён. Пароль: "{}".'.format(password))
      elif 'ok' not in password and 'ok' not in code:
        self.fail('Непредвиденная ошибка: {}'.format(mem))
