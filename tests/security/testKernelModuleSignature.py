# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from modules.kplpack.utils import Indicator, Terminal, StreamToLog, x, Return, dmesgCheck


class testKernelModuleSignature(BaseTest):

  def runTest(self):
    with Indicator( '  Проверка механизма подписи модулей ядра' ):
      with Terminal( StreamToLog( log ) ) as term:
        term.add( 'rmmod unsignedmod', ignore_errors=True ) \
            .info( '  Отключение модуля ядра unsignedmod (если включен)' )
        term.add( 'dkms remove -m unsignedmod -v 1.0 --all', ignore_errors=True )\
            .info( '  Удаление предыдущей версии unsignedmod...\n' )
        term.add( 'dkms add -m unsignedmod -v 1.0' )\
            .info( '  Подготовка исходников модуля unsignedmod...\n' )
        term.add( 'dkms build -m unsignedmod -v 1.0' )\
            .info( '  Сборка модуля unsignedmod...\n' )
        term.add( 'dkms install -m unsignedmod -v 1.0' )\
            .info( '  Установка модуля unsignedmod...\n' )
        term.add( 'dmesg -C' )\
            .info( '  Очистка dmesg...\n' )

      self.assert_( term.fail[0] == 0, 'При подготовке модуля ядра unsignedmod произошла ошибка [{}]: {}'
                                        .format( term.fail[0], term.fail[1] ) )

      log.info( '  Подключение и запуск модуля ядра unsignedmod...' )
      # out: ERROR: could not insert 'unsignedmod': Key was rejected by service
      code, out = x( 'modprobe unsignedmod', Return.codeout )

      conclusion =  dmesgCheck( 'Loading of unsigned module is rejected' ) \
                    and not dmesgCheck( 'unsignedmod: test failed!' )      \
                    and code == 1

    self.assert_( conclusion, 'Не получена ожидаемая ошибка при подключении неподписанного модуля ядра.' )

  def tearDown(self):
    with Terminal( StreamToLog( log ) ) as term:
      term.add( 'rmmod unsignedmod', ignore_errors=True ) \
          .info( '  Отключение модуля ядра unsignedmod' )
      term.add( 'dkms remove -m unsignedmod -v 1.0 --all' ) \
          .info( '  Удаление модуля ядра unsignedmod...\n' )

    self.assert_( term.fail[0] == 0, 'При удалении модуля ядра unsignedmod произошла ошибка [{}]: {}'
                             .format( term.fail[0], term.fail[1] ) )
