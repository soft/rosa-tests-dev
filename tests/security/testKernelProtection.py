# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os

from modules.kplpack.utils import kernelParamCheck, Indicator, Terminal


class testKernelProtection(BaseTest):
  """ #538 """

  paxtests_dir  = '/usr/lib64/paxtest'
  kernel_params = [ 'CONFIG_STACKPROTECTOR', 'CONFIG_STACKPROTECTOR_STRONG' ]

  # 'gcc',  - missing in package

  # ( test_name, OK_state, description )
  paxtests = \
  [
    ( 'anonmap'         , 'Killed',    ' 1. Исполнение кода в анонимном участке памяти' ),
    ( 'execbss'         , 'Killed',    ' 2. Исполнение кода в участке памяти неинициализированных переменных(bss segment)' ),
    ( 'execdata'        , 'Killed',    ' 3. Исполнение кода в участке памяти статических, глобальных переменных(инициализированных, data segment)' ),
    ( 'execheap'        , 'Killed',    ' 4. Исполнение кода в области динамической памяти(heap area)' ),
    ( 'execstack'       , 'Killed',    ' 5. Исполнение кода в области стека(stack area)' ),
    ( 'shlibbss'        , 'Killed',    ' 6. Исполнение измененного кода динамической библиотеки(bss segment)' ),
    ( 'shlibdata'       , 'Killed',    ' 7. Исполнение измененного кода динамической библиотеки(data segment)' ),
    # ( 'mprotanon'       , 'Killed',    ' 8. Атака п.1 с попыткой отключения защиты ядра используя mprotect()' ),
    # ( 'mprotbss'        , 'Killed',    ' 9. Атака п.2 с попыткой отключения защиты ядра используя mprotect()' ),
    # ( 'mprotdata'       , 'Killed',    '10. Атака п.3 с попыткой отключения защиты ядра используя mprotect()' ),
    # ( 'mprotheap'       , 'Killed',    '11. Атака п.4 с попыткой отключения защиты ядра используя mprotect()' ),
    # ( 'mprotstack'      , 'Killed',    '12. Атака п.5 с попыткой отключения защиты ядра используя mprotect()' ),
    # ( 'mprotshbss'      , 'Killed',    '13. Атака п.6 с попыткой отключения защиты ядра используя mprotect()' ),
    # ( 'mprotshdata'     , 'Killed',    '14. Атака п.7 с попыткой отключения защиты ядра используя mprotect()' ),
    # ( 'writetext'       , 'Killed',    '15. Исполнение измененного кода хранившегося в приложении(code segment)' ),

    ( 'randamap'        , '(guessed)', '16. Кол-во случайных бит используемых при выделении анонимного участка в памяти' ),
    ( 'randheap1'       , '(guessed)', '17. Кол-во случайных бит используемых при выделении участка в динамической памяти(heap segment, ET_EXEC)' ),
    ( 'randheap2'       , '(guessed)', '18. Кол-во случайных бит используемых при выделении участка в динамической памяти для библиотек(ET_DYN)' ),
    # https://gist.github.com/thestinger/b43b460cfccfade51b5a2220a0550c35
    # ( 'randmain1'       , '(guessed)', '19. Кол-во случайных бит используемых при выделении участка в памяти для исполняемого файла(ET_EXEC)' ),
    ( 'randmain2'       , '(guessed)', '20. Кол-во случайных бит используемых при выделении участка в памяти для исполняемого файла(ET_DYN)' ),
    ( 'randshlib'       , '(guessed)', '21. Кол-во случайных бит используемых при выделении участка в памяти для библиотек' ),
    ( 'randvdso'        , '(guessed)', '22. Кол-во случайных бит используемых при выделении участка в памяти для "virtual dynamic shared object"' ),
    ( 'randstack1'      , '(guessed)', '23. Кол-во случайных бит используемых при выделении участка в стеке(stack segment, SEGMEXEC)' ),
    ( 'randstack2'      , '(guessed)', '24. Кол-во случайных бит используемых при выделении участка в стеке(stack segment, PAGEEXEC)' ),
    ( 'randarg1'        , '(guessed)', '25. Кол-во случайных бит используемых в п.23 для параметров и переменных окружения' ),
    ( 'randarg2'        , '(guessed)', '26. Кол-во случайных бит используемых в п.24 для параметров и переменных окружения' ),
    ( 'randshlibdelta1' , '(guessed)', '27. Кол-во случайных бит используемых в смещениях памяти для динамичеких библиотек(ET_EXEC)' ),
    ( 'randshlibdelta2' , '(guessed)', '28. Кол-во случайных бит используемых в смещениях памяти для динамичеких библиотек(ET_DYN)' ),
    ( 'randexhaust1'    , '(guessed)', '29. Кол-во случайных бит используемых при нехватке памяти' ),
    ( 'randexhaust2'    , '(guessed)', '30. Кол-во случайных бит используемых при нехватке памяти' ),

    ( 'rettofunc1'      , 'NULL',      '31. Подмена адреса возврата в функцию в стеке(strcpy)' ),
    ( 'rettofunc2'      , 'Killed',    '32. Подмена адреса возврата в функцию в стеке(strcpy, RANDEXEC)' ),
    ( 'rettofunc1x'     , 'NULL',      '33. Подмена адреса возврата в функцию в стеке(memcpy)' ),
    ( 'rettofunc2x'     , 'Killed',    '34. Подмена адреса возврата в функцию в стеке(memcpy, RANDEXEC)' )
  ]

  def setUp(self):
    super( testKernelProtection, self ).setUp()
    os.environ['PAXTEST_MODE'] = '1'

    if not 'LD_LIBRARY_PATH' in os.environ:
      os.environ['LD_LIBRARY_PATH']= self.paxtests_dir

    os.chdir( self.paxtests_dir ) # oh!

  def runTest(self):
    log.info( '   Проверка конфигурации ядра... ' )

    for p in self.kernel_params:

      err = kernelParamCheck( p )

      if   err == 1:
        log.error( 'Параметр [{}] ядра не установлен.'.format( p ) )
      elif err == 2:
        log.error( 'Параметр ядра [{}] не найден в текущей конфигурации.'.format( p ) )
      elif err == 0:
        log.info( '   Параметр ядра обнаружен и соответствует ожидаемому зачению.')

      self.assert_( err == 0, 'Проверка конфигурации ядра не пройдена.')

    with Indicator(' Выполнение pax-тестов, ожидайте...'):
      with Terminal() as term:
        term.error( 'Во время проведения тестов защиты ядра, произошла ошибка [{code}]: {msg}' )

        for test in self.paxtests:
          term.add( '{}/{}'.format( self.paxtests_dir, test[0] ) )

    completed = 0
    log.info( '\t{:^105} {:^11} '.format( 'Описание', 'Статус' ) )

    for i, record in enumerate( term.log[1:] ):
      res = record[1].strip('\n').strip('\t').split(':', 1)
      ans = 'НЕ пройден'

      if self.paxtests[i][1] in res[1]:
        ans = 'пройден'
        completed += 1

      log.info( '\t{:105} {:^11} '.format( self.paxtests[i][2], ans ) )

    self.assertTrue( completed == len( self.paxtests ), 'Не все pax-тесты были пройдены!' )


  def tearDown(self):
    del os.environ['PAXTEST_MODE']

    if 'LD_LIBRARY_PATH' in os.environ:
      if os.environ['LD_LIBRARY_PATH'] == self.paxtests_dir:
        del os.environ['LD_LIBRARY_PATH']
