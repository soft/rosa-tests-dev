# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import pathlib
import paramiko
import modules.kplpack.utils as utils


class testLimitedLogins(BaseTest):
  """ #491 Ограничение на параллельные сеансы по атрибутам пользователя (FTA_MCS.2)
  """

  config = pathlib.Path('/etc/security/limits.d/10-maxlogins.conf')
  username = 'loginstester'
  password = 'LgIn,UsSr#32'
  too_many_logins_err = 254

  def login(self):
    if not hasattr(self, 'ssh_clients'):
      self.shells = []

    client = paramiko.SSHClient()
    client.load_system_host_keys()

    # prevent error: Server 'localhost' not found in known_hosts
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    client.connect('localhost', username=self.username, password=self.password)

    _, stdout, _ = client.exec_command('logname')
    code = stdout.channel.recv_exit_status()
    out = stdout.read().decode()

    if code == 0:
      log.info('Авторизован: {}'.format(out))
    else:
      log.info('Неудачная попытка авторизации: {} - {}'.format(code, out))

    self.shells.append(client.invoke_shell())
    return code

  def runTest(self):
    log.info('Определение максимального числа сессий для пользователя...')
    maxlogins = -1

    if not self.config.exists():
      self.fail('Конфигурация {} не обнаружена.'.format(self.config))
    else:
      with self.config.open('r') as cfg:
        for line in cfg.readlines():
          if 'maxlogins' in line:
            line = line.split()

            if line[-1].isdigit():
              maxlogins = int(line[-1])

    self.assert_(maxlogins != -1, 'Определить максимальное число сессий не удалось.')

    log.info('Максимальное кол-во сессий для пользователя = {}'.format(maxlogins))

    with utils.User(self.username, self.password, home='/home/{}'.format(self.username)):
      attempts = 0
      for _ in range(maxlogins + 1):
        attempts += 1

        if self.login() != self.too_many_logins_err and attempts > maxlogins:
          self.fail('Открыто больше сессий({}), чем позволяет конфигурация({})!'.format(attempts,maxlogins))

        elif attempts > maxlogins:
          log.info('Ожидаемый отказ в авторизации, попытка {}'.format(attempts))

# ╔══════════════════════════════════════════════════════════════════════╗
# ║                       Тест 141 - LimitedLogins                       ║
# ╟──────────────────────────────────────────────────────────────────────╢
# ║ Проверка ограничения на параллельные сеансы по атрибутам пользовател ║
# ║ я                                                                    ║
# ╟──────────────────────────────────────────────────────────────────────╢
# ║                           Ход тестирования                           ║
# ╟──────────────────────────────────────────────────────────────────────╢
# ║ Определение максимального числа сессий для пользователя...           ║
# ║ Максимальное кол-во сессий для пользователя = 2                      ║
# ║ Авторизован: loginstester                                            ║
# ║ Авторизован: loginstester                                            ║
# ║ Неудачная попытка авторизации: 254 - Too many logins for 'loginstest ║
# ║ er'.                                                                 ║
# ║ Ожидаемый отказ в авторизации, попытка 3                             ║
# ╟──────────────────────────────────┬───────────────────────────────────╢
# ║     Выполнение теста длилось     │            2.758 сек.             ║
# ╟──────────────────────────────────┼───────────────────────────────────╢
# ║            ЗАКЛЮЧЕНИЕ            │              пройден              ║
# ╚══════════════════════════════════╧═══════════════════════════════════╝
