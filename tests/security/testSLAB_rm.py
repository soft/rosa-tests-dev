# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from pathlib import Path
from modules.kplpack.utils import x, Return

class testSLAB_rm(BaseTest):

  tool        = Path( '/usr/lib64/rosa-tests/rm_test.py' )
  pass_phrase = 'rm_test: test passed'

  def runTest(self):
    self.assert_( self.tool.exists(), 'Утилита необходимая для проверки не обнаружена: {}'.format( self.tool.as_posix() ) )

    code, out = x( self.tool.as_posix(), Return.codeout )

    for l in out.splitlines():
      log.info( '  {}'.format( l ) )

    self.assert_( code == 0, 'Во время проверки произошла ошибка [{}]: {}'.format( code, out ) )
