# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import psutil
import os

from pathlib import Path
from modules.kplpack.utils import x, Return, Indicator

class testSLAB_swap(BaseTest):
  """ #487 """

  swp_copy = Path('/var/tmp/swp.bin')
  cleaner  = 'rosa-memory-clean --swap'

  def swp(self):
    return psutil.swap_memory().percent

  def setUp(self):
    super(testSLAB_swap, self).setUp()

    if self.swp_copy.exists():
      self.swp_copy.unlink()
    os.sync()

  def has_zram(self):
    code, out = x( 'zramctl -b -o name,disksize,mountpoint -n --raw', Return.codeout )

    total = 0
    devs  = []
    mnts  = []

    if code == 0:
      for l in out.splitlines():
        dev, size, mnt = l.split(' ')
        total += int( size )
        devs.append( dev )
        mnts.append( mnt )

    if total > 0 and len(devs) > 0:
      log.info( '  Обнаружен ZRAM: {} byte'.format( total ) )
      for i,d in enumerate(devs):
        log.info( '     {}'.format( d, mnts[i] ) )
      return True
    else:
      return False

  def runTest(self):

    if psutil.swap_memory().total > 0 and not self.has_zram():

      log.info( '  Обнаружен swap размером {} байт.'.format( psutil.swap_memory().total ) )

      log.info( '  Проверка доступной ОЗУ...')
      check_ram = psutil.virtual_memory().percent
      self.assert_( check_ram <= 60.0, ( 'Для данного теста необходимо иметь от 40% свободной ОЗУ.'
                                        'Текущий процент свободной ОЗУ: {}'.format( check_ram ) ) )

      log.info( '  Текущий swap = {}% '.format( self.swp() ) )
      log.info( '  Очистка swap c помощью "{}"...'.format( self.cleaner ) )

      code, out = x( self.cleaner, Return.codeout )
      self.assert_( code == 0, 'При очистке раздела swap произошла ошибка [{}]: {}'.format( code, out ) )

      os.sync()
      log.info( '  Текущий swap = {}% '.format( self.swp() ) )

      log.info( '  Клонирование нового раздела swap...' )

      with Indicator( ' Клонирование нового раздела swap' ):
        code, out = x( 'dd if=/dev/sda5 bs=512 count=3491840 oflag=nocache of={}'.format( self.swp_copy.as_posix() ), Return.codeout )
        self.assert_( code == 0, 'При клонировании раздела swap произошла ошибка [{}]: {}'.format( code, out ) )

      check_swp = self.swp()
      self.assert_( check_swp == 0.0, 'Во время клонирования был задействован swap( {}% ), освободите ОЗУ и попробуйте заново.'
                                      .format( check_swp ) )

      log.info( '  Во время клонирования swap задействован не был.' )

      log.info( '  Проверка данных склонированного раздела...')
      with Indicator( ' Проверка данных склонированного раздела' ):

        with open( self.swp_copy.as_posix(), 'rb' ) as F:
          zeroed   = 0
          notnull  = 0
          position = 0

          for i,byte in enumerate( F.read() ):
            if byte != 0:
              notnull += 1
              position = i
              break
            else:
              zeroed += 1

      self.assert_( notnull == 0, 'В чистом swap обнаружены данные. На момент обнаружения: зануленных байт {} до позиции {}.'
                                  .format( zeroed, position ) )

    else:
      log.info( '  Раздел swap не обнаружен. Проверка не требуется.' )


  def tearDown(self):
    if self.swp_copy.exists():
      self.swp_copy.unlink()
    os.sync()
