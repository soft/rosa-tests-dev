# -*- coding: utf-8 -*-

# - required for every test --------------
import shutil
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import os
import pathlib
import selinux

import modules.kplpack.utils as utils
from modules.kplpack.utils import x, Return

class testSecurityRolesSupport(BaseTest):
  """ #457 Роли безопасности (FMT_SMR.1)
  """
  username = 'roletester'
  password = 'geekRol#t3st,r'
  roles_test_files_tar = '/usr/share/rosa-tests/test_data/roles_test_files.tar'
  target_dir = '/tmp/rolestestdir'

  prepared_files = 4

  prepared_types = {
    'rosatest_secadm_object_t': False,
    'rosatest_sysadm_object_t': False,
    'rosatest_auditadm_object_t': False,
    'rosatest_user_object_t': False
  }

  users_n_roles = {
    'user_u': ['user_r'],
    'aib_u': ['auditadm_r', 'secadm_r', 'sysadm_r'],
    'sysadm_u': ['sysadm_r'],
    'auditor_u': ['auditadm_r', 'user_r']
  }

  def prepare_files(self):
    archive = pathlib.Path(self.roles_test_files_tar)
    target = pathlib.Path(self.target_dir)

    self.assert_(
      archive.exists(),
      'Архив {} с данными для проверки ассоциации ролей не найден!'
      .format(archive)
      )

    target.mkdir(0o777)
    code, out = x('tar -xvf {} --directory {}'.format(archive, target), Return.codeout)

    self.assert_(code == 0, 'Ошибка при распаковке архива {} - {}'.format(archive, out))

  def file_access_check(self, context:str):
    target_dir = pathlib.Path(self.target_dir)

    for f in list( target_dir.glob('*') ):
      log.info('Попытка получить атрибуты файла {}...'.format(f))
      #f.stat()
      selinux.setexeccon_raw(context)
      code = x('ls -l {}'.format(f))

  def runTest(self):
    log.info('Получение списка selinux-пользователей...')
    #users_n_roles = utils.User.get_se_default_roles_table()

    log.info()
    log.info('{:->25}  {:<40}'.format('Доступные пользователи', 'Доступные роли'))

    for user,roles in self.users_n_roles.items():
      log.info('{:->25}  {: <30}'.format(user, ', '.join(roles)))

    log.info()
    log.info('Подготовка тестовой директории и файлов...')
    self.prepare_files()

    types = set()
    with utils.Indicator(' Тестирование ролей selinux: ') as progress:
      for user,roles in self.users_n_roles.items():
        progress.set_suffix('субъект {} {}'.format(user, '{elaps}'))

        types.update([r.rstrip('r') + 't' for r in roles])

        log.info()
        log.info('Создание пользователя {}'.format(user))

        with utils.User(self.username, self.password, home='/home/roletester', se_user=user) as u:
          log.info('Пользователь {} создан.'.format(u.name))
          log.info('Контекст безопасности: {}'.format(u.context.full))
          log.info()

          for r in roles:
            log.info('Тестирование роли {}...'.format(r))
            u.context.r = r
            u.context.t = r.rstrip('r') + 't'
            context = u.context.update().full

            log.info('Создание процесса с контекстом {}...'.format(context))
            with utils.ConRun(u.uid, u.gid, raw_context=context) as mem:
              pid = os.getpid()
              print(pid)
              log.info('Процесс создан [{}]'.format(pid))

              current_process_context = selinux.getcon_raw()[1]
              log.info('Получен контекст: {}'.format(current_process_context))

              if current_process_context != context:
                self.fail('Планируемый контекст для процесса не равен фактическому: {} != {}'
                          .format(context, current_process_context))

              # working with target-dir
              self.file_access_check(context)

            log.info()
            for line in mem:
              if 'AssertionError' in line:
                self.fail('Процесс [{}] обнаружил нарушение: {}.'
                          .format(mem[0], line.split(':', 1)[1])
                         )

    log.info()
    log.info('Обнаружение конфликтов selinux:')
    checked = dict()
    allows = utils.collect_selinux_allows(
      120.0,
      group=True,
      include=['rosatest_'],
      exclude=['unlink']
    )

    for subj_type, rows in allows.items():
      if any(target_type == subj_type for target_type in types):
        check_list = self.prepared_types.copy()
        log.info()

        for values in rows:
          obj_type, obj, actions = values
          check_list[obj_type] = True
          log.info('{} {} {} {}'.format(subj_type, obj_type, obj, actions))
        checked[subj_type] = check_list

    count_subjects = 0
    for subj_type, check_list in checked.items():
      count_subjects += 1
      for obj_type, check in check_list.items():
        if not check and subj_type[:-2] not in obj_type:
          self.fail('Роль {} субъекта {} не доказана.'.format(subj_type.rstrip('t') + 'r', subj_type))

    self.assert_(count_subjects == len(self.users_n_roles),
                 'Не для всех проверяемых субъектов были обнаружены конфликты. '
                 'Попробуйте запустить тест повторно.' )

    log.info()
    log.info('Все конфликты недопустимой роли были обнаружены. '
             'Т.к. конфликты являются следствием отсутствия доступа, '
             'единственный отсутствующий конфликт для каждого типа субъекта '
             'свидетельствует о совпадении требуемой роли субъекта с фактической.')

  def tearDown(self):
    log.info()
    log.info('Удаление тестового пользователя...')
    x('userdel -Z {} -r -f'.format(self.username))

    log.info('Удаление тестовой директории {}...'.format(self.target_dir))
    if os.path.exists(self.target_dir):
      shutil.rmtree(self.target_dir)