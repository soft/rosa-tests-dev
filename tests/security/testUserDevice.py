# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

from modules.kplpack.utils import x, Return
from subprocess import call, PIPE, STDOUT, getstatusoutput

class testUserDevice(BaseTest):
  """
  Проверка обеспечения изделием сопоставления пользователя с устройством
  """

  def create_block_dev(self, delete=False):
      cmd_pull_create = [
          'dd if=/dev/zero of=hdd_test.img bs=1M count=1',
          'mkfs -t ext4 hdd_test.img',
          'mkdir /mnt/hdd_test/',
          'mount -t auto -o loop hdd_test.img /mnt/hdd_test/',
          'mount -o remount,rw /mnt/hdd_test'
      ]
      cmd_pull_delete = [
          'umount /mnt/hdd_test/', # error
          'rm -rf /mnt/hdd_test/',
          'rm -f hdd_test.img'
      ]

      cmd_pull = cmd_pull_delete if delete==True else cmd_pull_create

      log.debug('  cmdpull:' )
      for cmd in cmd_pull:
        log.debug( '    ' + cmd )

      for cm in cmd_pull:
          if call(cm, shell=True, stdout=PIPE, stderr=STDOUT) != 0:
              if delete != True:
                  self.create_block_dev(delete=True)
              raise Exception('Command failed: {} !'.format(cm))


  def reloadRules(self):
      res = x('udevadm control --reload-rules', return_type=Return.process)
      self.assertEqual(res.returncode, 0, res.stdout.read())

  def settings(self):
      self.create_block_dev()
      self.udevRulefile = "/etc/udev/rules.d/rmdeviotest.rules"
      self.user = 'root' #testsConfig.getTestUser()

      try:
          f = open(self.udevRulefile, 'w')
          rules = 'ACTION==\"add|change\", SUBSYSTEM==\"block\", ' \
                  'KERNEL==\"loop*\", OWNER=\"%s\"\n' % self.user
          log.debug('\n  1164: {}'.format(rules))
          f.write(rules)
          f.close()
          self.reloadRules()
      except:
          self.reloadRules()
          self.assertTrue(False, "Ошибка создания правила udev.")

  def runTest(self):
      self.settings()
      errcode, output = getstatusoutput("ls -l /dev/loop0")
      listloop = output.split(' ')

      self.assertEqual(listloop[2], self.user, output)


  def tearDown(self):
      self.create_block_dev(delete=True)
