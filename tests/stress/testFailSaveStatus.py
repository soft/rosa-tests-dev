# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import psutil
import os
import multiprocessing as MP
import mmap

from pathlib import Path
from modules.kplpack.utils import x, Return, User, Terminal, StreamToLog, Indicator, Damper, se_enforce

class testFailSaveStatus(BaseTest):
  """ #560 """

  tfs = Path( '/var/tmp/testfile.tfs' )
  utf = Path( '/unlimited.file' )
  mnt = Path( '/mnt/tfs/' )


  def init(self):
    with Terminal( StreamToLog( log ) ) as term:
      term.add( 'dd if=/dev/zero bs=1M count=1 of={}'.format( self.tfs.as_posix() ) )\
          .info( '  Выделение места под тестовую файловую систему...\n')
      term.add( 'mkfs -t ext4 {}'.format( self.tfs.as_posix() ) )\
          .info( '  Создание тестовой файловой системы(tfs)...\n' )
      term.add( 'mkdir {}'.format( self.mnt.as_posix() ) )\
          .info( '  Создание точки монтирования({})...\n'.format( self.mnt.as_posix() ) )
      term.add( 'mount -t auto -o loop {} {}'.format( self.tfs.as_posix(), self.mnt.as_posix() ) )\
          .info( '  Монтирование тестовой файловой системы(tfs) в {} \n'.format( self.mnt.as_posix() ) )
      term.add( 'mount -o remount,rw {}'.format( self.mnt.as_posix() ) )\
          .info( '  Переключение тестовой файловой системы(tfs) в режим чтения и записи.\n' )

    # code, out = X( 'dd if=/dev/zero bs=100 count=1 of={}'.format( self.testfile.as_posix() ), Return.codeout )

    self.assert_( term.fail[0] == 0, 'При создании тестовой файловой системы произошла ошибка [{}]: {}'
                                       .format( term.fail[0], term.fail[1] ) )

    self.user = User( 'rosa-test-user', home='/home/rosa-test-user' )

  @staticmethod
  def payload_ram(size):
    α = 1000
    Δ = b'44765425243242662457573698374592837465923764592364'
    free = size

    with mmap.mmap( -1, size ) as mem:

      while free > 0:
        free = size - mem.tell()

        if free >= α * len( Δ ):
          mem.write( α * Δ )
        elif len( Δ ) > 1:
           Δ = Δ[1:]
        elif α > 1:
          α -= 1

    return 0

  def runTest(self):

    self.init()

    log.info( '  Попытка создать исключительную ситуацию "Ошибка ввода-вывода (soft)"... ' )

    try:
      with open( self.mnt.as_posix() + '/testfile.t', 'w+' ) as f:
        for _ in range( self.tfs.stat().st_size + 1 ):
          f.write( '1' )
    except OSError as e:
      if e.errno == 28:
        log.info( '  Произошел ожидаемый сбой ввода\вывода при переполнении файловой системы!' )

    log.info( '  Попытка создать исключительную ситуацию "Ошибка ввода-вывода (hard)"')

    with Indicator( ' Заполнение свободного пространства / ...'):
      code, out = x( 'dd if=/dev/zero of={} bs=1G count=665'.format( self.utf.as_posix() ), Return.codeout )

    if code != 0:
      log.info( '  Произошел ожидаемый сбой ввода\вывода при переполнении файловой системы!' )
    else:
      self.fail( 'Произошел неожиданный сбой [{}]: {}'.format( code, out ) )

    log.info( '  Попытка создать исключительную ситуацию "Ошибка выделения памяти (soft)"')

    ram = psutil.virtual_memory().total
    swp = psutil.swap_memory().free
    rns = ram + swp

    log.info( '  Доступно RAM: {} SWAP: {}'.format( ram, swp ) )
    log.info( '  Попытка захвата памяти {} * 2 ...'.format( rns ) )

    with Indicator( ' Захват памяти, возможна кратковременная потеря контроля над ЭВМ...' ):
      with Damper():
        P = MP.Process( target=self.payload_ram, args=( rns, ) )
        P.start()
        P.join()

    if P.exitcode in [ -9, -7 ]:
      log.info( '  Произошел ожидаемый сбой при выделении памяти в ОЗУ!' )
    else:
      self.fail( 'Произошел неожиданный сбой [{}].'.format( P.exitcode ) )

    log.info( '  Попытка создать исключительную ситуацию "Устройство или ресурс занято (soft)"')
    try:
      self.mnt.rmdir()

    except OSError as e:
      if e.errno == 16:
        log.info( '  Произошел ожидаемый сбой "Устройство или ресурс занято"!' )
      else:
        self.fail( 'Произошел неожиданный сбой [{}]: {}'.format( e.errno, e.strerror ) )

    ## log.info( '  Попытка создать исключительную ситуацию "Превышено время ожидания (soft)"')
    ## dd if=/dev/sda1 of=/home/test/u.f bs=2G count=1 iflag=fullblock,direct oflag=nocache

    log.info( '  Проверка безопасности состояния после сбоев..."')

    # в текущей версии ОС Nickel 43051 невозможно программно переключать режимы
    # безопасности selinux из-за ограничений по Профилю Безопасности.
        # with Indicator( ' Проверка безопасности состояния после сбоев...' ):
    #   se_enforce( StreamToLog( log ) )
    #   log.info( '  Запуск rosa-security-test...')
    #   err = x( 'sudo -u {} rst --fault-code --logtype nolog'.format( self.user.name ) )
    #   se_enforce( StreamToLog( log ) )

    # self.assert_( err == 0, 'Не был пройден тест #{}!'.format( err ) )

    log.info( 'Для окончания проверки запустите rosa-security-test!(rst)')

  def tearDown(self):

    if self.mnt.exists() and os.path.ismount( self.mnt.as_posix() ):
      log.info( '  Попытка размонтировать тестовую файловую систему(tfs)...' )
      code, out = x( 'umount {}'.format( self.mnt.as_posix() ), Return.codeout )

      self.assert_( code == 0, 'При размонтировании тестовой файловой системы произошла ошибка [{}]: {}'.format( code, out ) )

    if self.mnt.exists():
      self.mnt.rmdir()

    if self.tfs.exists():
      self.tfs.unlink()

    if self.utf.exists():
      self.utf.unlink()

    log.info( '  Удаление тестового пользователя ...' )
    self.user.delete()
