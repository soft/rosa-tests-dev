# -*- coding: utf-8 -*-

# - required for every test --------------
from tests.common.BaseTest import BaseTest
import logging
log = logging.getLogger( "rts.libtests" )
# ----------------------------------------

import psutil
import os
import multiprocessing as MP
import keyboard

from time import sleep
from multiprocessing import Event
from modules.kplpack.utils import x, Return, User, Damper, Terminal, Indicator, SuspendGuard

class testStress(BaseTest):

  CPU_CORES = psutil.cpu_count()

  @staticmethod
  def payload_cpu():
    x( 'stress --cpu {} --timeout 20'.format( testStress.CPU_CORES ) )

  @staticmethod
  def payload_ram():
    x( 'stress --vm {} --vm-bytes 500M --vm-keep --timeout 20'.format( testStress.CPU_CORES ) )

  @staticmethod
  def payload_io():
    x( 'stress --io {} --timeout 20'.format( testStress.CPU_CORES ) )

  @staticmethod
  def payload_hdd():
    x( 'stress --hdd {} --timeout 20'.format( testStress.CPU_CORES ) )

  @staticmethod
  def sleep_guard():

    u = User()
    os.setgid( u.gid ); os.setuid( u.uid )

    sg = SuspendGuard( 'rosa-tests', 'stress test' )
    sg.block()

  def Go(self, payload, stop_event:Event, wait=True):

    if stop_event.is_set():
      return 0

    with Damper():
      process = MP.Process( target=payload )
      process.daemon = True
      process.start()

      if wait:
        while process.is_alive() and not stop_event.is_set():
          sleep(1)

        if process.is_alive() and stop_event.is_set():
          process.terminate()
          return 0
      else:
        return process

    return process.exitcode

  # runs from rosa-tests.py
  def make_hud_and_run():
    with Terminal() as term:
      term.add( 'tmux start-server' )
      term.add( 'tmux new-session -d -s "STRESSHUD"' )
      term.add( 'tmux splitw -h -p 75')
      term.add( 'tmux selectp -t 1' )
      term.add( 'tmux send-keys "htop" Enter' )
      term.add( 'tmux selectp -t 0' )

      term.add( 'tmux send-keys "rosa-tests 122" Enter' )
      term.add( 'tmux attach-session -t "STRESSHUD"' )

  def runTest(self):

    log.info( '  Начато нагрузочное тестирование.' )
    stop = Event()

    log.info( '  Отсчет до приостановки сеанса отменён.' )
    sg = self.Go( self.sleep_guard, stop, wait=False)

    desc = ' Идет нагрузочное тестирование [{type}] :'
    keyboard.add_hotkey('ctrl+space, esc', lambda x: x.set(), args=(stop,), suppress=True, timeout=2 )

    print( 'Внимание! Запущено нагрузочное тестирование.' )
    print( 'Каждые 20 секунд производится смена типа нагрузки [CPU, RAM, IO, HDD].' )
    print( 'Для завершения тестирование нажмите ctrl+space, затем esc.' )

    with Indicator( ' Старт нагрузочного тестирования', ' прошло {elaps}') as indicator:

      while not stop.is_set() and not sleep(1):

        indicator.set_message( desc.format( type='CPU' ) )
        cpu = self.Go( self.payload_cpu, stop )
        self.assert_( cpu == 0, ' Во время нагрузочного тестирования CPU произошла ошибка.' )

        indicator.set_message( desc.format( type='RAM' ) )
        ram = self.Go( self.payload_ram, stop )
        self.assert_( ram == 0, ' Во время нагрузочного тестирования RAM произошла ошибка.' )

        indicator.set_message( desc.format( type='IO' ) )
        io = self.Go( self.payload_io, stop )
        self.assert_( io == 0, ' Во время нагрузочного тестирования IO произошла ошибка.' )

        indicator.set_message( desc.format( type='HDD' ) )
        hdd = self.Go( self.payload_hdd, stop )
        self.assert_( hdd == 0, ' Во время нагрузочного тестирования HDD произошла ошибка.' )

    print( 'Нагрузочное тестирование завершено пользователем.' )
    log.info( '  Нагрузочное тестирование завершено.' )
    log.info( '  Отсчет до приостановки сеанса возобновлён.' )
    sg.terminate()

    sleep(3)
